package com.alpha.paanilo.authenticationModule;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.cardview.widget.CardView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.alpha.paanilo.FetchLocationActivity;
import com.alpha.paanilo.R;
import com.alpha.paanilo.model.LoginUserPojo;
import com.alpha.paanilo.model.vendor_auth_pkg.OtpVerficationRes;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.seller.Seller_SignUp_Activity;
import com.alpha.paanilo.utility.CheckNetwork;
import com.alpha.paanilo.utility.Constants;
import com.google.gson.Gson;

import in.aabhasjindal.otptextview.OtpTextView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OTPActivity extends AppCompatActivity implements View.OnClickListener {
    CardView btn_confirm;

    OtpTextView otp_view_Id;
    ProgressDialog pd;
    //    Constants constants;
    TextView tvEnterOtp;
    private Constants constants;
AppCompatImageView ivBackForgetId ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_o_t_p);

        constants = new Constants(getApplicationContext());
        Log.e("otp get ", "otp::: " + constants.getOTP());

        Log.e("Mobile:::: ", "Mobile::: " + constants.getMobile());

        btn_confirm = findViewById(R.id.btn_confirm);
        otp_view_Id = findViewById(R.id.otp_view_Id);
        tvEnterOtp = findViewById(R.id.tvEnterOtp);
        ivBackForgetId = findViewById(R.id.ivBackForgetId);
//        tvEnterOtp.setText("Get Opt On this Number " + constants.getMobile());

//        otpVerficaton();
        ivBackForgetId.setOnClickListener(this);
        btn_confirm.setOnClickListener(this);

        pd = new ProgressDialog(OTPActivity.this, R.style.AppCompatAlertDialogStyle);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_confirm:
//                Log.e("Ram ", "Ram11111 ::::: " + otp_view_Id.getOTP());
                if (otp_view_Id.getOTP().isEmpty() || otp_view_Id.getOTP().equals("") || otp_view_Id.getOTP().equals("null")) {
                    Toast.makeText(this, "Please Enter OTP ", Toast.LENGTH_SHORT).show();
                } else {
                    if (otp_view_Id.getOTP().equals("1234")) {
////                        Toast.makeText(this, " OTP ", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(OTPActivity.this, FetchLocationActivity.class);
                        startActivity(intent);
                    }
                }
//                    else if (!otp_view_Id.getOTP().equals(constants.getOTP())) {
//                        Toast.makeText(this, "Please Enter Valid OTP ", Toast.LENGTH_SHORT).show();
//                    } else {
//
//                        Toast.makeText(this, "Success otp", Toast.LENGTH_SHORT).show();
////                        Intent intent = new Intent(OTPActivity.this, FetchLocationActivity.class);
////                        startActivity(intent);
//                    }
//                }
                break;


          case   R.id.ivBackForgetId:
              Intent intet = new Intent(OTPActivity.this, SignUp_Activity.class);
              constants.setIsLogin("");
              startActivity(intet);
              finish();
            break;

        }
    }


    public void otpVerficaton() {
        pd.setMessage("Loading");
        pd.show();
        (ApiClient.getClient().otpVerfication(constants.getOTP(), constants.getMobile())).enqueue(new Callback<OtpVerficationRes>() {
            @Override
            public void onResponse(Call<OtpVerficationRes> call, Response<OtpVerficationRes> response) {
                pd.dismiss();
                if (response.isSuccessful()) {
                    try {
                        Log.e("TAG", "OTp response : " + new Gson().toJson(response.body()));
                        OtpVerficationRes userInfo_pojo = response.body();
                        Boolean repmsg = userInfo_pojo.getStatus();
                        String message = userInfo_pojo.getMessage();
                        if (repmsg) {
                            Toast.makeText(OTPActivity.this, message, Toast.LENGTH_SHORT).show();
                         /*   constants.setIsLogin("yes");
                            constants.setUserID(userInfo_pojo.getData().getUserId());
                            constants.setUsername(userInfo_pojo.getData().getUserFullname());
                            constants.setEmail(userInfo_pojo.getData().getUserEmail());

                            constants.setCity(userInfo_pojo.getData().getAddress());
                            constants.setImage(userInfo_pojo.getData().getUserImage());
//                               constants.setOTP(userInfo_pojo.getData().getOtp());
                            Log.e("Login :: ", "UserID :::: " + userInfo_pojo.getData().getUserId());
                            Log.e("Login :: ", "UserFull Name :::: " + userInfo_pojo.getData().getUserFullname());
                            Log.e("Login :: ", "UserEmail :::: " + userInfo_pojo.getData().getUserEmail());
                            Log.e("Login :: ", "Address :::: " + userInfo_pojo.getData().getAddress());

                            Log.e("Login :: ", "UserImage :::: " + userInfo_pojo.getData().getUserImage());
                            Log.e("Login :: ", "OOOO :::: " + userInfo_pojo.getData().getOtp());
*/
//                            startActivity(new Intent(OTPActivity.this, FetchLocationActivity.class)
//                                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
//                            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
//                            finish();

                        } else {
                            Toast.makeText(OTPActivity.this, message, Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                }


            }

            @Override
            public void onFailure(Call<OtpVerficationRes> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(OTPActivity.this, getString(R.string.server_errors), Toast.LENGTH_SHORT).show();
            }
        });
    }


}

    /*private void otpVerifcation() {
        pd.setMessage("Loading");
        pd.show();
        (ApiClient.getClient().otpVerfication(constants.getOTP(), constants.getMobile()).enqueue(new Callback<LoginUserPojo>() {
            @Override
            public void onResponse(Call<LoginUserPojo> call, Response<LoginUserPojo> response) {
                pd.dismiss();
                if (response.isSuccessful()) {
                    try {
                        Log.e("TAG", "OTp response : " + new Gson().toJson(response.body()));
                        LoginUserPojo userInfo_pojo = response.body();
                        Boolean repmsg = userInfo_pojo.getStatus();
                        String message = userInfo_pojo.getMessage();

                        if (repmsg) {
                            Toast.makeText(OTPActivity.this, message, Toast.LENGTH_SHORT).show();

                            constants.setIsLogin("yes");
                            constants.setUserID(userInfo_pojo.getData().getUserId());
                            constants.setUsername(userInfo_pojo.getData().getUserFullname());
                            constants.setEmail(userInfo_pojo.getData().getUserEmail());

                            constants.setCity(userInfo_pojo.getData().getAddress());
                            constants.setImage(userInfo_pojo.getData().getUserImage());
//                               constants.setOTP(userInfo_pojo.getData().getOtp());

                            Log.e("Login :: ", "UserID :::: " + userInfo_pojo.getData().getUserId());
                            Log.e("Login :: ", "UserFull Name :::: " + userInfo_pojo.getData().getUserFullname());
                            Log.e("Login :: ", "UserEmail :::: " + userInfo_pojo.getData().getUserEmail());
                            Log.e("Login :: ", "Address :::: " + userInfo_pojo.getData().getAddress());

                            Log.e("Login :: ", "UserImage :::: " + userInfo_pojo.getData().getUserImage());
                            Log.e("Login :: ", "OOOO :::: " + userInfo_pojo.getData().getOtp());

                            startActivity(new Intent(OTPActivity.this, FetchLocationActivity.class)
                                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                            finish();

                        } else {
                            Toast.makeText(OTPActivity.this, message, Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<LoginUserPojo> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(OTPActivity.this, getString(R.string.server_errors), Toast.LENGTH_SHORT).show();
            }
        });
    }*/












