package com.alpha.paanilo.authenticationModule;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.alpha.paanilo.R;
import com.alpha.paanilo.model.vendor_auth_pkg.Vendor_LoginUserInfo;
import com.alpha.paanilo.model.vendor_auth_pkg.Vendor_LoginUserPojo;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.utility.CheckNetwork;
import com.alpha.paanilo.utility.Constants;
import com.alpha.paanilo.utility.CustomToast;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.Place.Field;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;


public class Vendor_SignUp_Activity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "VendorSignUp";
    private static Animation shakeAnimation;
    AppCompatButton btn_signup;
    TextInputEditText tv_username, tv_email, tv_mobile_number, tv_password, tv_confirm_password;
    ProgressDialog pd;
    TextInputEditText edt_address;
    Double vendor_lat, vendor_long;
    int AUTOCOMPLETE_REQUEST_CODE = 1;
    String firbaseToken;
    private AppCompatImageView ivBackForgetId;
    private AppCompatTextView tv_back_signup;
    private Constants constant;
    private FusedLocationProviderClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setContentView(R.layout.vendor_activity_signup);

        init();

        Places.initialize(getApplicationContext(), getString(R.string.google_maps_key));

//        edt_address.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                List<Field> fields = Arrays.asList(Field.ADDRESS, Field.NAME, Field.LAT_LNG);
//                Intent intent = new Autocomplete.IntentBuilder(
//                        AutocompleteActivityMode.FULLSCREEN, fields)
//                        .build(Vendor_SignUp_Activity.this);
//                startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
//            }
//        });

        btn_signup = findViewById(R.id.btn_signup);
        btn_signup.setOnClickListener(this);

        constant = new Constants(getApplicationContext());
        pd = new ProgressDialog(Vendor_SignUp_Activity.this, R.style.AppCompatAlertDialogStyle);
        shakeAnimation = AnimationUtils.loadAnimation(this, R.anim.shake);

        FirebaseApp.initializeApp(Vendor_SignUp_Activity.this);
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            // Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }
                        firbaseToken = task.getResult().getToken();
                        Log.d("token", firbaseToken);
                    }
                });
    }

    //    ===================
  /*  @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);

                vendor_lat = place.getLatLng().latitude;
                vendor_long = place.getLatLng().longitude;

                edt_address.setText(place.getAddress());

            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }*/


   /* @RequiresApi(api = Build.VERSION_CODES.P)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                Uri resultUri = result.getUri();
                resultCrop(resultUri);

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Toast.makeText(this, "Cropping failed: " + error, Toast.LENGTH_LONG).show();
            }
        }

    }*/

    //    =======================
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_signup:

                Intent intent = new Intent(Vendor_SignUp_Activity.this, AdminConfirmation.class);
                startActivity(intent);

//                validation(v);

                break;
            case R.id.ivBackForgetId:
                CheckNetwork.backScreenWithFinis(Vendor_SignUp_Activity.this);
                break;
        }
    }

    private void init() {
        ivBackForgetId = findViewById(R.id.ivBackForgetId);
        tv_username = findViewById(R.id.tv_username);
        tv_email = findViewById(R.id.tv_email);
        edt_address = findViewById(R.id.autoCompleteTextView);
        tv_mobile_number = findViewById(R.id.tv_mobile_number);

        tv_password = findViewById(R.id.tv_password);
        tv_confirm_password = findViewById(R.id.tv_confirm_password);

        ivBackForgetId.setOnClickListener(this);
        shakeAnimation = AnimationUtils.loadAnimation(this, R.anim.shake);

        requestPermission();
        client = LocationServices.getFusedLocationProviderClient(this);

        edt_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(Vendor_SignUp_Activity.this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }

                client.getLastLocation().addOnSuccessListener(Vendor_SignUp_Activity.this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if (location != null) {
//                            tv_address.setText(location.toString());
                            try {
                                getAddres(location);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });

            }
        });


    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION}, 1);
    }

    private void getAddres(Location location) throws IOException {

        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()

        edt_address.setText("" + address);
        Log.e("address::::", "address::" + address);
        String city = addresses.get(0).getLocality();

        Log.e("city::::", "city::" + city);

        String state = addresses.get(0).getAdminArea();

        Log.e("state::::", "state::" + state);

        String country = addresses.get(0).getCountryName();
        Log.e("country::::", "country::" + country);

        String postalCode = addresses.get(0).getPostalCode();
        Log.e("postalCode::::", "postalCode::" + postalCode);

        String knownName = addresses.get(0).getFeatureName();

        Log.e("knownName::::", "knownName::" + knownName);

    }

    private void validation(View v) {
        if (tv_username.getText().toString().isEmpty()) {
            // new CustomToast().Show_Toast(this, v, getString(R.string.username_error));
            tv_username.startAnimation(shakeAnimation);
            tv_username.getBackground().mutate().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            tv_username.requestFocus();
            tv_username.setError(getString(R.string.username_error));

        } else if (!tv_username.getText().toString().matches("^[a-zA-Z\\s]+")) {
            //new CustomToast().Show_Toast(this, v, "Invalid user name");
            tv_username.startAnimation(shakeAnimation);
            tv_username.getBackground().mutate().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            tv_username.requestFocus();
            tv_username.setError(getString(R.string.invalid_user_name));
        } else if (tv_email.getText().toString().isEmpty()) {
            // new CustomToast().Show_Toast(this, v, getString(R.string.email_error));
            tv_email.startAnimation(shakeAnimation);
            tv_email.getBackground().mutate().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            tv_email.requestFocus();
            tv_email.setError(getString(R.string.email_error));

        } else if (!Constants.isValidEmail(tv_email.getText().toString())) {
            // new CustomToast().Show_Toast(this, v, getString(R.string.invalid_email_error));
            tv_email.startAnimation(shakeAnimation);
            tv_email.getBackground().mutate().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            tv_email.requestFocus();
            tv_email.setError(getString(R.string.invalid_email_error));

        } else if (edt_address.getText().toString().isEmpty()) {
            new CustomToast().Show_Toast(this, v, getString(R.string.address_error));
            edt_address.startAnimation(shakeAnimation);
            edt_address.getBackground().mutate().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            edt_address.requestFocus();
            //edt_address.setError(getString(R.string.address_error));
        } else if (tv_mobile_number.getText().toString().isEmpty()) {
            // new CustomToast().Show_Toast(this, v, getString(R.string.password_error));
            tv_mobile_number.startAnimation(shakeAnimation);
            tv_mobile_number.getBackground().mutate().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            tv_mobile_number.requestFocus();
            tv_mobile_number.setError(getString(R.string.mobile_error));

        } else if (tv_mobile_number.getText().toString().length() <= 10) {
            // new CustomToast().Show_Toast(this, v, getString(R.string.password_error));
            tv_mobile_number.startAnimation(shakeAnimation);
            tv_mobile_number.getBackground().mutate().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            tv_mobile_number.requestFocus();
            tv_mobile_number.setError(getString(R.string.valid_mobile_error));
        } else if (tv_password.getText().toString().isEmpty()) {
            // new CustomToast().Show_Toast(this, v, getString(R.string.password_error));
            tv_password.startAnimation(shakeAnimation);
            tv_password.getBackground().mutate().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            tv_password.requestFocus();
            tv_password.setError(getString(R.string.password_error));

        } else if (tv_password.getText().toString().length() <= 6) {
            // new CustomToast().Show_Toast(this, v, getString(R.string.password_error_sixdegit_graterthan));
            tv_password.startAnimation(shakeAnimation);
            tv_password.getBackground().mutate().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            tv_password.requestFocus();
            tv_password.setError(getString(R.string.password_error_sixdegit_graterthan));

        } else if (tv_confirm_password.getText().toString().isEmpty()) {
            // new CustomToast().Show_Toast(this, v, getString(R.string.confirm_password_empty));
            tv_confirm_password.startAnimation(shakeAnimation);
            tv_confirm_password.getBackground().mutate().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            tv_confirm_password.requestFocus();
            tv_confirm_password.setError(getString(R.string.confirm_password_empty));

        } else if (tv_confirm_password.getText().toString().length() <= 6) {
            //new CustomToast().Show_Toast(this, v, getString(R.string.confirm_password_digit_graterthan_six));
            tv_confirm_password.startAnimation(shakeAnimation);
            tv_confirm_password.getBackground().mutate().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            tv_confirm_password.requestFocus();
            tv_confirm_password.setError(getString(R.string.confirm_password_digit_graterthan_six));

        } else if (!(tv_password.getText().toString().equals(tv_confirm_password.getText().toString()))) {
            //new CustomToast().Show_Toast(this, v, getString(R.string.confirm_password_should_same));
            tv_confirm_password.startAnimation(shakeAnimation);
            tv_confirm_password.getBackground().mutate().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            tv_confirm_password.requestFocus();
            tv_confirm_password.setError(getString(R.string.confirm_password_should_same));

        } else if (CheckNetwork.isNetAvailable(this)) {
//            signup(tv_username.getText().toString().trim(),
//                    tv_email.getText().toString().trim(),
//                    tv_password.getText().toString().trim(),
//                    edt_address.getText().toString().trim()
//            );
        } else {
            new CustomToast().Show_Toast(this, v, "Check Network Connection");
        }
    }


   /* public void signup(String username, String email, String password, String address) {
        if (CheckNetwork.isNetAvailable(getApplicationContext())) {
            pd.setMessage("Loading");
            pd.show();
            Vendor_LoginUserInfo userInfo = new Vendor_LoginUserInfo();
            userInfo.setUser_fullname(username);
            userInfo.setUser_email(email);
            userInfo.setUser_password(password);
            userInfo.setFirebaseToken(firbaseToken);
            userInfo.setCurrent_location(address);
            userInfo.setLatitude(String.valueOf(vendor_lat));
            userInfo.setLongitude(String.valueOf(vendor_long));

            Log.e("TAG", "signup signUpVendor request : " + new Gson().toJson(userInfo));

            (ApiClient.getClient().signUpVendor(userInfo)).enqueue(new Callback<Vendor_LoginUserPojo>() {
                @Override
                public void onResponse(Call<Vendor_LoginUserPojo> call, Response<Vendor_LoginUserPojo> response) {
                    pd.dismiss();
                    if (response.isSuccessful()) {
                        try {
                            Log.e("TAG", "signup signUpVendor response :" + new Gson().toJson(response.body()));
                            Vendor_LoginUserPojo userInfo_pojo = response.body();
                            boolean repmsg = userInfo_pojo.getStatus();
                            String message = userInfo_pojo.getMessage();
                            if (repmsg) {
                                Toast.makeText(Vendor_SignUp_Activity.this, message, Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(Vendor_SignUp_Activity.this, AdminConfirmation.class));
                                finish();
                            } else {
                                Toast.makeText(Vendor_SignUp_Activity.this, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                    }
                }

                @Override
                public void onFailure(Call<Vendor_LoginUserPojo> call, Throwable t) {
                    pd.dismiss();
                    Toast.makeText(Vendor_SignUp_Activity.this, getString(R.string.server_errors), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            pd.dismiss();
            Toast.makeText(getApplicationContext(), getString(R.string.plz_check_your_intrenet_text), Toast.LENGTH_SHORT).show();
        }
    }*/


}
