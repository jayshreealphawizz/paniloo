package com.alpha.paanilo.authenticationModule;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.cardview.widget.CardView;

import com.alpha.paanilo.R;
import com.alpha.paanilo.locatinenable.LocationEnableActivity;
import com.alpha.paanilo.seller.SellerLoginActivity;
import com.alpha.paanilo.utility.Constants;

public class Loginoption extends AppCompatActivity implements View.OnClickListener {
    CardView abtnSignInCustomer, abtnSignInVendor;
    Constants constants;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_with);
        abtnSignInCustomer = findViewById(R.id.abtnSignInCustomerId);
        abtnSignInVendor = findViewById(R.id.abtnSignInVendorId);

        abtnSignInCustomer.setOnClickListener(this);
        abtnSignInVendor.setOnClickListener(this);
        constants = new Constants(getApplicationContext());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.abtnSignInCustomerId:
                constants.setLoginAs(Constants.USER_CUSTOMER);
                Intent intent = new Intent(Loginoption.this, LocationEnableActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                //    CheckNetwork.nextScreenWithFinish(Loginoption.this, Login_Activity.class);
                break;
            case R.id.abtnSignInVendorId:
                constants.setLoginAs(Constants.USER_VENDOR);
                Intent intentVendor = new Intent(Loginoption.this, SellerLoginActivity.class);
                startActivity(intentVendor);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                // CheckNetwork.nextScreenWithFinish(Loginoption.this, Vendor_SignUp_Activity.class);
                break;
        }
    }
}
