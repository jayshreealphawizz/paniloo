package com.alpha.paanilo.authenticationModule;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import com.alpha.paanilo.FetchLocationActivity;
import com.alpha.paanilo.GpsTracker;
import com.alpha.paanilo.HomeMainActivity;
import com.alpha.paanilo.R;
import com.alpha.paanilo.model.LoginUserInfo;
import com.alpha.paanilo.model.LoginUserPojo;
import com.alpha.paanilo.model.helpPkg.CategoryPozonew;
import com.alpha.paanilo.model.helpPkg.HelpPozo;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.utility.CheckNetwork;
import com.alpha.paanilo.utility.Constants;
import com.alpha.paanilo.utility.CustomToast;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login_Activity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "Login";
    private static Animation shakeAnimation;
    CardView btn_login ,tvRegisteredNow;
    AppCompatEditText tv_email, tv_password;
    ProgressDialog pd;
    Constants constants;
    String firbaseToken;
    AppCompatImageView fb_btn, googgle_btn, ivBackForget;
    private AppCompatTextView tvForgetPassword;
    private GoogleSignInClient googleSignInClient;
    private CallbackManager callbackManager;

    GpsTracker gpsTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setContentView(R.layout.activity_login);
//        getCategory();
        gpsTracker = new GpsTracker(getApplicationContext());

        constants = new Constants(getApplicationContext());
        pd = new ProgressDialog(Login_Activity.this, R.style.AppCompatAlertDialogStyle);
        shakeAnimation = AnimationUtils.loadAnimation(this, R.anim.shake);

        fb_btn = findViewById(R.id.facebook_signupButton);
        googgle_btn = findViewById(R.id.ivGoogleLoginId);

        tv_email = findViewById(R.id.tv_email);
        tv_password = findViewById(R.id.tv_password);
        ivBackForget = findViewById(R.id.ivBackForgetId);

        tvForgetPassword = findViewById(R.id.tvForgetPasswordId);
        tvRegisteredNow = findViewById(R.id.tvRegisteredNow);
        btn_login = findViewById(R.id.btn_login);

        ivBackForget.setOnClickListener(this);
        tvForgetPassword.setOnClickListener(this);
        tvRegisteredNow.setOnClickListener(this);
        btn_login.setOnClickListener(this);
        fb_btn.setOnClickListener(this);
        googgle_btn.setOnClickListener(this);

//
        // printHaskey();

        callbackManager = CallbackManager.Factory.create();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getResources().getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        googleSignInClient = GoogleSignIn.getClient(this, gso);


        updateUI();

        FirebaseApp.initializeApp(Login_Activity.this);
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            // Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }
                        firbaseToken = task.getResult().getToken();
                        // Log.d("token", token);
                    }
                });


        boolean loggedOut = AccessToken.getCurrentAccessToken() == null;
        if (!loggedOut) {
            getUserProfile(AccessToken.getCurrentAccessToken());
        }

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                AccessToken accessToken = loginResult.getAccessToken();
                getUserProfile(accessToken);
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {
            }
        });


        FirebaseApp.initializeApp(Login_Activity.this);
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            // Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }
                        firbaseToken = task.getResult().getToken();
                        // Log.d("token", token);
                    }
                });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivBackForgetId:
                CheckNetwork.backScreenWithFinis(Login_Activity.this);
                break;
            case R.id.tvForgetPasswordId:
                CheckNetwork.nextScreenWithoutFinish(Login_Activity.this, Forget_Activity.class);
                break;
            case R.id.btn_login:
//                Intent intent = new Intent(Login_Activity.this, HomeMainActivity.class);
//                startActivity(intent);
                validation(v);

                break;
            case R.id.tvRegisteredNow:
//                SignUp_Activity
                CheckNetwork.nextScreenWithoutFinish(Login_Activity.this, SignUp_Activity.class);
                break;
            case R.id.ivGoogleLoginId:
                GoogleSignInAccount alreadyloggedAccount = GoogleSignIn.getLastSignedInAccount(this);
                if (alreadyloggedAccount != null) {
                    onLoggedIn(alreadyloggedAccount);
                } else {
                    Intent signInIntent = googleSignInClient.getSignInIntent();
                    startActivityForResult(signInIntent, 101);
                }


                break;
            case R.id.facebook_signupButton:

                boolean loggedOut = AccessToken.getCurrentAccessToken() == null;
                if (!loggedOut) {
                    getUserProfile(AccessToken.getCurrentAccessToken());
                }

                LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email"));
//                LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("user_photos", "email", "public_profile", "user_posts"));
                break;
        }
    }

    //    =================================
//================== google---------------
    public void printHaskey() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.alpha.paanilo", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 101) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            // Signed in successfully, show authenticated UI.
            updateUI();
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
            updateUI();
        }
    }

    private void updateUI() {
        GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(getApplicationContext());
        if (acct != null) {
            String personName = acct.getDisplayName();
            String personGivenName = acct.getGivenName();
            String personFamilyName = acct.getFamilyName();
            String personEmail = acct.getEmail();
            String personId = acct.getId();
            Uri personPhoto = acct.getPhotoUrl();
            sociallogin(personName, personEmail, "1", "1");
        }
    }

    //Google Login Information
    private void onLoggedIn(GoogleSignInAccount googleSignInAccount) {
        String name = googleSignInAccount.getDisplayName();
        String email = googleSignInAccount.getEmail();
        sociallogin(name, email, "1", "1");
    }

    //Facebook Login Information
    private void getUserProfile(AccessToken currentAccessToken) {
        GraphRequest request = GraphRequest.newMeRequest(
                currentAccessToken, new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.d("TAG", object.toString());
                        try {
                            String first_name = object.getString("first_name");
                            String last_name = object.getString("last_name");
                            String id = object.getString("id");
                            String email = object.getString("email");
                            sociallogin(first_name + " " + last_name, email, "2", "2");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "first_name,last_name,email,id");
        request.setParameters(parameters);
        request.executeAsync();

    }

    public void sociallogin(String name, String email, final String status, final String loginstatus) {
        if (CheckNetwork.isNetAvailable(getApplicationContext())) {
            pd.setMessage("Loading");
            pd.show();
            JsonObject jsonObject = new JsonObject();
            try {
                jsonObject.addProperty("name", name);
                jsonObject.addProperty("email", email);
                jsonObject.addProperty("social", status);
                jsonObject.addProperty("loginstatus", loginstatus);
                jsonObject.addProperty("firebaseToken", firbaseToken);
               } catch (Exception e) {
                e.printStackTrace();
            }
            Log.e("TAG", "social login request : " + new Gson().toJson(jsonObject));

            (ApiClient.getClient().social_login(jsonObject)).enqueue(new Callback<LoginUserPojo>() {
                @Override
                public void onResponse(Call<LoginUserPojo> call, Response<LoginUserPojo> response) {
                    pd.dismiss();
                    if (response.isSuccessful()) {
                        try {
                            Log.e("TAG", "social login response : " + new Gson().toJson(response.body()));
                            LoginUserPojo userInfo_pojo = response.body();
                            Boolean repmsg = userInfo_pojo.getStatus();
                            String message = userInfo_pojo.getMessage();

                            if (repmsg) {
                                Toast.makeText(Login_Activity.this, message, Toast.LENGTH_SHORT).show();

                                constants.setIsLogin("yes");
                                constants.setIsLoginStatus(loginstatus);

                                constants.setUserID(userInfo_pojo.getData().getUserId());
                                constants.setUsername(userInfo_pojo.getData().getUserFullname());
                                constants.setEmail(userInfo_pojo.getData().getUserEmail());

                                Log.e("login status ", "status::::  " + loginstatus);
                                constants.setAddress(userInfo_pojo.getData().getAddress());

                                constants.setImage(userInfo_pojo.getData().getUserImage());


                                constants.setMobile(userInfo_pojo.getData().getUserPhone());
                                constants.setdob(userInfo_pojo.getData().getUserBdate());


                                Log.e("Login :: ", "UserID :::: " + userInfo_pojo.getData().getUserId());

                                Log.e("Login :: ", "UserFull Name :::: " + userInfo_pojo.getData().getUserFullname());
                                Log.e("Login :: ", "UserEmail :::: " + userInfo_pojo.getData().getUserEmail());
                                Log.e("Login :: ", "Address :::: " + userInfo_pojo.getData().getAddress());

                                Log.e("Login :: ", "UserImage :::: " + userInfo_pojo.getData().getUserImage());
                                Log.e("Login :: ", "OOOO :::: " + userInfo_pojo.getData().getOtp());

                                Log.e("Login :: ", "Mobile :::: " + userInfo_pojo.getData().getUserPhone());




                                startActivity(new Intent(Login_Activity.this, FetchLocationActivity.class)
                                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                                finish();

                                if (status.equals("1")) {
                                    googleSignInClient.signOut();
                                } else if (status.equals("2") || loginstatus.equals("2")) {
                                    LoginManager.getInstance().logOut();
                                } else {
                                    LoginManager.getInstance().logOut();
                                }
                            } else {
                                Toast.makeText(Login_Activity.this, message, Toast.LENGTH_SHORT).show();
                            }
//
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                    }
                }

                @Override
                public void onFailure(Call<LoginUserPojo> call, Throwable t) {
                    pd.dismiss();
                    Toast.makeText(Login_Activity.this, getString(R.string.server_errors), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            pd.dismiss();
            Toast.makeText(getApplicationContext(), getString(R.string.plz_check_your_intrenet_text), Toast.LENGTH_SHORT).show();
        }
    }

    //===================================
    private void validation(View v) {
        if (tv_email.getText().toString().isEmpty()) {
            // new CustomToast().Show_Toast(this, v, getString(R.string.email_error));
            tv_email.startAnimation(shakeAnimation);
            tv_email.getBackground().mutate().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            tv_email.requestFocus();
            tv_email.setError(getString(R.string.email_error));
        } else if (!Constants.isValidEmail(tv_email.getText().toString())) {
            //new CustomToast().Show_Toast(this, v, getString(R.string.invalid_email_error));
            tv_email.startAnimation(shakeAnimation);
            tv_email.getBackground().mutate().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            tv_email.requestFocus();
            tv_email.setError(getString(R.string.invalid_email_error));
        } else if (tv_password.getText().toString().isEmpty()) {
            // new CustomToast().Show_Toast(this, v, getString(R.string.password_error));
            tv_password.startAnimation(shakeAnimation);
            tv_password.getBackground().mutate().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            tv_password.requestFocus();
            tv_password.setError(getString(R.string.password_error));
        } else if (tv_password.getText().toString().length() < 6) {
            //new CustomToast().Show_Toast(this, v, getString(R.string.password_error_sixdegit_graterthan));
            tv_password.startAnimation(shakeAnimation);
            tv_password.getBackground().mutate().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            tv_password.requestFocus();
            tv_password.setError(getString(R.string.password_error_sixdegit_graterthan));
        } else if (CheckNetwork.isNetAvailable(this)) {
//            doctore_Login(tv_email.getText().toString().trim(),
//                    tv_password.getText().toString().trim());
            doctore_Login();
        } else {
            new CustomToast().Show_Toast(this, v, getString(R.string.plz_check_your_intrenet_text));
        }
    }

    private void doctore_Login() {
        pd.setMessage("Loading");
        pd.show();

        Log.e("rahulraj " , "tv_email " + tv_email.getText().toString().trim());
        Log.e("rahulraj " , "tv_password " + tv_password.getText().toString().trim());


        (ApiClient.getClient().login(tv_email.getText().toString().trim(), tv_password.getText().toString().trim(),firbaseToken)).enqueue(new Callback<LoginUserPojo>() {
            @Override
            public void onResponse(Call<LoginUserPojo> call, Response<LoginUserPojo> response) {
                pd.dismiss();
                Log.e("TAG", "login response : " + new Gson().toJson(response.body()));
                if (response.isSuccessful()) {
                    try {

                        LoginUserPojo userInfo_pojo = response.body();
                        Boolean repmsg = userInfo_pojo.getStatus();
                        String message = userInfo_pojo.getMessage();

                        if (repmsg) {
                            Toast.makeText(Login_Activity.this, message, Toast.LENGTH_SHORT).show();

                            constants.setIsLogin("yes");
                            constants.setUserID(userInfo_pojo.getData().getUserId());
                            constants.setUsername(userInfo_pojo.getData().getUserFullname());
                            constants.setEmail(userInfo_pojo.getData().getUserEmail());

//                            constants.setCity(userInfo_pojo.getData().getUserCity());

                            constants.setAddress(userInfo_pojo.getData().getAddress());

                            constants.setImage(userInfo_pojo.getData().getUserImage());


                            constants.setMobile(userInfo_pojo.getData().getUserPhone());
                            constants.setdob(userInfo_pojo.getData().getUserBdate());


//                            AppSession.setStringPreferences(LoginActivity.this, Constants.LOGINSTATUS, status);
//                               constants.setOTP(userInfo_pojo.getData().getOtp());


                            Log.e("OTP :: ", "sssllll :::: " + userInfo_pojo.getData().getLatitude());
                            Log.e("OTP :: ", "sssllOOOO :::: " + userInfo_pojo.getData().getLongitude());


                            Log.e("Login :: ", "UserID :::: " + userInfo_pojo.getData().getUserId());

                            Log.e("Login :: ", "UserFull Name :::: " + userInfo_pojo.getData().getUserFullname());
                            Log.e("Login :: ", "UserEmail :::: " + userInfo_pojo.getData().getUserEmail());
                            Log.e("Login :: ", "Address :::: " + userInfo_pojo.getData().getAddress());

                            Log.e("Login :: ", "UserImage :::: " + userInfo_pojo.getData().getUserImage());
                            Log.e("Login :: ", "OOOO :::: " + userInfo_pojo.getData().getOtp());

                            Log.e("Login :: ", "Mobile :::: " + userInfo_pojo.getData().getUserPhone());

                            startActivity(new Intent(Login_Activity.this, FetchLocationActivity.class)
                                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                            finish();

                        } else {
                            Toast.makeText(Login_Activity.this, message, Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {

                     try {
                            Log.e(TAG, "onResponse: errrlogin "+response.errorBody().string() );
                           } catch (IOException e) {
                                             e.printStackTrace();
                      }
                }
            }

            @Override
            public void onFailure(Call<LoginUserPojo> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(Login_Activity.this, getString(R.string.server_errors), Toast.LENGTH_SHORT).show();
            }
        });


    }

//    private void getCategory() {
////        pd.setMessage("Loading");
////        pd.show();
//        Log.e("TAG", "get Category 1111: ");
//
//        (ApiClient.getClient().getCategory()).enqueue(new Callback<CategoryPozonew>() {
//
//            @Override
//            public void onResponse(Call<CategoryPozonew> call, Response<CategoryPozonew> response) {
////                pd.dismiss();
//                Log.e("TAG", "get Category : " + new Gson().toJson(response.body()));
//                Log.e("TAG", "get Category 1111: ");
//                Log.e("TAG", "1111: " + response.toString());
//                Log.e("TAG", "2222: " + response.errorBody());
//                if (response.isSuccessful()) {
//                    try {
//                        CategoryPozonew termsAndConditionPozo = response.body();
//                        if (termsAndConditionPozo.getStatus()) {
////                            tv_text.setText(Html.fromHtml(termsAndConditionPozo.getData().getHelpDesc()));
//                        } else {
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                } else {
//                }
//            }
//
//            @Override
//            public void onFailure(Call<CategoryPozonew> call, Throwable t) {
////                pd.dismiss();
//                Log.e("TAG", "ttttttt: " + t.getMessage());
//
//                Toast.makeText(getApplicationContext(), getString(R.string.server_errors), Toast.LENGTH_SHORT).show();
//            }
//        });
//    }
}
