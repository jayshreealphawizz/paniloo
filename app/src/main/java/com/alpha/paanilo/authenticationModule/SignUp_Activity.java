package com.alpha.paanilo.authenticationModule;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.alpha.paanilo.FetchLocationActivity;
import com.alpha.paanilo.GpsTracker;
import com.alpha.paanilo.HomeMainActivity;
import com.alpha.paanilo.R;
import com.alpha.paanilo.editprofile_pkg.EditProfile_Activity;
import com.alpha.paanilo.model.LoginUserInfo;
import com.alpha.paanilo.model.LoginUserPojo;
import com.alpha.paanilo.model.profilePkg.EditProfilePozo;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.seller.Seller_SignUp_Activity;
import com.alpha.paanilo.utility.CheckNetwork;
import com.alpha.paanilo.utility.Constants;
import com.alpha.paanilo.utility.CustomToast;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;


public class SignUp_Activity extends AppCompatActivity implements View.OnClickListener {
    private static final int SELECT_PICTURE = 101;
    private static final int PERMISSION_READ_EXTERNAL_STORAGE = 100;
    private static Animation shakeAnimation;
    CardView btn_signup;
    TextInputEditText tv_username, tv_email, tv_mobile_number, tv_password, tv_confirm_password, tv_address;
    ProgressDialog pd;
    String firbaseToken;

    ImageView imagepencil, img_background;
    Geocoder geocoder;
    List<Address> addresses;
    double latitude, longitude;
    TextView tvLocation;
    private AppCompatImageView ivBackForgetId;
    private AppCompatTextView tv_back_signup;
    private Constants constants;
    private String imagePath;
    private File fileForImage;
    private GpsTracker gpsTracker;
    private FusedLocationProviderClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setContentView(R.layout.activity_signup);

        init();
        Places.initialize(getApplicationContext(), getString(R.string.google_maps_key));
        tv_back_signup = findViewById(R.id.tv_back_signup);
        ivBackForgetId = findViewById(R.id.ivBackForgetId);
        btn_signup = findViewById(R.id.btn_signup);

        imagepencil = findViewById(R.id.imagepencil);

        img_background = findViewById(R.id.img_background);


        tv_back_signup.setOnClickListener(this);
        ivBackForgetId.setOnClickListener(this);
        btn_signup.setOnClickListener(this);
        imagepencil.setOnClickListener(this);

        constants = new Constants(getApplicationContext());
        pd = new ProgressDialog(SignUp_Activity.this, R.style.AppCompatAlertDialogStyle);
        shakeAnimation = AnimationUtils.loadAnimation(this, R.anim.shake);

        FirebaseApp.initializeApp(SignUp_Activity.this);
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            // Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }
                        firbaseToken = task.getResult().getToken();
                        Log.d("token", firbaseToken);
                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivBackForgetId:
                CheckNetwork.backScreenWithFinis(SignUp_Activity.this);
                break;
            case R.id.btn_signup:
//                Intent intent = new Intent(SignUp_Activity.this, OTPActivity.class);
//                startActivity(intent);
                validation(v);

                break;
            case R.id.tv_back_signup:
                CheckNetwork.backScreenWithFinis(SignUp_Activity.this);
                break;
            case R.id.imagepencil:

                askStoragePermission();
//                CheckNetwork.backScreenWithFinis(SignUp_Activity.this);
                break;
        }
    }

    private void init() {
        tv_username = findViewById(R.id.tv_username);
        tv_email = findViewById(R.id.tv_email);
        tv_address = findViewById(R.id.tv_address);
        tvLocation = findViewById(R.id.tvLocation);
        tv_mobile_number = findViewById(R.id.tv_mobile_number);

        tv_password = findViewById(R.id.tv_password);
        tv_confirm_password = findViewById(R.id.tv_confirm_password);
        shakeAnimation = AnimationUtils.loadAnimation(this, R.anim.shake);

        requestPermission();
        getLocation();
        client = LocationServices.getFusedLocationProviderClient(this);

        tvLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(SignUp_Activity.this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }

                client.getLastLocation().addOnSuccessListener(SignUp_Activity.this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if (location != null) {
//                            tv_address.setText(location.toString());
                            try {
                                getAddres(location);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });

            }
        });

    }


    public void getLocation() {
        gpsTracker = new GpsTracker(SignUp_Activity.this);
        if (gpsTracker.canGetLocation()) {
            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();

            Log.e("latitude", "latitude::::::" + latitude);
            Log.e("latitude", "longitude:::::" + longitude);

        } else {
            gpsTracker.showSettingsAlert();
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION}, 1);
    }

    private void getAddres(Location location) throws IOException {

        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()

        tv_address.setText("" + address);
        Log.e("address::::", "address::" + address);
        String city = addresses.get(0).getLocality();

        Log.e("city::::", "city::" + city);

        String state = addresses.get(0).getAdminArea();

        Log.e("state::::", "state::" + state);

        String country = addresses.get(0).getCountryName();
        Log.e("country::::", "country::" + country);

        String postalCode = addresses.get(0).getPostalCode();
        Log.e("postalCode::::", "postalCode::" + postalCode);

        String knownName = addresses.get(0).getFeatureName();

        Log.e("knownName::::", "knownName::" + knownName);

    }


    private void askStoragePermission() {
        if (ActivityCompat.checkSelfPermission(SignUp_Activity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            {
                ActivityCompat.requestPermissions(SignUp_Activity.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        PERMISSION_READ_EXTERNAL_STORAGE);
            }
        } else {
            chooseFromGallery();
        }
    }

    private void validation(View v) {
        if (tv_username.getText().toString().isEmpty()) {
            //new CustomToast().Show_Toast(this, v, getString(R.string.username_error));
            tv_username.startAnimation(shakeAnimation);
            tv_username.getBackground().mutate().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            tv_username.requestFocus();
            tv_username.setError(getString(R.string.username_error));

        } else if (!tv_username.getText().toString().matches("^[a-zA-Z\\s]+")) {
            //new CustomToast().Show_Toast(this, v, "Invalid user name");
            tv_username.startAnimation(shakeAnimation);
            tv_username.getBackground().mutate().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            tv_username.requestFocus();
            tv_username.setError(getString(R.string.invalid_user_name));

        } else if (tv_email.getText().toString().isEmpty()) {
            //new CustomToast().Show_Toast(this, v, getString(R.string.email_error));
            tv_email.startAnimation(shakeAnimation);
            tv_email.getBackground().mutate().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            tv_email.requestFocus();
            tv_email.setError(getString(R.string.email_error));

        } else if (!Constants.isValidEmail(tv_email.getText().toString())) {
            // new CustomToast().Show_Toast(this, v, getString(R.string.invalid_email_error));
            tv_email.startAnimation(shakeAnimation);
            tv_email.getBackground().mutate().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            tv_email.requestFocus();
            tv_email.setError(getString(R.string.invalid_email_error));

        } else if (tv_address.getText().toString().isEmpty()) {
            //new CustomToast().Show_Toast(this, v, getString(R.string.username_error));
            tv_address.startAnimation(shakeAnimation);
            tv_address.getBackground().mutate().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            tv_address.requestFocus();
            tv_address.setError(getString(R.string.address_error));

        } /*else if (!tv_address.getText().toString().matches("^[a-zA-Z\\s]+")) {
            //new CustomToast().Show_Toast(this, v, "Invalid user name");
            tv_address.startAnimation(shakeAnimation);
            tv_address.getBackground().mutate().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            tv_address.requestFocus();
            tv_address.setError(getString(R.string.invalid_address));

        }*/ else if (tv_mobile_number.getText().toString().isEmpty()) {
            // new CustomToast().Show_Toast(this, v, getString(R.string.password_error));
            tv_mobile_number.startAnimation(shakeAnimation);
            tv_mobile_number.getBackground().mutate().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            tv_mobile_number.requestFocus();
            tv_mobile_number.setError(getString(R.string.mobile_error));

        } else if (tv_mobile_number.getText().toString().length() <= 9) {
            // new CustomToast().Show_Toast(this, v, getString(R.string.password_error));
            tv_password.startAnimation(shakeAnimation);
            tv_password.getBackground().mutate().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            tv_password.requestFocus();
            tv_mobile_number.setError(getString(R.string.valid_mobile_error));
        } else if (tv_password.getText().toString().isEmpty()) {
            //  new CustomToast().Show_Toast(this, v, getString(R.string.password_error));
            tv_password.startAnimation(shakeAnimation);
            tv_password.getBackground().mutate().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            tv_password.requestFocus();
            tv_password.setError(getString(R.string.password_error));

        } else if (tv_password.getText().toString().length() <= 6) {
            // new CustomToast().Show_Toast(this, v, getString(R.string.password_error_sixdegit_graterthan));
            tv_password.startAnimation(shakeAnimation);
            tv_password.getBackground().mutate().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            tv_password.requestFocus();
            tv_password.setError(getString(R.string.password_error_sixdegit_graterthan));

        } else if (tv_confirm_password.getText().toString().isEmpty()) {
            // new CustomToast().Show_Toast(this, v, getString(R.string.confirm_password_empty));
            tv_confirm_password.startAnimation(shakeAnimation);
            tv_confirm_password.getBackground().mutate().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            tv_confirm_password.requestFocus();
            tv_confirm_password.setError(getString(R.string.confirm_password_empty));

        } else if (tv_confirm_password.getText().toString().length() <= 6) {
            // new CustomToast().Show_Toast(this, v, getString(R.string.confirm_password_digit_graterthan_six));
            tv_confirm_password.startAnimation(shakeAnimation);
            tv_confirm_password.getBackground().mutate().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            tv_confirm_password.requestFocus();
            tv_confirm_password.setError(getString(R.string.confirm_password_digit_graterthan_six));

        } else if (!(tv_password.getText().toString().equals(tv_confirm_password.getText().toString()))) {
            // new CustomToast().Show_Toast(this, v, getString(R.string.confirm_password_should_same));
            tv_confirm_password.startAnimation(shakeAnimation);
            tv_confirm_password.getBackground().mutate().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            tv_confirm_password.requestFocus();
            tv_confirm_password.setError(getString(R.string.confirm_password_should_same));

        } else if (CheckNetwork.isNetAvailable(this)) {
            /*signup(tv_username.getText().toString().trim(),
                    tv_email.getText().toString().trim(),
                    tv_address.getText().toString().trim(),
                    tv_password.getText().toString().trim()

          );*/
            signup();
        } else {
            new CustomToast().Show_Toast(this, v, "Check Network Connection");
        }
    }

    private void signup() {
        pd.setMessage("Loading");
        pd.show();
        MultipartBody.Part imgFileStation = null;

        if (imagePath == null) {
        } else {
            fileForImage = new File(imagePath);
//            File fileForImage = file;
            RequestBody requestFileOne = RequestBody.create(MediaType.parse("multipart/form-data"), fileForImage);
            imgFileStation = MultipartBody.Part.createFormData("user_image", fileForImage.getName(), requestFileOne);
        }

        String tv_userName = tv_username.getText().toString();
        String tv_emailAddress = tv_email.getText().toString();
//        final String tv_mobileNumber = tv_mobile_number.getText().toString();
//        String tvDob = tv_dob.getText().toString();
        final String password = tv_password.getText().toString();
        final String address = tv_address.getText().toString();

        String mobilenumber = tv_mobile_number.getText().toString();


        MultipartBody.Part firebasetoken = MultipartBody.Part.createFormData("firebasetoken", firbaseToken);
        MultipartBody.Part user_fname = MultipartBody.Part.createFormData("user_fullname", tv_userName);
        MultipartBody.Part user_email = MultipartBody.Part.createFormData("user_email", tv_emailAddress);
//        MultipartBody.Part user_mobile = MultipartBody.Part.createFormData("user_phone", tv_mobileNumber);
        MultipartBody.Part user_Password = MultipartBody.Part.createFormData("user_password", password);
        MultipartBody.Part user_address = MultipartBody.Part.createFormData("address", address);

        MultipartBody.Part user_mobile = MultipartBody.Part.createFormData("user_phone", mobilenumber);


        Log.e("FirebaseToken :: ", "firebasetoken :::: " + firbaseToken);
//        Log.e("FirebaseToken :: ", "user_fname :::: " + user_fname.toString());
//        Log.e("FirebaseToken :: ", "user_email :::: " + user_email.toString());
//
//        Log.e("FirebaseToken :: ", "user_Password :::: " + user_Password.toString());
//        Log.e("FirebaseToken :: ", "user_address :::: " + user_address.toString());

        Log.e("FirebaseToken :: ", "mobile :::: " + user_mobile.toString());

//        Log.e("FirebaseToken :: ", "imgFileStation :::: " + imgFileStation);

//    Log.e("OTP :: ", "UserEmail :::: " + userInfo_pojo.getData().getUserEmail());
//        System.out.println("id===" + userid + "==user_fullname===" + tv_userName + "==tv_emailAddress==" + tv_emailAddress + "==tv_mobileNumber==" + tv_mobileNumber
//                + tvDob + "==city=" + city + "===image==" + imgFileStation);

        (ApiClient.getClient().signUp(user_fname, user_email, user_Password,
                user_address, user_mobile, firebasetoken, imgFileStation)).enqueue(new Callback<LoginUserPojo>() {
            @Override
            public void onResponse(Call<LoginUserPojo> call, Response<LoginUserPojo> response) {
                pd.dismiss();
                if (response.isSuccessful()) {
                    try {
                        pd.dismiss();
                        Log.e("TAG", "SingUp Response " + new Gson().toJson(response.body()));
                        LoginUserPojo userInfoPojo = response.body();
                        Boolean repmsg = userInfoPojo.getStatus();
                        String message = userInfoPojo.getMessage();
                        if (repmsg) {

                            constants.setIsLogin("yes");
                            constants.setUserID(userInfoPojo.getData().getUserId());

                            constants.setUsername(userInfoPojo.getData().getUserFullname());

                            constants.setEmail(userInfoPojo.getData().getUserEmail());

                            constants.setAddress(userInfoPojo.getData().getAddress());

                            constants.setImage(userInfoPojo.getData().getUserImage());


                            constants.setMobile(userInfoPojo.getData().getUserPhone());

                            constants.setOTP(userInfoPojo.getData().getOtp());
//                            constants.setdob(userInfo_pojo.getData().getd());
                            constants.setdob(userInfoPojo.getData().getUserBdate());



                            Log.e("OTP :: ", "llll :::: " + userInfoPojo.getData().getLatitude());
                            Log.e("OTP :: ", "llOOOO :::: " + userInfoPojo.getData().getLongitude());



                            Log.e("OTP :: ", "UserID :::: " + userInfoPojo.getData().getUserId());
                            Log.e("OTP :: ", "UserFull Name :::: " + userInfoPojo.getData().getUserFullname());
                            Log.e("OTP :: ", "UserEmail :::: " + userInfoPojo.getData().getUserEmail());
                            Log.e("OTP :: ", "Address :::: " + userInfoPojo.getData().getAddress());

                            Log.e("OTP :: ", "UserImage :::: " + userInfoPojo.getData().getUserImage());
                            Log.e("OTP :: ", "OOOO :::: " + userInfoPojo.getData().getOtp());


                            Log.e("OTP :: ", "Mobile number :::: " + userInfoPojo.getData().getUserPhone());


                            Toast.makeText(SignUp_Activity.this, message, Toast.LENGTH_SHORT).show();

                            startActivity(new Intent(SignUp_Activity.this, OTPActivity.class));
                            finish();
                        } else {
                            Toast.makeText(SignUp_Activity.this, message, Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    System.out.println("error==========");
                }
            }

            @Override
            public void onFailure(Call<LoginUserPojo> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(SignUp_Activity.this, getString(R.string.server_errors), Toast.LENGTH_SHORT).show();
            }
        });

    }


    private void chooseFromGallery() {
        CropImage.activity().start(SignUp_Activity.this);
    }

    private void resultCrop(Uri resultUri) {
        imagePath = compressImage(String.valueOf(resultUri));
//        tv_image_path.setText(imagePath);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    chooseFromGallery();
                } else {
                    Toast.makeText(SignUp_Activity.this, "Permission Denied", Toast.LENGTH_LONG).show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    public String compressImage(String imageUri) {

        String filePath = getRealPathFromURI(Uri.parse(imageUri));

        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

        //      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
        //      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);


        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

        //      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 900.0f;
        float maxWidth = 670.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

        //      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

        //      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

        //      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

        //      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[8 * 512];

        try {
            //          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
            //            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            //            bmp.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            //            byte[] imageInByte = stream.toByteArray();
            //            long lengthbmp = imageInByte.length;
            //            System.out.println(lengthbmp);

        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

        //      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);

            //          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        img_background.setImageBitmap(scaledBitmap);
        //return scaledBitmap;
        return filename;

    }

    public String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), getString(R.string.app_name) + "/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }
        return inSampleSize;
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getApplicationContext().getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        System.out.println("Path is file get real path se====" + result);
        return result;
    }

//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
//        startActivity(intent);
//        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
//        finish();
//    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                Uri resultUri = result.getUri();
                resultCrop(resultUri);

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Toast.makeText(this, "Cropping failed: " + error, Toast.LENGTH_LONG).show();
            }
        }

    }


}
