package com.alpha.paanilo.authenticationModule;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.alpha.paanilo.FetchLocationActivity;
import com.alpha.paanilo.GpsTracker;
import com.alpha.paanilo.R;

import com.alpha.paanilo.cart_pkg.TicketDetailsActivity;
import com.alpha.paanilo.seller.FetchLocationActivitySeller;
import com.alpha.paanilo.seller.ImagesActivity;
import com.alpha.paanilo.seller.NewActivity;
import com.alpha.paanilo.seller.SellerHomeActivity;
import com.alpha.paanilo.utility.CheckNetwork;
import com.alpha.paanilo.utility.Constants;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Splash_Activity extends AppCompatActivity /*implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, PermissionResultCallBack, LocationListener*/ {
    Constants constant;
    String TAG = getClass().getSimpleName();
    private String loginEntry;
    private GpsTracker gpsTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        constant = new Constants(getApplicationContext());

//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                Intent intent = new Intent(Splash_Activity.this, TicketDetailsActivity.class);
//                startActivity(intent);
//                finish();
//            }
//        }, 50);

        gpsTracker = new GpsTracker(Splash_Activity.this);
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.alpha.paanilo",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }


        nextScreen();
    }

      /*  permissionUtils = new PermissionUtils(this);
        // for device model check===================
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            allPermission();
        } else {
            mGoogleApiClient = new GoogleApiClient.Builder(this).
                    addApi(LocationServices.API).
                    addConnectionCallbacks(this).
                    addOnConnectionFailedListener(this).build();
        }*/



 /*   @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!checkPlayServices()) {
            Toast.makeText(Splash_Activity.this, "You need to install Google Play Services to use the App properly", Toast.LENGTH_LONG).show();
        }


    }

    @Override
    protected void onPause() {
        super.onPause();
        // stop location updates
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST);
            } else {
                finish();
            }
            return false;
        }
        return true;
    }*/

   /* @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("onActivityResult()", Integer.toString(resultCode));

        switch (requestCode) {
            case REQUEST_LOCATION:
                switch (resultCode) {
                    case Activity.RESULT_OK: {
                        System.out.println("ok callllllllllll");
                        Location MobLoc = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                        try {

                            System.out.println(MobLoc.getLatitude() + "address=====" + MobLoc.getLongitude());
                            Geocoder geocoder;
                            List<Address> addresses;
                            geocoder = new Geocoder(this, Locale.getDefault());

                            try {
                                addresses = geocoder.getFromLocation(MobLoc.getLatitude(), MobLoc.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
//                                String city = addresses.get(0).getLocality();
                                String state = addresses.get(0).getAdminArea();
                                String country = addresses.get(0).getCountryName();
                                String postalCode = addresses.get(0).getPostalCode();
                                String knownName = addresses.get(0).getFeatureName();


                                String currentLocation = address;
                               // String city = addresses.get(0).getLocality() + "," + state;

                                String city = addresses.get(0).getLocality() != null ? addresses.get(0).getLocality() + "," + state : addresses.get(0).getSubAdminArea();


                                System.out.println(city + "address=====" + currentLocation);
                                constants.setCurrentLocation(currentLocation);
                                constants.setCity(city);
                                constants.setLatitude(String.valueOf(addresses.get(0).getLatitude()));
                                constants.setLongitude(String.valueOf(addresses.get(0).getLongitude()));
                                nextScreen();

                                // callNextScreenIntent(city, addresses.get(0).getLatitude(), addresses.get(0).getLongitude());

                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } catch (Exception e) {
                            allPermission();
                            nextScreen();
                           // Toast.makeText(this, "Location not fetch, Please click on try again!", Toast.LENGTH_SHORT).show();
                        }

                        // All required changes were successfully made
//                        Toast.makeText(FetchLocationActivity.this, "Location enabled by user!", Toast.LENGTH_LONG).show();
                        break;
                    }
                    case Activity.RESULT_CANCELED: {
                        System.out.println("cancel callllllllllll");
                        // The user was asked to change settings, but chose not to
                        Toast.makeText(Splash_Activity.this, "Location not enabled, user cancelled.", Toast.LENGTH_LONG).show();
                        break;
                    }
                    default: {
                        System.out.println("defalut callllllllllll");
                        break;
                    }
                }
                break;
        }
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(2 * 1000);
        mLocationRequest.setFastestInterval(4 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);
        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                //final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:

                        try {
                            Location MobLoc = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                            List<Address> addresses;
                            Geocoder geocoder;

                            geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                            if (MobLoc != null) {
                                addresses = geocoder.getFromLocation(MobLoc.getLatitude(), MobLoc.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
//                                String city = addresses.get(0).getLocality();
                                String state = addresses.get(0).getAdminArea();
                                String country = addresses.get(0).getCountryName();
                                String postalCode = addresses.get(0).getPostalCode();
                                String knownName = addresses.get(0).getFeatureName();

                                String currentLocation = address;
                                String city = addresses.get(0).getLocality() + "," + state;

                                constants.setCurrentLocation(currentLocation);
                                constants.setCity(city);
                                constants.setLatitude(String.valueOf(addresses.get(0).getLatitude()));
                                constants.setLongitude(String.valueOf(addresses.get(0).getLongitude()));

                                nextScreen();

                                System.out.println(city + "111111111address=====" + currentLocation);
                            }
                            else {
                                nextScreen();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        //...
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(
                                    Splash_Activity.this,
                                    REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        //...
                        break;
                }
            }
        });
    }

    public void allPermission() {
        alForPermission = new ArrayList<>();
        alForPermission.add(ACCESS_FINE_LOCATION);
        alForPermission.add(ACCESS_COARSE_LOCATION);
        permissionUtils.check_permission(alForPermission, getString(R.string.premission_des), REQUEST_CODE);
    }

    @Override
    public void PermissionGranted(int request_code) {
        mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(Splash_Activity.this)
                .addOnConnectionFailedListener(Splash_Activity.this).build();
    }

    @Override
    public void PartialPermissionGranted(int request_code, ArrayList<String> granted_permissions) {
    }

    @Override
    public void PermissionDenied(int request_code) {
        allPermission();
    }

    @Override
    public void NeverAskAgain(int request_code) {
        allPermission();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        try {
            permissionUtils.onRequestPermissionsResult(requestCode, permissions, grantResults);
            if (requestCode == 200) {
                switch (grantResults[0]) {
                    // Toast.makeText(getApplicationContext(), "Permission required", Toast.LENGTH_LONG).show();
                    case Activity.RESULT_CANCELED: {
                        nextScreen();
                        break;
                    }
                    default: {
                        allPermission();
                        break;
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {

    }
*/

    private void nextScreen() {
        try {
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {

                    Log.e(TAG, "run: " + constant.getIsLogin());
                    if (constant.getIsLogin().equals("yes")) {
                        CheckNetwork.nextScreenWithFinish(Splash_Activity.this, FetchLocationActivity.class);
                    } else if (constant.getIsLogin().equalsIgnoreCase("yes_seller")) {

                        CheckNetwork.nextScreenWithFinish(Splash_Activity.this, SellerHomeActivity.class);

                    } else {
                        CheckNetwork.nextScreenWithFinish(Splash_Activity.this, Loginoption.class);
                    }
                }
            }, 5000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
