package com.alpha.paanilo.authenticationModule;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;

import com.alpha.paanilo.R;
import com.alpha.paanilo.utility.Constants;

public class AdminConfirmation extends AppCompatActivity implements View.OnClickListener {
    AppCompatButton abtnSignInCustomer, abtnSignInVendor;
    Constants constants;
    AppCompatImageView ivBackForget;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.admin_confirmation);
        ivBackForget = findViewById(R.id.ivBackForgetId);
        ivBackForget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.abtnSignInCustomerId:
//                constants.setLoginAs(Constants.USER_CUSTOMER);
//                CheckNetwork.nextScreenWithFinish(AdminConfirmation.this, Login_Activity.class);
//                break;
        }
    }
}
