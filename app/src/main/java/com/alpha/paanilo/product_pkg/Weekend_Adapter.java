package com.alpha.paanilo.product_pkg;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.alpha.paanilo.R;
import com.alpha.paanilo.Roomdatabase.DatabaseClient;
import com.alpha.paanilo.Roomdatabase.ProductDataTask;
import com.alpha.paanilo.model.AddCartCustomModel;
import com.alpha.paanilo.model.productByCategory.ProductData;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.utility.Constants;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

public class Weekend_Adapter extends RecyclerView.Adapter<Weekend_Adapter.MyViewHolder> {
    List<ProductData> weekendList;
    Context context;
    String category_comes, vendorId;
    private LayoutInflater mInflater;
    private final WeekendProductOnClickListener productOnClickListener;
    private final Constants constants;

    public Weekend_Adapter(Context activity, String come, WeekendProductOnClickListener productOnClickListener) {
        context = activity;
        category_comes = come;
        this.productOnClickListener = productOnClickListener;
        constants = new Constants(activity);
        vendorId = getCartDataTasks();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_fragment_weekend_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        try {
            final ProductData datum = weekendList.get(position);
            holder.tv_productName.setText(datum.getProductName());
            holder.tvProductDesc.setText(datum.getTitle());
            holder.tv_productPrice.setText(context.getString(R.string.rupee) + " " + Constants.getTotalWithSeparater(datum.getPrice()));

            if (datum.getQuantity().equalsIgnoreCase("0")) {
                holder.tvoutOfStock.setVisibility(View.VISIBLE);
                holder.btn_send.setVisibility(View.GONE);
            } else {
                holder.tvoutOfStock.setVisibility(View.GONE);
                holder.btn_send.setVisibility(View.VISIBLE);
            }

            Log.e("pro", "imgNameee " + datum.getProductImage());
            if (datum.getProductImage().equals("")) {
            } else {

                String imgName = datum.getProductImage().substring((datum.getProductImage().lastIndexOf(",") + 1));

                Log.e("pro", "imgName" + imgName);


                Glide.with(context)
                        .load(ApiClient.CATEGORY_PRODUCT_URL + imgName)
                        .placeholder(R.drawable.placeholder)
                        .fitCenter()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .dontTransform()
                        .into(holder.id_userpic);
            }

            holder.btn_send.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (constants.getVendrorClick().equalsIgnoreCase("clicked")) {
                        if (vendorId == null || vendorId.isEmpty()) {
                            saveTask(weekendList,
                                    position,
                                    holder.rlAddToCart
                                    , holder.quntityende);
                        } else if (constants.getVendror().equalsIgnoreCase(vendorId)) {
                            saveTask(weekendList,
                                    position,
                                    holder.rlAddToCart
                                    , holder.quntityende);
                        } else {
                            alertDialog("If you want to purchase this stall product then checkout to previous");
                        }

                       /* saveTask(weekendList,
                                position,
                                holder.rlAddToCart
                                , holder.quntityende);*/
                    } else {
                        Toast.makeText(context, "Select any stall near to you", Toast.LENGTH_LONG).show();
                    }
                }
            });

            holder.increment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int numtest = Integer.parseInt(holder.quntity.getText().toString());
                    int mainprice = Integer.parseInt(weekendList.get(position).getPrice());
                    numtest = numtest + 1;
              /*  numtest = numtest + 1;
                holder.quntity.setText("" + numtest);
                int subtotal = numtest * mainprice;*/
                    if (numtest <= Integer.parseInt(weekendList.get(position).getQuantity())) {
                        holder.quntity.setText("" + numtest);
                        int subtotal = numtest * mainprice;
                        updateTask(subtotal, numtest, weekendList.get(position).getProductId());
                    } else {
                        holder.increment.setEnabled(false);
                        Toast.makeText(context, "You can select a maximum " + weekendList.get(position).getQuantity() + " at one time", Toast.LENGTH_LONG).show();
                    }
                }
            });

            holder.decrement.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.increment.setEnabled(true);
                    int numtest = Integer.parseInt(holder.quntity.getText().toString());
                    int mainprice = Integer.parseInt(weekendList.get(position).getPrice());
                    if (numtest > 1) {
                        numtest = numtest - 1;
                        holder.quntity.setText(numtest + "");
                        int subtotal = numtest * mainprice;
                        // totalmain.setText("" + subtotal);
                        updateTask(subtotal, numtest, weekendList.get(position).getProductId());
                    } else {
                        deleteTask(weekendList.get(position).getProductId(), holder.rlAddToCart
                                , holder.quntityende);
                    }
                }
            });
            checkProductAvailbility(weekendList.get(position).getProductId(), holder.rlAddToCart, holder.quntityende, holder.quntity);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void addWeekendProductList(List<ProductData> weekendList) {
        this.weekendList = weekendList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (category_comes.equals("viewall")) {
            return weekendList == null ? 0 : weekendList.size();
        } else {
            if (weekendList == null) {
                return 0;
            } else if (weekendList.size() < 2) {
                return weekendList == null ? 0 : weekendList.size();
            } else {
                return weekendList == null ? 0 : 2;
            }
        }
    }

    private void updateTask(final int subTotl, final int numtest, final String productId) {
        class UpdateTask extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                DatabaseClient.getInstance(context).getAppDatabase()
                        .taskDao()
                        .updatea(String.valueOf(subTotl), String.valueOf(numtest), productId);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                getCartCount();
            }
        }
        UpdateTask ut = new UpdateTask();
        ut.execute();
    }

    private void deleteTask(final String productId, final LinearLayout addToCart, final LinearLayout qty) {
        class DeleteTask extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... voids) {
                ProductDataTask task = new ProductDataTask();
                DatabaseClient.getInstance(context).getAppDatabase()
                        .taskDao()
                        .deleteByUserId(productId);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                qty.setVisibility(View.GONE);
                addToCart.setVisibility(View.VISIBLE);
                getCartCount();
            }
        }
        DeleteTask dt = new DeleteTask();
        dt.execute();
    }


    //    ===================

    private void saveTask(final List<ProductData> productList, final int position,
                          final LinearLayout rlAddtoCart, final LinearLayout rlQty) {
        class SaveTask extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... voids) {
                ProductDataTask task = new ProductDataTask();
                task.setProduct_id(productList.get(position).getProductId());
                task.setProduct_name(productList.get(position).getProductName());
                task.setProduct_description(productList.get(position).getProductDescription());
                task.setProduct_image(productList.get(position).getProductImage());
                task.setIn_stock(productList.get(position).getInStock());
                task.setPrice(productList.get(position).getPrice());
                task.setVendorId(productList.get(position).getFarmer_id());
                task.setQuantity("1");
                task.setTotal(String.valueOf(productList.get(position).getPrice()));
                DatabaseClient.getInstance(context).getAppDatabase()
                        .taskDao()
                        .insert(task);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                rlQty.setVisibility(View.VISIBLE);
                rlAddtoCart.setVisibility(View.GONE);
                getCartCount();
            }
        }
        SaveTask st = new SaveTask();
        st.execute();
    }

    private void getCartCount() {
        class GetTasks extends AsyncTask<Void, Void, String> {
            @Override
            protected String doInBackground(Void... voids) {
                String taskList = DatabaseClient
                        .getInstance(context)
                        .getAppDatabase()
                        .taskDao()
                        .getCount();
                return taskList;
            }

            @Override
            protected void onPostExecute(String tasks) {
                super.onPostExecute(tasks);
                AddCartCustomModel.getInstance().setCartCount("" + tasks);
            }
        }
        GetTasks gt = new GetTasks();
        gt.execute();
    }

    private void checkProductAvailbility(final String productId, final LinearLayout addToCart, final LinearLayout qtnBox, final AppCompatTextView qty) {
        class SaveTask extends AsyncTask<Void, Void, ProductDataTask> {
            @Override
            protected ProductDataTask doInBackground(Void... voids) {
                ProductDataTask task = DatabaseClient
                        .getInstance(context)
                        .getAppDatabase()
                        .taskDao()
                        .getproductid(productId);
                return task;
            }

            @Override
            protected void onPostExecute(ProductDataTask task) {
                super.onPostExecute(task);
                try {
                    if (task != null) {
                        if (Integer.parseInt(task.getQuantity()) > 0) {
                            qtnBox.setVisibility(View.VISIBLE);
                            addToCart.setVisibility(View.GONE);
                            qty.setText(task.getQuantity());
                        } else {
                            qtnBox.setVisibility(View.GONE);
                            addToCart.setVisibility(View.VISIBLE);
                        }
                    } else {
                        qtnBox.setVisibility(View.GONE);
                        addToCart.setVisibility(View.VISIBLE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        SaveTask st = new SaveTask();
        st.execute();
    }

    private String getCartDataTasks() {
        class GetTasks extends AsyncTask<Void, Void, List<ProductDataTask>> {
            @Override
            protected List<ProductDataTask> doInBackground(Void... voids) {
                List<ProductDataTask> taskList = DatabaseClient
                        .getInstance(context)
                        .getAppDatabase()
                        .taskDao()
                        .getAll();
                return taskList;
            }

            @Override
            protected void onPostExecute(List<ProductDataTask> tasks) {
                super.onPostExecute(tasks);
                if (tasks.size() == 0) {
                } else {
                    for (int i = 0; i < 1; i++) {
                        vendorId = tasks.get(i).getVendorId();
                    }

                }
            }
        }

        GetTasks gt = new GetTasks();
        gt.execute();
        return vendorId;
    }

    public void alertDialog(String msg) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.restrication_dailog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        TextView tvMsgId = dialog.findViewById(R.id.tvMsgId);
        Button btnOkId = dialog.findViewById(R.id.btnOkId);
        tvMsgId.setText(msg);
        btnOkId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        dialog.show();
    }

    public interface WeekendProductOnClickListener {
        void weeKendonClick(View view, int position, ProductData productData);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvProductDesc, tv_productPrice, tv_productName;
        ImageView id_userpic;
        LinearLayout rlAddToCart, quntityende;
        AppCompatImageView increment, decrement;
        AppCompatTextView quntity, tvoutOfStock;

        AppCompatButton btn_send;

        public MyViewHolder(View view) {
            super(view);
            tvoutOfStock = view.findViewById(R.id.tvoutOfStockId);
            btn_send = view.findViewById(R.id.btn_send);
            tv_productPrice = view.findViewById(R.id.tv_productPriceId);
            tv_productName = view.findViewById(R.id.tv_productNameId);
            tvProductDesc = view.findViewById(R.id.tvProductCatID);
            id_userpic = view.findViewById(R.id.id_userpic);
            rlAddToCart = view.findViewById(R.id.rlAddToCartId);
            quntityende = view.findViewById(R.id.quntityende);
            increment = view.findViewById(R.id.increment);
            decrement = view.findViewById(R.id.decrement);
            quntity = view.findViewById(R.id.quntity);
            id_userpic.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            productOnClickListener.weeKendonClick(v, getAdapterPosition(), weekendList.get(getAdapterPosition()));
        }
    }
}