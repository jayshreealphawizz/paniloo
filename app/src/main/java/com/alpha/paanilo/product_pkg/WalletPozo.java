package com.alpha.paanilo.product_pkg;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class WalletPozo {

   /* @SerializedName("status")
    @Expose
    private Integer status;

    @SerializedName("error_code")
    @Expose
    private Integer error_code;

    public String getWallet() {
        return wallet;
    }

    public void setWallet(String wallet) {
        this.wallet = wallet;
    }

    @SerializedName("error_line")
    @Expose
    private Integer error_line;

    @SerializedName("wallet")
    @Expose
    private String wallet;

    public Integer getError_code() {
        return error_code;
    }

    public void setError_code(Integer error_code) {
        this.error_code = error_code;
    }

    public Integer getError_line() {
        return error_line;
    }

    public void setError_line(Integer error_line) {
        this.error_line = error_line;
    }

    public ArrayList<Walletdata> getData() {
        return data;
    }

    public void setData(ArrayList<Walletdata> data) {
        this.data = data;
    }

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private ArrayList<Walletdata> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public class Walletdata {

        @SerializedName("id")
        @Expose
        private String id;

        @SerializedName("user_id")
        @Expose
        private String user_id;

        @SerializedName("amount")
        @Expose
        private String amount;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getSign() {
            return sign;
        }

        public void setSign(String sign) {
            this.sign = sign;
        }

        public String getWallet_status() {
            return wallet_status;
        }

        public void setWallet_status(String wallet_status) {
            this.wallet_status = wallet_status;
        }

        public String getCreate_dt() {
            return create_dt;
        }

        public void setCreate_dt(String create_dt) {
            this.create_dt = create_dt;
        }

        @SerializedName("sign")
        @Expose
        private String sign;

        @SerializedName("wallet_status")
        @Expose
        private String wallet_status;

        @SerializedName("create_dt")
        @Expose
        private String create_dt;
    }*/

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("error_code")
    @Expose
    private Integer errorCode;
    @SerializedName("error_line")
    @Expose
    private Integer errorLine;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("wallet_balance")
    @Expose
    private String walletBalance;
    @SerializedName("data")
    @Expose
    private List<WalletGetDatum> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public Integer getErrorLine() {
        return errorLine;
    }

    public void setErrorLine(Integer errorLine) {
        this.errorLine = errorLine;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getWalletBalance() {
        return walletBalance;
    }

    public void setWalletBalance(String walletBalance) {
        this.walletBalance = walletBalance;
    }

    public List<WalletGetDatum> getData() {
        return data;
    }

    public void setData(List<WalletGetDatum> data) {
        this.data = data;
    }
}

