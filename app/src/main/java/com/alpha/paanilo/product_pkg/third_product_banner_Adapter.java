package com.alpha.paanilo.product_pkg;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.alpha.paanilo.R;
import com.alpha.paanilo.model.banner_pkg.BannerInfo;
import com.alpha.paanilo.retrofit.ApiClient;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.List;

public class third_product_banner_Adapter extends PagerAdapter {
        private Context mContext;
        List<BannerInfo> imageList;

    public third_product_banner_Adapter(Context context, List<BannerInfo> imageList) {
        mContext = context;
        this.imageList = imageList;
    }

        @Override
        public Object instantiateItem(ViewGroup collection, int position) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.product_banner_item, collection, false);
            RoundedImageView ivSlideId = layout.findViewById(R.id.ivSlideId);

                Glide.with(mContext)
                        .load(ApiClient.BANNER_URL+imageList.get(position).getProductUrl())
                        .placeholder(R.drawable.placeholder)
                        .fitCenter()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .dontTransform()
                        .into(ivSlideId);

            collection.addView(layout);
            return layout;
        }


        @Override
        public void destroyItem(ViewGroup collection, int position, Object view) {
            collection.removeView((View) view);
        }

        @Override
        public int getCount() {
            return imageList == null ? 0 : imageList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }
    }