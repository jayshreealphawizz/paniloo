package com.alpha.paanilo.product_pkg;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.alpha.paanilo.R;
import com.alpha.paanilo.model.subcatpozo.SubCatDatum;
import com.alpha.paanilo.model.subcatpozo.SubCategoryPozo;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.utility.CheckNetwork;
import com.google.gson.Gson;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewAllSubCategoryActivity extends AppCompatActivity implements View.OnClickListener {

    private AppCompatTextView tvNotFound;
    AppCompatImageView iv_back;
    RecyclerView rv_city;
    View id_toolbar;
    AppCompatTextView tv_title, tv_nodata;
    private ProgressDialog pd;
    private List<SubCatDatum> subcategorydatumList;
    private Explor_SubCat_Adapter explorSubCatAdapter;
    private SwipeRefreshLayout srlCategory;
    String catName, catID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_all_category);

        catID =  getIntent().getStringExtra("catID");
        catName = getIntent().getStringExtra("catName");

        init();

        srlCategory.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (CheckNetwork.isNetAvailable(getApplicationContext())) {
                    getSubCategory(catID);
                } else {
                    Toast.makeText(getApplicationContext(), "Check Network Connection", Toast.LENGTH_LONG).show();
                }
                srlCategory.setRefreshing(false);
            }
        });
    }

    private void init() {
        id_toolbar = findViewById(R.id.id_toolbar);
        iv_back = id_toolbar.findViewById(R.id.id_back);
        tv_title = id_toolbar.findViewById(R.id.tv_tittle);
        iv_back.setOnClickListener(this);
        rv_city = findViewById(R.id.id_rv);
        tvNotFound = findViewById(R.id.tvNotFoundId);
        srlCategory = findViewById(R.id.srlCategoryId);

        tv_title.setText(catName);

        pd = new ProgressDialog(ViewAllSubCategoryActivity.this, R.style.AppCompatAlertDialogStyle);


        GridLayoutManager gridLayoutManager1 = new GridLayoutManager(getApplicationContext(), 3);
        rv_city.setLayoutManager(gridLayoutManager1);


        if (CheckNetwork.isNetAvailable(getApplicationContext())) {
            getSubCategory(catID);
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.plz_check_your_intrenet_text), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.id_back:
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                break;
        }
    }

    public void getSubCategory(String catID) {
        System.out.println("cat id ===="+catID);
        pd.setMessage("Loading");
        pd.show();
        (ApiClient.getClient().getAllSubCategory(catID)).enqueue(new Callback<SubCategoryPozo>() {
            @Override
            public void onResponse(Call<SubCategoryPozo> call, Response<SubCategoryPozo> response) {
                pd.dismiss();
                if (response.isSuccessful()) {
                    try {

                        Log.e("TAG", "all sub category : " + new Gson().toJson(response.body()));

                        SubCategoryPozo categoryPozo = response.body();
                        if (categoryPozo.getStatus()) {
                            subcategorydatumList = categoryPozo.getData();
                            explorSubCatAdapter = new Explor_SubCat_Adapter(getApplicationContext(),"ViewAll");
                            rv_city.setAdapter(explorSubCatAdapter);
                            explorSubCatAdapter.addCategoryList(subcategorydatumList);
                        } else {
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<SubCategoryPozo> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(getApplicationContext(), getString(R.string.server_errors), Toast.LENGTH_SHORT).show();
            }
        });
    }

}
