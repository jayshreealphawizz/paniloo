package com.alpha.paanilo.product_pkg;

public class WalletModel {
    String transactionhtitle;
    String transactionhdate;

    public WalletModel(String transactionhtitle, String transactionhdate) {
        this.transactionhtitle = transactionhtitle;
        this.transactionhdate = transactionhdate;
    }

    public String getTransactionhtitle() {
        return transactionhtitle;
    }

    public void setTransactionhtitle(String transactionhtitle) {
        this.transactionhtitle = transactionhtitle;
    }

    public String getTransactionhdate() {
        return transactionhdate;
    }

    public void setTransactionhdate(String transactionhdate) {
        this.transactionhdate = transactionhdate;
    }
}
