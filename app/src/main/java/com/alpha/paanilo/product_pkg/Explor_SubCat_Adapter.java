package com.alpha.paanilo.product_pkg;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.alpha.paanilo.R;
import com.alpha.paanilo.allproduct_pkg.All_Product_Activity;
import com.alpha.paanilo.model.subcatpozo.SubCatDatum;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.utility.AppSession;
import com.alpha.paanilo.utility.Constants;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

public class Explor_SubCat_Adapter extends RecyclerView.Adapter<Explor_SubCat_Adapter.MyViewHolder> {
    List<SubCatDatum> categorydatumList;
    Context context;
    private LayoutInflater mInflater;
    String category_comes;
    Constants constants;
    String VENDOR_ID;

    public Explor_SubCat_Adapter(Context applicationContext, String come) {
        context = applicationContext;
        category_comes = come;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView id_tv_title;
        ImageView id_userpic;

        public MyViewHolder(View view) {
            super(view);
            id_tv_title = view.findViewById(R.id.id_tv_title);
            id_userpic = view.findViewById(R.id.id_userpic);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_fragment_explore_cat_item, parent, false);
        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        final SubCatDatum datum = categorydatumList.get(position);


        Glide.with(context)
                .load(ApiClient.SUB_CATEGORY_URL + datum.getSubcat_img())
                .placeholder(R.drawable.placeholder)
                .fitCenter()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .dontTransform()
                .into(holder.id_userpic);


        holder.id_tv_title.setText(datum.getSubcat_name());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                constants = new Constants(context);
                VENDOR_ID = constants.getHomeVendrorID();

                System.out.println("sub cat vendore is====="+VENDOR_ID);

                if(datum.getSubcatchild())
                {
                    AppSession.setStringPreferences(context, "catID", datum.getCat_id());
                    AppSession.setStringPreferences(context, "catName", datum.getCat_name());
                    context.startActivity(new Intent(context, ViewAllSubCategoryChildActivity.class)
                            .putExtra("catID", datum.getSub_cat_id())
                            .putExtra("catName", datum.getSubcat_name())
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));

                }
                else {

                    VENDOR_ID = constants.getHomeVendrorID();

                    System.out.println("explor cat vendore is====="+VENDOR_ID);

                    if(VENDOR_ID.equals("") || VENDOR_ID.isEmpty()) {
                        Toast.makeText(context, "Please select stall first", Toast.LENGTH_SHORT).show();
                    }
                    else{
                        AppSession.setStringPreferences(context, "catID", datum.getCat_id());
                        AppSession.setStringPreferences(context, "catName", datum.getCat_name());
                        context.startActivity(new Intent(context, All_Product_Activity.class)
                                .putExtra("catID", datum.getCat_id())
                                .putExtra("catName", datum.getCat_name())
                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                    }
                }

            }
        });

    }

    public void addCategoryList(List<SubCatDatum> categorydatumList) {
        this.categorydatumList = categorydatumList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {

        int cate_size = categorydatumList.size();

        if (category_comes.equals("ViewAll")) {
            return categorydatumList == null ? 0 : cate_size;
        }
        else if (cate_size >= 3) {
            return categorydatumList == null ? 0 : 3;
        }
        else if (cate_size == 3) {
            return categorydatumList == null ? 0 : 3;

        } else if (cate_size == 2) {
            return categorydatumList == null ? 0 : 2;
        }
        else  if (cate_size == 1){
            return categorydatumList == null ? 0 : 1;
        }
        else{
            return categorydatumList == null ? 0 : 0;
        }
    }
}