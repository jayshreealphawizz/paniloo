package com.alpha.paanilo.product_pkg;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.alpha.paanilo.R;
import com.alpha.paanilo.model.categoryPkg.CategoryPozo;
import com.alpha.paanilo.model.categoryPkg.Datum;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.utility.CheckNetwork;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewAllCategoryActivity extends AppCompatActivity implements View.OnClickListener {

    private AppCompatTextView tvNotFound;
    AppCompatImageView iv_back;
    RecyclerView rv_city;
    View id_toolbar;
    AppCompatTextView tv_title, tv_nodata;
    private ProgressDialog pd;
    private List<Datum> categorydatumList;
    private Explor_Cat_Adapter explor_cat_adapter;
    private SwipeRefreshLayout srlCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_all_category);
        init();

        srlCategory.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (CheckNetwork.isNetAvailable(getApplicationContext())) {
                    getCategory();
                } else {
                    Toast.makeText(getApplicationContext(), "Check Network Connection", Toast.LENGTH_LONG).show();
                }
                srlCategory.setRefreshing(false);
            }
        });
    }

    private void init() {
        id_toolbar = findViewById(R.id.id_toolbar);
        iv_back = id_toolbar.findViewById(R.id.id_back);
        tv_title = id_toolbar.findViewById(R.id.tv_tittle);
        iv_back.setOnClickListener(this);
        rv_city = findViewById(R.id.id_rv);
        tvNotFound = findViewById(R.id.tvNotFoundId);
        srlCategory = findViewById(R.id.srlCategoryId);

        tv_title.setText(getString(R.string.category));

        pd = new ProgressDialog(ViewAllCategoryActivity.this, R.style.AppCompatAlertDialogStyle);


        GridLayoutManager gridLayoutManager1 = new GridLayoutManager(getApplicationContext(), 3);
        rv_city.setLayoutManager(gridLayoutManager1);


        if (CheckNetwork.isNetAvailable(getApplicationContext())) {
            getCategory();
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.plz_check_your_intrenet_text), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.id_back:
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                break;
        }
    }

    public void getCategory() {
        pd.setMessage("Loading");
        pd.show();
        (ApiClient.getClient().allCategories()).enqueue(new Callback<CategoryPozo>() {
            @Override
            public void onResponse(Call<CategoryPozo> call, Response<CategoryPozo> response) {
                pd.dismiss();
                if (response.isSuccessful()) {
                    try {
                        CategoryPozo categoryPozo = response.body();
                        if (categoryPozo.getStatus()) {
                            categorydatumList = categoryPozo.getData();
                            explor_cat_adapter = new Explor_Cat_Adapter(getApplicationContext(),"ViewAll");
                            rv_city.setAdapter(explor_cat_adapter);
                            explor_cat_adapter.addCategoryList(categorydatumList);
                        } else {
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }
            @Override
            public void onFailure(Call<CategoryPozo> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(getApplicationContext(), getString(R.string.server_errors), Toast.LENGTH_SHORT).show();
            }
        });
    }

}
