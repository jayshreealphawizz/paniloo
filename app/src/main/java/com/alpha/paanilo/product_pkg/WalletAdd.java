package com.alpha.paanilo.product_pkg;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WalletAdd {

    @SerializedName("wallet")
    @Expose
    private Integer wallet;

    public Integer getWallet() {
        return wallet;
    }

    public void setWallet(Integer wallet) {
        this.wallet = wallet;
    }

}
