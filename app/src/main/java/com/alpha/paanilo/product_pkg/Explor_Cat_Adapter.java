package com.alpha.paanilo.product_pkg;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.alpha.paanilo.expandable.CategorySectionMainActivity;
import com.alpha.paanilo.nearby_stall_pkg.All_NearBy_StallActivity;
import com.alpha.paanilo.utility.AppSession;
import com.alpha.paanilo.utility.Constants;
import com.bumptech.glide.Glide;
import com.alpha.paanilo.R;
import com.alpha.paanilo.model.categoryPkg.Datum;
import com.alpha.paanilo.retrofit.ApiClient;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

public class Explor_Cat_Adapter extends RecyclerView.Adapter<Explor_Cat_Adapter.MyViewHolder> {
    List<Datum> categorydatumList;
    Context context;
    private LayoutInflater mInflater;
    String category_comes;
    Constants constants ;
    String VENDOR_ID;


    public Explor_Cat_Adapter(Context applicationContext, String come) {
        context = applicationContext;
        category_comes = come;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView id_tv_title;
        ImageView id_userpic;

        public MyViewHolder(View view) {
            super(view);
            id_tv_title = view.findViewById(R.id.id_tv_title);
            id_userpic = view.findViewById(R.id.id_userpic);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_fragment_explore_cat_item, parent, false);
        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        constants = new Constants(context);
        final Datum datum = categorydatumList.get(position);

        Glide.with(context)
                .load(ApiClient.CATEGORY_URL + datum.getImage())
                .placeholder(R.drawable.placeholder)
                .fitCenter()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .dontTransform()
                .into(holder.id_userpic);


        holder.id_tv_title.setText(datum.getTitle());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(datum.isSubcat())
                {
//                    AppSession.setStringPreferences(context, "catID", datum.getId());
//                    AppSession.setStringPreferences(context, "catName", datum.getTitle());
//                    context.startActivity(new Intent(context, ViewAllSubCategoryActivity.class)
//                            .putExtra("catID", datum.getId())
//                            .putExtra("catName", datum.getTitle())
//                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));

                    AppSession.setStringPreferences(context, "catID", datum.getId());
                    AppSession.setStringPreferences(context, "catName", datum.getTitle());
                    context.startActivity(new Intent(context, CategorySectionMainActivity.class)
                            .putExtra("catID", datum.getId())
                            .putExtra("catName", datum.getTitle())
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));

                }
                else {

                    Intent intent = new Intent(context, All_NearBy_StallActivity.class);
                    intent.putExtra("comes_from","category");
                    intent.putExtra("subcatid", "");
                    intent.putExtra("catid", datum.getId());
                    intent.putExtra("subcatchildid","");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);

//
//                    VENDOR_ID = constants.getHomeVendrorID();
//                    System.out.println("explor cat vendore is====="+VENDOR_ID);
//
//                    if(VENDOR_ID.equals("") || VENDOR_ID.isEmpty()) {
//                        Toast.makeText(context, "Please select stall first", Toast.LENGTH_SHORT).show();
//                    }
//                    else{
//                        AppSession.setStringPreferences(context, "catID", datum.getId());
//                        AppSession.setStringPreferences(context, "catName", datum.getTitle());
//                        context.startActivity(new Intent(context, All_Product_Activity.class)
//                                .putExtra("catID", datum.getId())
//                                .putExtra("catName", datum.getTitle())
//                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
//                    }
                }
            }
        });

    }

    public void addCategoryList(List<Datum> categorydatumList) {
        this.categorydatumList = categorydatumList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {

        int cate_size = categorydatumList.size();
        return categorydatumList == null ? 0 : cate_size;

//        if (category_comes.equals("ViewAll")) {
//            return categorydatumList == null ? 0 : cate_size;
//        }
//        else if (cate_size >= 3) {
//            return categorydatumList == null ? 0 : 3;
//        }
//        else if (cate_size == 3) {
//            return categorydatumList == null ? 0 : 3;
//
//        } else if (cate_size == 2) {
//            return categorydatumList == null ? 0 : 2;
//        }
//        else  if (cate_size == 1){
//            return categorydatumList == null ? 0 : 1;
//        }
//        else{
//            return categorydatumList == null ? 0 : 0;
//        }
    }
}