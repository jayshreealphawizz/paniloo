package com.alpha.paanilo.product_pkg;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.alpha.paanilo.R;
import com.alpha.paanilo.model.productByCategory.ProductByCategoryPozo;
import com.alpha.paanilo.model.productByCategory.ProductData;
import com.alpha.paanilo.product_detail_pkg.Product_details_Activity;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.utility.AppSession;
import com.alpha.paanilo.utility.CheckNetwork;
import com.alpha.paanilo.utility.Constants;
import com.google.gson.JsonObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewAllWeekendProductActivity extends AppCompatActivity implements View.OnClickListener, Weekend_Adapter.WeekendProductOnClickListener {

    private AppCompatTextView tvNotFound;
    AppCompatImageView iv_back;
    RecyclerView rv_city;
    View id_toolbar;
    AppCompatTextView tv_title, tv_nodata;
    private ProgressDialog pd;
    private List<ProductData> weekendList;
    private Weekend_Adapter adapter;
    private Constants constants;
    private SwipeRefreshLayout srlCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_all_category);
        init();
        srlCategory.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (CheckNetwork.isNetAvailable(getApplicationContext())) {
                    getWeekendProduct();
                } else {
                    Toast.makeText(getApplicationContext(), "Check Network Connection", Toast.LENGTH_LONG).show();
                }
                srlCategory.setRefreshing(false);
            }
        });

    }

    private void init() {
        constants = new Constants(getApplicationContext());
        id_toolbar = findViewById(R.id.id_toolbar);
        iv_back = id_toolbar.findViewById(R.id.id_back);
        tv_title = id_toolbar.findViewById(R.id.tv_tittle);
        iv_back.setOnClickListener(this);
        rv_city = findViewById(R.id.id_rv);
        tvNotFound = findViewById(R.id.tvNotFoundId);
        srlCategory = findViewById(R.id.srlCategoryId);

        tv_title.setText(getString(R.string.weekendpromo));

        pd = new ProgressDialog(ViewAllWeekendProductActivity.this, R.style.AppCompatAlertDialogStyle);


        GridLayoutManager gridLayoutManager1 = new GridLayoutManager(getApplicationContext(), 2);
        rv_city.setLayoutManager(gridLayoutManager1);

        adapter = new Weekend_Adapter(getApplicationContext(), "viewall", this);
        rv_city.setAdapter(adapter);


        if (CheckNetwork.isNetAvailable(getApplicationContext())) {
            getWeekendProduct();
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.plz_check_your_intrenet_text), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.id_back:
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                break;
        }
    }


    public void getWeekendProduct() {
        pd.setMessage("Loading");
        pd.show();
        JsonObject jsonObject = new JsonObject();
        try {
            jsonObject.addProperty("farmer_id", constants.getVendror());
        } catch (Exception e) {
            e.printStackTrace();
        }
        (ApiClient.getClient().weekendProduct(jsonObject)).enqueue(new Callback<ProductByCategoryPozo>() {
            @Override
            public void onResponse(Call<ProductByCategoryPozo> call, Response<ProductByCategoryPozo> response) {
                pd.dismiss();
                if (response.isSuccessful()) {
                    try {
                        ProductByCategoryPozo productByCategoryPozo = response.body();
                        if (productByCategoryPozo.getStatus()) {
                            weekendList = productByCategoryPozo.getData();
                            adapter.addWeekendProductList(weekendList);
                        } else {
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<ProductByCategoryPozo> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(getApplicationContext(), getString(R.string.server_errors), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void weeKendonClick(View view, int position, ProductData productData) {
        switch (view.getId()) {
            case R.id.id_userpic:
                AppSession.setStringPreferences(getApplicationContext(), "entryFlag", "weekendProduct");
                AppSession.setStringPreferences(getApplicationContext(), "productID", productData.getProductId());
                startActivity(new Intent(getApplicationContext(), Product_details_Activity.class)
                        .putExtra("productID", productData.getProductId())
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                finish();
                break;
        }
    }
}
