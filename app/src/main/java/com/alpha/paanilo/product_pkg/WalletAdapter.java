package com.alpha.paanilo.product_pkg;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.alpha.paanilo.R;

import java.util.List;

import retrofit2.Callback;


public class WalletAdapter extends RecyclerView.Adapter<WalletAdapter.MyViewHolder> {
    List<WalletGetDatum> arraylist;
    Context context;
    //        private final WalletClickListener walletClickListener;
    public WalletAdapter(Context context, List<WalletGetDatum> arraylist) {
        this.context = context;
        this.arraylist = arraylist;
    }

    @NonNull
    @Override
    public WalletAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_walletadapter, parent, false);
        MyViewHolder viewHolder = new MyViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull WalletAdapter.MyViewHolder holder, final int position) {
        WalletGetDatum modelclass = arraylist.get(position);

//        if (position == 0) {
        holder.tvTitelName.setText(modelclass.getWalletStatus());
        if (modelclass.getWalletStatus().equalsIgnoreCase("Free Bottle")) {
            holder.tvPrice.setText("Qty : " + modelclass.getAmount());
            holder.ivSign.setVisibility(View.GONE);
        } else {
            holder.tvPrice.setText("Rs." + modelclass.getAmount());
            holder.tvTitelName.setText(modelclass.getWalletStatus());
        }

        holder.tvDate.setText(modelclass.getCreateDt());
//        holder.tvTime.setText("02:12");

        holder.tvPrice.setTextColor(Color.parseColor("#16A085"));
        holder.ivImage.setImageResource(R.drawable.ic_walltopicon);


        if (modelclass.getSign().equals("+")) {
            holder.tvPrice.setTextColor(Color.parseColor("#16A085"));
            holder.ivSign.setImageResource(R.drawable.ic_plushistory);
        } else {
            holder.ivSign.setImageResource(R.drawable.ic_minushistory);
            holder.tvPrice.setTextColor(Color.parseColor("#E96E60"));
        }

//        } else if (position == 1) {
//            holder.tvTitelName.setText("Withdraw Money");
//            holder.tvDate.setText("05 July 2020");
//            holder.tvTime.setText("02:12");
//            holder.tvPrice.setText("Rs.05");
//            holder.tvPrice.setTextColor(Color.parseColor("#E96E60"));
//            holder.ivImage.setImageResource(R.drawable.ic_walletmoney);
//            holder.ivSign.setImageResource(R.drawable.ic_minushistory);
//        } else if (position == 2) {
//            holder.tvTitelName.setText("Send to other user");
//            holder.tvDate.setText("02 July 2020");
//            holder.tvTime.setText("02:12");
//            holder.tvPrice.setText("Rs.25");
//            holder.tvPrice.setTextColor(Color.parseColor("#E96E60"));
//            holder.ivImage.setImageResource(R.drawable.ic_walletusericon);
//            holder.ivSign.setImageResource(R.drawable.ic_minushistory);
//        } else if (position == 3) {
//            holder.tvTitelName.setText("E-Wallet Top Up");
//            holder.tvDate.setText("12 July 2020");
//            holder.tvTime.setText("02:12");
//            holder.tvPrice.setText("Rs.12");
//            holder.tvPrice.setTextColor(Color.parseColor("#16A085"));
//            holder.ivImage.setImageResource(R.drawable.ic_walltopicon);
//            holder.ivSign.setImageResource(R.drawable.ic_plushistory);
//        }

//        holder.carditem.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                walletClickListener.walletClickListener(v, position);
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return arraylist.size();
    }


//    public interface WalletClickListener {
//        void walletClickListener(View v, int position);
//    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitelName, tvDate, tvTime, tvPrice;
        ImageView ivImage, ivSign;
        CardView carditem;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvTitelName = itemView.findViewById(R.id.tvTitelName);
            tvDate = itemView.findViewById(R.id.tvDate);

            tvTime = itemView.findViewById(R.id.tvTime);
            tvPrice = itemView.findViewById(R.id.tvPrice);

            ivImage = itemView.findViewById(R.id.ivImage);
            ivSign = itemView.findViewById(R.id.ivSign);

            carditem = itemView.findViewById(R.id.carditem);
        }
    }
}
