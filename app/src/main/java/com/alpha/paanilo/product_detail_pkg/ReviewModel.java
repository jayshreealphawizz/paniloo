package com.alpha.paanilo.product_detail_pkg;

import com.alpha.paanilo.model.singleProductPkg.Product;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ReviewModel {
   /* @SerializedName("status")
    @Expose
    private Boolean status;


    @SerializedName("message")
    @Expose
    private String message;



    @SerializedName("Products")
    @Expose
    private List<ProductReview> products = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ProductReview> getProducts() {
        return products;
    }

    public void setProducts(List<ProductReview> products) {
        this.products = products;
    }*/

//    @SerializedName("status")
//    @Expose
//    private Boolean status;
//    @SerializedName("message")
//    @Expose
//    private String message;
//    @SerializedName("average_rating")
//    @Expose
//    private String averageRating;
//    @SerializedName("Products")
//    @Expose
//    private List<ProductReview> products = null;
//
//    public Boolean getStatus() {
//        return status;
//    }
//
//    public void setStatus(Boolean status) {
//        this.status = status;
//    }
//
//    public String getMessage() {
//        return message;
//    }
//
//    public void setMessage(String message) {
//        this.message = message;
//    }
//
//    public String getAverageRating() {
//        return averageRating;
//    }
//
//    public void setAverageRating(String averageRating) {
//        this.averageRating = averageRating;
//    }
//
//    public List<ProductReview> getProducts() {
//        return products;
//    }
//
//    public void setProducts(List<ProductReview> products) {
//        this.products = products;
//    }


    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("average_rating")
    @Expose
    private String averageRating;
    @SerializedName("Products")
    @Expose
    private List<ProductReview> products = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(String averageRating) {
        this.averageRating = averageRating;
    }

    public List<ProductReview> getProducts() {
        return products;
    }

    public void setProducts(List<ProductReview> products) {
        this.products = products;
    }

}
