package com.alpha.paanilo.product_detail_pkg;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataCouponcount {
    @SerializedName("wallet_amount")
    @Expose
    private Integer walletAmount;



    @SerializedName("total_coupon")
    @Expose
    private Integer total_coupon;

    public Integer getTotal_coupon() {
        return total_coupon;
    }

    public void setTotal_coupon(Integer total_coupon) {
        this.total_coupon = total_coupon;
    }

    public Integer getWalletAmount() {
        return walletAmount;
    }

    public void setWalletAmount(Integer walletAmount) {
        this.walletAmount = walletAmount;
    }


}
