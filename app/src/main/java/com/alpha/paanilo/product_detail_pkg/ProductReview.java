package com.alpha.paanilo.product_detail_pkg;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductReview {
//    @SerializedName("id")
//    @Expose
//    private String id;
//    @SerializedName("product_id")
//    @Expose
//    private String productId;
//    @SerializedName("user_id")
//    @Expose
//    private String userId;
//    @SerializedName("rating")
//    @Expose
//    private Integer rating;
//    @SerializedName("comment")
//    @Expose
//    private String comment;
//    @SerializedName("approved")
//    @Expose
//    private String approved;
//    @SerializedName("time")
//    @Expose
//    private String time;
//    @SerializedName("name")
//    @Expose
//    private String name;
//    @SerializedName("vendor_id")
//    @Expose
//    private String vendorId;
//    @SerializedName("water_quality")
//    @Expose
//    private String waterQuality;
//    @SerializedName("bottel_quality")
//    @Expose
//    private String bottelQuality;
//    @SerializedName("deliver_on_time")
//    @Expose
//    private String deliverOnTime;
//
//
//
//    @SerializedName("user_fullname")
//    @Expose
//    private String userFullname;
//
//
//
//
//
//    public String getId() {
//        return id;
//    }
//
//    public void setId(String id) {
//        this.id = id;
//    }
//
//    public String getProductId() {
//        return productId;
//    }
//
//    public void setProductId(String productId) {
//        this.productId = productId;
//    }
//
//    public String getUserId() {
//        return userId;
//    }
//
//    public void setUserId(String userId) {
//        this.userId = userId;
//    }
//
//    public Integer getRating() {
//        return rating;
//    }
//
//    public void setRating(Integer rating) {
//        this.rating = rating;
//    }
//
//    public String getComment() {
//        return comment;
//    }
//
//    public void setComment(String comment) {
//        this.comment = comment;
//    }
//
//    public String getApproved() {
//        return approved;
//    }
//
//    public void setApproved(String approved) {
//        this.approved = approved;
//    }
//
//    public String getTime() {
//        return time;
//    }
//
//    public void setTime(String time) {
//        this.time = time;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public String getVendorId() {
//        return vendorId;
//    }
//
//    public void setVendorId(String vendorId) {
//        this.vendorId = vendorId;
//    }
//
//    public String getWaterQuality() {
//        return waterQuality;
//    }
//
//    public void setWaterQuality(String waterQuality) {
//        this.waterQuality = waterQuality;
//    }
//
//    public String getBottelQuality() {
//        return bottelQuality;
//    }
//
//    public void setBottelQuality(String bottelQuality) {
//        this.bottelQuality = bottelQuality;
//    }
//
//    public String getDeliverOnTime() {
//        return deliverOnTime;
//    }
//
//    public void setDeliverOnTime(String deliverOnTime) {
//        this.deliverOnTime = deliverOnTime;
//    }
//
//    public String getUserFullname() {
//        return userFullname;
//    }
//
//    public void setUserFullname(String userFullname) {
//        this.userFullname = userFullname;
//    }

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("vendor_id")
    @Expose
    private String vendorId;
    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("rating")
    @Expose
    private Integer rating;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("approved")
    @Expose
    private String approved;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("water_quality")
    @Expose
    private String waterQuality;
    @SerializedName("bottel_quality")
    @Expose
    private String bottelQuality;
    @SerializedName("deliver_on_time")
    @Expose
    private String deliverOnTime;
    @SerializedName("user_fullname")
    @Expose
    private String userFullname;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getApproved() {
        return approved;
    }

    public void setApproved(String approved) {
        this.approved = approved;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWaterQuality() {
        return waterQuality;
    }

    public void setWaterQuality(String waterQuality) {
        this.waterQuality = waterQuality;
    }

    public String getBottelQuality() {
        return bottelQuality;
    }

    public void setBottelQuality(String bottelQuality) {
        this.bottelQuality = bottelQuality;
    }

    public String getDeliverOnTime() {
        return deliverOnTime;
    }

    public void setDeliverOnTime(String deliverOnTime) {
        this.deliverOnTime = deliverOnTime;
    }

    public String getUserFullname() {
        return userFullname;
    }

    public void setUserFullname(String userFullname) {
        this.userFullname = userFullname;
    }
}
