package com.alpha.paanilo.product_detail_pkg;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;

import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alpha.paanilo.R;
import com.alpha.paanilo.model.singleProductPkg.SingleProductPozoshop;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.utility.CheckNetwork;
import com.alpha.paanilo.utility.Constants;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductDescription extends AppCompatActivity implements View.OnClickListener, ReviewshowAdapter.ReviewshowOnclick {

    TextView tvShopName, tvStallDistance, tvRatingPoint, tvReviewsDes, tvAddReview;
    RatingBar Rating;
    CardView AddToReviewText;
    String productID, Distance;
    TextView tvWater;
    String UserId;
    String getId, getUserId;
    private List<ProductReview> productReviews;
    private Dialog dialog;
    private RatingBar rbid, rbIdmain, ratingWater, ratingBottle, ratingDeliver;
    private AppCompatEditText AedreviewId;
    private ProgressBar pbProductDetail, pbLoginId;
    private RecyclerView RereviewId;
    private ReviewshowAdapter reviewshowAdapter;
    private String userid, getRating, waterRating, bottleRating, deliveronTime, productid;
    private ProgressDialog pd;
    private Constants constants;


    View toolbar;
    RelativeLayout rl_whole;
    RecyclerView rv_orderitem;
    AppCompatTextView tv_tittle, tvPromotionDisouct;
    AppCompatImageView id_notification, id_back, iv_add_address;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_description);


        toolbar = findViewById(R.id.id_toolbar);
        tv_tittle = toolbar.findViewById(R.id.tv_tittle);
        id_notification = toolbar.findViewById(R.id.id_notification);
        id_back = toolbar.findViewById(R.id.id_back);

        id_notification.setVisibility(View.GONE);


        tv_tittle.setText("Add Reviews");


        productReviews = new ArrayList<>();
        pd = new ProgressDialog(ProductDescription.this, R.style.AppCompatAlertDialogStyle);
        tvShopName = findViewById(R.id.tvShopName);

        RereviewId = findViewById(R.id.RereviewId);


        constants = new Constants(getApplicationContext());

        productID = getIntent().getStringExtra("userid");
        Distance = getIntent().getStringExtra("dis");
        productid = getIntent().getStringExtra("pro");


        UserId = constants.getUserID();

        Log.e("Rahul ", "PPPPp::: " + UserId);

//        RereviewId.setVisibility(View.VISIBLE);

        Log.e("Rahul ", "PPPPp::: " + productID);

        Log.e("Rahul ", "pro::: " + productid);

        tvStallDistance = findViewById(R.id.tvStallDistance);
        tvRatingPoint = findViewById(R.id.tvRatingPoint);
        tvReviewsDes = findViewById(R.id.tvReviewsDes);
        Rating = findViewById(R.id.Rating);

        tvWater = findViewById(R.id.tvWater);

        AedreviewId = findViewById(R.id.edreviewId);
        ratingWater = findViewById(R.id.ratingWater);
        ratingBottle = findViewById(R.id.ratingBottle);
        ratingDeliver = findViewById(R.id.ratingDeliver);

        AddToReviewText = findViewById(R.id.AddToReviewText);

        ratingWater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int noofstars = ratingWater.getNumStars();
                float getrating = ratingWater.getRating();
                tvWater.setText("Rating: " + getrating + "/" + noofstars);
                // Toast.makeText(ProductDescription.this, "", Toast.LENGTH_SHORT).show();
            }
        });

        ratingBottle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int noofstars = ratingWater.getNumStars();
                float getrating = ratingWater.getRating();
                tvWater.setText("Rating: " + getrating + "/" + noofstars);
                // Toast.makeText(ProductDescription.this, "", Toast.LENGTH_SHORT).show();
            }
        });

        ratingDeliver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int noofstars = ratingWater.getNumStars();
                float getrating = ratingWater.getRating();
                tvWater.setText("Rating: " + getrating + "/" + noofstars);
                // Toast.makeText(ProductDescription.this, "", Toast.LENGTH_SHORT).show();
            }
        });


        AddToReviewText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (ratingWater.getRating() == 0  ) {
//                    ratingWater.requestFocus();
//                    Toast.makeText(getApplicationContext(), "Select Rating", Toast.LENGTH_SHORT).show();
//                }
                /* else if (ratingBottle.getRating() == 0) {
                    ratingBottle.requestFocus();
                    Toast.makeText(getApplicationContext(), "Select Rating B", Toast.LENGTH_SHORT).show();
                } else if (ratingDeliver.getRating() == 0) {
                    ratingDeliver.requestFocus();
                    Toast.makeText(getApplicationContext(), "Select Rating D", Toast.LENGTH_SHORT).show();
                }*/
                if (AedreviewId.getText().toString().isEmpty()) {
                    AedreviewId.requestFocus();
                    AedreviewId.setError("Can't be empty");
                } else {
                    if (CheckNetwork.isNetAvailable(getApplicationContext())) {
                        waterRating = String.valueOf(ratingWater.getRating());
                        bottleRating = String.valueOf(ratingBottle.getRating());
                        deliveronTime = String.valueOf(ratingDeliver.getRating());

                        Log.e("rahul::productid: ", "uuuuu " + productid);
                        Log.e("rahul::: ", "UserId " + UserId);

                        if (productid.equals("") || productid.equals("null") || productid == null) {

                        } else {
                            addReview(productid, UserId, AedreviewId.getText().toString(), waterRating, bottleRating, deliveronTime);
                        }
                    } else {
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.network), Toast.LENGTH_LONG).show();
                    }
                }
            }
        });


        tvAddReview = findViewById(R.id.tvAddReview);

        pd = new ProgressDialog(ProductDescription.this, R.style.AppCompatAlertDialogStyle);


//        LinearLayoutManager layoutManager = new LinearLayoutManager(ProductDescription.this, RecyclerView.VERTICAL, false);
//        RereviewId.setLayoutManager(layoutManager);
//        reviewshowAdapter = new ReviewshowAdapter(ProductDescription.this, this);
//        reviewshowAdapter.allproductreview(productReviews);
//        RereviewId.setAdapter(reviewshowAdapter);
//        reviewshowAdapter.notifyDataSetChanged();


        Collections.reverse(productReviews);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        RereviewId.setHasFixedSize(true);
        RereviewId.setLayoutManager(layoutManager);
        reviewshowAdapter = new ReviewshowAdapter(ProductDescription.this, this);
        reviewshowAdapter.allproductreview(productReviews);
        RereviewId.setAdapter(reviewshowAdapter);
        reviewshowAdapter.notifyDataSetChanged();


//        Toast.makeText(this, "nnnn ", Toast.LENGTH_SHORT).show();

        tvAddReview.setOnClickListener(this);
        id_back.setOnClickListener(this);
        getProductDetailshp(productID);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvAddReview:
//                submitReviews();
                break;
            case R.id.id_back:
                onBackPressed();
                finish();
//                submitReviews();
                break;


        }
    }


    public void deleteCheckReivewDialoge(final String reviewid, final String getUserId) {
        dialog = new Dialog(ProductDescription.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.delete_review);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        AppCompatButton btnCancel = dialog.findViewById(R.id.btnCancelId);
        AppCompatButton btnyes = dialog.findViewById(R.id.btnyesId);
        btnyes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckNetwork.isNetAvailable(getApplicationContext())) {
                    dialog.dismiss();
//                    deleteReview(productID, productid);

                    Log.e("yes ", "getId " + reviewid);
                    Log.e("yes ", "getuesrid " + getUserId);
//                    Log.e("yes ", "UserId " + UserId);


                    deleteReview(reviewid, getUserId);

                } else {
//                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.network), Toast.LENGTH_LONG).show();
                }
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    private void addReview(String productId, String userid, String getreview, String getwaterRating, String getBottleRating, String deliverontime) {
        pd.setMessage("Loading");
        pd.show();

        (ApiClient.getClient().addReview(productId, userid, getreview, getwaterRating, getBottleRating, deliverontime)).enqueue(new Callback<RatingModle>() {
            @Override
            public void onResponse(Call<RatingModle> call, Response<RatingModle> response) {
                Log.e("response ", "response add review " + new Gson().toJson(response.body()));
                pd.dismiss();

                if (response.isSuccessful()) {
                    pd.dismiss();
                    RatingModle getLoginModle = response.body();
                    Log.e("rahul ::::", ":::rahul:::" + getLoginModle.toString());

                    if (getLoginModle.getStatus()) {
                        Log.e("rahul ::::", ":::mohan:::");
                        Toast.makeText(getApplicationContext(), "Review Added Successfully", Toast.LENGTH_LONG).show();

                        if (CheckNetwork.isNetAvailable(getApplicationContext())) {
                            getReview();

                        } else {
//                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.network), Toast.LENGTH_LONG).show();
                        }
                    }


                } else {
                    if (response.code() == 400) {
//                        pbProductDetail.setVisibility(View.GONE);
                        if (!response.isSuccessful()) {
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(response.errorBody().string());
                                String message = jsonObject.getString("message");
                                dialog.dismiss();
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<RatingModle> call, Throwable t) {
//                pbProductDetail.setVisibility(View.GONE);
                pd.dismiss();
            }
        });
    }


    private void getReview() {
//        pbLoginId.setVisibility(View.VISIBLE);
        pd.setMessage("Loading");
        pd.show();

        Log.e("rahul raj", "rahul::" + productID);

        (ApiClient.getClient().gerReviewProductId(productID)).enqueue(new Callback<ReviewModel>() {
            @Override
            public void onResponse(Call<ReviewModel> call, Response<ReviewModel> response) {
                pd.dismiss();
                Log.e("TAG", "product review response  review data : " + new Gson().toJson(response.body()));
                if (response.isSuccessful()) {
                    ReviewModel productlist = response.body();
                    if (productlist.getStatus()) {
                        Rating.setRating(Float.parseFloat("" + productlist.getAverageRating()));
                        tvRatingPoint.setText("" + productlist.getAverageRating());

                        Log.e("Rating :: ", "UserID :::: " + productlist.getAverageRating());

                        getId = productlist.getProducts().get(0).getId();

                        getUserId = productlist.getProducts().get(0).getUserId();
                        Log.e("bama", "bana::: ");

                        productReviews = productlist.getProducts();
//                        reviewshowAdapter.allproductreview(productReviews);
//                        RereviewId.setVisibility(View.VISIBLE);
//                        reviewshowAdapter.notifyDataSetChanged();

                        reviewshowAdapter.allproductreview(productReviews);
                        RereviewId.setAdapter(reviewshowAdapter);
                        RereviewId.setVisibility(View.VISIBLE);
                        reviewshowAdapter.notifyDataSetChanged();

                        AedreviewId.setText("");
                        ratingWater.setRating(Float.parseFloat("0"));
                        ratingBottle.setRating(Float.parseFloat("0"));
                        ratingDeliver.setRating(Float.parseFloat("0"));

                    } else {
                        reviewshowAdapter.notifyDataSetChanged();
                        reviewshowAdapter.allproductreview(productReviews);
                        RereviewId.setAdapter(reviewshowAdapter);
                        RereviewId.setVisibility(View.GONE);
                        reviewshowAdapter.notifyDataSetChanged();
//                        reviewshowAdapter.notifyDataSetChanged();
//                        RereviewId.setVisibility(View.GONE);
//                        Toast.makeText(ProductDescription.this, "MMMMMM", Toast.LENGTH_SHORT).show();

                    }
                } else {

//                     Toast.makeText(ProductDescription.this, "yyyyyy", Toast.LENGTH_SHORT).show();
                    reviewshowAdapter.notifyDataSetChanged();
                    reviewshowAdapter.allproductreview(productReviews);
                    RereviewId.setAdapter(reviewshowAdapter);
                    RereviewId.setVisibility(View.GONE);
                    reviewshowAdapter.notifyDataSetChanged();
//                    RereviewId.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<ReviewModel> call, Throwable t) {
//                pbLoginId.setVisibility(View.GONE);
                Log.e("message ", "message ::: " + t.getMessage());
                pd.dismiss();
            }
        });
    }


    private void getProductDetailshp(final String productID) {
        pd.setMessage("Loading");
        pd.show();

        (ApiClient.getClient().getProductDetailshop(productID)).enqueue(new Callback<SingleProductPozoshop>() {
            @Override
            public void onResponse(Call<SingleProductPozoshop> call, Response<SingleProductPozoshop> response) {
                pd.dismiss();
                Log.e("response : ", "response Details::::::: " + new Gson().toJson(response.body()));
                if (response.isSuccessful()) {
                    try {
                        SingleProductPozoshop singleProduct = response.body();
                        if (singleProduct.getStatus()) {

                            Log.e("response : ", "response DetailsAuccess ");
                            tvReviewsDes.setText("(" + singleProduct.getData().getTotalReview() + " Reviews)");
                            tvShopName.setText("" + singleProduct.getData().getStore_name());

                            String valuuu = String.format("%.2f", Double.valueOf(Distance));
                            System.out.println("==========" + valuuu);
                            tvStallDistance.setText("(" + valuuu + " Kms)");

                            reviewshowAdapter.allproductreview(productReviews);
//                            RereviewId.setVisibility(View.VISIBLE);

                            if (singleProduct.getData().getRating().equals("")) {
                                // Toast.makeText(Product_details_Activity.this, "Bana Sa" +singleProduct.getData().getRating(), Toast.LENGTH_SHORT).show();

                                tvRatingPoint.setText("0.0");
                                Rating.setRating(Float.parseFloat("0"));
                            } else {
                                //  Toast.makeText(Product_details_Activity.this, "Bana 1111Sa" +singleProduct.getData().getRating(), Toast.LENGTH_SHORT).show();

                                tvRatingPoint.setText(singleProduct.getData().getRating());
                                Rating.setRating(Float.parseFloat(singleProduct.getData().getRating()));
                            }


//                            tvReviewsDes.setText("Reviews " + singleProduct.getData().getTotalReview());

                            getReview();
                            System.out.println("current imaege =====" + singleProduct.getData().getUserImage());

//                            Log.e("User id ::::::", "User id ::::" + getUserId());

                       /*     if (product.getUserImage().equals("")) {
                            } else {
                                Glide.with(Product_details_Activity.this)
                                        .load(ApiClient.VENDOR_PROFILE_URL + product.getUserImage())
                                        .placeholder(R.drawable.placeholder)
                                        .fitCenter()
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .dontTransform()
                                        .into(productimageID);
                            }*/


                        } else {
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                }

            }

            @Override
            public void onFailure(Call<SingleProductPozoshop> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(ProductDescription.this, getString(R.string.server_errors), Toast.LENGTH_SHORT).show();

            }
        });

    }


    @Override
    public void onClick(View view, int position, ProductReview productReview) {
        switch (view.getId()) {

            case R.id.ivDeleteReviewId:

                deleteCheckReivewDialoge(productReview.getId(), productReview.getUserId());

                //  Toast.makeText(this, "now click ", Toast.LENGTH_SHORT).show();
                break;
        }

    }


    private void deleteReview(String productId, String userid) {
        pd.setMessage("Loading");
        pd.show();
//        apiServices.removeReview(productId, userid)
        Log.e("response : ", "request Delete review::::::: " + productId + "--" + userid);


        (ApiClient.getClient().removeReview(productId, userid)).enqueue(new Callback<DeleteRatingModle>() {
            @Override
            public void onResponse(Call<DeleteRatingModle> call, Response<DeleteRatingModle> response) {
                Log.e("response : ", "response Delete review::::::: " + new Gson().toJson(response.body()));

                pd.dismiss();
                if (response.isSuccessful()) {
                    pd.dismiss();

                    Log.e("rahu:::::: ", "bbbb");
                    //    pbProductDetail.setVisibility(View.GONE);
                    DeleteRatingModle getLoginModle = response.body();
                    if (getLoginModle.getStatus()) {
//                        Toast.makeText(getApplicationContext(), getLoginModle.getMessage(), Toast.LENGTH_LONG).show();
                        // dialog.dismiss();
                        Log.e("rahu:::::: ", "hjhjhjhjhj");
                        if (CheckNetwork.isNetAvailable(getApplicationContext())) {
                            Log.e("rahu:::::: ", "yes ");
                            getReview();
                        } else {
//                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.network), Toast.LENGTH_LONG).show();
                        }
                    }
                } else {
                    if (response.code() == 400) {
                        //   pbProductDetail.setVisibility(View.GONE);
                        if (!response.isSuccessful()) {
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(response.errorBody().string());
                                String message = jsonObject.getString("message");
                                dialog.dismiss();
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<DeleteRatingModle> call, Throwable t) {
                // pbProductDetail.setVisibility(View.GONE);
                // dialog.dismiss();
                pd.dismiss();
            }
        });
    }


}