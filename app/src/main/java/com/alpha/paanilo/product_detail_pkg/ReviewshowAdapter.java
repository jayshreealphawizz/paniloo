package com.alpha.paanilo.product_detail_pkg;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.alpha.paanilo.R;
import com.alpha.paanilo.utility.Constants;


import java.util.List;


public class ReviewshowAdapter extends RecyclerView.Adapter<ReviewshowAdapter.ViewHolder> {
    private final Context context;
    private final ReviewshowOnclick reviewshowOnclick;
    String UserId;
    private List<ProductReview> productReviews;
    private String user_id;
    private Constants constants;

    public ReviewshowAdapter(Context context, ReviewshowOnclick reviewshowOnclick) {
        this.context = context;
        this.reviewshowOnclick = reviewshowOnclick;
//        user_id = AppSession.getStringPreferences(context, Constants.USEREId);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.product_review_row, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.usernameProductreviewId.setText(productReviews.get(position).getUserFullname());
        holder.commentProductreviewId.setText(productReviews.get(position).getComment());
//        holder.commentProductreviewId.setText(productReviews.get(position).getComment());
        holder.dateProductreviewId.setText(productReviews.get(position).getTime());
//        holder.rbHomeId.setRating(productReviews.get(position).getRating());


        Log.e("IDGET ", "User " + productReviews.get(position).getUserId());
        Log.e("IDGET ", "Prodcut  " + productReviews.get(position).getProductId());
        Log.e("IDGET ", "Prodcutid  " + productReviews.get(position).getId());


        if (productReviews.get(position).getWaterQuality().equals("")) {

        } else {
            holder.rbWater.setRating(Float.parseFloat("" + productReviews.get(position).getWaterQuality()));
        }


        if (productReviews.get(position).getBottelQuality().equals("")) {
        } else {
            holder.rbBottle.setRating(Float.parseFloat("" + productReviews.get(position).getBottelQuality()));
        }

        if (productReviews.get(position).getDeliverOnTime().equals("")) {
        } else {
            holder.rbDeliver.setRating(Float.parseFloat("" + productReviews.get(position).getDeliverOnTime()));
        }


        constants = new Constants(context);

        UserId = constants.getUserID();

        Log.e("ram ::: ", "raam ::: " + UserId);

        if (UserId.equalsIgnoreCase(productReviews.get(position).getUserId())) {
            holder.ivDeleteReview.setVisibility(View.VISIBLE);
        } else {
            holder.ivDeleteReview.setVisibility(View.GONE);
        }

    }

    public int getItemCount() {

        return productReviews == null ? 0 : productReviews.size();


    }

    public void allproductreview(List<ProductReview> productReviews) {
        this.productReviews = productReviews;
        notifyDataSetChanged();
    }

    public interface ReviewshowOnclick {
        void onClick(View view, int position, ProductReview productReview);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        AppCompatTextView usernameProductreviewId, commentProductreviewId, dateProductreviewId;
        RatingBar rbHomeId, rbWater, rbBottle, rbDeliver;
        AppCompatImageView ivDeleteReview;

        public ViewHolder(View itemView) {
            super(itemView);
            ivDeleteReview = itemView.findViewById(R.id.ivDeleteReviewId);
            dateProductreviewId = itemView.findViewById(R.id.dateProductreviewId);
            usernameProductreviewId = itemView.findViewById(R.id.usernameProductreviewId);
            commentProductreviewId = itemView.findViewById(R.id.commentProductreviewId);

            rbHomeId = itemView.findViewById(R.id.rbHomeId);

            rbWater = itemView.findViewById(R.id.rbWater);
            rbBottle = itemView.findViewById(R.id.rbBottle);
            rbDeliver = itemView.findViewById(R.id.rbDeliver);

            ivDeleteReview.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            reviewshowOnclick.onClick(v, getAdapterPosition(), productReviews.get(getAdapterPosition()));
        }
    }
}
