package com.alpha.paanilo.product_detail_pkg;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.alpha.paanilo.HomeMainActivity;
import com.alpha.paanilo.R;
import com.alpha.paanilo.Roomdatabase.DatabaseClient;
import com.alpha.paanilo.Roomdatabase.ProductDataTask;

import com.alpha.paanilo.cart_pkg.OrderConfirmation_Activity;
import com.alpha.paanilo.model.AddCartCustomModel;
import com.alpha.paanilo.model.HistoryData;

import com.alpha.paanilo.model.singleProductPkg.OfflineOnlineCheckModel;
import com.alpha.paanilo.model.singleProductPkg.Product;
import com.alpha.paanilo.model.singleProductPkg.SingleProductPozo;
import com.alpha.paanilo.model.singleProductPkg.SingleProductPozoshop;

import com.alpha.paanilo.retrofit.ApiClient;

import com.alpha.paanilo.utility.AppSession;
import com.alpha.paanilo.utility.CheckNetwork;
import com.alpha.paanilo.utility.Constants;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Product_details_Activity extends AppCompatActivity implements View.OnClickListener {
    public ArrayList<HistoryData> historyList;
    String TAG = getClass().getSimpleName();
    View v;


    View toolbar;
    RelativeLayout rl_whole;
    RecyclerView rv_orderitem;
    AppCompatTextView tv_tittle, tvPromotionDisouct;
    AppCompatImageView id_notification, id_back, iv_add_address;


    LinearLayout btn_addtocart;
    AppCompatImageView ivBackSearchId, productimageID;
    ImageView ivBottleImage;
    String productID, entryFlag, vendorId;
    ImageView id_userpic;
    LinearLayout rlAddToCart, quntityende;

    AppCompatImageView increment1, decrement1;
    AppCompatTextView quntity1;

    AppCompatImageView increment, decrement;
    AppCompatTextView quntity;
    AppCompatButton btn_send;
    Product product;
    TextView tvCartCountTag, tvProductNameOutOfStock;
    int inStock;
    LinearLayout ll_addtocartbox;

    AppCompatImageView id_stallpic;
    LinearLayout ll_wholebox;

    LinearLayout lllayout;
    String Distance;
    RatingBar Rating;
    TextView tvName, tvStallDistance, tvDes, tvRatingPoint, tvReviewsDes;
    String pid;
    AppCompatButton tvAddReviewbtn;
    private TextView tvProductName, tvProductPrice, tvPrdDesc, tvVendorName;
    private ProgressDialog pd;
    private int numtest, subtotal, mainprice;
    private Constants constants;
    TextView text ;
    View layout ;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_details);
        LayoutInflater inflater = getLayoutInflater();
        layout = inflater.inflate(R.layout.toast_layout,
                (ViewGroup) findViewById(R.id.toast_layout_root));

        init();
//        clearCart();
        Log.e(TAG, "onCreate: ");
//        Intent intent = getIntent();
    }

    private void init() {

        toolbar = findViewById(R.id.id_toolbar);
        tv_tittle = toolbar.findViewById(R.id.tv_tittle);
        id_notification = toolbar.findViewById(R.id.id_notification);
        id_back = toolbar.findViewById(R.id.id_back);

        id_notification.setVisibility(View.GONE);


        tv_tittle.setText("Product Details");
        constants = new Constants(getApplicationContext());
        tvProductNameOutOfStock = findViewById(R.id.tvProductNameOutOfStockId);
        tvVendorName = findViewById(R.id.tvVendorNameId);
        tvCartCountTag = findViewById(R.id.tvCartCountTagId);
        tvProductName = findViewById(R.id.tvProductNameId);
        tvProductPrice = findViewById(R.id.tvProductPriceId);
        tvPrdDesc = findViewById(R.id.tvPrdDescId);
        productimageID = findViewById(R.id.productimageID);
        tvAddReviewbtn = findViewById(R.id.tvAddReviewbtn);
        ivBottleImage = findViewById(R.id.ivBottleImage);

        ll_addtocartbox = findViewById(R.id.ll_addtocartbox);

        btn_addtocart = findViewById(R.id.btn_addtocart);
        ivBackSearchId = findViewById(R.id.ivBackSearchId);

        btn_send = findViewById(R.id.btn_send);
        rlAddToCart = findViewById(R.id.rlAddToCartId);
        quntityende = findViewById(R.id.quntityende);
        increment = findViewById(R.id.increment);
        decrement = findViewById(R.id.decrement);
        quntity = findViewById(R.id.quntity);

        increment1 = findViewById(R.id.increment1);
        decrement1 = findViewById(R.id.decrement1);
        quntity1 = findViewById(R.id.quntity1);

        tvName = findViewById(R.id.tvName);
        tvStallDistance = findViewById(R.id.tvStallDistance);
        tvDes = findViewById(R.id.tvDes);
        tvRatingPoint = findViewById(R.id.tvRatingPoint);
        tvReviewsDes = findViewById(R.id.tvReviewsDes);

        Rating = findViewById(R.id.Rating);
        lllayout = findViewById(R.id.lllayout);
        text = (TextView) layout.findViewById(R.id.text);
        decrement1.setOnClickListener(this);
        increment1.setOnClickListener(this);
        id_back.setOnClickListener(this);
        decrement.setOnClickListener(this);
        increment.setOnClickListener(this);
        rlAddToCart.setOnClickListener(this);
        btn_send.setOnClickListener(this);
        btn_addtocart.setOnClickListener(this);
        ivBackSearchId.setOnClickListener(this);
        tvAddReviewbtn.setOnClickListener(this);
        lllayout.setOnClickListener(this);

        historyList = new ArrayList<>();

        pd = new ProgressDialog(Product_details_Activity.this, R.style.AppCompatAlertDialogStyle);


//        if (productID.equals("")) {
        productID = getIntent().getStringExtra("userid");

        Distance = getIntent().getStringExtra("distance");


        Log.e("Distance:::", " Distance:::" + Distance);

        Log.e("productID:::", " productID:::" + productID);

        entryFlag = getIntent().getStringExtra("entryFlag");
        entryFlag = AppSession.getStringPreferences(getApplicationContext(), "entryFlag");
//        productID = AppSession.getStringPreferences(getApplicationContext(), "productID");

        Log.e("productID:HHHH::", " productID:BB::" + productID);


        if (CheckNetwork.isNetAvailable(getApplicationContext())) {

            getProductDetailshp(productID);

        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.plz_check_your_intrenet_text), Toast.LENGTH_SHORT).show();
        }

        //checkProductAvailbility();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_addtocart:
                Log.e("Userid", "productID Id:::;" + productID);
                Log.e("Userid", "vendorId Id:::;" + vendorId);

                Intent intent = new Intent(Product_details_Activity.this, OrderConfirmation_Activity.class);
                intent.putExtra("userid", productID);
                intent.putExtra("entry", "detail");
                startActivity(intent);
                finish();

                break;
            case R.id.ivBackSearchId:
//                Toast.makeText(this, "now click", Toast.LENGTH_SHORT).show();
                onBackPressed();
//               finish();
                break;
            case R.id.btn_send:
//                Toast.makeText(this, "now click", Toast.LENGTH_SHORT).show();
//                Log.e("VVVV::: ", "Vendro ::" + quntity);
//                Log.e("VVVV:1111111:: ", "Vendro ::" + numtest);
                checkvendoronlineoffline(productID);

//fdfdf



                break;
            case R.id.rlAddToCartId:
                break;
            case R.id.quntityende:
                break;
            case R.id.id_back:

                Intent intent1 = new Intent(Product_details_Activity.this, HomeMainActivity.class);
                startActivity(intent1);

//                onBackPressed();


                break;
            case R.id.increment:
                numtest = Integer.parseInt(quntity.getText().toString());
                numtest = numtest + 1;
                if (numtest <= inStock) {
                    quntity.setText("" + numtest);
                    tvCartCountTag.setText("Buy Now"/* + numtest*/);
                    subtotal = numtest * mainprice;
                    updateTask();
                } else {
                    increment.setOnClickListener(null);
                    Toast.makeText(getApplicationContext(), "You can select a maximum " + inStock + " at one time", Toast.LENGTH_LONG).show();
                }

                break;
            case R.id.decrement:
                increment.setOnClickListener(this);
                numtest = Integer.parseInt(quntity.getText().toString());
                if (numtest > 1) {
                    numtest = numtest - 1;
                    quntity.setText(numtest + "");
                    tvCartCountTag.setText("Buy Now"/* + numtest*/);
                    subtotal = numtest * mainprice;
                    //totalmain.setText("" + subtotal);
                    updateTask();
                } else {
                    deleteTask();
                }
                break;
            case R.id.tvAddReviewbtn:
//                Toast.makeText(this, "new page one", Toast.LENGTH_SHORT).show();
                Intent inten = new Intent(Product_details_Activity.this, ProductDescription.class);
                inten.putExtra("userid", productID);
                inten.putExtra("dis", Distance);
                inten.putExtra("pro", pid);

                startActivity(inten);

//                Log.e("productID:RRR::", "productID:::" + productID);
//                Log.e("Distance:RRR::", "Distance:::" + Distance);

                Log.e("Distance:RRR::", "kjkjk:::" + pid);


                break;


        }
    }


    private void getProductDetailshp(final String farmerid) {
        pd.setMessage("Loading");
        pd.show();

        (ApiClient.getClient().getProductDetailshop(farmerid)).enqueue(new Callback<SingleProductPozoshop>() {
            @Override
            public void onResponse(Call<SingleProductPozoshop> call, Response<SingleProductPozoshop> response) {
                pd.dismiss();
                Log.e("response : ", "response Details " + new Gson().toJson(response.body()));

                if (response.isSuccessful()) {
                    try {
                        SingleProductPozoshop singleProduct = response.body();
                        if (singleProduct.getStatus()) {

                            //Log.e("response : ", "response DetailsAuccess " + singleProduct.getData().getUserImage());

                            if (singleProduct.getData().getUserImage().equals("")) {

                            } else {

                                Glide.with(Product_details_Activity.this)
                                        .load(ApiClient.VENDOR_PROFILE_URL + singleProduct.getData().getUserImage())
                                        .placeholder(R.drawable.userimage)
                                        .into(productimageID);
                            }

                            if (singleProduct.getData().getRating().equals("")) {
                                // Toast.makeText(Product_details_Activity.this, "Bana Sa" +singleProduct.getData().getRating(), Toast.LENGTH_SHORT).show();

                                tvRatingPoint.setText("0.0");
                                Rating.setRating(Float.parseFloat("0"));
                            } else {
                                //  Toast.makeText(Product_details_Activity.this, "Bana 1111Sa" +singleProduct.getData().getRating(), Toast.LENGTH_SHORT).show();
                                tvRatingPoint.setText(singleProduct.getData().getRating());
                                Rating.setRating(Float.parseFloat(singleProduct.getData().getRating()));
                            }

                            tvReviewsDes.setText("(" + singleProduct.getData().getTotalReview() + " Reviews)");

                            pid = singleProduct.getData().getProductId();
//                            pid = productID ;

                            Log.e("pid : ", "response pid " + pid);

                            checkProductAvailbility();

                            if (singleProduct.getData().getProductId().isEmpty() || singleProduct.getData().getProductId().equals("") || singleProduct.getData().getProductId().equals("null")) {

                                ll_addtocartbox.setVisibility(View.GONE);
                            } else {
//                                Log.e("Rahul:::  ", "Rahul:::" + singleProduct.getData().getProductId());
                                //  Toast.makeText(Product_details_Activity.this, "NNNNNNN ", Toast.LENGTH_SHORT).show();
                                ll_addtocartbox.setVisibility(View.VISIBLE);

                                getProductDetail("" + singleProduct.getData().getProductId());

                            }

                            tvName.setText("" + singleProduct.getData().getStore_name());

                            String valuuu = String.format("%.2f", Double.valueOf(Distance));
//                            System.out.println("==========" + valuuu);
                            tvStallDistance.setText("(" + valuuu + " Kms)");

                            tvDes.setText(singleProduct.getData().getCurrentLocation());


                            System.out.println("current imaege =====" + singleProduct.getData().getUserImage());

//                            Rating.setRating(Float.valueOf(singleProduct.getData().getRating()).floatValue());

//                            tvRatingPoint.setText("" + singleProduct.getData().getRating());


                        } else {
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                }

            }

            @Override
            public void onFailure(Call<SingleProductPozoshop> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(Product_details_Activity.this, getString(R.string.server_errors), Toast.LENGTH_SHORT).show();

            }
        });

    }

    public void getProductDetail(final String productID) {
        pd.setMessage("Loading");
        pd.show();
        JsonObject jsonObject = new JsonObject();
        try {
            jsonObject.addProperty("product_id", productID);
        } catch (Exception e) {
            e.printStackTrace();
        }

        (ApiClient.getClient().getProductDetail(jsonObject)).enqueue(new Callback<SingleProductPozo>() {
            @Override
            public void onResponse(Call<SingleProductPozo> call, Response<SingleProductPozo> response) {
                pd.dismiss();
                Log.e("response : ", "response Single Product " + new Gson().toJson(response.body()));

                if (response.isSuccessful()) {
                    try {
                        SingleProductPozo singleProductPozo = response.body();
                        if (singleProductPozo.getStatus()) {
                            product = singleProductPozo.getProduct();


                            vendorId = product.getFarmerId();
                            Log.e("vendorId : ", "vendorId vendorId " + vendorId);

//Constants
                            constants.setVendor(vendorId);
//                            constants.setVendor(product.getFarmerId());
//ffd
//
//                            tvProductName.setText(product.getProductName());
                            tvProductPrice.setText(getString(R.string.rupee) + " " + Constants.getTotalWithSeparater(product.getPrice()));
                            tvPrdDesc.setText(product.getProductDescription());
                            tvVendorName.setText(product.getStoreName());
                            mainprice = Integer.parseInt(product.getPrice());
                            inStock = Integer.parseInt(product.getQuantity());

                            if (singleProductPozo.getProduct().getQuantity().equalsIgnoreCase("0")) {
                                tvProductNameOutOfStock.setVisibility(View.VISIBLE);
                                btn_send.setVisibility(View.GONE);
                                ll_addtocartbox.setVisibility(View.GONE);
                            } else {
                                tvProductNameOutOfStock.setVisibility(View.GONE);
                                btn_send.setVisibility(View.VISIBLE);
                                ll_addtocartbox.setVisibility(View.VISIBLE);
//                                Toast.makeText(Product_details_Activity.this, "visibkle", Toast.LENGTH_SHORT).show();
                            }
//                            Toast.makeText(Product_details_Activity.this, "now " + product.getProductImage(), Toast.LENGTH_SHORT).show();

                            Log.e("pro", "pro" + product.getProductImage());
//                            if (product.getProductImage().isEmpty()) {
//                            } else {
//                                String imgName = product.getProductImage().substring((product.getProductImage().lastIndexOf(",") + 1));
//
//                                Log.e("pro", "imgName" + imgName);
//
//                                Glide.with(getApplicationContext())
//                                        .load(ApiClient.CATEGORY_PRODUCT_URL + imgName)
//                                        .into(ivBottleImage);
////                                productimageID
//                            }


                        } else {
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<SingleProductPozo> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(getApplicationContext(), getString(R.string.server_errors), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void cartdialoge() {
        final Dialog dialog = new Dialog(Product_details_Activity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.clear_cart_dialoge);
        AppCompatButton no = dialog.findViewById(R.id.mbtnNoId);
        AppCompatButton mbtnYes = dialog.findViewById(R.id.mbtnYesId);

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        mbtnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearCart();
                dialog.dismiss();
                AppSession.setStringPreferences(Product_details_Activity.this, "final_vendorID", "");
            }
        });
        dialog.show();
    }

    private void clearCart() {
        class GetTasks extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... voids) {
                DatabaseClient.getInstance(Product_details_Activity.this)
                        .getAppDatabase()
                        .taskDao()
                        .delete();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
            }
        }
        GetTasks gt = new GetTasks();
        gt.execute();
    }

    private void saveTask() {
        class SaveTask extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... voids) {
                ProductDataTask task = new ProductDataTask();
                Log.e("pid:::::11::: ", "Product ID :::" + pid);

//                Log.e("pid:::::1111::: ", "Product ID :::" + productID);

                task.setProduct_id(pid);
                task.setProduct_name(product.getProductName());
                task.setProduct_description(product.getProductDescription());
                task.setProduct_image(product.getProductImage());
                task.setIn_stock(product.getQuantity());
                task.setPrice(product.getPrice());
                task.setVendorId(vendorId);
                task.setQuantity("1");
                task.setTotal(String.valueOf(product.getPrice()));
                DatabaseClient.getInstance(getApplicationContext()).getAppDatabase()
                        .taskDao()
                        .insert(task);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                quntityende.setVisibility(View.VISIBLE);
                rlAddToCart.setVisibility(View.GONE);

                getCartCount();
            }
        }
        SaveTask st = new SaveTask();
        st.execute();
    }


    private void updateTask() {
        class UpdateTask extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... voids) {
                DatabaseClient.getInstance(getApplicationContext()).getAppDatabase()
                        .taskDao()
                        .updatea(String.valueOf(subtotal), String.valueOf(numtest), pid);

                Log.e("bana:: ", "banasa::: " + subtotal + "  " + numtest + " " + " " + pid);

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
            }
        }

        UpdateTask ut = new UpdateTask();
        ut.execute();
    }

    private void deleteTask() {
        class DeleteTask extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... voids) {
                ProductDataTask task = new ProductDataTask();
                DatabaseClient.getInstance(getApplicationContext()).getAppDatabase()
                        .taskDao()
                        .deleteByUserId(pid);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
//                quntityende.setVisibility(View.GONE);
//                rlAddToCart.setVisibility(View.GONE);
                quntityende.setVisibility(View.GONE);
                rlAddToCart.setVisibility(View.VISIBLE);
            }
        }

        DeleteTask dt = new DeleteTask();
        dt.execute();

    }

    private void getTasks1() {
        class GetTasks extends AsyncTask<Void, Void, String> {
            @Override
            protected String doInBackground(Void... voids) {
                String taskList = DatabaseClient
                        .getInstance(getApplicationContext())
                        .getAppDatabase()
                        .taskDao()
                        .getCount();
                return taskList;
            }

            @Override
            protected void onPostExecute(String tasks) {
                super.onPostExecute(tasks);
                ///  mCallback.passData(tasks);
            }
        }

        GetTasks gt = new GetTasks();
        gt.execute();
    }

    private void checkProductAvailbility() {

        Log.e(TAG, "checkProductAvailbility: ");
        class SaveTask extends AsyncTask<Void, Void, ProductDataTask> {
            @Override
            protected ProductDataTask doInBackground(Void... voids) {
                ProductDataTask task = DatabaseClient
                        .getInstance(Product_details_Activity.this)
                        .getAppDatabase()
                        .taskDao()
                        .getproductid(pid);
                return task;
            }

            @Override
            protected void onPostExecute(ProductDataTask task) {
                super.onPostExecute(task);


                try {
                    if (task != null) {
                        Log.e(TAG, task.getQuantity() + "  onPostExecute: " + task.getProduct_name());

                        //Log.e(TAG, task.getQuantity()+"  onPostExecute: 111 "+task.getProduct_name() );
                        if (Integer.parseInt(task.getQuantity()) > 0) {
                            quntityende.setVisibility(View.VISIBLE);
                            rlAddToCart.setVisibility(View.GONE);
                            quntity.setText("" + task.getQuantity());
                            tvCartCountTag.setText("Buy Now" /*+ task.getQuantity()*/);

                            Log.e("task.getQuantity():: ", "task.getQuantity()" + task.getQuantity());
                            // totalmain.setText(task.getTotal());
                        } else {
                            quntityende.setVisibility(View.GONE);
                            rlAddToCart.setVisibility(View.VISIBLE);
                        }
                    } else {
                        quntityende.setVisibility(View.GONE);
                        rlAddToCart.setVisibility(View.VISIBLE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        SaveTask st = new SaveTask();
        st.execute();
    }

    private void getCartCount() {
        class GetTasks extends AsyncTask<Void, Void, String> {
            @Override
            protected String doInBackground(Void... voids) {
                String taskList = DatabaseClient
                        .getInstance(getApplicationContext())
                        .getAppDatabase()
                        .taskDao()
                        .getCount();
                return taskList;
            }

            @Override
            protected void onPostExecute(String tasks) {
                super.onPostExecute(tasks);
                AddCartCustomModel.getInstance().setCartCount("" + tasks);
            }
        }
        GetTasks gt = new GetTasks();
        gt.execute();
    }


    @Override
    public void onBackPressed() {
        finish();
       /* try {
            if (entryFlag.equalsIgnoreCase("catProduct")) {
                finish();
            } else if (entryFlag.equalsIgnoreCase("FilterProduct")) {
                Intent intent = new Intent(getApplicationContext(), Filter_All_Product_Activity.class);
                startActivity(intent);
                finish();
            } else if (entryFlag.equalsIgnoreCase("weekendProduct")) {
                Intent intent = new Intent(getApplicationContext(), ViewAllWeekendProductActivity.class);
                startActivity(intent);
                finish();
            } else if (entryFlag.equalsIgnoreCase("profile")) {
                //Intent intent = new Intent(getApplicationContext(), StallProfileActivity.class);
                //  startActivity(intent);
                finish();
            } else {
                Intent intent = new Intent();
                intent.putExtra("flag", "msg");
                setResult(2, intent);
                finish();//finishing activity
                //Refresh code
                AddRefreshModel.getInstance().setCartCount("" + "0");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    private String getCartDataTasks() {
        class GetTasks extends AsyncTask<Void, Void, List<ProductDataTask>> {
            @Override
            protected List<ProductDataTask> doInBackground(Void... voids) {
                List<ProductDataTask> taskList = DatabaseClient
                        .getInstance(getApplicationContext())
                        .getAppDatabase()
                        .taskDao()
                        .getAll();
                return taskList;
            }

            @Override
            protected void onPostExecute(List<ProductDataTask> tasks) {
                super.onPostExecute(tasks);
                if (tasks.size() == 0) {
                } else {
                    for (int i = 0; i < 1; i++) {
                        vendorId = tasks.get(i).getVendorId();
                    }

                }
            }
        }

        GetTasks gt = new GetTasks();
        gt.execute();
        return vendorId;
    }

    public void alertDialog(String msg) {
        final Dialog dialog = new Dialog(Product_details_Activity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.restrication_dailog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        TextView tvMsgId = dialog.findViewById(R.id.tvMsgId);
        Button btnOkId = dialog.findViewById(R.id.btnOkId);
        tvMsgId.setText(msg);
        btnOkId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        dialog.show();
    }

    @Override
    protected void onResume() {
//        getProductDetailshp(productID);
//        checkProductAvailbility();
//        getCartCount();
//        getTasks1();
//        Log.e("rah" ,"rahul "+productID);
        getProductDetailshp(productID);
        super.onResume();
    }


    private void checkvendoronlineoffline(final String userid) {
        pd.setMessage("Loading");
        pd.show();

        (ApiClient.getClient().checkvendoronlineoffline(userid)).enqueue(new Callback<OfflineOnlineCheckModel>() {
            @Override
            public void onResponse(Call<OfflineOnlineCheckModel> call, Response<OfflineOnlineCheckModel> response) {
                pd.dismiss();
                Log.e("response : ", "response Details " + new Gson().toJson(response.body()));
                OfflineOnlineCheckModel offlineOnlineCheckModel = response.body();

//                Toast.makeText(Product_details_Activity.this, ""+offlineOnlineCheckModel.getMessage(), Toast.LENGTH_SHORT).show();






                if (response.isSuccessful()) {

                    text.setText(""+offlineOnlineCheckModel.getMessage());

                    Toast toast = new Toast(getApplicationContext());
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(layout);
                    toast.show();



                    pd.dismiss();
                    if (offlineOnlineCheckModel.getStatus() == true) {
                        String FINALVENDORID = AppSession.getStringPreferences(getApplicationContext(), "final_vendorID");
                        System.out.println("taskList_size" + FINALVENDORID + "===Select any stall near===========" + vendorId);

//                Log.e(":FINALVENDORID:::", "FINALVENDORID::: :: ");

                        if (FINALVENDORID.isEmpty()) {
                            numtest = 1;
                            rlAddToCart.setVisibility(View.GONE);
                            subtotal = numtest * mainprice;
                            saveTask();
                        } else {
//                    if (FINALVENDORID.equals(vendorId)) {
                            numtest = 1;
                            rlAddToCart.setVisibility(View.GONE);
                            subtotal = numtest * mainprice;
                            saveTask();
//                    }

//                    else {
////                        cartdialoge();
//                    }

                        }
                    } else {
                        Toast.makeText(Product_details_Activity.this, ""+offlineOnlineCheckModel.getMessage(), Toast.LENGTH_SHORT).show();
                    }
//                    try {
//                        SingleProductPozoshop singleProduct = response.body();
//                        if (singleProduct.getStatus()) {
//
//                            //Log.e("response : ", "response DetailsAuccess " + singleProduct.getData().getUserImage());
//
//                            if (singleProduct.getData().getUserImage().equals("")) {
//
//                            } else {
//
//                                Glide.with(Product_details_Activity.this)
//                                        .load(ApiClient.VENDOR_PROFILE_URL + singleProduct.getData().getUserImage())
//                                        .placeholder(R.drawable.userimage)
//                                        .into(productimageID);
//                            }
//
//                            if (singleProduct.getData().getRating().equals("")) {
//                                // Toast.makeText(Product_details_Activity.this, "Bana Sa" +singleProduct.getData().getRating(), Toast.LENGTH_SHORT).show();
//
//                                tvRatingPoint.setText("0.0");
//                                Rating.setRating(Float.parseFloat("0"));
//                            } else {
//                                //  Toast.makeText(Product_details_Activity.this, "Bana 1111Sa" +singleProduct.getData().getRating(), Toast.LENGTH_SHORT).show();
//                                tvRatingPoint.setText(singleProduct.getData().getRating());
//                                Rating.setRating(Float.parseFloat(singleProduct.getData().getRating()));
//                            }
//
//                            tvReviewsDes.setText("(" + singleProduct.getData().getTotalReview() + " Reviews)");
//
//                            pid = singleProduct.getData().getProductId();
////                            pid = productID ;
//
//                            Log.e("pid : ", "response pid " + pid);
//
//                            checkProductAvailbility();
//
//                            if (singleProduct.getData().getProductId().isEmpty() || singleProduct.getData().getProductId().equals("") || singleProduct.getData().getProductId().equals("null")) {
//
//                                ll_addtocartbox.setVisibility(View.GONE);
//                            } else {
////                                Log.e("Rahul:::  ", "Rahul:::" + singleProduct.getData().getProductId());
//                                //  Toast.makeText(Product_details_Activity.this, "NNNNNNN ", Toast.LENGTH_SHORT).show();
//                                ll_addtocartbox.setVisibility(View.VISIBLE);
//
//                                getProductDetail("" + singleProduct.getData().getProductId());
//
//                            }
//
//                            tvName.setText("" + singleProduct.getData().getStore_name());
//
//                            String valuuu = String.format("%.2f", Double.valueOf(Distance));
////                            System.out.println("==========" + valuuu);
//                            tvStallDistance.setText("(" + valuuu + " Kms)");
//
//                            tvDes.setText(singleProduct.getData().getCurrentLocation());
//
//
//                            System.out.println("current imaege =====" + singleProduct.getData().getUserImage());
//
////                            Rating.setRating(Float.valueOf(singleProduct.getData().getRating()).floatValue());
//
////                            tvRatingPoint.setText("" + singleProduct.getData().getRating());
//
//
//                        } else {
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
                }

            }

            @Override
            public void onFailure(Call<OfflineOnlineCheckModel> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(Product_details_Activity.this, getString(R.string.server_errors), Toast.LENGTH_SHORT).show();

            }
        });

    }

//    private void clearCart() {
//        class GetTasks extends AsyncTask<Void, Void, Void> {
//            @Override
//            protected Void doInBackground(Void... voids) {
//                DatabaseClient.getInstance(Product_details_Activity.this)
//                        .getAppDatabase()
//                        .taskDao()
//                        .delete();
//                return null;
//            }
//
//            @Override
//            protected void onPostExecute(Void aVoid) {
//                super.onPostExecute(aVoid);
//
//            }
//        }
//        GetTasks gt = new GetTasks();
//        gt.execute();
//    }
}
