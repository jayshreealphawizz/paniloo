package com.alpha.paanilo.product_detail_pkg;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.alpha.paanilo.R;
import com.alpha.paanilo.Roomdatabase.DatabaseClient;
import com.alpha.paanilo.model.HistoryData;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

public class Product_details_Fragment extends Fragment {
    View v;
    public ArrayList<HistoryData> historyList;
    private ViewPager vpProductDetailsImages;
    private TabLayout tlProductDetailsTab;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.product_details, container, false);
//        vpProductDetailsImages = v.findViewById(R.id.vpProductDetailsImageslid);
//        tlProductDetailsTab = v.findViewById(R.id.tlProductDetailsTabId);

//        clearCart();
        historyList = new ArrayList<>();
//        getSliderImage();
        return v;
    }
//    private void getSliderImage() {
//        for (int i = 0; i < 2; i++) {
//            historyList.add(new HistoryData("hi"));
//            Product_details_banner_Adapter adapter = new Product_details_banner_Adapter(getContext(), historyList);
//            vpProductDetailsImages.setAdapter(adapter);
//            tlProductDetailsTab.setupWithViewPager(vpProductDetailsImages);
//        }
//    }



}
