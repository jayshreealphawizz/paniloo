package com.alpha.paanilo.product_detail_pkg;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CouponNumberModel {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private DataTotalCoupon data;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataTotalCoupon getData() {
        return data;
    }

    public void setData(DataTotalCoupon data) {
        this.data = data;
    }

}
