package com.alpha.paanilo.product_detail_pkg;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataTotalCoupon {
    @SerializedName("total_coupon")
    @Expose
    private Integer totalCoupon;

    public Integer getTotalCoupon() {
        return totalCoupon;
    }

    public void setTotalCoupon(Integer totalCoupon) {
        this.totalCoupon = totalCoupon;
    }

}
