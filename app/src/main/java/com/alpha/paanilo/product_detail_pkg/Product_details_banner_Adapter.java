package com.alpha.paanilo.product_detail_pkg;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.viewpager.widget.PagerAdapter;

import com.alpha.paanilo.R;
import com.alpha.paanilo.model.HistoryData;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.List;

public class Product_details_banner_Adapter extends PagerAdapter {
    private Context mContext;
    List<HistoryData> imageList;

    public Product_details_banner_Adapter(Context context, List<HistoryData> imageList) {
        mContext = context;
        this.imageList = imageList;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.product_detail_banner_item, collection, false);
        RoundedImageView ivSlideId = layout.findViewById(R.id.ivSlideId);
        collection.addView(layout);
        return layout;
    }


    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return imageList == null ? 0 : imageList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
}