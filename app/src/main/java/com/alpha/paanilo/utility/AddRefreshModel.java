package com.alpha.paanilo.utility;

public class AddRefreshModel {
    public interface OnCustomCartListener {
        void changeCartState();
    }
    private static AddRefreshModel mInstance;
    private OnCustomCartListener mListener;
    private String mCurrentCartCount;

    private AddRefreshModel() {
    }

    public static AddRefreshModel getInstance() {
        if (mInstance == null) {
            mInstance = new AddRefreshModel();
        }
        return mInstance;
    }

    public void setListener(OnCustomCartListener listener) {
        mListener = listener;
    }

    public void setCartCount(String state) {
        if (mListener != null) {
            mCurrentCartCount = state;
            notifyStateChange();
        }
    }

    public String getCartCount() {
        return mCurrentCartCount;
    }

    private void notifyStateChange() {
        mListener.changeCartState();
    }
}
