package com.alpha.paanilo.utility;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.widget.Toast;

import com.alpha.paanilo.model.categoryPkg.Datum;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Constants {

    public static final String PACKAGE_NAME = "waranguku";
    public static final String USER_VENDOR = "2";
    public static final String USER_CUSTOMER = "1";
    public static final String STATUS = "";
    public static ArrayList<Datum> AllCategory = new ArrayList<>();
    private static SharedPreferences pref;
    private static SharedPreferences.Editor editor;


    public static final void customToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    @SuppressLint("CommitPrefEdits")
    public Constants(Context context) {
        pref = context.getSharedPreferences(PACKAGE_NAME, Context.MODE_PRIVATE);
        editor = pref.edit();
    }


    public static boolean emailValidator(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static boolean isValidEmail(String target) {
        boolean flag;
        if (TextUtils.isEmpty(target)) {
            return true;
        } else {
            flag = emailValidator(target);
            if (flag) {
                // return flag;
            }
            return flag;
        }
        // return (!TextUtils.isEmpty(target) || Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public static boolean vehicleNumberValidator(String number) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[A-Za-z]{2}[ -][0-9]{1,2}(?: [A-Za-z])?(?: [A-Za-z]*)? [0-9]{4}$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(number);
        return matcher.matches();
    }


    public static boolean isValidVehicleNumber(String target) {
        boolean flag;
        if (TextUtils.isEmpty(target)) {
            return true;
        } else {
            flag = vehicleNumberValidator(target);
            if (flag) {
                // return flag;
            }
            return flag;
        }
        // return (!TextUtils.isEmpty(target) || Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }


    public static String currentDateAndTime() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String strDate = sdf.format(c.getTime());
        return strDate;
    }

    public static String changeTimeFormat(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm";
        String outputPattern = "dd-MM-yyy hh:mm";

        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }



    public String getaddLatitude() {
        return pref.getString("Latitudee", "");
    }

    public void setaddLatitude(String Latitudee) {
        editor.putString("Latitudee", Latitudee);
        editor.commit();
    }

    public String getaddLongitude() {
        return pref.getString("Longitudee", "");
    }

    public void setaddLongitude(String Longitudee) {
        editor.putString("Longitudee", Longitudee);
        editor.commit();
    }


    public String getLatitude() {
        return pref.getString("Latitude", "");
    }

    public void setLatitude(String Latitude) {
        editor.putString("Latitude", Latitude);
        editor.commit();
    }

    public String getLongitude() {
        return pref.getString("Longitude", "");
    }

    public void setLongitude(String Longitude) {
        editor.putString("Longitude", Longitude);
        editor.commit();
    }


    public String getCurrentLocation() {
        return pref.getString("CurrentLocation", "");
    }

    public void setCurrentLocation(String CurrentLocation) {
        editor.putString("CurrentLocation", CurrentLocation);
        editor.commit();
    }

    public String getLoginAs() {
        return pref.getString("LoginAs", "");
    }

    public void setLoginAs(String LoginAs) {
        editor.putString("LoginAs", LoginAs);
        editor.commit();
    }

    public String getIsLogin() {
        return pref.getString("isLogin", "0");
    }

    public void setIsLogin(String isLogin) {
        editor.putString("isLogin", isLogin);
        editor.commit();
    }


    public String getIsLoginStatus(String s) {
        return pref.getString("isLoginstatus", "0");
    }

    public void setIsLoginStatus(String isLoginStatus) {
        editor.putString("isLoginstatus", isLoginStatus);
        editor.commit();
    }


    public String getUserID() {
        return pref.getString("UserID", "0");
    }

    public void setUserID(String UserID) {
        editor.putString("UserID", UserID);
        editor.commit();
    }


    public String getCountno() {
        return pref.getString("CountNO", "0");
    }

    public void setCountno(String CountNO) {
        editor.putString("CountNO", CountNO);
        editor.commit();
    }





    public String getresponseUserID() {
        return pref.getString("UserIDRES", "0");
    }

    public void setresponseUserID(String UserIDRES) {
        editor.putString("UserIDRES", UserIDRES);
        editor.commit();
    }

    public String getUsername() {
        return pref.getString("Username", "0");
    }

    public void setUsername(String User) {
        editor.putString("Username", User);
        editor.commit();
    }

    public String getShopName() {
        return pref.getString("Shopname", "0");
    }

    public void setShopName(String Shopname) {
        editor.putString("Shopname", Shopname);
        editor.commit();
    }

    public String getProductName() {
        return pref.getString("ProductName", "0");
    }

    public void setProductName(String ProductName) {
        editor.putString("ProductName", ProductName);
        editor.commit();
    }

    public String getProductMeasurement() {
        return pref.getString("ProductMes", "0");
    }

    public void setProductMeasurement(String ProductMes) {
        editor.putString("ProductMes", ProductMes);
        editor.commit();
    }


    public String getProductPrice() {
        return pref.getString("ProductPrice", "0");
    }

    public void setProductPrice(String ProductPrice) {
        editor.putString("ProductPrice", ProductPrice);
        editor.commit();
    }


    public String getProductQuantity() {
        return pref.getString("ProductQuty", "0");
    }

    public void setProductQuantity(String ProductQuty) {
        editor.putString("ProductQuty", ProductQuty);
        editor.commit();
    }
    public String getProductDest() {
        return pref.getString("ProductDes", "0");
    }

    public void setProductDest(String ProductDes) {
        editor.putString("ProductDes", ProductDes);
        editor.commit();
    }


    public String getProductImage() {
        return pref.getString("ProductImage", "0");
    }

    public void setProductImage(String ProductImage) {
        editor.putString("ProductImage", ProductImage);
        editor.commit();
    }


    public String getProductAddId() {
        return pref.getString("ProductID", "0");
    }

    public void setProductAddId(String ProductID) {
        editor.putString("ProductID", ProductID);
        editor.commit();
    }
    public String getEmail() {
        return pref.getString("Email", "0");
    }

    public void setEmail(String Email) {
        editor.putString("Email", Email);
        editor.commit();
    }

    public String getCity() {
        return pref.getString("City", "Location");
    }

    public void setCity(String City) {
        editor.putString("City", City);
        editor.commit();
    }

    public String getCityAddress() {
        return pref.getString("CityAddress", "Location");
    }

    public void setCityAddress(String CityAddress) {
        editor.putString("CityAddress", CityAddress);
        editor.commit();
    }
    public String getDeliveyaddress() {
        return pref.getString("Deliveyaddress", "Deliveyaddress");
    }

    public void setDeliveyaddress(String Deliveyaddress) {
        editor.putString("Deliveyaddress", Deliveyaddress);
        editor.commit();
    }



    public String getOnline() {
        return pref.getString("Offline", "Offline");
    }

    public void setOnline(String Offline) {
        editor.putString("Offline", Offline);
        editor.commit();
    }

    public String getStatusCheck() {
        return pref.getString("Statuscheck", "statuscheck");
    }

    public void setStatusCheck(String Statuscheck) {
        editor.putString("Statuscheck", Statuscheck);
        editor.commit();
    }
    public String getAddress() {
        return pref.getString("Address", "address");
    }

    public void setAddress(String Address) {
        editor.putString("Address", Address);
        editor.commit();
    }


   /* public String getVendorId() {
        return pref.getString("Vendorid", "Vendorid");
    }

    public void setVendorId(String Vendorid) {
        editor.putString("Vendorid", Vendorid);
        editor.commit();
    }*/



    public String getOTP() {
        return pref.getString("OTP", "otp");
    }

    public void setOTP(String OTP) {
        editor.putString("OTP", OTP);
        editor.commit();
    }


    public String getRating() {
        return pref.getString("RAT", "rat");
    }

    public void setRating(String RAT) {
        editor.putString("RAT", RAT);
        editor.commit();
    }






    public String getVendror() {
        return pref.getString("VendorId", "0");
    }

    public void setVendor(String VendorId) {
        editor.putString("VendorId", VendorId);
        editor.commit();
    }


    public String getVendrorClick() {
        return pref.getString("VendorIdClick", "0");
    }

    public void setVendorClick(String VendorId) {
        editor.putString("VendorIdClick", VendorId);
        editor.commit();
    }


    public String getdob() {
        return pref.getString("dob", "0");
    }

    public void setdob(String dob) {
        editor.putString("dob", dob);
        editor.commit();
    }

    public String getMobile() {
        return pref.getString("Mobile", "0");
    }

    public void setMobile(String Mobile) {
        editor.putString("Mobile", Mobile);
        editor.commit();
    }

    public String getImage() {
        return pref.getString("Image", "0");
    }

    public void setImage(String Image) {
        editor.putString("Image", Image);
        editor.commit();
    }


    public String getImageDoct() {
        return pref.getString("Imagedoc", "0");
    }

    public void setImageDoct(String Imagedoc) {
        editor.putString("Imagedoc", Imagedoc);
        editor.commit();
    }

    public static boolean isPostalCodeValid(String postalcode) {
        return postalcode.matches("^[1-9][0-9]{5}$");
    }

    public String getHomeVendrorID() {
        return pref.getString("HomeVendorId", "");
    }

    public void setHomeVendrorID(String HomeVendorId) {
        editor.putString("HomeVendorId", HomeVendorId);
        editor.commit();
    }

    public  void clearAllSharedPreferences(Context context) {
//        SharedPreferences.Editor editor = context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE).edit();
        editor.clear();
        editor.commit();
    }


    public static String getTotalWithSeparater(String total) {
        //double amount = Double.parseDouble(String.valueOf(total));
        // DecimalFormat formatter = new DecimalFormat("#,####");
         //String yourFormattedString = formatter.format(100000);
        return  NumberFormat.getNumberInstance(Locale.UK).format(Float.parseFloat(total));
        //return formatter.format(amount);
    }
}
