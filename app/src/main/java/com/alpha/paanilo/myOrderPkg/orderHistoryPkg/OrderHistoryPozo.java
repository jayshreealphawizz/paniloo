
package com.alpha.paanilo.myOrderPkg.orderHistoryPkg;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderHistoryPozo {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("myOrder")
    @Expose
    private List<MyOrder> myOrder = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<MyOrder> getMyOrder() {
        return myOrder;
    }

    public void setMyOrder(List<MyOrder> myOrder) {
        this.myOrder = myOrder;
    }

}
