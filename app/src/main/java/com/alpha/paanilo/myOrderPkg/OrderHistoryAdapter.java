package com.alpha.paanilo.myOrderPkg;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.alpha.paanilo.R;
import com.alpha.paanilo.myOrderPkg.orderHistoryPkg.MyOrder;
import com.alpha.paanilo.utility.Constants;

import java.util.List;

public class OrderHistoryAdapter extends RecyclerView.Adapter<OrderHistoryAdapter.ViewHolder> {
    private final Context context;
    private final OrderOnClick orderOnClick;
    private List<MyOrder> historyList;

    public OrderHistoryAdapter(Context context, OrderOnClick orderOnClick) {
        this.context = context;
        this.orderOnClick = orderOnClick;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.order_history_row, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        try {
            //   double amount = Double.parseDouble(String.valueOf(historyList.get(position).getTotalAmount()));
            //  DecimalFormat formatter = new DecimalFormat("#,####");
            holder.tvTotalOrderAmount.setText(context.getResources().getString(R.string.rupee) + " " + Constants.getTotalWithSeparater(String.valueOf(historyList.get(position).getTotalAmount())));
            holder.tvTotalOrderDateAndTime.setText(historyList.get(position).getOrderTime());
            holder.tvTotalOrderDeliveryAddres.setText(historyList.get(position).getDeliveryAddress());
            //    holder.tvTotalOrderAmount.setText(context.getResources().getString(R.string.rupee) + " " + +historyList.get(position).getTotalAmount());
            holder.tvTotalOrderPaymetMode.setText(historyList.get(position).getPaymentMethod());
            holder.tvOrderNumber.setText("Order Number:" + historyList.get(position).getOrderInvoiceId());
            if (historyList.get(position).getStatus().equalsIgnoreCase("0")) {
                holder.tvOrderStatus.setText("Pending");
                holder.tvOrderStatus.setBackground(context.getResources().getDrawable(R.drawable.button_shape));
                holder.tvOrderStatus.setTextColor(context.getResources().getColor(R.color.white));
            } else if (historyList.get(position).getStatus().equalsIgnoreCase("1")) {
                holder.tvOrderStatus.setBackground(context.getResources().getDrawable(R.drawable.button_shape));
                holder.tvOrderStatus.setTextColor(context.getResources().getColor(R.color.white));
                holder.tvOrderStatus.setText("Confirm");
            } else if (historyList.get(position).getStatus().equalsIgnoreCase("2")) {
                holder.tvOrderStatus.setText("Delivered");
                holder.tvOrderStatus.setBackground(context.getResources().getDrawable(R.drawable.button_shape_white));
                holder.tvOrderStatus.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            } else if (historyList.get(position).getStatus().equalsIgnoreCase("3")) {
                holder.tvOrderStatus.setText("Cancelled");
                holder.tvOrderStatus.setBackground(context.getResources().getDrawable(R.drawable.button_shape_white));
                holder.tvOrderStatus.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            } else if (historyList.get(position).getStatus().equalsIgnoreCase("4")) {
                holder.tvOrderStatus.setText("Completed");
                holder.tvOrderStatus.setBackground(context.getResources().getDrawable(R.drawable.button_shape_white));
                holder.tvOrderStatus.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void addOrdeHistory(List<MyOrder> historyList) {
        this.historyList = historyList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return historyList == null ? 0 : historyList.size();
    }

    public interface OrderOnClick {
        void orderOnClick(View view, int position, MyOrder myOrder);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CardView cartclick;
        private final AppCompatTextView tvViewMyOrder;
        private final AppCompatTextView tvOrderNumber;
        private final AppCompatTextView tvTotalOrderDateAndTime;
        private final AppCompatTextView tvOrderStatus;
        private final AppCompatTextView tvTotalOrderDeliveryAddres;
        private final AppCompatTextView tvTotalOrderAmount;
        private final AppCompatTextView tvTotalOrderPaymetMode;

        public ViewHolder(View itemView) {
            super(itemView);
            tvOrderStatus = itemView.findViewById(R.id.tvOrderStatusId);
            tvViewMyOrder = itemView.findViewById(R.id.tvViewMyOrderId);
            tvOrderNumber = itemView.findViewById(R.id.tvOrderNumberId);
            tvTotalOrderDateAndTime = itemView.findViewById(R.id.tvTotalOrderDateAndTimeId);
            tvTotalOrderDeliveryAddres = itemView.findViewById(R.id.tvTotalOrderDeliveryAddresId);
            tvTotalOrderAmount = itemView.findViewById(R.id.tvTotalOrderAmountId);
            tvTotalOrderPaymetMode = itemView.findViewById(R.id.tvTotalOrderPaymetModeId);
            cartclick = itemView.findViewById(R.id.cartclick);

            tvViewMyOrder.setOnClickListener(this);
//            tvOrderStatus.setOnClickListener(this);
            cartclick.setOnClickListener(this);
//            tvOrderStatus.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            orderOnClick.orderOnClick(v, getAdapterPosition(), historyList.get(getAdapterPosition()));
        }
    }
}
