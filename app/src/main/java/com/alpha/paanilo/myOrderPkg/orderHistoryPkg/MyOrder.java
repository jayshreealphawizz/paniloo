
package com.alpha.paanilo.myOrderPkg.orderHistoryPkg;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyOrder {

    @SerializedName("order_id")
    @Expose
    private String orderId;
    @SerializedName("order_invoice_id")
    @Expose
    private String orderInvoiceId;
    @SerializedName("order_time")
    @Expose
    private String orderTime;
    @SerializedName("payment_method")
    @Expose
    private String paymentMethod;
    @SerializedName("total_amount")
    @Expose
    private Integer totalAmount;
    @SerializedName("delivery_charge")
    @Expose
    private String deliveryCharge;
    @SerializedName("delivery_address")
    @Expose
    private String deliveryAddress;

    @SerializedName("Status")
    @Expose
    private String Status;


    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }


    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderInvoiceId() {
        return orderInvoiceId;
    }

    public void setOrderInvoiceId(String orderInvoiceId) {
        this.orderInvoiceId = orderInvoiceId;
    }

    public String getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public Integer getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Integer totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getDeliveryCharge() {
        return deliveryCharge;
    }

    public void setDeliveryCharge(String deliveryCharge) {
        this.deliveryCharge = deliveryCharge;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

}
