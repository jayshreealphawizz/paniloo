package com.alpha.paanilo.firebase_pkg;

import android.content.Context;

public class MyNotificationManager {
//    private Context mCtx;
//    private static MyNotificationManager mInstance;
//
//    private MyNotificationManager(Context context) {
//        mCtx = context;
//    }
//
//    public static synchronized MyNotificationManager getInstance(Context context) {
//        if (mInstance == null) {
//            mInstance = new MyNotificationManager(context);
//        }
//        return mInstance;
//    }


    private Context mCtx;
    private static MyNotificationManager mInstance;

    private MyNotificationManager(Context context) {
        mCtx = context;
    }

    public static synchronized MyNotificationManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new MyNotificationManager(context);
        }
        return mInstance;
    }
}