package com.alpha.paanilo.firebase_pkg;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.alpha.paanilo.FetchLocationActivity;
import com.alpha.paanilo.HomeMainActivity;
import com.alpha.paanilo.MyOrder_Activity;
import com.alpha.paanilo.R;
import com.alpha.paanilo.authenticationModule.Loginoption;
import com.alpha.paanilo.authenticationModule.Splash_Activity;
import com.alpha.paanilo.seller.SellerHomeActivity;
import com.alpha.paanilo.utility.CheckNetwork;
import com.alpha.paanilo.utility.Constants;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;


public class MyFirebaseMessagingService extends FirebaseMessagingService {
    PendingIntent pendingIntent;
    String TAG = getClass().getSimpleName();
    Constants constant;

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        String message = remoteMessage.getData().toString();
        Log.d("responce", String.valueOf(remoteMessage));
        sendNotification(remoteMessage.getData().put("notification", message),
                remoteMessage.getData().put("flag", "flag"));
        Log.e("rahulraj", "message:::::::" + message);

        Log.e("rahulraj", "onMessageReceived: ");


        Log.e("rahulraj", "onMessageReceived: ");

        Log.e("rahulraj", "onMessageReceived: " + remoteMessage.getData().put("message", "message"));


        Log.e("rahulraj", "onMessageReceived: " + remoteMessage.getData().put("flag", "flag"));

//        constant = new Constants(getApplicationContext());
//        Log.e("rahulraj", "rahulraj::::::" + constant.getIsLogin());
//        String massage = remoteMessage.getData().toString();

//        System.out.println("firebas response======"+remoteMessage.getNotification());

//        Log.d("responce firebase", String.valueOf(remoteMessage));

//        sendNotification(remoteMessage.getData().put("message", "message"), remoteMessage.getData().put("flag", "flag"));


    }

    private void sendNotification(String messageBody, String flag) {

//         Log.d("responce", String.valueOf(messageBody));
//        Log.d("responceflag", flag);
//        message_info
//                message


//        if (constant.getIsLogin().equals("yes")) {
//
//            CheckNetwork.nextScreenWithFinish(this, FetchLocationActivity.class);
//
//
//        } else if (constant.getIsLogin().equalsIgnoreCase("yes_seller")) {
//
//            CheckNetwork.nextScreenWithFinish(this, SellerHomeActivity.class);
//
//        }
//        Log.e("rahulraj", "onMessageReceivedbbbbb::::: " + constant.getIsLogin());

        if (messageBody != null) {
//            if (constant.getIsLogin().equals("yes")) {
//            Log.e("rahulraj", "onMessageReceived::::: " + constant.getIsLogin());
//            if (constant.getIsLogin().equals("yes")) {
//                Intent intent = new Intent(this, MyOrder_Activity.class);
////            Intent intent = new Intent(this, HomeMainActivity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                intent.putExtra("flag", flag);
//                pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
//            }else if (constant.getIsLogin().equalsIgnoreCase("yes_seller")) {
                Intent inten = new Intent(this, Splash_Activity.class);
                inten.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                inten.putExtra("notification", messageBody);
                pendingIntent = PendingIntent.getActivity(this, 0, inten, PendingIntent.FLAG_ONE_SHOT);
//            } else if (constant.getIsLogin().equalsIgnoreCase("yes_seller")) {
//                Intent inten = new Intent(this, SellerHomeActivity.class);
//                inten.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                inten.putExtra("notification", messageBody);
//                pendingIntent = PendingIntent.getActivity(this, 0, inten, PendingIntent.FLAG_ONE_SHOT);
//            }
//            }

        } else {
//            Log.e("rahulraj", "onMessageReceived::111::: " + constant.getIsLogin());
//            if (constant.getIsLogin().equals("yes")) {
//                Intent intent = new Intent(this, MyOrder_Activity.class);
////            Intent intent = new Intent(this, HomeMainActivity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                intent.putExtra("flag", flag);
//                pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
//
//            } else if (constant.getIsLogin().equalsIgnoreCase("yes_seller")) {
                Intent intent = new Intent(this, Splash_Activity.class);
//            Intent intent = new Intent(this, HomeMainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("flag", flag);
                pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
//            }
//            if (constant.getIsLogin().equals("yes")) {

//            } else if (constant.getIsLogin().equalsIgnoreCase("yes_seller")) {
//                Intent intent = new Intent(this, SellerHomeActivity.class);
////            Intent intent = new Intent(this, HomeMainActivity.class);
//
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                intent.putExtra("flag", flag);
//                pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
//            }
        }


        Log.e("rahulraj", "rahulrajflag : " + flag);
        String channelId = getString(R.string.notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Log.e("rahulraj", "defaultSoundUri" + defaultSoundUri);
        Log.e("rahulraj", "pendingIntent" + pendingIntent);

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.mipmap.applogo)
                        .setContentTitle(getResources().getString(R.string.app_name))
                        .setContentText("" + messageBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)

                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId, "Channel human readable title", NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }
        notificationManager.notify(0, notificationBuilder.build());
    }

//        try {
//            Log.d("responce send noti", String.valueOf(messageBody));
////        if(messageBody!=null){
////            if(messageBody.contains("confirmed")){
//            if(messageBody.contains("confirmed")){
//            Intent intent = new Intent(this, HomeMainActivity.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            intent.putExtra("data", flag);
//            pendingIntent = PendingIntent.getActivity(this, /*(int) when*/0, intent, PendingIntent.FLAG_ONE_SHOT);
//        }
//
//        else{
//            Intent intent = new Intent(this, HomeMainActivity.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            intent.putExtra("data", flag);
//            pendingIntent = PendingIntent.getActivity(this, /*(int) when*/0, intent, PendingIntent.FLAG_ONE_SHOT);
//        }


//        }catch (NullPointerException e){
//
//        }

       /* String channelId = getString(R.string.notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.mipmap.applogo)
                        .setContentTitle(getResources().getString(R.string.app_name))
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId, "Channel human readable title", NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }
        notificationManager.notify(0 *//* ID of notification *//*, notificationBuilder.build());
    }*/
}