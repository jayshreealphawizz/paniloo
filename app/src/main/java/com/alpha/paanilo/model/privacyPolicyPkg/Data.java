
package com.alpha.paanilo.model.privacyPolicyPkg;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("privacy_id")
    @Expose
    private String privacyId;
    @SerializedName("privacy_title")
    @Expose
    private String privacyTitle;
    @SerializedName("privacy_desc")
    @Expose
    private String privacyDesc;

    public String getPrivacyId() {
        return privacyId;
    }

    public void setPrivacyId(String privacyId) {
        this.privacyId = privacyId;
    }

    public String getPrivacyTitle() {
        return privacyTitle;
    }

    public void setPrivacyTitle(String privacyTitle) {
        this.privacyTitle = privacyTitle;
    }

    public String getPrivacyDesc() {
        return privacyDesc;
    }

    public void setPrivacyDesc(String privacyDesc) {
        this.privacyDesc = privacyDesc;
    }

}
