package com.alpha.paanilo.model.banner_pkg;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BannerInfo {

@SerializedName("product_url")
@Expose
private String productUrl;

public String getProductUrl() {
return productUrl;
}

public void setProductUrl(String productUrl) {
this.productUrl = productUrl;
}

}