package com.alpha.paanilo.model.myorderPkg;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyOrderInfo {

    @SerializedName("product_image")
    @Expose
    private String product_image;

    @SerializedName("price")
    @Expose
    private String price;

    @SerializedName("sale_id")
    @Expose
    private String sale_id;

    public String getSale_id() {
        return sale_id;
    }

    public void setSale_id(String sale_id) {
        this.sale_id = sale_id;
    }


    public String getProduct_image() {
        return product_image;
    }

    public void setProduct_image(String product_image) {
        this.product_image = product_image;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @SerializedName("status")
    @Expose
    private String status;

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    @SerializedName("user_id")
    @Expose
    private String userId;

    @SerializedName("product_name")
    @Expose
    private String product_name;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}