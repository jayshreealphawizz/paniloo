
package com.alpha.paanilo.model.termsAndConditionPkg;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("help_id")
    @Expose
    private String termId;
    @SerializedName("help_title")
    @Expose
    private String termTitle;
    @SerializedName("help_desc")
    @Expose
    private String termDesc;

    public String getTermId() {
        return termId;
    }

    public void setTermId(String termId) {
        this.termId = termId;
    }

    public String getTermTitle() {
        return termTitle;
    }

    public void setTermTitle(String termTitle) {
        this.termTitle = termTitle;
    }

    public String getTermDesc() {
        return termDesc;
    }

    public void setTermDesc(String termDesc) {
        this.termDesc = termDesc;
    }

}
