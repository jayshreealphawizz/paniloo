package com.alpha.paanilo.model.notificationPkg;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class NotificationPojo {

@SerializedName("status")
@Expose
private Boolean status;


    @SerializedName("count")
    @Expose
    private String count;

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    @SerializedName("message")
@Expose
private String message;
@SerializedName("data")
@Expose
private ArrayList<NotificationInfo> data = null;

public Boolean getStatus() {
return status;
}

public void setStatus(Boolean status) {
this.status = status;
}

public String getMessage() {
return message;
}

public void setMessage(String message) {
this.message = message;
}

public ArrayList<NotificationInfo> getData() {
return data;
}

public void setData(ArrayList<NotificationInfo> data) {
this.data = data;
}

}