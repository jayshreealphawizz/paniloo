
package com.alpha.paanilo.model.vendor_auth_pkg;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Vendor_LoginUserPojo {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private Vendor_LoginUserInfo data;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Vendor_LoginUserInfo getData() {
        return data;
    }

    public void setData(Vendor_LoginUserInfo data) {
        this.data = data;
    }

}
