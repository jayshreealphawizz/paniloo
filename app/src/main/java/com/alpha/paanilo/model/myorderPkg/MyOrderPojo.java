package com.alpha.paanilo.model.myorderPkg;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MyOrderPojo {

@SerializedName("data")
@Expose
private ArrayList<MyOrderInfo> data = null;
@SerializedName("message")
@Expose
private String message;
@SerializedName("status")
@Expose
private Boolean status;

public ArrayList<MyOrderInfo> getData() {
return data;
}

public void setData(ArrayList<MyOrderInfo> data) {
this.data = data;
}

public String getMessage() {
return message;
}

public void setMessage(String message) {
this.message = message;
}

public Boolean getStatus() {
return status;
}

public void setStatus(Boolean status) {
this.status = status;
}

}