package com.alpha.paanilo.model.banner_pkg;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Data {

@SerializedName("top_highlights")
@Expose
private ArrayList<BannerInfo> topHighlights = null;
@SerializedName("middle_highlights")
@Expose
private ArrayList<BannerInfo> middleHighlights = null;
@SerializedName("bottom_highlights")
@Expose
private ArrayList<BannerInfo> bottomHighlights = null;

public ArrayList<BannerInfo> getTopHighlights() {
return topHighlights;
}

public void setTopHighlights(ArrayList<BannerInfo> topHighlights) {
this.topHighlights = topHighlights;
}

public ArrayList<BannerInfo> getMiddleHighlights() {
return middleHighlights;
}

public void setMiddleHighlights(ArrayList<BannerInfo> middleHighlights) {
this.middleHighlights = middleHighlights;
}

public ArrayList<BannerInfo> getBottomHighlights() {
return bottomHighlights;
}

public void setBottomHighlights(ArrayList<BannerInfo> bottomHighlights) {
this.bottomHighlights = bottomHighlights;
}

}