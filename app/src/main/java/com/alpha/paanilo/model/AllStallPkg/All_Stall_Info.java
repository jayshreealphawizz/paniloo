package com.alpha.paanilo.model.AllStallPkg;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class All_Stall_Info {

@SerializedName("user_id")
@Expose
private String userId;
@SerializedName("user_fullname")
@Expose
private String userFullname;
@SerializedName("user_image")
@Expose
private String userImage;
@SerializedName("current_location")
@Expose
private String currentLocation;

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getSubcategory_id() {
        return subcategory_id;
    }

    public void setSubcategory_id(String subcategory_id) {
        this.subcategory_id = subcategory_id;
    }

    public String getChildcat_id() {
        return childcat_id;
    }

    public void setChildcat_id(String childcat_id) {
        this.childcat_id = childcat_id;
    }

    @SerializedName("latitude")
@Expose
private String latitude;

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    @SerializedName("longitude")
    @Expose
    private String longitude;

@SerializedName("distance")
@Expose
private String distance;

    @SerializedName("category_id")
    @Expose
    private String category_id;

    @SerializedName("subcategory_id")
    @Expose
    private String subcategory_id;

    @SerializedName("childcat_id")
    @Expose
    private String childcat_id;

    @SerializedName("login_status")
    @Expose
    private String login_status;


    public String getLogin_status() {
        return login_status;
    }

    public void setLogin_status(String login_status) {
        this.login_status = login_status;
    }

    public String getUserId() {
return userId;
}

public void setUserId(String userId) {
this.userId = userId;
}

public String getUserFullname() {
return userFullname;
}

public void setUserFullname(String userFullname) {
this.userFullname = userFullname;
}

public String getUserImage() {
return userImage;
}

public void setUserImage(String userImage) {
this.userImage = userImage;
}

public String getCurrentLocation() {
return currentLocation;
}

public void setCurrentLocation(String currentLocation) {
this.currentLocation = currentLocation;
}

public String getLatitude() {
return latitude;
}

public void setLatitude(String latitude) {
this.latitude = latitude;
}

public String getLongitude() {
return longitude;
}

public void setLongitude(String longitude) {
this.longitude = longitude;
}

}