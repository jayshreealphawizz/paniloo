package com.alpha.paanilo.model;

public class AddCartCustomModel {
    public interface OnCustomCartListener {
        void changeCartState();
    }
    private static AddCartCustomModel mInstance;
    private OnCustomCartListener mListener;
    private String mCurrentCartCount;

    private AddCartCustomModel() {
    }

    public static AddCartCustomModel getInstance() {
        if (mInstance == null) {
            mInstance = new AddCartCustomModel();
        }
        return mInstance;
    }

    public void setListener(OnCustomCartListener listener) {
        mListener = listener;
    }

    public void setCartCount(String state) {
        if (mListener != null) {
            mCurrentCartCount = state;
            notifyStateChange();
        }
    }

    public String getCartCount() {
        return mCurrentCartCount;
    }

    private void notifyStateChange() {
        mListener.changeCartState();
    }
}
