
package com.alpha.paanilo.model.subcatpozo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubCatDatum {
    public boolean isExpanded;

    @SerializedName("sub_cat_id")
    @Expose
    private String sub_cat_id;

    public boolean isExpanded() {
        return isExpanded;
    }

    public void setExpanded(boolean expanded) {
        isExpanded = expanded;
    }

    @SerializedName("cat_id")
    @Expose
    private String cat_id;

    public Boolean getSubcatchild() {
        return subcatchild;
    }

    public void setSubcatchild(Boolean subcatchild) {
        this.subcatchild = subcatchild;
    }

    @SerializedName("subcatchild")
    @Expose
    private Boolean subcatchild;

    @SerializedName("subcat_name")
    @Expose
    private String subcat_name;

    public String getCat_name() {
        return cat_name;
    }

    public void setCat_name(String cat_name) {
        this.cat_name = cat_name;
    }

    @SerializedName("cat_name")
    @Expose
    private String cat_name;

    @SerializedName("subcat_img")
    @Expose
    private String subcat_img;

    public String getSub_cat_id() {
        return sub_cat_id;
    }

    public void setSub_cat_id(String sub_cat_id) {
        this.sub_cat_id = sub_cat_id;
    }

    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public String getSubcat_name() {
        return subcat_name;
    }

    public void setSubcat_name(String subcat_name) {
        this.subcat_name = subcat_name;
    }

    public String getSubcat_img() {
        return subcat_img;
    }

    public void setSubcat_img(String subcat_img) {
        this.subcat_img = subcat_img;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("created_at")
    @Expose
    private String created_at;

}
