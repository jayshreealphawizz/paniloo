package com.alpha.paanilo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginUserInfo {
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("user_phone")
    @Expose
    private String userPhone;
    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("user_fullname")
    @Expose
    private String userFullname;
    @SerializedName("user_email")
    @Expose
    private String userEmail;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("user_bdate")
    @Expose
    private String userBdate;
    @SerializedName("user_password")
    @Expose
    private String userPassword;
    @SerializedName("user_city")
    @Expose
    private String userCity;
    @SerializedName("varification_code")
    @Expose
    private String varificationCode;
    @SerializedName("user_image")
    @Expose
    private String userImage;
    @SerializedName("pincode")
    @Expose
    private String pincode;
    @SerializedName("otp_verify")
    @Expose
    private String otpVerify;
    @SerializedName("socity_id")
    @Expose
    private String socityId;
    @SerializedName("house_no")
    @Expose
    private String houseNo;
    @SerializedName("mobile_verified")
    @Expose
    private String mobileVerified;
    @SerializedName("user_gcm_code")
    @Expose
    private String userGcmCode;
    @SerializedName("user_ios_token")
    @Expose
    private String userIosToken;
    @SerializedName("varified_token")
    @Expose
    private String varifiedToken;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("reg_code")
    @Expose
    private String regCode;
    @SerializedName("wallet")
    @Expose
    private String wallet;
    @SerializedName("rewards")
    @Expose
    private String rewards;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("modified")
    @Expose
    private String modified;
    @SerializedName("otp")
    @Expose
    private String otp;
    @SerializedName("social")
    @Expose
    private String social;
    @SerializedName("firebaseToken")
    @Expose
    private String firebaseToken;
    @SerializedName("radius")
    @Expose
    private String radius;
    @SerializedName("is_email_verified")
    @Expose
    private String isEmailVerified;
    @SerializedName("email_verification_token")
    @Expose
    private String emailVerificationToken;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("shop_image")
    @Expose
    private String shopImage;


    @SerializedName("latitude")
    @Expose
    private String latitude;

    @SerializedName("longitude")
    @Expose
    private String longitude;


    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserFullname() {
        return userFullname;
    }

    public void setUserFullname(String userFullname) {
        this.userFullname = userFullname;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getUserBdate() {
        return userBdate;
    }

    public void setUserBdate(String userBdate) {
        this.userBdate = userBdate;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserCity() {
        return userCity;
    }

    public void setUserCity(String userCity) {
        this.userCity = userCity;
    }

    public String getVarificationCode() {
        return varificationCode;
    }

    public void setVarificationCode(String varificationCode) {
        this.varificationCode = varificationCode;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getOtpVerify() {
        return otpVerify;
    }

    public void setOtpVerify(String otpVerify) {
        this.otpVerify = otpVerify;
    }

    public String getSocityId() {
        return socityId;
    }

    public void setSocityId(String socityId) {
        this.socityId = socityId;
    }

    public String getHouseNo() {
        return houseNo;
    }

    public void setHouseNo(String houseNo) {
        this.houseNo = houseNo;
    }

    public String getMobileVerified() {
        return mobileVerified;
    }

    public void setMobileVerified(String mobileVerified) {
        this.mobileVerified = mobileVerified;
    }

    public String getUserGcmCode() {
        return userGcmCode;
    }

    public void setUserGcmCode(String userGcmCode) {
        this.userGcmCode = userGcmCode;
    }

    public String getUserIosToken() {
        return userIosToken;
    }

    public void setUserIosToken(String userIosToken) {
        this.userIosToken = userIosToken;
    }

    public String getVarifiedToken() {
        return varifiedToken;
    }

    public void setVarifiedToken(String varifiedToken) {
        this.varifiedToken = varifiedToken;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRegCode() {
        return regCode;
    }

    public void setRegCode(String regCode) {
        this.regCode = regCode;
    }

    public String getWallet() {
        return wallet;
    }

    public void setWallet(String wallet) {
        this.wallet = wallet;
    }

    public String getRewards() {
        return rewards;
    }

    public void setRewards(String rewards) {
        this.rewards = rewards;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getSocial() {
        return social;
    }

    public void setSocial(String social) {
        this.social = social;
    }

    public String getFirebaseToken() {
        return firebaseToken;
    }

    public void setFirebaseToken(String firebaseToken) {
        this.firebaseToken = firebaseToken;
    }

    public String getRadius() {
        return radius;
    }

    public void setRadius(String radius) {
        this.radius = radius;
    }

    public String getIsEmailVerified() {
        return isEmailVerified;
    }

    public void setIsEmailVerified(String isEmailVerified) {
        this.isEmailVerified = isEmailVerified;
    }

    public String getEmailVerificationToken() {
        return emailVerificationToken;
    }

    public void setEmailVerificationToken(String emailVerificationToken) {
        this.emailVerificationToken = emailVerificationToken;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getShopImage() {
        return shopImage;
    }

    public void setShopImage(String shopImage) {
        this.shopImage = shopImage;
    }









    /*@SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("user_phone")
    @Expose
    private String userPhone;
    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("user_fullname")
    @Expose
    private String userFullname;
    @SerializedName("user_email")
    @Expose
    private String userEmail;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("user_bdate")
    @Expose
    private String userBdate;
    @SerializedName("user_password")
    @Expose
    private String userPassword;
    @SerializedName("user_city")
    @Expose
    private String userCity;
    @SerializedName("varification_code")
    @Expose
    private String varificationCode;
    @SerializedName("user_image")
    @Expose
    private String userImage;
    @SerializedName("pincode")
    @Expose
    private String pincode;
    @SerializedName("socity_id")
    @Expose
    private String socityId;
    @SerializedName("house_no")
    @Expose
    private String houseNo;
    @SerializedName("mobile_verified")
    @Expose
    private String mobileVerified;
    @SerializedName("user_gcm_code")
    @Expose
    private String userGcmCode;
    @SerializedName("user_ios_token")
    @Expose
    private String userIosToken;
    @SerializedName("varified_token")
    @Expose
    private String varifiedToken;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("reg_code")
    @Expose
    private String regCode;
    @SerializedName("wallet")
    @Expose
    private String wallet;
    @SerializedName("rewards")
    @Expose
    private String rewards;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("modified")
    @Expose
    private String modified;
    @SerializedName("otp")
    @Expose
    private String otp;
    @SerializedName("social")
    @Expose
    private String social;
    @SerializedName("firebaseToken")
    @Expose
    private String firebaseToken;
    @SerializedName("radius")
    @Expose
    private String radius;
    @SerializedName("is_email_verified")
    @Expose
    private String isEmailVerified;
    @SerializedName("email_verification_token")
    @Expose
    private String emailVerificationToken;
    @SerializedName("otp_verify")
    @Expose
    private String otpVerify;
    @SerializedName("address")
    @Expose
    private String address;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getUserFullname() {
        return userFullname;
    }

    public void setUserFullname(String userFullname) {
        this.userFullname = userFullname;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getUserBdate() {
        return userBdate;
    }

    public void setUserBdate(String userBdate) {
        this.userBdate = userBdate;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserCity() {
        return userCity;
    }

    public void setUserCity(String userCity) {
        this.userCity = userCity;
    }

    public String getVarificationCode() {
        return varificationCode;
    }

    public void setVarificationCode(String varificationCode) {
        this.varificationCode = varificationCode;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getSocityId() {
        return socityId;
    }

    public void setSocityId(String socityId) {
        this.socityId = socityId;
    }

    public String getHouseNo() {
        return houseNo;
    }

    public void setHouseNo(String houseNo) {
        this.houseNo = houseNo;
    }

    public String getMobileVerified() {
        return mobileVerified;
    }

    public void setMobileVerified(String mobileVerified) {
        this.mobileVerified = mobileVerified;
    }

    public String getUserGcmCode() {
        return userGcmCode;
    }

    public void setUserGcmCode(String userGcmCode) {
        this.userGcmCode = userGcmCode;
    }

    public String getUserIosToken() {
        return userIosToken;
    }

    public void setUserIosToken(String userIosToken) {
        this.userIosToken = userIosToken;
    }

    public String getVarifiedToken() {
        return varifiedToken;
    }

    public void setVarifiedToken(String varifiedToken) {
        this.varifiedToken = varifiedToken;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRegCode() {
        return regCode;
    }

    public void setRegCode(String regCode) {
        this.regCode = regCode;
    }

    public String getWallet() {
        return wallet;
    }

    public void setWallet(String wallet) {
        this.wallet = wallet;
    }

    public String getRewards() {
        return rewards;
    }

    public void setRewards(String rewards) {
        this.rewards = rewards;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getSocial() {
        return social;
    }

    public void setSocial(String social) {
        this.social = social;
    }

    public String getFirebaseToken() {
        return firebaseToken;
    }

    public void setFirebaseToken(String firebaseToken) {
        this.firebaseToken = firebaseToken;
    }

    public String getRadius() {
        return radius;
    }

    public void setRadius(String radius) {
        this.radius = radius;
    }

    public String getIsEmailVerified() {
        return isEmailVerified;
    }

    public void setIsEmailVerified(String isEmailVerified) {
        this.isEmailVerified = isEmailVerified;
    }

    public String getEmailVerificationToken() {
        return emailVerificationToken;
    }

    public void setEmailVerificationToken(String emailVerificationToken) {
        this.emailVerificationToken = emailVerificationToken;
    }

    public String getOtpVerify() {
        return otpVerify;
    }

    public void setOtpVerify(String otpVerify) {
        this.otpVerify = otpVerify;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }*/






}
