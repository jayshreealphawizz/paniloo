
package com.alpha.paanilo.model.searchingModlePkg;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("product_description")
    @Expose
    private String productDescription;
    @SerializedName("product_image")
    @Expose
    private String productImage;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("product_pincode")
    @Expose
    private String productPincode;
    @SerializedName("in_stock")
    @Expose
    private String inStock;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("availability")
    @Expose
    private String availability;
    @SerializedName("volume")
    @Expose
    private String volume;
    @SerializedName("mrp")
    @Expose
    private String mrp;
    @SerializedName("tax")
    @Expose
    private String tax;
    @SerializedName("unit_value")
    @Expose
    private String unitValue;
    @SerializedName("unit")
    @Expose
    private String unit;
    @SerializedName("increament")
    @Expose
    private String increament;
    @SerializedName("rewards")
    @Expose
    private String rewards;
    @SerializedName("loca_category")
    @Expose
    private String locaCategory;
    @SerializedName("quantity")
    @Expose
    private String quantity;
    @SerializedName("net_weight")
    @Expose
    private String netWeight;
    @SerializedName("State_code")
    @Expose
    private String stateCode;
    @SerializedName("Location_filter")
    @Expose
    private String locationFilter;
    @SerializedName("thumbnails_image")
    @Expose
    private String thumbnailsImage;
    @SerializedName("farmer_id")
    @Expose
    private String farmerId;
    @SerializedName("discount_price")
    @Expose
    private String discountPrice;
    @SerializedName("off_percentage")
    @Expose
    private String offPercentage;
    @SerializedName("weekend_promo")
    @Expose
    private String weekendPromo;
    @SerializedName("wishlist_status")
    @Expose
    private Boolean wishlistStatus;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("rating")
    @Expose
    private Integer rating;
    @SerializedName("images")
    @Expose
    private List<String> images = null;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getProductPincode() {
        return productPincode;
    }

    public void setProductPincode(String productPincode) {
        this.productPincode = productPincode;
    }

    public String getInStock() {
        return inStock;
    }

    public void setInStock(String inStock) {
        this.inStock = inStock;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getUnitValue() {
        return unitValue;
    }

    public void setUnitValue(String unitValue) {
        this.unitValue = unitValue;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getIncreament() {
        return increament;
    }

    public void setIncreament(String increament) {
        this.increament = increament;
    }

    public String getRewards() {
        return rewards;
    }

    public void setRewards(String rewards) {
        this.rewards = rewards;
    }

    public String getLocaCategory() {
        return locaCategory;
    }

    public void setLocaCategory(String locaCategory) {
        this.locaCategory = locaCategory;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getNetWeight() {
        return netWeight;
    }

    public void setNetWeight(String netWeight) {
        this.netWeight = netWeight;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getLocationFilter() {
        return locationFilter;
    }

    public void setLocationFilter(String locationFilter) {
        this.locationFilter = locationFilter;
    }

    public String getThumbnailsImage() {
        return thumbnailsImage;
    }

    public void setThumbnailsImage(String thumbnailsImage) {
        this.thumbnailsImage = thumbnailsImage;
    }

    public String getFarmerId() {
        return farmerId;
    }

    public void setFarmerId(String farmerId) {
        this.farmerId = farmerId;
    }

    public String getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(String discountPrice) {
        this.discountPrice = discountPrice;
    }

    public String getOffPercentage() {
        return offPercentage;
    }

    public void setOffPercentage(String offPercentage) {
        this.offPercentage = offPercentage;
    }

    public String getWeekendPromo() {
        return weekendPromo;
    }

    public void setWeekendPromo(String weekendPromo) {
        this.weekendPromo = weekendPromo;
    }

    public Boolean getWishlistStatus() {
        return wishlistStatus;
    }

    public void setWishlistStatus(Boolean wishlistStatus) {
        this.wishlistStatus = wishlistStatus;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

}
