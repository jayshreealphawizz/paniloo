
package com.alpha.paanilo.model.bankDetailPkg;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("bankAcoountHolderName")
    @Expose
    private String bankAcoountHolderName;
    @SerializedName("bankName")
    @Expose
    private String bankName;
    @SerializedName("accountNo")
    @Expose
    private String accountNo;
    @SerializedName("ifscCode")
    @Expose
    private String ifscCode;

    public String getBankAcoountHolderName() {
        return bankAcoountHolderName;
    }

    public void setBankAcoountHolderName(String bankAcoountHolderName) {
        this.bankAcoountHolderName = bankAcoountHolderName;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }

}
