package com.alpha.paanilo.model.vendor_auth_pkg;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Vendor_LoginUserInfo {
    @SerializedName("current_location")
    @Expose
    private String current_location;
    @SerializedName("latitude")
    @Expose
    private String latitude;

    public String getCurrent_location() {
        return current_location;
    }

    public void setCurrent_location(String current_location) {
        this.current_location = current_location;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @SerializedName("longitude")
    @Expose
    private String longitude;



    @SerializedName("firebaseToken")
    @Expose
    private String firebaseToken;

    @SerializedName("user_fullname")
    @Expose
    private String user_fullname;

    @SerializedName("user_email")
    @Expose
    private String user_email;
    @SerializedName("user_password")
    @Expose
    private String user_password;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    @SerializedName("user_id")
    @Expose
    private String user_id;

    public String getFirebaseToken() {
        return firebaseToken;
    }

    public void setFirebaseToken(String firebaseToken) {
        this.firebaseToken = firebaseToken;
    }

    public String getUser_fullname() {
        return user_fullname;
    }

    public void setUser_fullname(String user_fullname) {
        this.user_fullname = user_fullname;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getUser_password() {
        return user_password;
    }

    public void setUser_password(String user_password) {
        this.user_password = user_password;
    }
}
