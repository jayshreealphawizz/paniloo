package com.alpha.paanilo.expandable;

import com.alpha.paanilo.model.subcatpozo.SubCatDatum;

/**
 * interface to listen changes in state of sections
 */
public interface SectionStateChangeListener {
    void onSectionStateChanged(SubCatDatum section, boolean isOpen);

}