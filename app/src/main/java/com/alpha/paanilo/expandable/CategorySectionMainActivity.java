package com.alpha.paanilo.expandable;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.alpha.paanilo.R;
import com.alpha.paanilo.model.subcatchild_pozo.SubCatChildDatum;
import com.alpha.paanilo.model.subcatchild_pozo.SubCategoryChildPozo;
import com.alpha.paanilo.model.subcatpozo.SubCatDatum;
import com.alpha.paanilo.model.subcatpozo.SubCategoryPozo;
import com.alpha.paanilo.nearby_stall_pkg.All_NearBy_StallActivity;
import com.alpha.paanilo.retrofit.ApiClient;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CategorySectionMainActivity extends AppCompatActivity implements ItemClickListener, View.OnClickListener {
    RecyclerView mRecyclerView;
    private List<SubCatDatum> subcategorydatumList;
    private List<SubCatChildDatum> subcategorydatumList_child;


    SectionedExpandableLayoutHelper sectionedExpandableLayoutHelper;
    private LinkedHashMap stringParentCategorySectionHashMap;
    ProgressBar pbLoginId;
    AppCompatImageView backimage; AppCompatTextView ivCartHomeId;
    AppCompatEditText etSearchBox;
    RelativeLayout rlCartHomeId;
    private ProgressDialog pd;
    View id_toolbar;
    AppCompatImageView iv_back;
    AppCompatTextView tv_title, tv_nodata;
    String catName, catID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_des_faq_expandablelist);

        pd = new ProgressDialog(CategorySectionMainActivity.this, R.style.AppCompatAlertDialogStyle);

        id_toolbar = findViewById(R.id.id_toolbar);
        iv_back = id_toolbar.findViewById(R.id.id_back);
        tv_title = id_toolbar.findViewById(R.id.tv_tittle);
        iv_back.setOnClickListener(this);


        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        sectionedExpandableLayoutHelper = new SectionedExpandableLayoutHelper(this, mRecyclerView, this, 3);
        subcategorydatumList = new ArrayList<>();
        subcategorydatumList_child = new ArrayList<>();
        stringParentCategorySectionHashMap = new LinkedHashMap();


        catID =  getIntent().getStringExtra("catID");
        catName = getIntent().getStringExtra("catName");

        tv_title.setText(catName);
        getSubCategory(catID);
    }

    public void getSubCategory(final String catID) {
        System.out.println("cat id ===="+catID);
        pd.setMessage("Loading");
        pd.show();
        (ApiClient.getClient().getAllSubCategory(catID)).enqueue(new Callback<SubCategoryPozo>() {
            @Override
            public void onResponse(Call<SubCategoryPozo> call, Response<SubCategoryPozo> response) {
                pd.dismiss();
                if (response.isSuccessful()) {
                    try {
                        Log.e("TAG", "all sub category : " + new Gson().toJson(response.body()));
                        SubCategoryPozo categoryPozo = response.body();
                        if (categoryPozo.getStatus()) {
                            subcategorydatumList = categoryPozo.getData();
                            System.out.println("yes show subcat==="+subcategorydatumList.size());
                            for(int i=0; i<subcategorydatumList.size(); i++){

                                if(subcategorydatumList.get(i).getSubcatchild()) {
                                    getSubCategoryChild(subcategorydatumList.get(i), subcategorydatumList.get(i).getSub_cat_id());
                                    sectionedExpandableLayoutHelper.addParentCategory(stringParentCategorySectionHashMap);
                                    sectionedExpandableLayoutHelper.notifyDataSetChanged();
                                }
                                else{

                                    stringParentCategorySectionHashMap.put(subcategorydatumList.get(i), subcategorydatumList);
                                    sectionedExpandableLayoutHelper.addParentCategory(stringParentCategorySectionHashMap);
                                    sectionedExpandableLayoutHelper.notifyDataSetChanged();
//                                    Toast.makeText(CategorySectionMainActivity.this, "Not subcatchild", Toast.LENGTH_SHORT).show();
                                }
                            }

                        } else {
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    if (response.code() == 400) {
                        if (!false) {
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(response.errorBody().string());
//                                pbLoginId.setVisibility(View.GONE);
//                                tvProduct.setVisibility(View.VISIBLE);
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<SubCategoryPozo> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(getApplicationContext(), getString(R.string.server_errors), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getSubCategoryChild(final SubCatDatum subCatDatum, String catID) {
        System.out.println("sub cat id ===="+catID);
        pd.setMessage("Loading");
        pd.show();
        (ApiClient.getClient().getAllSubChildCategory(catID)).enqueue(new Callback<SubCategoryChildPozo>() {
            @Override
            public void onResponse(Call<SubCategoryChildPozo> call, Response<SubCategoryChildPozo> response) {
                pd.dismiss();
                if (response.isSuccessful()) {
                    SubCategoryChildPozo subCategoryChildPozo = response.body();
                    Log.e("TAG", "all sub category child : " + new Gson().toJson(response.body()));
                    if (response.isSuccessful()) {
                        subcategorydatumList_child = subCategoryChildPozo.getData();
                        stringParentCategorySectionHashMap.put(subCatDatum, subcategorydatumList_child);
                        sectionedExpandableLayoutHelper.addParentCategory(stringParentCategorySectionHashMap);
                    }
                    sectionedExpandableLayoutHelper.notifyDataSetChanged();
                }
                else {
                    if (response.code() == 400) {
                        if (!false) {
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(response.errorBody().string());
//                                pbLoginId.setVisibility(View.GONE);
//                                tvProduct.setVisibility(View.VISIBLE);
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<SubCategoryChildPozo> call, Throwable t) {
                pd.dismiss();
                System.out.println("cslllll"+t.getMessage());
                Toast.makeText(getApplicationContext(), getString(R.string.server_errors), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void itemClicked(SubCatChildDatum item, int position) {
//        Toast.makeText(this, "sub item click==="+item.getName(), Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(CategorySectionMainActivity.this, All_NearBy_StallActivity.class);
        intent.putExtra("comes_from","subcatchild");
        intent.putExtra("subcatid", "");
        intent.putExtra("catid", "");
        intent.putExtra("subcatchildid",item.getId());
        startActivity(intent);
//        finish();
    }

    @Override
    public void itemClicked(SubCatDatum section, int position) {
//        Toast.makeText(this, "item click==="+section.getSubcat_name(), Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(CategorySectionMainActivity.this, All_NearBy_StallActivity.class);
        intent.putExtra("comes_from","subcat");
        intent.putExtra("subcatid", section.getSub_cat_id());
        intent.putExtra("catid", "");
        intent.putExtra("subcatchildid","");
        startActivity(intent);
//        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.id_back:
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                break;
        }
    }
}
