package com.alpha.paanilo.expandable;


import com.alpha.paanilo.model.subcatchild_pozo.SubCatChildDatum;
import com.alpha.paanilo.model.subcatpozo.SubCatDatum;

public interface ItemClickListener {
    void itemClicked(SubCatChildDatum item, int position);
    void itemClicked(SubCatDatum section, int position);
}
