package com.alpha.paanilo.expandable;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alpha.paanilo.R;
import com.alpha.paanilo.model.subcatchild_pozo.SubCatChildDatum;
import com.alpha.paanilo.model.subcatpozo.SubCatDatum;
import com.alpha.paanilo.retrofit.ApiClient;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.makeramen.roundedimageview.RoundedImageView;


import java.util.ArrayList;

public class SectionedExpandableGridAdapter extends RecyclerView.Adapter<SectionedExpandableGridAdapter.ViewHolder> {

    //data array
    private ArrayList<Object> mDataArrayList;

    //context
    private final Context mContext;

    //listeners
    private final ItemClickListener mItemClickListener;
    private final SectionStateChangeListener mSectionStateChangeListener;

    //view type
    private static final int VIEW_TYPE_SECTION = R.layout.search_des_faq_expandablelist_row;
    private static final int VIEW_TYPE_ITEM = R.layout.product_fragment_explore_cat_item; //TODO : change this

    public SectionedExpandableGridAdapter(Context context, ArrayList<Object> dataArrayList,
                                          final GridLayoutManager gridLayoutManager, ItemClickListener itemClickListener,
                                          SectionStateChangeListener sectionStateChangeListener) {
        mContext = context;
        mItemClickListener = itemClickListener;
        mSectionStateChangeListener = sectionStateChangeListener;
        mDataArrayList = dataArrayList;

        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return isSection(position) ? gridLayoutManager.getSpanCount() : 1;
            }
        });
    }

    private boolean isSection(int position) {
        return mDataArrayList.get(position) instanceof SubCatDatum;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(mContext).inflate(viewType, parent, false), viewType);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        switch (holder.viewType) {

            case VIEW_TYPE_ITEM:
                final SubCatChildDatum item = (SubCatChildDatum) mDataArrayList.get(position);
                holder.itemTextView.setText(item.getName());


                Glide.with(mContext)
//                        .load(ApiClient.SUB_CATEGORY_URL + item.getImage())
                        .load(ApiClient.SUB_CATEGORY_CHILD_URL + item.getImage())
                        .placeholder(R.drawable.placeholder)
                        .fitCenter()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .dontTransform()
                        .into(holder.iv_subcat);

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mItemClickListener.itemClicked(item, position);
                    }
                });

                break;

            case VIEW_TYPE_SECTION:
                final SubCatDatum section = (SubCatDatum) mDataArrayList.get(position);


                holder.sectionTextView.setText(section.getSubcat_name());
//                holder.tv_subcat_name.setText(section.getSubcat_name());

                Glide.with(mContext)
                        .load(ApiClient.SUB_CATEGORY_URL + section.getSubcat_img())
//                        .load(ApiClient.SUB_CATEGORY_CHILD_URL + section.getSubcat_img())
                        .placeholder(R.drawable.placeholder)
                        .fitCenter()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .dontTransform()
                        .into(holder.iv_catimg);


                holder.sectionToggleButton.setChecked(section.isExpanded);
                holder.sectionToggleButton_hide.setChecked(section.isExpanded);

                if (section.getSubcatchild()) {
                    if (section.isExpanded) {
                        holder.sectionToggleButton.setButtonDrawable(mContext.getDrawable(R.drawable.ic_arrow_up));
                    } else {
                        holder.sectionToggleButton.setButtonDrawable(mContext.getDrawable(R.drawable.ic_arrow_down));
                    }

                    holder.sectionToggleButton_hide.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            mSectionStateChangeListener.onSectionStateChanged(section, isChecked);

                            if (isChecked) {
                                holder.sectionToggleButton.setButtonDrawable(mContext.getDrawable(R.drawable.ic_arrow_up));
                            } else {
                                holder.sectionToggleButton.setButtonDrawable(mContext.getDrawable(R.drawable.ic_arrow_down));
                            }
                        }
                    });

                } else {
                    System.out.println("else me aaya===="+VIEW_TYPE_SECTION);
                    holder.rl_whole.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mItemClickListener.itemClicked(section, position);
                        }
                    });

                }
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mDataArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (isSection(position))
            return VIEW_TYPE_SECTION;
        else return VIEW_TYPE_ITEM;
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {

        //common
        View view;
        int viewType;

        //for section
        AppCompatTextView sectionTextView;
        AppCompatTextView tv_subcat_name;
        ToggleButton sectionToggleButton, sectionToggleButton_hide;
        RoundedImageView iv_catimg;
        RelativeLayout rl_whole;

        //for item
        TextView itemTextView;
        AppCompatImageView iv_subcat;

        public ViewHolder(View view, int viewType) {
            super(view);
            this.viewType = viewType;
            this.view = view;
            if (viewType == VIEW_TYPE_ITEM) {
                itemTextView = view.findViewById(R.id.id_tv_title);
                iv_subcat = view.findViewById(R.id.id_userpic);

            } else {
                iv_catimg = view.findViewById(R.id.iv_catimg);
                tv_subcat_name = view.findViewById(R.id.tv_subcat_name);
                sectionTextView = view.findViewById(R.id.textViewGroup);
                sectionToggleButton = (ToggleButton) view.findViewById(R.id.toggle_button_section);
                sectionToggleButton_hide = (ToggleButton) view.findViewById(R.id.toggle_button_section11);
                rl_whole = view.findViewById(R.id.rl_whole);
            }
        }
    }


}
