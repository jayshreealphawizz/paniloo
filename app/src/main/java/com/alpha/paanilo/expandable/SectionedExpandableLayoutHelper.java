package com.alpha.paanilo.expandable;

import android.content.Context;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alpha.paanilo.model.subcatchild_pozo.SubCatChildDatum;
import com.alpha.paanilo.model.subcatpozo.SubCatDatum;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by bpncool on 2/23/2016.
 */
public class SectionedExpandableLayoutHelper implements SectionStateChangeListener {

    //data list
    private HashMap<SubCatDatum, ArrayList<SubCatChildDatum>> wholeDataMap = new HashMap<>();
    private ArrayList<Object> mDataArrayList = new ArrayList<Object>();
    private ArrayList<Object> mDataArrayList11 = new ArrayList<Object>();

    //section map
    //TODO : look for a way to avoid this

    //adapter
    private SectionedExpandableGridAdapter mSectionedExpandableGridAdapter;

    //recycler view
    RecyclerView mRecyclerView;

    public SectionedExpandableLayoutHelper(Context context, RecyclerView recyclerView, ItemClickListener itemClickListener,
                                           int gridSpanCount) {

        //setting the recycler view
        GridLayoutManager gridLayoutManager = new GridLayoutManager(context, gridSpanCount);
        recyclerView.setLayoutManager(gridLayoutManager);

        mSectionedExpandableGridAdapter = new SectionedExpandableGridAdapter(context, mDataArrayList, gridLayoutManager, itemClickListener, this);
        recyclerView.setAdapter(mSectionedExpandableGridAdapter);


        mRecyclerView = recyclerView;
    }

    public void notifyDataSetChanged() {
        //TODO : handle this condition such that these functions won't be called if the recycler view is on scroll
        generateDataList();
        mSectionedExpandableGridAdapter.notifyDataSetChanged();
    }

    public void addParentCategory(LinkedHashMap<SubCatDatum, ArrayList<SubCatChildDatum>> stringParentCategorySectionHashMap) {
        this.wholeDataMap = stringParentCategorySectionHashMap;
    }


    private void generateDataList () {
        mDataArrayList.clear();
        for (Map.Entry<SubCatDatum, ArrayList<SubCatChildDatum>> entry : wholeDataMap.entrySet()) {
            SubCatDatum key;
            mDataArrayList.add((key = entry.getKey()));
            if (key.isExpanded)
                mDataArrayList.addAll(entry.getValue());
        }
    }


    @Override
    public void onSectionStateChanged(SubCatDatum section, boolean isOpen) {
//        notifyDataSetChanged();

        if (!mRecyclerView.isComputingLayout()) {
            section.isExpanded = isOpen;
            notifyDataSetChanged();
        }
    }
}
