package com.alpha.paanilo.filterPkg;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alpha.paanilo.R;
import com.alpha.paanilo.model.HistoryData;
import com.alpha.paanilo.model.productByCategory.ProductData;
import com.alpha.paanilo.product_detail_pkg.Product_details_Activity;
import com.alpha.paanilo.utility.AppSession;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class Filter_All_Product_Activity extends AppCompatActivity implements View.OnClickListener, Filter_All_Product_Adapter.ProductOnClickListener {
    AppCompatImageView iv_back;
    RecyclerView rv_allproduct;
    ArrayList<HistoryData> historyList;
    View id_toolbar;
    AppCompatTextView tv_catname, tvProductFound;
    TextView tv_nodata;
    RelativeLayout rl_notification;
    LinearLayout ll_filer;
    Filter_All_Product_Adapter searchAdapter;
    ProgressDialog pd;
    private ArrayList<ProductData> productList;

    private AppCompatEditText etProductSearch;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setContentView(R.layout.filter_all_product_rv);
        init();

        etProductSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int
                    count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                searchAdapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {
                searchAdapter.getFilter().filter(s);
            }
        });
    }

    private void init() {
        tv_nodata = findViewById(R.id.tv_nodata);
        tvProductFound = findViewById(R.id.tvProductFoundId);
        id_toolbar = findViewById(R.id.id_toolbar_product);
        iv_back = id_toolbar.findViewById(R.id.id_back);
        etProductSearch = id_toolbar.findViewById(R.id.etProductSearchId);

        iv_back.setOnClickListener(this);

        rv_allproduct = findViewById(R.id.id_rv_allproduct);

        productList = new ArrayList<>();
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
        rv_allproduct.setLayoutManager(gridLayoutManager);
        searchAdapter = new Filter_All_Product_Adapter(Filter_All_Product_Activity.this, this);
        rv_allproduct.setAdapter(searchAdapter);


        pd = new ProgressDialog(Filter_All_Product_Activity.this, R.style.AppCompatAlertDialogStyle);

        try {
            Intent intent = getIntent();
            Bundle args = intent.getBundleExtra("bundle");
            productList = (ArrayList<ProductData>) args.getSerializable("filterdata");
        } catch (Exception e) {
            e.printStackTrace();
        }
        productList = getArrayList("key");

        if (productList.size() > 0) {
            tvProductFound.setText(productList.size() + " Products Found");
            searchAdapter.addProductList(productList);
        } else {
            tv_nodata.setVisibility(View.VISIBLE);
        }

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.id_back:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onClick(View view, int position, ProductData productData) {
        switch (view.getId()) {
            case R.id.id_userpic:
                AppSession.setStringPreferences(getApplicationContext(), "entryFlag", "FilterProduct");
                AppSession.setStringPreferences(getApplicationContext(), "productID", productData.getProductId());
                startActivity(new Intent(getApplicationContext(), Product_details_Activity.class)
                        .putExtra("productID", productData.getProductId())
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                finish();
                break;
        }

    }

    public ArrayList<ProductData> getArrayList(String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<ArrayList<ProductData>>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    @Override
    public void onBackPressed() {
//        Intent intent = new Intent(Filter_All_Product_Activity.this, All_Product_Activity.class);
//        startActivity(intent);
        finish();
    }
}

