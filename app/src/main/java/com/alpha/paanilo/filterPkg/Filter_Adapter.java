package com.alpha.paanilo.filterPkg;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;


import com.alpha.paanilo.R;
import com.alpha.paanilo.model.categoryPkg.Datum;

import java.util.ArrayList;

public class Filter_Adapter extends RecyclerView.Adapter<Filter_Adapter.MyViewHolder> {
        Context context;
        private LayoutInflater mInflater;
    ArrayList<Datum> catList;



    public Filter_Adapter(Context applicationContext, ArrayList<Datum> allCategory) {
        context = applicationContext;
        catList = allCategory;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
            AppCompatTextView id_catname;
            CheckBox checkbox;

            public MyViewHolder(View view) {
                super(view);
                id_catname = view.findViewById(R.id.id_catname);
                checkbox = view.findViewById(R.id.checkbox);
            }
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.filter_row, parent, false);
            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
           final Datum detail = catList.get(position);

           holder.id_catname.setText(detail.getTitle());
            holder.checkbox.setTag(position);

            holder.checkbox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Integer pos = (Integer) holder.checkbox.getTag();
                    if (catList.get(pos).getSelected()) {
                        catList.get(pos).setSelected(false);
                    } else {
                        catList.get(pos).setSelected(true);
                    }
                }
            });

        }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
        public int getItemCount() {
            return catList.size();
        }
    }