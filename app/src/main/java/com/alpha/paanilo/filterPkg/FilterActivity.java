package com.alpha.paanilo.filterPkg;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alpha.paanilo.R;
import com.alpha.paanilo.model.categoryPkg.Datum;
import com.alpha.paanilo.model.filterPkg.FilterPojo;
import com.alpha.paanilo.model.productByCategory.ProductData;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.utility.Constants;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.Serializable;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FilterActivity extends AppCompatActivity implements View.OnClickListener {
    AppCompatImageView iv_back;
    RecyclerView rvFilter;
    View id_toolbar;
    AppCompatTextView tv_title;
    Filter_Adapter filter_adapter;
    ArrayList<String> selected = new ArrayList<String>();
    ArrayList<ProductData> filter_product;
    AppCompatButton btn_apply;
    ProgressDialog pd;
    private Constants constants;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        init();
    }

    private void init() {
        constants = new Constants(getApplicationContext());
        id_toolbar = findViewById(R.id.id_toolbar);
        iv_back = id_toolbar.findViewById(R.id.id_back);
        tv_title = id_toolbar.findViewById(R.id.tv_tittle);
        iv_back.setOnClickListener(this);
        rvFilter = findViewById(R.id.rvFilterId);
        btn_apply = findViewById(R.id.btn_apply);
        pd = new ProgressDialog(FilterActivity.this, R.style.AppCompatAlertDialogStyle);

        btn_apply.setOnClickListener(this);
        filter_product = new ArrayList<>();

        tv_title.setText(getString(R.string.filter));
        rvFilter.setLayoutManager(new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false));

        filter_adapter = new Filter_Adapter(getApplicationContext(), Constants.AllCategory);
        rvFilter.setAdapter(filter_adapter);


    }

    public void getFilter(final String catId) {
        pd.setMessage("Loading");
        pd.show();
        JsonObject jsonObject = new JsonObject();
        try {
            jsonObject.addProperty("category_id", catId);
            jsonObject.addProperty("farmer_id", constants.getVendror());
        } catch (Exception e) {
            e.printStackTrace();
        }

        (ApiClient.getClient().getFilter(jsonObject)).enqueue(new Callback<FilterPojo>() {
            @Override
            public void onResponse(Call<FilterPojo> call, Response<FilterPojo> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {
                        Log.e("TAG", "filter  response : " + new Gson().toJson(response.body()));
                        FilterPojo filterPojo = response.body();
                        if (filterPojo.getStatus()) {
                            filter_product = filterPojo.getData();
                            saveArrayList(filter_product, "key");
                            Intent intent = new Intent(FilterActivity.this, Filter_All_Product_Activity.class);
                            Bundle args = new Bundle();
                            args.putSerializable("filterdata", (Serializable) filter_product);
                            intent.putExtra("bundle", args);
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(FilterActivity.this, filterPojo.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<FilterPojo> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(FilterActivity.this, getString(R.string.server_errors), Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.id_back:
                onBackPressed();
               // finish();

                break;

            case R.id.btn_apply:
                selected.clear();
                for (int i = 0; i < Constants.AllCategory.size(); i++) {
                    Datum item = Constants.AllCategory.get(i);
                    if (item.getSelected()) {
                        selected.add(item.getId());
                    }
                }

                String selectedItem = android.text.TextUtils.join(",", selected);
                if (selectedItem.endsWith(",")) {
                    selectedItem = selectedItem.substring(0, selectedItem.length() - 1);
                }

                System.out.println("selectedItem=========" + selectedItem);
                getFilter(selectedItem);
                break;
        }
    }

    public void saveArrayList(ArrayList<ProductData> list, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.apply();

    }

    @Override
    public void onBackPressed() {
        //overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
//        Intent intent = new Intent(FilterActivity.this, All_Product_Activity.class);
//        startActivity(intent);
        finish();
    }
}
