package com.alpha.paanilo.locatinenable;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.alpha.paanilo.GpsTracker;
import com.alpha.paanilo.R;
import com.alpha.paanilo.authenticationModule.Login_Activity;
import com.alpha.paanilo.authenticationModule.Splash_Activity;

public class LocationEnableActivity extends AppCompatActivity {

    LocationManager locationManager;
    GpsTracker gpsTracker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_enable);

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);


        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            book_diloge1();
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    gpsTracker = new GpsTracker(getApplicationContext());
                        Intent start = new Intent(LocationEnableActivity.this, Login_Activity.class);
                        startActivity(start);
                        finish();
                        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                        //finish();

                }
            }, 2000);
        }





    }


    public void book_diloge1() {
        final Dialog dialog = new Dialog(LocationEnableActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        dialog.setContentView(R.layout.dailog_location);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        RelativeLayout rlSigninId = dialog.findViewById(R.id.rlSigninId);
        rlSigninId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent1, 2);
                dialog.dismiss();

            }
        });
        dialog.show();

    }





    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2) {
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                book_diloge1();
            } else {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        gpsTracker = new GpsTracker(getApplicationContext());

                            Intent start = new Intent(LocationEnableActivity.this, Login_Activity.class);
                            startActivity(start);
                            finish();
                            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                            //finish();

                    }
                }, 2000);

            }

        }
    }





}