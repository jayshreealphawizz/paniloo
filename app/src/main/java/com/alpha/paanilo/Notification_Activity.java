package com.alpha.paanilo;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.alpha.paanilo.invoicePkg.Invoice_Activity;
import com.alpha.paanilo.model.notificationPkg.NotificationClearPozo;
import com.alpha.paanilo.model.notificationPkg.NotificationInfo;
import com.alpha.paanilo.model.notificationPkg.NotificationPojo;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.seller.SellerAllOrderDetailsActivity;
import com.alpha.paanilo.utility.CheckNetwork;
import com.alpha.paanilo.utility.Constants;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.FacebookSdk.getApplicationContext;

public class Notification_Activity extends AppCompatActivity implements View.OnClickListener {
    AppCompatImageView iv_back;
    RecyclerView rv_city;
    ArrayList<NotificationInfo> notificationList;
    View id_toolbar;
    AppCompatTextView tv_title;
    TextView tv_nodata;
    RelativeLayout rl_notification;
    ProgressDialog pd;
    Constants constants;
    private TextView tvClearAll;
    private SwipeRefreshLayout srlNotification;

    public static String doubleToStringNoDecimal(double d) {
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        formatter.applyPattern("#,###");
        return formatter.format(d);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification_rv);
        init();
        if (CheckNetwork.isNetAvailable(getApplicationContext())) {
            getNotification();
        } else {
            Toast.makeText(getApplicationContext(), "Check Network Connection", Toast.LENGTH_LONG).show();
        }

        srlNotification.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (CheckNetwork.isNetAvailable(getApplicationContext())) {
                    getNotification();
                } else {
                    Toast.makeText(getApplicationContext(), "Check Network Connection", Toast.LENGTH_LONG).show();
                }
                srlNotification.setRefreshing(false);
            }
        });
    }

    private void init() {
        id_toolbar = findViewById(R.id.id_toolbar);
        iv_back = id_toolbar.findViewById(R.id.id_back);
        tv_title = id_toolbar.findViewById(R.id.tv_tittle);
        pd = new ProgressDialog(Notification_Activity.this, R.style.AppCompatAlertDialogStyle);
        constants = new Constants(getApplicationContext());
        srlNotification = findViewById(R.id.srlNotificationId);
        tv_nodata = findViewById(R.id.tv_nodata);
        rv_city = findViewById(R.id.id_rv);
        tvClearAll = findViewById(R.id.tvClearAllId);
        iv_back.setOnClickListener(this);
        tvClearAll.setOnClickListener(this);
        tv_title.setText(getString(R.string.notification));
        notificationList = new ArrayList<>();
//        rv_city.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
//        Collections.reverse(notificationList);
//        LinearLayoutManager layoutMa = new LinearLayoutManager(Notification_Activity.this, RecyclerView.VERTICAL, true);
//        layoutMa.setReverseLayout(true);
//        rv_city.setLayoutManager(layoutMa);

        Collections.reverse(notificationList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        rv_city.setHasFixedSize(true);
        rv_city.setLayoutManager(layoutManager);

    }

    public void getNotification() {
        pd.setMessage("Loading");
        pd.show();
        JsonObject jsonObject = new JsonObject();
        try {
            jsonObject.addProperty("user_id", constants.getUserID());
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e("TAG", "noitifictaion  request : " + new Gson().toJson(jsonObject));
        (ApiClient.getClient().getNotification(jsonObject)).enqueue(new Callback<NotificationPojo>() {
            @Override
            public void onResponse(Call<NotificationPojo> call, Response<NotificationPojo> response) {
                pd.dismiss();
                if (response.isSuccessful()) {
                    try {
                        Log.e("TAG", "noitifictaion  response buyer : " + new Gson().toJson(response.body()));
                        NotificationPojo notificationPojo = response.body();
                        if (notificationPojo.getStatus()) {
                            tv_nodata.setVisibility(View.GONE);
                            tvClearAll.setVisibility(View.VISIBLE);
                            notificationList = notificationPojo.getData();
                            SearchAdapter searchAdapter = new SearchAdapter(getApplicationContext(), notificationList);
                            rv_city.setAdapter(searchAdapter);
                            searchAdapter.notifyDataSetChanged();
                        } else {
                            tv_nodata.setVisibility(View.VISIBLE);
                            tvClearAll.setVisibility(View.GONE);

                            Toast.makeText(Notification_Activity.this, notificationPojo.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<NotificationPojo> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(getApplicationContext(), getString(R.string.server_errors), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvClearAllId:
                if (CheckNetwork.isNetAvailable(getApplicationContext())) {
                    clearNotification();
                } else {
                    Toast.makeText(getApplicationContext(), "Check Network Connection", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.id_back:
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                break;
        }
    }

    public void clearNotification() {
        pd.setMessage("Loading");
        pd.show();
        JsonObject jsonObject = new JsonObject();
        try {
            jsonObject.addProperty("user_id", constants.getUserID());
        } catch (Exception e) {
            e.printStackTrace();
        }
        (ApiClient.getClient().ClearNotification(jsonObject)).enqueue(new Callback<NotificationClearPozo>() {
            @Override
            public void onResponse(Call<NotificationClearPozo> call, Response<NotificationClearPozo> response) {
                pd.dismiss();
                if (response.isSuccessful()) {
                    try {
                        NotificationClearPozo notificationPojo = response.body();
                        if (notificationPojo.getStatus()) {
                            tv_nodata.setVisibility(View.VISIBLE);
                            tvClearAll.setVisibility(View.GONE);
                            rv_city.setVisibility(View.GONE);
                            //  getNotification();
                        } else {
                            tv_nodata.setVisibility(View.VISIBLE);
                            Toast.makeText(Notification_Activity.this, notificationPojo.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<NotificationClearPozo> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(getApplicationContext(), getString(R.string.server_errors), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.MyViewHolder> {
        public ArrayList<NotificationInfo> searchList;
        Context context;
        private LayoutInflater mInflater;

        public SearchAdapter(Context activity, ArrayList<NotificationInfo> List) {
            searchList = List;
            context = activity;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_item, parent, false);
            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
            final NotificationInfo detail = searchList.get(position);
            holder.id_username.setText(detail.getNotification());
            holder.tv_time.setText(Constants.changeTimeFormat(detail.getDateTime()));


            holder.cardclick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Toast.makeText(context, "now clik "+ position, Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(Notification_Activity.this, Invoice_Activity.class);
                    intent.putExtra("saleId", notificationList.get(position).getOrder_id());
                    intent.putExtra("status", notificationList.get(position).getStatus());
                    startActivity(intent);
//                Log.e("banasa ", "banasaoooo1111:::" + notificationList.get(position).getOrder_id());
//                Log.e("banasa ", "banasaSSSS1111::: " + notificationList.get(position).getStatus());


                    //                    Intent intent = new Intent(getApplicationContext() ,Pro)
                  /*  Intent intent = new Intent(Notification_Activity.this, SellerAllOrderDetailsActivity.class);
                intent.putExtra("prodcutname", arrayList.get(position).getUserPhone());
                intent.putExtra("productquantity", completeOrderDatum.getQuantity());
                intent.putExtra("producttotamount", completeOrderDatum.getTotalAmount());
                intent.putExtra("productimage", completeOrderDatum.getProductImage());
                intent.putExtra("productuserid", completeOrderDatum.getUserId());
                intent.putExtra("productSaleid", completeOrderDatum.getSaleId());
                intent.putExtra("productstatus", completeOrderDatum.getStatus());
                intent.putExtra("getDeliveryAddress", completeOrderDatum.getDeliveryAddress());
                intent.putExtra("productdate", completeOrderDatum.getDeliveryTime());
                intent.putExtra("userfullname", completeOrderDatum.getUserFullname());
                intent.putExtra("OrderId" ,  completeOrderDatum.getOrderId());
                intent.putExtra("paymentmethod" ,  completeOrderDatum.getPaymentMethod());
                    //                Log.e("rahul", "rahul Userid " + openOrderDatum.getUserId());
//                Log.e("rahul", "rahul getSaleId " + openOrderDatum.getSaleId());
                startActivity(intent);*/

                }
            });

            String doublevalue = doubleToStringNoDecimal(Double.valueOf(detail.getAmount()));

//            holder.tvRupees.setText(context.getResources().getString(R.string.rupee) + " " + doublevalue);
            holder.tv_invoice.setText(context.getResources().getString(R.string.orderid) + " " + detail.getInvoiceId());
        }

        @Override
        public int getItemCount() {
            return searchList.size();

        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_time, id_username, tv_status, tv_tokenno, tvRupees, tv_invoice;
            ImageView id_userpic;
            CardView cardclick;

            public MyViewHolder(View view) {
                super(view);
                id_username = view.findViewById(R.id.id_username);
                tv_time = view.findViewById(R.id.tv_time);
                tvRupees = view.findViewById(R.id.tvRupeesId);
                tv_invoice = view.findViewById(R.id.tv_invoiceId);

                cardclick = view.findViewById(R.id.cardclick);
            }
        }
    }
}
