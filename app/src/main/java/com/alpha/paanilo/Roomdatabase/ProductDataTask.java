package com.alpha.paanilo.Roomdatabase;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class ProductDataTask  implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "product_id")
    private String product_id;

    public String getDiscount_price() {
        return discount_price;
    }

    public void setDiscount_price(String discount_price) {
        this.discount_price = discount_price;
    }

    public boolean isOff_percentage() {
        return off_percentage;
    }

    public void setOff_percentage(boolean off_percentage) {
        this.off_percentage = off_percentage;
    }

    @ColumnInfo(name = "product_name")
    private String product_name;

    @ColumnInfo(name = "product_description")
    private String product_description;

    @ColumnInfo(name = "product_image")
    private String product_image;

    @ColumnInfo(name = "in_stock")
    private String in_stock;

    @ColumnInfo(name = "price")
    private String price;

    @ColumnInfo(name = "discount_price")
    private String discount_price;

    @ColumnInfo(name = "off_percentage")
    private boolean off_percentage;

    @ColumnInfo(name = "quantity")
    private String quantity;

    @ColumnInfo(name = "total")
    private String total;

    @ColumnInfo(name = "vendorId")
    private String vendorId;


    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_description() {
        return product_description;
    }

    public void setProduct_description(String product_description) {
        this.product_description = product_description;
    }

    public String getProduct_image() {
        return product_image;
    }

    public void setProduct_image(String product_image) {
        this.product_image = product_image;
    }

    public String getIn_stock() {
        return in_stock;
    }

    public void setIn_stock(String in_stock) {
        this.in_stock = in_stock;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }


}