package com.alpha.paanilo.Roomdatabase;

import androidx.room.Database;
import androidx.room.RoomDatabase;


@Database(entities = {ProductDataTask.class}, version = 2, exportSchema = false)
public abstract class AppDatabaseMain extends RoomDatabase {
    public abstract TaskDao taskDao();
}