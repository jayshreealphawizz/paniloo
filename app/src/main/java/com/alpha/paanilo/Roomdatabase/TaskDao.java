package com.alpha.paanilo.Roomdatabase;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface TaskDao {
    @Query("SELECT * FROM ProductDataTask")
    List<ProductDataTask> getAll();

    @Insert
    void insert(ProductDataTask task);

    @Delete
    void delete(ProductDataTask task);

    @Update
    void update(ProductDataTask task);

    @Query("SELECT * FROM ProductDataTask WHERE product_id = :id")
    ProductDataTask getproductid(String id);

    @Query("SELECT vendorId FROM ProductDataTask WHERE vendorId = :vendorId")
    boolean getVendorId(String vendorId);

    @Query("UPDATE ProductDataTask SET price = :amount, quantity = :quntity WHERE product_id =:id")
    void updatea(String amount, String quntity, String id);

    @Query("DELETE FROM ProductDataTask WHERE product_id = :userId")
     void deleteByUserId(String userId);

    @Query("SELECT SUM(price) FROM ProductDataTask")
    String totalSum();

    @Query("SELECT COUNT(*) FROM ProductDataTask")
    String getCount();

    @Query("DELETE FROM ProductDataTask")
    void delete();

}