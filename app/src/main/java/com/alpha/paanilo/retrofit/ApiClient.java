package com.alpha.paanilo.retrofit;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class ApiClient {
    //    public static String BASE_URL = "https://warungkiita.com/adminapp/api/";
//  public static String BASE_URL = "https://alphawizztest.tk/PaaniLo/api/";

    public static String BASE_URL = "https://alphawizztest.tk/PaaniLoo/api/";

    public static String CATEGORY_URL = "https://alphawizztest.tk/PaaniLoo/uploads/category/";
    public static String SUB_CATEGORY_URL = "https://alphawizztest.tk/PaaniLoo/uploads/subcat/";
    public static String SUB_CATEGORY_CHILD_URL = "https://alphawizztest.tk/PaaniLoo/uploads/childsubcat/";
    public static String VENDOR_PROFILE_URL = "https://alphawizztest.tk/PaaniLoo/uploads/profile/";

    public static String USER_PROFILE_URL = "https://alphawizztest.tk/PaaniLoo/uploads/profile/";

    public static String UPLOAD_PRODUCT = "https://alphawizztest.tk/PaaniLoo/uploads/products/";
//    https://alphawizztest.tk/PaaniLoo/uploads/products/
    public static String CATEGORY_PRODUCT_URL = "https://alphawizztest.tk/PaaniLoo/uploads/products/";
    public static String BANNER_URL = "https://alphawizztest.tk/PaaniLoo/uploads/sliders/";

    private static Retrofit retrofit = null;

    public static RestService getClient() {
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
        clientBuilder.connectTimeout(5, TimeUnit.MINUTES);

        // change your base URL
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(clientBuilder.build())
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        //Creating object for our interface
        RestService api = retrofit.create(RestService.class);
        return api; // return the APIInterface object
    }
}