package com.alpha.paanilo.retrofit;

import com.alpha.paanilo.AllOrderModel;
import com.alpha.paanilo.SellerMyProdcutActiveModel;
import com.alpha.paanilo.SellerMyProdcutModel;
import com.alpha.paanilo.StallPkg.stallProfilePkg.StallProfilePozo;
import com.alpha.paanilo.VendorAddProdcutModel;
import com.alpha.paanilo.cart_pkg.addAddressPkg.AddAddressResponseModle;
import com.alpha.paanilo.cart_pkg.showAddressModlePkg.AddressListResponseModle;
import com.alpha.paanilo.cart_pkg.showAddressModlePkg.deleteAddressPkg.DeleteAddressModle;
import com.alpha.paanilo.coupanPkg.couponModlePkg.CouponListPozo;
import com.alpha.paanilo.invoicePkg.InvoicePojo;
import com.alpha.paanilo.model.AllStallPkg.All_StallDataPojo;
import com.alpha.paanilo.model.DownloadInvoicePozo;
import com.alpha.paanilo.model.LoginUserPojo;
import com.alpha.paanilo.model.PaymentSlipPozo;
import com.alpha.paanilo.model.bankDetailPkg.BankDetailPozo;
import com.alpha.paanilo.model.categoryPkg.CategoryPozo;
import com.alpha.paanilo.model.checkoutModlePkg.CheckoutPozo;
import com.alpha.paanilo.model.deliveryPkg.DeliveryChargePozo;
import com.alpha.paanilo.model.filterPkg.FilterPojo;
import com.alpha.paanilo.model.helpPkg.CategoryPozonew;
import com.alpha.paanilo.model.helpPkg.HelpPozo;
import com.alpha.paanilo.model.notificationPkg.NotificationClearPozo;
import com.alpha.paanilo.model.notificationPkg.NotificationCountModel;
import com.alpha.paanilo.model.notificationPkg.NotificationPojo;
import com.alpha.paanilo.model.privacyPolicyPkg.PrivacyPolicyPozo;
import com.alpha.paanilo.model.productByCategory.ProductByCategoryPozo;
import com.alpha.paanilo.model.profilePkg.EditProfilePozo;
import com.alpha.paanilo.model.searchingModlePkg.SearchingPozo;
import com.alpha.paanilo.model.singleProductPkg.OfflineOnlineCheckModel;
import com.alpha.paanilo.model.singleProductPkg.SingleProductPozo;
import com.alpha.paanilo.model.singleProductPkg.SingleProductPozoshop;
import com.alpha.paanilo.model.stallPkg.StallDataPojo;
import com.alpha.paanilo.model.subcatchild_pozo.SubCategoryChildPozo;
import com.alpha.paanilo.model.subcatpozo.SubCategoryPozo;
import com.alpha.paanilo.model.termsAndConditionPkg.TermsAndConditionPozo;
import com.alpha.paanilo.model.vendor_auth_pkg.OtpVerficationRes;
import com.alpha.paanilo.model.vendor_auth_pkg.Vendor_LoginUserInfo;
import com.alpha.paanilo.model.vendor_auth_pkg.Vendor_LoginUserPojo;
import com.alpha.paanilo.myOrderPkg.orderHistoryPkg.CancelOrderPozo;
import com.alpha.paanilo.myOrderPkg.orderHistoryPkg.OrderHistoryPozo;
import com.alpha.paanilo.product_detail_pkg.CouponCountModle;
import com.alpha.paanilo.product_detail_pkg.CouponNotificationModel;
import com.alpha.paanilo.product_detail_pkg.CouponNumberModel;
import com.alpha.paanilo.product_detail_pkg.DeleteRatingModle;
import com.alpha.paanilo.product_detail_pkg.GetPriceModel;
import com.alpha.paanilo.product_detail_pkg.GetRewartModel;
import com.alpha.paanilo.product_detail_pkg.RatingModle;
import com.alpha.paanilo.product_detail_pkg.ReviewModel;
import com.alpha.paanilo.product_pkg.WalletInfo;
import com.alpha.paanilo.product_pkg.WalletPozo;
import com.alpha.paanilo.seller.LoginSellerPojo;
import com.alpha.paanilo.seller.VendorLoginModel;
import com.alpha.paanilo.seller.VendorOptModel;
import com.alpha.paanilo.seller.WithdrawMondeyModel;
import com.alpha.paanilo.seller.WithdrawPozo;
import com.alpha.paanilo.seller.model.VerndorReportPojo;
import com.alpha.paanilo.seller.model.completeorder.CompleteOrderPojo;
import com.alpha.paanilo.seller.model.notification.SellerNotificationDetailPojo;
import com.alpha.paanilo.seller.model.notification.SellerNotificationPojo;
import com.alpha.paanilo.seller.model.openorder.OnlineStatusmodel;
import com.alpha.paanilo.seller.model.openorder.OpenOrderPojo;
import com.google.gson.JsonObject;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface RestService {
//    @POST("Authentication/customerRegistration")
//    Call<LoginUserPojo> signUp(@Body LoginUserInfo jsonObject);
//    @POST("Authentication/customerRegistration")
//    Call<LoginUserPojo> signUp(@Body LoginUserInfo jsonObject);


    @Multipart
    @POST("Authentication/customerRegistration")
    Call<LoginUserPojo> signUp(@Part MultipartBody.Part user_fullname,
                               @Part MultipartBody.Part user_email,
                               @Part MultipartBody.Part user_password,
                               @Part MultipartBody.Part address,
                               @Part MultipartBody.Part user_phone,
                               @Part MultipartBody.Part firebasetoken,
                               @Part MultipartBody.Part user_image);

    @FormUrlEncoded
    @POST("Authentication/customerLogin")
    Call<LoginUserPojo> login(@Field("email") String email,
                              @Field("user_password") String user_password,
                              @Field("firebaseToken") String firebaseToken);

    //    https://alphawizztest.tk/PaaniLo/api/Authentication/otpVarification
    @FormUrlEncoded
    @POST("Authentication/otpVarification")
    Call<OtpVerficationRes> otpVerfication(@Field("otp") String otp,
                                           @Field("mobile") String mobile);


    @POST("Authentication/vendorRegistration")
    Call<Vendor_LoginUserPojo> signUpVendor(@Body Vendor_LoginUserInfo jsonObject);


    @POST("Authentication/resetPasswordForCustomer")
    Call<JsonObject> getForgetPassword(@Body JsonObject jsonObject);


    @POST("Payment/add_money_to_wallet")
    Call<WalletInfo> addWalletAmount(@Body JsonObject jsonObject);


    @POST("Payment/wallet_history")
    Call<WalletPozo> getwallethistory(@Body JsonObject jsonObject);


//    @POST("Authentication/customerLogin")
//    Call<LoginUserPojo> login(@Body LoginUserInfo jsonObject);

    @FormUrlEncoded
    @POST("Products/AddReviews")
    Call<RatingModle> addReview(@Field("product_id") String product_id,
                                @Field("user_id") String user_id,
                                @Field("comments") String comments,
                                @Field("water_quality") String water_quality,
                                @Field("bottel_quality") String bottel_quality,
                                @Field("deliver_on_time") String deliver_on_time);

    @FormUrlEncoded
    @POST("Products/removeReviews")
    Call<DeleteRatingModle> removeReview(@Field("review_id") String vendor_id,
                                         @Field("user_id") String user_id);


//    @FormUrlEncoded
//    @POST("Authentication/add_coupon_amount_vendor_wallet")
//    Call<CouponCountModle> getCouponCount(@Field("vendor_id") String vendor_id,
//                                         @Field("amount") String amount);


    @POST("Authentication/add_coupon_amount_wallet")
    Call<CouponCountModle> getCoupanCount(@Body JsonObject jsonObject);
//ram

    @FormUrlEncoded
    @POST("Authentication/get_coupon_count")
    Call<CouponNumberModel> getCoupanNumber(@Field("user_id") String user_id);

    //    https://alphawizztest.tk/PaaniLoo/api/Authentication/get_coupon_price
    @GET("Authentication/get_coupon_price")
    Call<GetPriceModel> getCouponPrice();
//    https://alphawizztest.tk/PaaniLoo/api/Authentication/add_coupon_amount_vendor_wallet

//    https://alphawizztest.tk/PaaniLoo/api/Authentication/send_notification_coupon_count

    @FormUrlEncoded
    @POST("Authentication/send_notification_coupon_count")
    Call<CouponNotificationModel> getNotificationCounter(@Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("Products/getReviews")
    Call<ReviewModel> gerReviewProductId(@Field("vendor_id") String vendor_id);


//    @POST("Authentication/customerLogin")
//    Call<LoginUserPojo> login(@Body String jsonBody); //2


//    @Multipart
//    @POST("User_Dashboard/updateCustomerProfile")
//    Call<EditProfilePozo> editProfile(@Part MultipartBody.Part user_id,
//                                      @Part MultipartBody.Part user_fullname,
//                                      @Part MultipartBody.Part user_email,
//                                      @Part MultipartBody.Part user_phone,
//                                      @Part MultipartBody.Part user_bdate,
//                                      @Part MultipartBody.Part user_city,
//                                      @Part MultipartBody.Part user_image
//    );

    @Multipart
    @POST("User_Dashboard/updateCustomerProfile")
    Call<EditProfilePozo> editProfile(@Part MultipartBody.Part user_id,
                                      @Part MultipartBody.Part user_fullname,
                                      @Part MultipartBody.Part user_email,
                                      @Part MultipartBody.Part user_phone,
                                      @Part MultipartBody.Part user_bdate,
                                      @Part MultipartBody.Part user_city,
                                      @Part MultipartBody.Part user_image);

    @Multipart
    @POST("User_Dashboard/updateVendorProfile")
    Call<EditProfilePozo> editProfileupdateVendorProfile(@Part MultipartBody.Part user_id,
                                                         @Part MultipartBody.Part user_fullname,
                                                         @Part MultipartBody.Part user_email,
                                                         @Part MultipartBody.Part user_phone,
                                                         @Part MultipartBody.Part user_bdate,
                                                         @Part MultipartBody.Part user_city,
                                                         @Part MultipartBody.Part user_image);

    @Multipart
    @POST("Authentication/transeferPaySlip")
    Call<PaymentSlipPozo> transeferPaySlip(@Part MultipartBody.Part user_id,
                                           @Part MultipartBody.Part file);

    @POST("Authentication/GetMyInvoice")
    Call<InvoicePojo> getMyInvoice(@Body JsonObject jsonObject);

    @POST("SocialLogin/Login")
    Call<LoginUserPojo> social_login(@Body JsonObject jsonObject);

    @Headers("Content-Type: application/json")
    @POST("Authentication/checkout")
    Call<CheckoutPozo> checkout(@Body String jsonBody);

    @GET("Authentication/terms_conditions")
    Call<TermsAndConditionPozo> termsCondition();

    @GET("Authentication/help")
    Call<HelpPozo> help();

//    @GET("Authentication/getcategory")
//    Call<CategoryPozonew> getCategory();


    @GET("Authentication/privacy_policy")
    Call<PrivacyPolicyPozo> privacy_policy();


    @GET("Authentication/couponListing")
    Call<CouponListPozo> couponListing();

//    @GET("Authentication/Banner")
//    Call<BannerPojo> getAllBanner();

    @GET("Authentication/allCategories")
    Call<CategoryPozo> allCategories();

    @POST("Authentication/getAdminDetails")
    Call<BankDetailPozo> getAdminDetails(@Body JsonObject jsonObject);

    @FormUrlEncoded
    @POST("Authentication/allSubCategories")
    Call<SubCategoryPozo> getAllSubCategory(@Field("category_id") String category_id);

    @FormUrlEncoded
    @POST("Products/ChildCategory")
    Call<SubCategoryChildPozo> getAllSubChildCategory(@Field("subcat_id") String subcat_id);

    @GET("Products/searchProducts")
    Call<SearchingPozo> searchProducts();

    @POST("Authentication/showDeliveryCharges")
    Call<DeliveryChargePozo> showDeliveryCharges(@Body JsonObject jsonObject);


//    https://alphawizztest.tk/PaaniLoo/api/Authentication/reward_list

    @FormUrlEncoded
    @POST("Authentication/reward_list")
    Call<GetRewartModel> getRewartList(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("Authentication/redeem_reward")
    Call<GetRewartModel> redeenRewart(@Field("reward_id") String reward_id,
                                      @Field("user_id") String user_id,
                                      @Field("amount") String amount);

//    https://alphawizztest.tk/PaaniLoo/api/Authentication/redeem_reward

 /*   @POST("Authentication/showDeliveryCharges")
    Call<DeliveryChargePozo> showDeliveryCharges(@Body JsonObject jsonObject);*/

    @POST("Authentication/filterByCategory")
    Call<FilterPojo> getFilter(@Body JsonObject jsonObject);

    @POST("Authentication/GetNotificationList")
    Call<NotificationPojo> getNotification(@Body JsonObject jsonObject);


    @FormUrlEncoded
    @POST("Authentication/clearNotification")
    Call<NotificationCountModel> clearNotification(@Field("user_id") String user_id);


    @POST("Authentication/ClearNotification")
    Call<NotificationClearPozo> ClearNotification(@Body JsonObject jsonObject);

    @POST("Authentication/myOrder")
    Call<OrderHistoryPozo> getMyOrder(@Body JsonObject jsonObject);

    @POST("PDF/invoice")
    Call<DownloadInvoicePozo> invoice(@Body JsonObject jsonObject);

    @POST("Authentication/cancelOrder")
    Call<CancelOrderPozo> cancelOrder(@Body JsonObject jsonObject);

    @POST("Authentication/getProductByCategory")
    Call<ProductByCategoryPozo> getProductByCategory(@Body JsonObject jsonObject);

    @POST("Authentication/getVendorandproductbyid")
    Call<StallProfilePozo> getVendorandproductbyid(@Body JsonObject jsonObject);

    @POST("Authentication/singleProductDescription")
    Call<SingleProductPozo> getProductDetail(@Body JsonObject jsonObject);


    //    https://alphawizztest.tk/PaaniLoo/api/Authentication/check_vendor_online_offline
    @FormUrlEncoded
    @POST("Authentication/check_vendor_online_offline")
    Call<OfflineOnlineCheckModel> checkvendoronlineoffline(@Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("Authentication/shop_details")
    Call<SingleProductPozoshop> getProductDetailshop(@Field("user_id") String user_id);


    @POST("Authentication/weekendPromo")
        //farmer_id:55
    Call<ProductByCategoryPozo> weekendProduct(@Body JsonObject jsonObject);

    @POST("Authentication/stallsNearbyMe")
    Call<StallDataPojo> getstallnearbyme(@Body JsonObject jsonObject);

    @POST("Authentication/AllStallNeayByCategory")
    Call<All_StallDataPojo> getstallnearbyme_bycat(@Body JsonObject jsonObject);

    @FormUrlEncoded
    @POST("Authentication/getDeliveryAddress")
    Call<AddressListResponseModle> getDeliveryAddress(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("Authentication/deleteAddress")
    Call<DeleteAddressModle> deleteAddress(
            @Field("user_id") String user_id,
            @Field("delivery_id") String delivery_id);

    @FormUrlEncoded
    @POST("Authentication/editDeliveryAddress")
    Call<AddAddressResponseModle> editDeliveryAddress(@Field("user_id") String user_id,
                                                      @Field("delivery_address") String delivery_address,
                                                      @Field("landmark") String landmark,
                                                      @Field("area") String area,
                                                      @Field("city") String city,
                                                      @Field("state") String state,
                                                      @Field("pincode") String pincode,
                                                      @Field("mobile") String mobile,
                                                      @Field("delivery_id") String delivery_id,
                                                      @Field("delivery_type") String delivery_type,
                                                      @Field("latitude") String latitude,
                                                      @Field("longitude") String longitude);

    @FormUrlEncoded
    @POST("Authentication/addDeliveryAddress")
    Call<AddAddressResponseModle> addAddress(@Field("user_id") String user_id,
                                             @Field("delivery_address") String delivery_address,
                                             @Field("landmark") String landmark,
                                             @Field("area") String area,
                                             @Field("city") String city,
                                             @Field("state") String state,
                                             @Field("pincode") String pincode,
                                             @Field("mobile") String mobile,
                                             @Field("delivery_type") String delivery_type,
                                             @Field("latitude") String latitude,
                                             @Field("longitude") String longitude);


    ///////////////seller api implementation

    @Multipart
    @POST("Authentication/vendorRegistration")
    Call<LoginSellerPojo> signUpSeller(@Part MultipartBody.Part store_name,

                                       @Part MultipartBody.Part user_fullname,
                                       @Part MultipartBody.Part user_email,
                                       @Part MultipartBody.Part user_password,
                                       @Part MultipartBody.Part user_phone,
                                       @Part MultipartBody.Part current_location,
                                       @Part MultipartBody.Part latitude,
                                       @Part MultipartBody.Part longitude,
                                       @Part MultipartBody.Part firebasetoken,
                                       @Part MultipartBody.Part user_image,
                                       @Part MultipartBody.Part[] user_document

    );


   /* @Multipart
    @POST("Authentication/vendorRegistration")
    Call<LoginSellerPojo> signUpSellernew(@Part("user_fullname") RequestBody user_fullname,
                                          @Part("user_email") RequestBody user_email,
                                          @Part("user_password") RequestBody user_password,
                                          @Part("user_phone") RequestBody user_phone,
                                          @Part("current_location") RequestBody current_location,
                                          @Part("latitude") RequestBody latitude,
                                          @Part("longitude") RequestBody longitude,
                                          @Part("firebaseToken") RequestBody firebaseToken,
                                          @Part MultipartBody.Part user_image,
//                                          @Part List<MultipartBody.Part> user_document
                                          @Part MultipartBody.Part[] user_document

    );//5*/


//    @Part List<MultipartBody.Part> user_document
//    @Part List<MultipartBody.Part> user_document
//    @Part MultipartBody.Part[] user_document

//    @Part MultipartBody.Part user_document[]


//    @Multipart
//    @POST("Authentication/updateOrderStatus")
//    Call<DeleteAddressModle> updateOrderStatus(@Part MultipartBody.Part user_id,
//                                               @Part MultipartBody.Part sale_id,
//                                               @Part MultipartBody.Part status,
//                                               @Part MultipartBody.Part order_delivery_time
//
//    );


    @FormUrlEncoded
    @POST("Products/updateOrderStatus")
    Call<DeleteAddressModle> updateOrderStatus(@Field("user_id") String user_id,
                                               @Field("sale_id") String sale_id,
                                               @Field("status") String status
    );


    @FormUrlEncoded
    @POST("Authentication/vendorLogin")
    Call<VendorLoginModel> getVendorLogin(@Field("user_email") String user_email,
                                          @Field("user_password") String user_password,
                                          @Field("firebaseToken") String firebaseToken);


    @FormUrlEncoded
    @POST("Authentication/otpVendorVarification")
    Call<VendorOptModel> getOTPVendor(@Field("otp") String otp);


    @FormUrlEncoded
    @POST("Products/vendorCompleteOrders")
    Call<CompleteOrderPojo> getCompleteOrderList(@Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("Products/vendorAllOrders")
    Call<AllOrderModel> getAllOrderList(@Field("user_id") String user_id,
                                        @Field("date") String date);


    @FormUrlEncoded
    @POST("Products/vendorAllNotification")
    Call<SellerNotificationPojo> getSellerNotificationList(@Field("user_id") String user_id);


//    https://alphawizztest.tk/PaaniLoo/api/Products/vendorOrders_byorder_id

    @FormUrlEncoded
    @POST("Products/vendorOrders_byorder_id")
    Call<SellerNotificationDetailPojo> vendorOrdersbyorderid(@Field("order_id") String order_id);


    @FormUrlEncoded
    @POST("products/clearNotificationFarmer")
    Call<NotificationCountModel> clearNotificationFarmer(@Field("farmer_id") String farmer_id);


    @FormUrlEncoded
    @POST("Products/vendorOpenOrders")
    Call<OpenOrderPojo> getVenderOpenOrders(@Field("user_id") String user_id);

    //    https://alphawizztest.tk/PaaniLoo/api/Authentication/vendor_online_offline
    @FormUrlEncoded
    @POST("Authentication/vendor_online_offline")
    Call<OnlineStatusmodel> vendoronlineoffline(@Field("user_id") String user_id,
                                                @Field("status") String status);


    @FormUrlEncoded
    @POST("Authentication/otpVendorVarification")
    Call<VerndorReportPojo> otpVendorVarification(@Field("otp") String otp);

    @FormUrlEncoded
    @POST("Products/vendorReport")
    Call<VerndorReportPojo> getVenderReport(@Field("user_id") String user_id);

    @POST("Payment/withdraw_money")
    Call<WithdrawMondeyModel> withdrawMoney(@Body JsonObject jsonObject);

    @POST("Payment/vendor_wallet_history")
    Call<WithdrawPozo> walletHistory(@Body JsonObject jsonObject);


//    @FormUrlEncoded
//    @POST("Products/addvendorProduct")
//    Call<SellerNaddVendorprodcutpojootificationPojo> getaddvendorProduct(@Field("product_name") String product_name,
//                                                     @Field("unit") String unit,
//                                                     @Field("price") String price,
//                                                     @Field("quantity") String quantity,
//                                                     @Field("product_description") String product_description,
//                                                     @Field("category_id") String category_id);


    /*@Multipart
    @POST("Products/addvendorProduct")
    Call<VendorAddProdcutModel> getaddvendorProduct(@Part MultipartBody.Part product_name,
                                                    @Part MultipartBody.Part unit,
                                                    @Part MultipartBody.Part price,
                                                    @Part MultipartBody.Part quantity,
                                                    @Part MultipartBody.Part product_description,
                                                    @Part MultipartBody.Part category_id,
                                                    @Part MultipartBody.Part product_image,
                                                    @Part MultipartBody.Part[] thumbnails_image);*/


    @Multipart
    @POST("Products/addvendorProduct")
    Call<VendorAddProdcutModel> getaddvendorProductnew(@Part("price") RequestBody price,
                                                       @Part("product_description") RequestBody product_description,
                                                       @Part("vendor_id") RequestBody vendor_id
            /* @Part MultipartBody.Part[] thumbnails_image*/);//5

    @Multipart
    @POST("Products/editvendorProduct")
    Call<VendorAddProdcutModel> getaddvendorProductEdit(@Part("product_id") RequestBody product_id,
                                                        @Part("price") RequestBody price,
                                                        @Part("product_description") RequestBody product_description,
                                                        @Part("vendor_id") RequestBody vendor_id





            /* @Part MultipartBody.Part[] thumbnails_image*/);//5


    //    https://alphawizztest.tk/PaaniLoo/api/products/product_active_deactive
    @FormUrlEncoded
    @POST("products/product_active_deactive")
    Call<SellerMyProdcutActiveModel> getProductactive(@Field("product_id") String product_id,
                                                      @Field("user_id") String user_id,
                                                      @Field("status") String status);


//    @Multipart
//    @POST("Products/get_my_product")
//    Call<SellerMyProdcutModel> getMyProduct(@Field("user_id") String user_id);//5

    @FormUrlEncoded
    @POST("Products/get_my_product")
    Call<SellerMyProdcutModel> getMyProduct(@Field("user_id") String user_id);


//    https://alphawizztest.tk/PaaniLo/api/Products/vendorOpenOrders
//    https://alphawizztest.tk/PaaniLo/api/Payment/vendor_wallet_history
//    https://alphawizztest.tk/PaaniLo/api/Payment/withdraw_money
//    https://alphawizztest.tk/PaaniLo/api/Products/vendorReport
//    https://alphawizztest.tk/PaaniLo/api/Authentication/otpVendorVarification
//    https://alphawizztest.tk/PaaniLo/api/Authentication/vendorRegistration
//    https://alphawizztest.tk/PaaniLo/api/Products/vendorAllNotification
//    https://alphawizztest.tk/PaaniLo/api/Products/addvendorProduct

}

