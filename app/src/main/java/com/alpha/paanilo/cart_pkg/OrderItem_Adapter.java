package com.alpha.paanilo.cart_pkg;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.alpha.paanilo.R;
import com.alpha.paanilo.Roomdatabase.DatabaseClient;
import com.alpha.paanilo.Roomdatabase.ProductDataTask;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.utility.Constants;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

public class OrderItem_Adapter extends RecyclerView.Adapter<OrderItem_Adapter.MyViewHolder> {
    private final ProductitemOnClickListener productitemOnClickListener;
    Context context;
    int numtest, subtotal, mainprice;
    List<ProductDataTask> tasks;
    private LayoutInflater mInflater;


    public OrderItem_Adapter(Context activity, ProductitemOnClickListener productitemOnClickListener) {
        this.productitemOnClickListener = productitemOnClickListener;
        context = activity;
    }

    public void allproductlist(List<ProductDataTask> tasks) {
        this.tasks = tasks;
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.orderconfirm_order_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        try {
            final ProductDataTask detail = tasks.get(position);

//            holder.id_username.setText(detail.getProduct_name());
            holder.tvProductPriceId.setText(context.getResources().getString(R.string.rupee) + " " + Constants.getTotalWithSeparater("" + detail.getTotal()));


            Log.e("detail.getQuantity()", "detail.getQuantity()" + detail.getQuantity());


            holder.quntity.setText(detail.getQuantity());
            holder.decrement.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        holder.increment.setEnabled(true);
                        numtest = Integer.parseInt(holder.quntity.getText().toString());
                        // mainprice = Integer.parseInt(detail.getTotal());
                        mainprice = Integer.parseInt(tasks.get(position).getTotal());
                        if (numtest > 1) {
                            numtest = numtest - 1;
                            holder.quntity.setText(numtest + "");
                            subtotal = numtest * mainprice;
                            // holder.tvProductPriceId.setText(context.getResources().getString(R.string.rupee) + subtotal);
                            updateTask(detail.getProduct_id());
                            productitemOnClickListener.carOnAmountRefresh();
                        } else {
                            productitemOnClickListener.carOnDelete(position, tasks.get(position));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });

            //updateTask(detail.getProduct_id());
            holder.increment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    numtest = Integer.parseInt(holder.quntity.getText().toString());
                    mainprice = Integer.parseInt(tasks.get(position).getTotal());
                    numtest = numtest + 1;
                    if (numtest <= Integer.parseInt(tasks.get(position).getIn_stock())) {
                        holder.quntity.setText("" + numtest);
                        subtotal = numtest * mainprice;
                        updateTask(detail.getProduct_id());
                    } else {
                        holder.increment.setEnabled(false);
                        Toast.makeText(context, "You can select a maximum " + tasks.get(position).getIn_stock() + " at one time", Toast.LENGTH_LONG).show();
                    }

                    productitemOnClickListener.carOnAmountRefresh();
                }
            });

            Log.e("rahulraj ", "rahulrajimage " + detail.getProduct_image());

//            if (detail.getProduct_image().equals("") || detail.getProduct_image().isEmpty()) {
//            } else {
//
//                String imgName = detail.getProduct_image().substring((detail.getProduct_image().lastIndexOf(",") + 1));
//
//                Log.e("pro", "imgName" + imgName);
//
//                Glide.with(context)
//                        .load(ApiClient.CATEGORY_PRODUCT_URL + imgName)
//                        .placeholder(R.drawable.placeholder)
//                        .fitCenter()
//                        .diskCacheStrategy(DiskCacheStrategy.ALL)
//                        .dontTransform()
//                        .into(holder.ivProduct);
//            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
//        return tasks == null ? 0 : tasks.size();
        return tasks == null ? 0 : 1;
    }

    private void updateTask(final String id) {
        class UpdateTask extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... voids) {
                DatabaseClient.getInstance(context).getAppDatabase()
                        .taskDao()
                        .updatea(String.valueOf(subtotal), String.valueOf(numtest), id);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                getCartCount();
            }
        }

        UpdateTask ut = new UpdateTask();
        ut.execute();
    }

    private void getCartCount() {
        class GetTasks extends AsyncTask<Void, Void, String> {
            @Override
            protected String doInBackground(Void... voids) {
                String taskList = DatabaseClient
                        .getInstance(context)
                        .getAppDatabase()
                        .taskDao()
                        .getCount();
                return taskList;
            }

            @Override
            protected void onPostExecute(String tasks) {
                super.onPostExecute(tasks);
                // AddCartCustomModel.getInstance().setCartCount("" + tasks);
            }
        }
        GetTasks gt = new GetTasks();
        gt.execute();
    }


    public interface ProductitemOnClickListener {
        void addtotal(String as);

        void carOnAmountRefresh();

        void carOnDelete(int position, ProductDataTask datum);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final ImageView ivProduct;
        private final TextView id_username;
        private final TextView tvProductPriceId;
        private final TextView quntity;
        private final AppCompatImageView decrement;
        private final AppCompatImageView increment;

        public MyViewHolder(View view) {
            super(view);
            ivProduct = view.findViewById(R.id.ivProductId);
            id_username = view.findViewById(R.id.id_username);
            tvProductPriceId = view.findViewById(R.id.tvProductPriceId);
            quntity = view.findViewById(R.id.quntity);
            decrement = view.findViewById(R.id.decrement);
            increment = view.findViewById(R.id.increment);
        }
    }
}