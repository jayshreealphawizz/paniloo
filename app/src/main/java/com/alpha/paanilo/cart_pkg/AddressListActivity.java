package com.alpha.paanilo.cart_pkg;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.alpha.paanilo.HomeMainActivity;
import com.alpha.paanilo.R;
import com.alpha.paanilo.cart_pkg.showAddressModlePkg.AddressList;
import com.alpha.paanilo.cart_pkg.showAddressModlePkg.AddressListResponseModle;
import com.alpha.paanilo.cart_pkg.showAddressModlePkg.deleteAddressPkg.DeleteAddressModle;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.utility.AppSession;
import com.alpha.paanilo.utility.CheckNetwork;
import com.alpha.paanilo.utility.Constants;
import com.alpha.paanilo.utility.CustomProgressbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddressListActivity extends AppCompatActivity implements View.OnClickListener, AddressListadapter.AddressOnClick {
    private RelativeLayout rlHomeDelivery, rlPickup;
    private View id_toolbar;
    private String userId, addressEntry;
    private CardView mbtSaveAndSend;
    private RecyclerView rvAddresslist;
    private LinearLayoutManager linearLayoutManager;
    private AddressListadapter addressListadapter;
    private List<AddressList> addressList;
    private SwipeRefreshLayout srlAddressList;
    private AppCompatTextView tvAddressUnavailable, tv_title;
    AppCompatImageView iv_back;
    Constants constants;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_list_frag);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        constants = new Constants(getApplicationContext());
        userId = constants.getUserID();
        addressEntry = AppSession.getStringPreferences(getApplicationContext(), "addEntry");
        init();
        if (CheckNetwork.isNetAvailable(this)) {
            addressList(userId);
        } else {
            Toast.makeText(this, "Check Network Connection", Toast.LENGTH_LONG).show();
        }

        srlAddressList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (CheckNetwork.isNetAvailable(AddressListActivity.this)) {
                    addressList(userId);
                } else {
                    Toast.makeText(AddressListActivity.this, "Check Network Connection", Toast.LENGTH_LONG).show();
                }
                srlAddressList.setRefreshing(false);
            }
        });
    }


    private void init() {
        tvAddressUnavailable = findViewById(R.id.tvAddressUnavailableId);
        srlAddressList = findViewById(R.id.srlAddressListId);
        rvAddresslist = findViewById(R.id.rvAddresslistId);
        mbtSaveAndSend = findViewById(R.id.mbtSaveAndSendId);
        id_toolbar = findViewById(R.id.id_toolbar);
        tv_title = id_toolbar.findViewById(R.id.tv_tittle);
        tv_title.setText(getString(R.string.AddressList));
        iv_back = id_toolbar.findViewById(R.id.id_back);
        iv_back.setOnClickListener(this);
        mbtSaveAndSend.setOnClickListener(this);
        linearLayoutManager = new LinearLayoutManager(AddressListActivity.this, RecyclerView.VERTICAL, false);
        rvAddresslist.setLayoutManager(linearLayoutManager);
        addressListadapter = new AddressListadapter(AddressListActivity.this, this);
        rvAddresslist.setAdapter(addressListadapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mbtSaveAndSendId:
                Intent intent = new Intent(AddressListActivity.this, AddAddressActivity.class);
                intent.putExtra("listAddress", "list");
                startActivityForResult(intent, 2);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                // CheckNetwork.nextScreenWithoutFinish(getActivity(), DeliveryDetailFrag.class);
                break;
            case R.id.id_back:
                onBackPressed();
                break;

        }
    }

    @Override
    public void onBackPressed() {
        if (addressEntry.equalsIgnoreCase("home")) {
            Intent intent = new Intent(getApplicationContext(), HomeMainActivity.class);
            startActivity(intent);
            finish();
        } else {
            finish();
        }
    }

    @Override
    public void addOnClick(View view, int position, AddressList addressList) {
        switch (view.getId()) {
            case R.id.ivDeleteAddressid:
                deleteAddress(addressList.getDeliveryId(), position);
                break;
            case R.id.ivEditAddressId:
                Intent bundle = new Intent(AddressListActivity.this, EditAddressActivity.class);
                bundle.putExtra("deliveryid", addressList.getDeliveryId());
                bundle.putExtra("area", addressList.getArea());
                bundle.putExtra("city", addressList.getCity());
                bundle.putExtra("deliveryAdd", addressList.getDeliveryAddress());
                bundle.putExtra("landmark", addressList.getLandmark());
                bundle.putExtra("mobile", addressList.getMobile());
                bundle.putExtra("pincode", addressList.getPincode());
                bundle.putExtra("state", addressList.getState());
                bundle.putExtra("listAddress", "list");
                bundle.putExtra("deliverytype", addressList.getDelivery_type());

                bundle.putExtra("latitude",  addressList.getLatitude());
                bundle.putExtra("longitude",  addressList.getLongitude());

//                constants.setaddLatitude(addAddressResponseModle.getAddAddress().getLatitude());
//                constants.setaddLongitude(addAddressResponseModle.getAddAddress().getLongitude());
                Log.e("rahulraj" ,"rahulrajgetLatitude"+addressList.getLatitude() );
                Log.e("rahulraj" ,"rahulrajgetLongitude"+addressList.getLongitude() );

                startActivityForResult(bundle, 2);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                break;

        }
    }


    private void addressList(String userId) {
        CustomProgressbar.showProgressBar(this, false);
        ApiClient.getClient().getDeliveryAddress(userId).enqueue(new Callback<AddressListResponseModle>() {
            @Override
            public void onResponse(Call<AddressListResponseModle> call, Response<AddressListResponseModle> response) {
                if (response.isSuccessful()) {
                    CustomProgressbar.hideProgressBar();
                    AddressListResponseModle addAddressResponseModle = response.body();
                    if (addAddressResponseModle.getStatus()) {
                        addressList = addAddressResponseModle.getAddressList();
                        addressListadapter.addAddressList(addressList);
                        tvAddressUnavailable.setVisibility(View.GONE);
                    } else {
                        //  Toast.makeText(getActivity(), addAddressResponseModle.getMessage(), Toast.LENGTH_LONG).show();
                        tvAddressUnavailable.setVisibility(View.VISIBLE);
                    }
                } else {
                    if (response.code() == 400) {
                        if (!false) {
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(response.errorBody().string());
                                CustomProgressbar.hideProgressBar();
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<AddressListResponseModle> call, Throwable t) {
                CustomProgressbar.hideProgressBar();
            }
        });


    }

    private void deleteAddress(final String addressId, final int position) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.delete_account_dailoge);
        AppCompatButton no = dialog.findViewById(R.id.mbtnNoId);
        AppCompatButton mbtnYes = dialog.findViewById(R.id.mbtnYesId);
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        mbtnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckNetwork.isNetAvailable(AddressListActivity.this)) {
                    delete(addressId, dialog, position);
                } else {
                    Toast.makeText(AddressListActivity.this, "Check network Connection", Toast.LENGTH_LONG).show();
                }

            }
        });

        dialog.show();
    }


    private void delete(String addressId, final Dialog dialog, final int position) {
        CustomProgressbar.showProgressBar(AddressListActivity.this, false);
        ApiClient.getClient().deleteAddress(userId, addressId).enqueue(new Callback<DeleteAddressModle>() {
            @Override
            public void onResponse(Call<DeleteAddressModle> call, Response<DeleteAddressModle> response) {
                if (response.isSuccessful()) {
                    CustomProgressbar.hideProgressBar();
                    DeleteAddressModle addAddressResponseModle = response.body();
                    if (addAddressResponseModle.getStatus()) {
                        dialog.dismiss();
                        addressList.remove(position);
                        addressListadapter.addAddressList(addressList);
                        Toast.makeText(AddressListActivity.this, addAddressResponseModle.getMessage(), Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(AddressListActivity.this, addAddressResponseModle.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    if (response.code() == 400) {
                        if (!false) {
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(response.errorBody().string());
                                CustomProgressbar.hideProgressBar();
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<DeleteAddressModle> call, Throwable t) {
                CustomProgressbar.hideProgressBar();
            }
        });


    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if (requestCode == 2) {
            if (CheckNetwork.isNetAvailable(this)) {
                addressList(userId);
            } else {
                Toast.makeText(this, "Check Network Connection", Toast.LENGTH_LONG).show();
            }
        }
    }


}
