package com.alpha.paanilo.cart_pkg;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import com.alpha.paanilo.R;
import com.alpha.paanilo.cart_pkg.addAddressPkg.AddAddressResponseModle;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.utility.CheckNetwork;
import com.alpha.paanilo.utility.Constants;
import com.alpha.paanilo.utility.CustomProgressbar;
import com.alpha.paanilo.utility.CustomToast;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddAddressActivity extends AppCompatActivity implements View.OnClickListener {
    AppCompatTextView tv_title;
    AppCompatImageView iv_back;
    Constants constants;
    RadioGroup groupid;
    RadioButton radioHome, radioWork, radioOther;
    String selectedSuperStar;
    int AUTOCOMPLETE_REQUEST_CODE = 1;
    Double vendor_lat, vendor_long;
    String lat, longt;
    private TextInputEditText tieCity, tieEnterLandmark, tieState,
            tieSubArea, tieEnterDeliveryAdd, tiePinCode, tieMobilNumberAdd;
    private CardView mbtSaveDeliveryAdd;
    private AppCompatImageView ivBackDelivery;
    private String userid, listAddress;
    private View id_toolbar;
    private Animation shakeAnimation;
    private String userId;
    private Geocoder geocoder;
    private GoogleMap mMap;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.delivery_detail_frag);
        listAddress = getIntent().getStringExtra("listAddress");
        init();
    }

    private void init() {
        tieEnterLandmark = findViewById(R.id.tieEnterLandmarkId);
        tieCity = findViewById(R.id.tieCityId);
        tieMobilNumberAdd = findViewById(R.id.tieMobilNumberAddId);
        tieState = findViewById(R.id.tieStateId);
        groupid = findViewById(R.id.groupradio);
        radioHome = findViewById(R.id.radioHome);
        radioWork = findViewById(R.id.radioWork);
        radioOther = findViewById(R.id.radioOther);
        geocoder = new Geocoder(AddAddressActivity.this, Locale.getDefault());
        Places.initialize(AddAddressActivity.this, getString(R.string.google_maps_key));

        groupid.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (radioHome.isChecked()) {
                    selectedSuperStar = radioHome.getText().toString();
                    //   Toast.makeText(AddAddressActivity.this, "11 " + selectedSuperStar , Toast.LENGTH_SHORT).show();
                } else if (radioWork.isChecked()) {
                    selectedSuperStar = radioWork.getText().toString();
                    //  Toast.makeText(AddAddressActivity.this, "22 " + selectedSuperStar , Toast.LENGTH_SHORT).show();

                } else if (radioOther.isChecked()) {
                    selectedSuperStar = radioOther.getText().toString();
                    //  Toast.makeText(AddAddressActivity.this, "33 " + selectedSuperStar , Toast.LENGTH_SHORT).show();

                }
            }
        });

//        if (radioHome.isChecked()) {
//            selectedSuperStar = radioHome.getText().toString();
//           // Toast.makeText(this, "se place::; " + selectedSuperStar, Toast.LENGTH_SHORT).show();
//
//        } else if (radioWork.isChecked()) {
//            selectedSuperStar = radioWork.getText().toString();
//           // Toast.makeText(this, "se place111::; " + selectedSuperStar, Toast.LENGTH_SHORT).show();
//
//        } else if (radioOther.isChecked()) {
//            selectedSuperStar = radioOther.getText().toString();
//            //Toast.makeText(this, "se place222::; " + selectedSuperStar, Toast.LENGTH_SHORT).show();
//
//        }
//        Toast.makeText(this, "se place::; " + selectedSuperStar, Toast.LENGTH_SHORT).show();
//        Toast.makeText(getApplicationContext(), selectedSuperStar, Toast.LENGTH_LONG).show(); // print the value of selected super star

        tieSubArea = findViewById(R.id.tieSubAreaId);
        tieEnterDeliveryAdd = findViewById(R.id.tieEnterDeliveryAddId);
        tiePinCode = findViewById(R.id.tiePinCodeId);
        mbtSaveDeliveryAdd = findViewById(R.id.mbtSaveDeliveryAddId);
        mbtSaveDeliveryAdd.setOnClickListener(this);
        id_toolbar = findViewById(R.id.id_toolbar);
        tv_title = id_toolbar.findViewById(R.id.tv_tittle);
        tv_title.setText(getString(R.string.addaddress));
        iv_back = id_toolbar.findViewById(R.id.id_back);
        iv_back.setOnClickListener(this);
        shakeAnimation = AnimationUtils.loadAnimation(this, R.anim.shake);
        constants = new Constants(getApplicationContext());
        userId = constants.getUserID();


        tieEnterDeliveryAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<Place.Field> fields = Arrays.asList(Place.Field.ADDRESS, Place.Field.NAME, Place.Field.LAT_LNG);
                Intent intent = new Autocomplete.IntentBuilder(
                        AutocompleteActivityMode.FULLSCREEN, fields)
                        .build(AddAddressActivity.this);
                startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mbtSaveDeliveryAddId:
                validation(v);
                break;
            case R.id.id_back:
                finish();
                break;
        }
    }

    private void validation(View v) {
        if (tieEnterDeliveryAdd.getText().toString().isEmpty()) {
            new CustomToast().Show_Toast(this, v, "Delivery Address Can't Empty");
            tieEnterDeliveryAdd.startAnimation(shakeAnimation);
            tieEnterDeliveryAdd.getBackground().mutate().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            tieEnterDeliveryAdd.requestFocus();
        } else if (tieSubArea.getText().toString().isEmpty()) {
            new CustomToast().Show_Toast(this, v, "Area Can't Empty");
            tieSubArea.startAnimation(shakeAnimation);
            tieSubArea.getBackground().mutate().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            tieSubArea.requestFocus();
        } else if (tieCity.getText().toString().isEmpty()) {
            new CustomToast().Show_Toast(getApplicationContext(), v, "City Can't Empty");
            tieCity.startAnimation(shakeAnimation);
            tieCity.getBackground().mutate().setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            tieCity.requestFocus();
        } else if (tiePinCode.getText().toString().isEmpty()) {
            new CustomToast().Show_Toast(getApplicationContext(), v, "Pincode Can't Empty");
            tiePinCode.startAnimation(shakeAnimation);
            tiePinCode.getBackground().mutate().setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            tiePinCode.requestFocus();
        } else if (!Constants.isPostalCodeValid(tiePinCode.getText().toString())) {
            new CustomToast().Show_Toast(getApplicationContext(), v, "Invalid Pincode");
            tiePinCode.startAnimation(shakeAnimation);
            tiePinCode.getBackground().mutate().setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            tiePinCode.requestFocus();
        } else if (tieMobilNumberAdd.getText().toString().isEmpty()) {
            new CustomToast().Show_Toast(getApplicationContext(), v, "Mobile Number Can't Empty");
            tieMobilNumberAdd.startAnimation(shakeAnimation);
            tieMobilNumberAdd.getBackground().mutate().setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            tieMobilNumberAdd.requestFocus();
        } else if (tieMobilNumberAdd.getText().toString().length() <= 9) {
            // new CustomToast().Show_Toast(this, v, getString(R.string.valid_mobile_error));
            tieMobilNumberAdd.startAnimation(shakeAnimation);
            tieMobilNumberAdd.getBackground().mutate().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            tieMobilNumberAdd.requestFocus();
            tieMobilNumberAdd.setError(getString(R.string.valid_mobile_error));
        } else if (selectedSuperStar == null) {
            // new CustomToast().Show_Toast(this, v, getString(R.string.valid_mobile_error));
//            tieMobilNumberAdd.startAnimation(shakeAnimation);
//            tieMobilNumberAdd.getBackground().mutate().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
//            tieMobilNumberAdd.requestFocus();
            Toast.makeText(this, "select Place", Toast.LENGTH_SHORT).show();
//            radioHome.setError(getString(R.string.placeselect));
        } else if (CheckNetwork.isNetAvailable(this)) {
//            Toast.makeText(this, "select::: " + selectedSuperStar, Toast.LENGTH_SHORT).show();
//            List<Place.Field> fields = Arrays.asList(Place.Field.ADDRESS, Place.Field.NAME, Place.Field.LAT_LNG);
//            Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields).build(getActivity());
//            startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
            addAddress(tieEnterDeliveryAdd.getText().toString().trim(),
                    tieEnterLandmark.getText().toString().trim(),
                    tieSubArea.getText().toString().trim(),
                    tieCity.getText().toString().trim(),
                    tieState.getText().toString().trim(),
                    tiePinCode.getText().toString().trim(),
                    tieMobilNumberAdd.getText().toString().trim(),
                    selectedSuperStar
                    , lat, longt
            );
        } else {
            new CustomToast().Show_Toast(this, v, "Check Network Connection");
        }
    }

    private void addAddress(String deilveryAdd, String landmark, String area, String city, String stat, String pincoe, String mobileNumber, String delivery_type, String lat, String longt) {
        CustomProgressbar.showProgressBar(this, false);

//      constants.setLatitude(constants.getAddress());
//        constants.setLongitude(constants.getAddress());

        Log.e("rahulraj", "long" + vendor_lat);
        Log.e("rahulraj", "long" + vendor_long);
//     String let = constants.setLatitude();
        lat = String.valueOf(vendor_lat);
        longt = String.valueOf(vendor_long);

        Log.e("rahulraj", "lat" + lat);
        Log.e("rahulraj", "longt" + longt);
        ApiClient.getClient().addAddress(userId, deilveryAdd, landmark, area, city, stat, pincoe, mobileNumber, delivery_type, lat, longt).enqueue(new Callback<AddAddressResponseModle>() {
            @Override
            public void onResponse(Call<AddAddressResponseModle> call, Response<AddAddressResponseModle> response) {

                if (response.isSuccessful()) {
                    Log.e("Address", "ResponseAddress : " + new Gson().toJson(response.body()));
                    CustomProgressbar.hideProgressBar();
                    AddAddressResponseModle addAddressResponseModle = response.body();
                    if (addAddressResponseModle.getStatus()) {
                        //constants.setaddLatitude(addAddressResponseModle.getAddAddress().getLatitude());
                        //  constants.setaddLongitude(addAddressResponseModle.getAddAddress().getLongitude());


                        Toast.makeText(getApplicationContext(), addAddressResponseModle.getMessage(), Toast.LENGTH_LONG).show();
                        onBackPressed();
                    } else {
                        Toast.makeText(getApplicationContext(), addAddressResponseModle.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    if (response.code() == 400) {
                        if (!false) {
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(response.errorBody().string());
                                CustomProgressbar.hideProgressBar();
                                String message = jsonObject.getString("message");
                                // Toast.makeText(SignupActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<AddAddressResponseModle> call, Throwable t) {
                CustomProgressbar.hideProgressBar();
            }
        });


    }

    @Override
    public void onBackPressed() {
        if (listAddress.equalsIgnoreCase("list")) {
            Intent intent = new Intent();
            intent.putExtra("flag", "text");
            setResult(2, intent);
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
            finish();
        } else {
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
            finish();
        }

    }

    /*@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                vendor_lat = place.getLatLng().latitude;
                vendor_long = place.getLatLng().longitude;
                List<Address> addresses;
                try {
                    addresses = geocoder.getFromLocation(vendor_lat, vendor_long, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                    String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    String state = addresses.get(0).getAdminArea();
                    String currentLocation = address;
                    String city = addresses.get(0).getLocality() != null ? addresses.get(0).getLocality() + "," + state : addresses.get(0).getSubAdminArea();
                    constants.setCurrentLocation(currentLocation);
                    constants.setCity(city);
                    constants.setLatitude(String.valueOf(vendor_lat));
                    constants.setLongitude(String.valueOf(vendor_long));

                    mMap.getUiSettings().setMyLocationButtonEnabled(true);

                    LatLng sydney = new LatLng(vendor_lat, vendor_long);
                    mMap.addMarker(new MarkerOptions().position(sydney));

                    mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

                    mMap.animateCamera(CameraUpdateFactory.zoomTo(16), 1500, null);


                    constants.setLatitude(String.valueOf(vendor_lat));
                    constants.setLongitude(String.valueOf(vendor_long));

                    Log.e("lat", "vendor_lat" + vendor_lat);
                    Log.e("long", "vendor_long" + vendor_long);
//                    tv_location.setText(city);
//                    Toast.makeText(getContext(), "ssssssss", Toast.LENGTH_SHORT).show();
//                    getStallNearBy("" + latitude, "" + longi);
//                    getStallNearBy("" + vendor_lat, "" + vendor_long);

                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
            } else if (resultCode == RESULT_CANCELED) {
            }
        }
    }*/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);

                vendor_lat = place.getLatLng().latitude;
                vendor_long = place.getLatLng().longitude;

                tieEnterDeliveryAdd.setText(place.getAddress());


                Log.e("vendor_lat", "vendor_latadd" + vendor_lat);
                Log.e("vendor_long", "vendor_longadd" + vendor_long);


            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }
}
