package com.alpha.paanilo.cart_pkg;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.location.Geocoder;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;

import com.alpha.paanilo.R;
import com.alpha.paanilo.cart_pkg.addAddressPkg.AddAddressResponseModle;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.utility.CheckNetwork;
import com.alpha.paanilo.utility.Constants;
import com.alpha.paanilo.utility.CustomProgressbar;
import com.alpha.paanilo.utility.CustomToast;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditAddressActivity extends AppCompatActivity implements View.OnClickListener {
    Constants constants;
    AppCompatTextView tv_title;
    AppCompatImageView iv_back;
    RadioButton radioHome, radioWork, radioOther;
    String selectedSuperStar;
    RadioGroup groupradio;
    private TextInputEditText tieCity, tieEnterLandmark, tieState,
            tieSubArea, tieEnterDeliveryAdd, tiePinCode, tieMobilNumberAdd;
    private AppCompatButton mbtSaveDeliveryAdd;
    private AppCompatImageView ivBackDelivery;
    private String userid;
    private Animation shakeAnimation;
    private String deliveryid, area, city, deliveryAdd, landmark, mobile, pincode, state;
    private String listAddress, deliverytype;
    private View id_toolbar;
    Double vendor_lat, vendor_long;
    int AUTOCOMPLETE_REQUEST_CODE = 1;
    private Geocoder geocoder;
    private GoogleMap mMap;
    String lat,longt ;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_address_fragment);
        constants = new Constants(getApplicationContext());
        userid = constants.getUserID();
        deliveryid = getIntent().getStringExtra("deliveryid");
        area = getIntent().getStringExtra("area");
        city = getIntent().getStringExtra("city");
        deliveryAdd = getIntent().getStringExtra("deliveryAdd");
        landmark = getIntent().getStringExtra("landmark");
        mobile = getIntent().getStringExtra("mobile");
        pincode = getIntent().getStringExtra("pincode");
        state = getIntent().getStringExtra("state");
        listAddress = getIntent().getStringExtra("listAddress");
        deliverytype = getIntent().getStringExtra("deliverytype");

//        Log.e("rahulraj ", "rahulraj " + deliverytype);

        if(deliverytype.equals("Home")){
            Log.e("rahulraj ", "rahulraj11 " + deliverytype);

          //  radioHome.setChecked(true);

//            radioHome.setClickable(true);
//            radioHome.setChecked(true);
//            radioHome.isSelected() ;
//            radioHome.isChecked();
        }else if(deliverytype.equals("Work")){
//            radioWork.setChecked(true);
//            Log.e("rahulraj ", "rahulraj22 " + deliverytype);

//            radioWork.isChecked();
        }else if(deliverytype.equals("Other")){
//            Log.e("rahulraj ", "rahulraj333 " + deliverytype);

//            radioOther.setChecked(true);
//            radioOther.isChecked();
        }


        Log.e("rahulraj ", "rahulraj " + state);
        init();
    }


    private void init() {
        tieEnterLandmark = findViewById(R.id.tieEnterLandmarkId);
        tieCity = findViewById(R.id.tieCityId);
        tieMobilNumberAdd = findViewById(R.id.tieMobilNumberAddId);
        Places.initialize(getApplicationContext(), getString(R.string.google_maps_key));
        radioHome = findViewById(R.id.radioHome);
        radioWork = findViewById(R.id.radioWork);
        radioOther = findViewById(R.id.radioOther);

        groupradio = findViewById(R.id.groupradio);

        tieState = findViewById(R.id.tieStateId);
        tieSubArea = findViewById(R.id.tieSubAreaId);
        tieEnterDeliveryAdd = findViewById(R.id.tieEnterDeliveryAddId);
        tiePinCode = findViewById(R.id.tiePinCodeId);
        mbtSaveDeliveryAdd = findViewById(R.id.mbtSaveDeliveryAddId);
        mbtSaveDeliveryAdd.setOnClickListener(this);
        shakeAnimation = AnimationUtils.loadAnimation(EditAddressActivity.this, R.anim.shake);
        tieEnterDeliveryAdd.setText(deliveryAdd);
        tieCity.setText(city);
        tieMobilNumberAdd.setText(mobile);
        tieState.setText(state);
        tieSubArea.setText(area);
        tiePinCode.setText(pincode);
        tieEnterLandmark.setText(landmark);
        id_toolbar = findViewById(R.id.id_toolbar);
        tv_title = id_toolbar.findViewById(R.id.tv_tittle);
        tv_title.setText(getString(R.string.editaddress));
        iv_back = id_toolbar.findViewById(R.id.id_back);
        iv_back.setOnClickListener(this);



        tieEnterDeliveryAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<Place.Field> fields = Arrays.asList(Place.Field.ADDRESS, Place.Field.NAME, Place.Field.LAT_LNG);
                Intent intent = new Autocomplete.IntentBuilder(
                        AutocompleteActivityMode.FULLSCREEN, fields)
                        .build(EditAddressActivity.this);
                startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
            }
        });

        groupradio.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (radioHome.isChecked()) {
                    selectedSuperStar = radioHome.getText().toString();
                  //  Toast.makeText(EditAddressActivity.this, "11 " + selectedSuperStar, Toast.LENGTH_SHORT).show();
                } else if (radioWork.isChecked()) {
                    selectedSuperStar = radioWork.getText().toString();
                    //Toast.makeText(EditAddressActivity.this, "22 " + selectedSuperStar, Toast.LENGTH_SHORT).show();

                } else if (radioOther.isChecked()) {
                    selectedSuperStar = radioOther.getText().toString();
                 //   Toast.makeText(EditAddressActivity.this, "33 " + selectedSuperStar, Toast.LENGTH_SHORT).show();

                }else
                if(deliverytype.equals("Home")){
                    Log.e("rahulraj ", "rahul222 " + deliverytype);
                   // radioHome.isEnabled();
//            radioHome.setChecked(true);
//            radioHome.isChecked();
                }else if(deliverytype.equals("Work")){
//            radioWork.setChecked(true);
//                    Log.e("rahulraj ", "rahul2 " + deliverytype);

//            radioWork.isChecked();
                }else if(deliverytype.equals("Other")){
//                    Log.e("rahulraj ", "rahul333 " + deliverytype);

//            radioOther.setChecked(true);
//            radioOther.isChecked();
                }
            }
        });

//        if (radioHome.isChecked()) {
//            selectedSuperStar = radioHome.getText().toString();
//        } else if (radioWork.isChecked()) {
//            selectedSuperStar = radioWork.getText().toString();
//        } else if (radioOther.isChecked()) {
//            selectedSuperStar = radioOther.getText().toString();
//        }

        //Toast.makeText(getApplicationContext(), selectedSuperStar, Toast.LENGTH_LONG).show(); // print the value of selected super star

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.id_back:
                finish();
                break;
            case R.id.mbtSaveDeliveryAddId:
                validation(v);
                break;
        }
    }

    private void validation(View v) {
        if (tieEnterDeliveryAdd.getText().toString().isEmpty()) {
            new CustomToast().Show_Toast(EditAddressActivity.this, v, "Delivery Address Can't Empty");
            tieEnterDeliveryAdd.startAnimation(shakeAnimation);
            tieEnterDeliveryAdd.getBackground().mutate().setColorFilter(ContextCompat.getColor(EditAddressActivity.this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        } else if (tieSubArea.getText().toString().isEmpty()) {
            new CustomToast().Show_Toast(EditAddressActivity.this, v, "Area Can't Empty");
            tieSubArea.startAnimation(shakeAnimation);
            tieSubArea.getBackground().mutate().setColorFilter(ContextCompat.getColor(EditAddressActivity.this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        } else if (tieCity.getText().toString().isEmpty()) {
            new CustomToast().Show_Toast(EditAddressActivity.this, v, "City Can't Empty");
            tieCity.startAnimation(shakeAnimation);
            tieCity.getBackground().mutate().setColorFilter(ContextCompat.getColor(EditAddressActivity.this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        } else if (tiePinCode.getText().toString().isEmpty()) {
            new CustomToast().Show_Toast(EditAddressActivity.this, v, "Pincode Can't Empty");
            tiePinCode.startAnimation(shakeAnimation);
            tiePinCode.getBackground().mutate().setColorFilter(ContextCompat.getColor(EditAddressActivity.this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        } else if (tieMobilNumberAdd.getText().toString().isEmpty()) {
            new CustomToast().Show_Toast(EditAddressActivity.this, v, "Mobile Number Can't Empty");
            tieMobilNumberAdd.startAnimation(shakeAnimation);
            tieMobilNumberAdd.getBackground().mutate().setColorFilter(ContextCompat.getColor(EditAddressActivity.this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        } else if (tieMobilNumberAdd.getText().toString().length() <= 9) {
            // new CustomToast().Show_Toast(this, v, getString(R.string.valid_mobile_error));
            tieMobilNumberAdd.startAnimation(shakeAnimation);
            tieMobilNumberAdd.getBackground().mutate().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            tieMobilNumberAdd.requestFocus();
            tieMobilNumberAdd.setError(getString(R.string.valid_mobile_error));
        } else if (selectedSuperStar == null) {
            // new CustomToast().Show_Toast(this, v, getString(R.string.valid_mobile_error));
//            tieMobilNumberAdd.startAnimation(shakeAnimation);
//            tieMobilNumberAdd.getBackground().mutate().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
//            tieMobilNumberAdd.requestFocus();
            Toast.makeText(this, "select Place", Toast.LENGTH_SHORT).show();

//            radioHome.setError(getString(R.string.placeselect));
        } else if (CheckNetwork.isNetAvailable(EditAddressActivity.this)) {

            addAddress(tieEnterDeliveryAdd.getText().toString().trim(),
                    tieEnterLandmark.getText().toString().trim(),
                    tieSubArea.getText().toString().trim(),
                    tieCity.getText().toString().trim(),
                    tieState.getText().toString().trim(),
                    tiePinCode.getText().toString().trim(),
                    tieMobilNumberAdd.getText().toString().trim(),
                    selectedSuperStar,
                    lat,longt

            );
        } else {
            new CustomToast().Show_Toast(EditAddressActivity.this, v, "Check Network Connection");
        }
    }

    private void addAddress(String deilveryAdd, String landmark, String area, String city, String stat, String pincoe, String mobileNumber, String delivery_type,String lat ,String longt) {
        CustomProgressbar.showProgressBar(EditAddressActivity.this, false);
        Log.e("userid:::::", "getLatitude" + constants.getLatitude());
        Log.e("userid:::::", " constants.getLongitude()" +  constants.getLongitude());

         lat=String.valueOf(vendor_lat);
         longt =String.valueOf(vendor_long);

        Log.e("lat", "long::" + lat);
        Log.e("longt", "long::" + longt);

        ApiClient.getClient().editDeliveryAddress(userid, deilveryAdd, landmark, area, city, stat, pincoe, mobileNumber, deliveryid, delivery_type,lat,longt).enqueue(new Callback<AddAddressResponseModle>() {
            @Override
            public void onResponse(Call<AddAddressResponseModle> call, Response<AddAddressResponseModle> response) {
                if (response.isSuccessful()) {
                    Log.e("Address", "Edit ResponseAddress : " + new Gson().toJson(response.body()));

                    CustomProgressbar.hideProgressBar();
                    AddAddressResponseModle addAddressResponseModle = response.body();
                    if (addAddressResponseModle.getStatus()) {
                        Toast.makeText(EditAddressActivity.this, addAddressResponseModle.getMessage(), Toast.LENGTH_LONG).show();

//                        constants.setaddLatitude(""+addAddressResponseModle.getAddAddress().getLatitude());
//                        constants.setaddLongitude(""+addAddressResponseModle.getAddAddress().getLongitude());

                        onBackPressed();
                        // getFragmentManager().popBackStack();
                     /*   AddressListActivity editAddresslFrag = new AddressListActivity();
                        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.container, editAddresslFrag);
                        fragmentTransaction.commit();*/
                    } else {
                        Toast.makeText(EditAddressActivity.this, addAddressResponseModle.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    if (response.code() == 400) {
                        if (!false) {
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(response.errorBody().string());
                                CustomProgressbar.hideProgressBar();
                                String message = jsonObject.getString("message");
                                // Toast.makeText(SignupActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<AddAddressResponseModle> call, Throwable t) {
                CustomProgressbar.hideProgressBar();
            }
        });


    }

    @Override
    public void onBackPressed() {
        if (listAddress.equalsIgnoreCase("list")) {
            Intent intent = new Intent();
            intent.putExtra("flag", "text");
            setResult(2, intent);
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
            finish();
        } else {
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
            finish();
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);

                vendor_lat = place.getLatLng().latitude;
                vendor_long = place.getLatLng().longitude;

                tieEnterDeliveryAdd.setText(place.getAddress());

         Log.e("vendor_lat","vendor_lat" + vendor_lat)   ;
                Log.e("vendor_long","vendor_long" + vendor_long)   ;

            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }
}
