
package com.alpha.paanilo.cart_pkg.addAddressPkg;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddAddressResponseModle {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("addAddress")
    @Expose
    private AddAddress addAddress;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public AddAddress getAddAddress() {
        return addAddress;
    }

    public void setAddAddress(AddAddress addAddress) {
        this.addAddress = addAddress;
    }

}
