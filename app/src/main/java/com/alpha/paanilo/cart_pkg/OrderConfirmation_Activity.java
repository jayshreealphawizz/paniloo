package com.alpha.paanilo.cart_pkg;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alpha.paanilo.HomeMainActivity;
import com.alpha.paanilo.Notification_Activity;
import com.alpha.paanilo.PaymentMethodActivity;
import com.alpha.paanilo.R;
import com.alpha.paanilo.Roomdatabase.DatabaseClient;
import com.alpha.paanilo.Roomdatabase.ProductDataTask;
import com.alpha.paanilo.coupanPkg.CoupanActivity;
import com.alpha.paanilo.model.categoryPkg.Datum;
import com.alpha.paanilo.model.checkoutModlePkg.CheckoutPozo;
import com.alpha.paanilo.model.deliveryPkg.DeliveryChargePozo;
import com.alpha.paanilo.online_payment_pkg.OnlinePayment_Activity;
import com.alpha.paanilo.online_payment_pkg.OrderPlaced_Activity;
import com.alpha.paanilo.product_detail_pkg.CouponCountModle;
import com.alpha.paanilo.product_detail_pkg.CouponNumberModel;
import com.alpha.paanilo.product_detail_pkg.DeleteRatingModle;
import com.alpha.paanilo.product_detail_pkg.Product_details_Activity;
import com.alpha.paanilo.product_detail_pkg.ReviewModel;
import com.alpha.paanilo.product_pkg.WalletAdapter;
import com.alpha.paanilo.product_pkg.WalletInfo;
import com.alpha.paanilo.product_pkg.WalletPozo;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.utility.AppSession;
import com.alpha.paanilo.utility.CheckNetwork;
import com.alpha.paanilo.utility.Constants;
import com.alpha.paanilo.utility.NetworkUtility;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Field;


public class OrderConfirmation_Activity extends AppCompatActivity implements View.OnClickListener, OrderItem_Adapter.ProductitemOnClickListener, PaymentResultListener {

    final String fixamount = "200";
    String TAG = getClass().getSimpleName();
    View v;
    View toolbar;
    RelativeLayout rl_whole;
    RecyclerView rv_orderitem;
    AppCompatTextView tv_tittle, tvPromotionDisouct;
    AppCompatImageView id_notification, id_back, iv_add_address;
    AppCompatButton btn_paynow, btn_cancel, btn_send;
    int size = 0;
    Constants constants;
    String deliveryCharge;
    List<ProductDataTask> tasksSize;
    String result, goToScreen, goToScreenId, productID, Distance;
    float walletBalance;
    String paymethod;
    TextView text;
    View layout;
    Button timefirst, timesecond, timethird, timeunselect, timesecondunselet, timethirdunselect;
    String selectTime = "";
    EditText tvCounponCount;
    LinearLayout llcod;
    String COUPONNO = "";
    AppCompatButton ivCoponbtn;
    CheckBox cb_cod, cb_online, cb_onlineIdwallet;
    private AppCompatTextView tvTotal, tvSubTotalItems, tvSubTotalItemsPrice, id_address;
    private OrderItem_Adapter orderItem_adapter;
    private String userid, total_amount, delivery_address, pincode, couponCode, couponCodeDiscount;
    private JSONArray jsonArray;
    private JSONObject jsonObject;
    private ProgressDialog pd;
    private EditText etAddNotes;
    private AppCompatTextView tvDeliveryAmount, tv_totalamount;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_confirmation_fragment);
        LayoutInflater inflater = getLayoutInflater();
        layout = inflater.inflate(R.layout.toast_layout,
                (ViewGroup) findViewById(R.id.toast_layout_root));

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        constants = new Constants(getApplicationContext());

        toolbar = findViewById(R.id.id_toolbar);
        tv_tittle = toolbar.findViewById(R.id.tv_tittle);
        id_notification = toolbar.findViewById(R.id.id_notification);
        id_back = toolbar.findViewById(R.id.id_back);
        text = layout.findViewById(R.id.text);
        tvDeliveryAmount = findViewById(R.id.tvDeliveryAmountId);
        btn_paynow = findViewById(R.id.btn_paynow);
        btn_cancel = findViewById(R.id.btn_cancel);
        tvTotal = findViewById(R.id.tvTotalId);

        timefirst = findViewById(R.id.timefirst);
        timesecond = findViewById(R.id.timesecond);
        timethird = findViewById(R.id.timethird);

        timeunselect = findViewById(R.id.timeunselect);
        timesecondunselet = findViewById(R.id.timesecondunselet);
        timethirdunselect = findViewById(R.id.timethirdunselect);


        id_address = findViewById(R.id.id_addressId);
        etAddNotes = findViewById(R.id.etAddNotesId);
        tvSubTotalItems = findViewById(R.id.tvSubTotalItemsId);
        tvSubTotalItemsPrice = findViewById(R.id.tvSubTotalItemsPriceId);
        tvPromotionDisouct = findViewById(R.id.tvPromotionDisouctId);
        iv_add_address = findViewById(R.id.iv_add_address);
        btn_send = findViewById(R.id.btn_send);
        tv_tittle.setText(getString(R.string.orderconfirmation));
        btn_send.setOnClickListener(this);
        id_back.setOnClickListener(this);


        timefirst.setOnClickListener(this);
        timesecond.setOnClickListener(this);
        timethird.setOnClickListener(this);
        timeunselect.setOnClickListener(this);
        timesecondunselet.setOnClickListener(this);
        timethirdunselect.setOnClickListener(this);


        id_notification.setOnClickListener(this);
        btn_paynow.setOnClickListener(this);
        btn_cancel.setOnClickListener(this);
        iv_add_address.setOnClickListener(this);


        userid = constants.getUserID();

        /*Intent intent = getIntent();

        if (intent != null) {
            String data = intent.getStringExtra("resdatumfood");
            Log.e("rahulraj", "rahulrajget data now " + data);
        }*/

        pd = new ProgressDialog(OrderConfirmation_Activity.this, R.style.AppCompatAlertDialogStyle);
        rv_orderitem = findViewById(R.id.id_rv_orderlist);

//        constants.getCurrentLocation()


//        Log.e("rahulraj", "rahulraj::::::::::::::: " + constants.getAddress());

//        Log.e("rahulraj", "rahulraj::::::::getCity::::::: " + constants.getCity());
//        Log.e("rahulraj", "rahulraj::::::::getCurrentLocation::::::: " + constants.getCurrentLocation());


        String orderproduct = getIntent().getStringExtra("productlist");

        delivery_address = getIntent().getStringExtra("delivery_address");
        pincode = getIntent().getStringExtra("pincode");


//        delivery_address

//        Log.e("constantdelivertaddrss", "constantdelivertaddrss::: " + constantdelivertaddrss);


        goToScreen = getIntent().getStringExtra("entry");

        goToScreenId = getIntent().getStringExtra("userid");

//        intent.putExtra("userid", productID);
//        intent.putExtra("entry", "detail");
//        Log.e("delivery_address::: ", "delivery_address::: " + delivery_address);
//        Log.e("delivery_address::: ", "getAddress::: " + constants.getAddress());

        Log.e("goToscreen", "gotoScreen::: " + goToScreen);

//        Log.e("goToscreen::: ", "userid::: " + userid);

        Log.e("goToscreen::: ", "goToScreenId::: " + goToScreenId);

        // constants.getCurrentLocation();

        tasksSize = new Gson().fromJson(orderproduct, new TypeToken<ArrayList<ProductDataTask>>() {
        }.getType());

//        tasksSize = new Gson().fromJson(orderproduct, new TypeToken<ArrayList<ProductDataTask>>() {
//        }.getType());


        rv_orderitem.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        orderItem_adapter = new OrderItem_Adapter(getApplicationContext(), this);
        rv_orderitem.setAdapter(orderItem_adapter);

//        orderItem_adapter.notifyDataSetChanged();
//        orderItem_adapter.notifyItemRemoved(0);
//        orderItem_adapter.notifyItemRangeRemoved(0, size);// clear list

        getTasks();
        getTasks1();

        if (constants.getCityAddress().equals("") || constants.getCityAddress().equals("null") || constants.getCityAddress().isEmpty()) {
//            id_address.setText(constants.getAddress());
//            id_address.setText(delivery_address);
        } else {
//            id_address.setText(delivery_address);
            id_address.setText(constants.getCityAddress());
        }
//        constants.setCity(arressLists.get(position).getDeliveryAddress());
//        constants.getDeliveyaddress();
        Log.e("ram", "ram::::" + delivery_address);
        Log.e("ram", "ram:1111:::" + constants.getCityAddress());


        Log.e("ram", "address:1111:::" + constants.getaddLatitude());

        Log.e("ram", "address:2222:::" + constants.getaddLongitude());
//        Log.e("constantdelivertaddrss", "constants.getDeliveyaddress()::: " + constants.getDeliveyaddress());

//        constants.getLatitude();
//        constants.getLongitude();



        try {
            if (delivery_address.isEmpty()) {
//                id_address.setText(constants.getDeliveyaddress());
            } else {
                id_address.setText(delivery_address);


//                 constants.setDeliveyaddress("");
            }
        } catch (Exception e) {

        }


//         if (!constants.getDeliveyaddress().isEmpty()) {
//            id_address.setText(constants.getDeliveyaddress());
//        } else {
//             id_address.setText(delivery_address);
//         }


        /*try {
            if (constants.getDeliveyaddress().isEmpty()) {

            } else {
                id_address.setText(constants.getDeliveyaddress());
            }
        } catch (Exception e) {

        }*/


        //   getMyOrderList();
    }


    public void showBottomSheetDialog() {
        final AppCompatTextView tv_onlinepayment, tv_cod, tv_wallet;


        CouponNumberModel couponNumberModel;

        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(this, R.style.SheetDialog);

        final View view = getLayoutInflater().inflate(R.layout.choose_payment_bottomsheet, null);

        tv_onlinepayment = view.findViewById(R.id.tv_onlinepayment);
        tv_cod = view.findViewById(R.id.tv_cod);

        tv_totalamount = view.findViewById(R.id.tv_totalamount);

        cb_cod = view.findViewById(R.id.cb_codId);
        cb_online = view.findViewById(R.id.cb_onlineId);
        llcod = view.findViewById(R.id.llcod);
        ivCoponbtn = view.findViewById(R.id.ivCoponbtn);
        tvCounponCount = view.findViewById(R.id.tvCounponCount);

        cb_onlineIdwallet = view.findViewById(R.id.cb_onlineIdwallet);
        tv_wallet = view.findViewById(R.id.tv_wallet);
        tvCounponCount.getText().toString();


        getCoupanNumber();
        ivCoponbtn.setVisibility(View.GONE);

        Log.e("rahulrajraj ", "fgfgfg::::: " + COUPONNO);

       /* if (COUPONNO.isEmpty() || COUPONNO.equals("") || COUPONNO.equals("0") || COUPONNO.equals("null")) {

//            cb_cod.setEnabled(false);
//            cb_cod.setSelectAllOnFocus(false);
//            cb_cod.focu
//            android:checked="false"
//            android:enabled="false"
//            android:clickable="false"
//            android:focusable="false"
            llcod.setVisibility(View.GONE);
//            ivCoponbtn.setVisibility(View.VISIBLE);
//            cb_cod.setClickable(false);

        } else {
            llcod.setVisibility(View.VISIBLE);
//            ivCoponbtn.setVisibility(View.INVISIBLE);
//            cb_cod.setChecked(true);
//            cb_cod.setEnabled(true);
//            cb_cod.setSelectAllOnFocus(true);

        }*/

        ivCoponbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckNetwork.isNetAvailable(getApplicationContext())) {

//                    if (COUPONNO.equals("0") || COUPONNO.equals("") || COUPONNO.equals("null") || COUPONNO.isEmpty()) {
//                        Toast.makeText(getApplicationContext(), "Enter Amount", Toast.LENGTH_SHORT).show();
////                        new CustomToast().Show_Toast(getApplicationContext(), v, "Amount Can't Empty");
////                        tvCoupanAMount.getBackground().mutate().setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
////                        tvCoupanAMount.requestFocus();
//                    } else {

//                        addAmount_Wallet();
                    startRazorPayPayment(fixamount);
//                    }
                } else {

                    Toast.makeText(getApplicationContext(), "Check Network Connection", Toast.LENGTH_LONG).show();
                }
            }
        });

        getMyWalletList();

        getCartDataTasks();

        cb_cod.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
//                    Toast.makeText(OrderConfirmation_Activity.this, "now click", Toast.LENGTH_SHORT).show();
                    cb_cod.setChecked(true);
                    if (NetworkUtility.isNetAvailable(getApplicationContext())) {
                        Log.e("checlout ", "COD");
                        checkoutApi("COD");
                    } else {
                        Toast.makeText(getApplicationContext(), "Check Network Connection", Toast.LENGTH_LONG).show();
                    }
                    bottomSheetDialog.dismiss();
                } else {

                }
            }
        });

        cb_online.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    cb_online.setChecked(true);
                    Intent intent = new Intent(OrderConfirmation_Activity.this, OnlinePayment_Activity.class);
                    intent.putExtra("user_id", userid);
                    intent.putExtra("delivery_charge", deliveryCharge);
                    intent.putExtra("delivery_time", getCurrentDate() + " " + getCurrentTime());
                    intent.putExtra("add_coupon", couponCode);
                    intent.putExtra("payment_method", "ONLINE");
                    intent.putExtra("total_amount", String.valueOf(total_amount));
                    intent.putExtra("delivery_address", id_address.getText().toString());
                    intent.putExtra("outlets_pickup_location", "");
                    intent.putExtra("total_items", "" + tasksSize.size());
                    intent.putExtra("date", getCurrentDate());
                    intent.putExtra("center_pickup_date", getCurrentDate());
                    intent.putExtra("center_pickup_time", getCurrentTime());
                    intent.putExtra("add_note", etAddNotes.getText().toString());
                    intent.putExtra("products", jsonArray.toString());
                    intent.putExtra("latitude", constants.getaddLatitude());
                    intent.putExtra("longitude", constants.getaddLongitude());

                    intent.putExtra("order_delivery_time", selectTime);



                    /*jsonObj_.put("user_id", userid);
                    jsonObj_.put("delivery_charge", deliveryCharge);
                    jsonObj_.put("delivery_time", getCurrentDate() + " " + getCurrentTime());
                    jsonObj_.put("add_coupon", couponCode);
                    jsonObj_.put("payment_method", paymethod);
                    jsonObj_.put("total_amount", total_amount);
                    jsonObj_.put("delivery_address", id_address.getText().toString());
                    jsonObj_.put("outlets_pickup_location", "");
                    jsonObj_.put("total_items", "" + tasksSize.size());
                    jsonObj_.put("date", getCurrentDate());
                    jsonObj_.put("center_pickup_date", getCurrentDate());
                    jsonObj_.put("center_pickup_time", getCurrentTime());
                    jsonObj_.put("add_note", etAddNotes.getText().toString());

                    jsonObj_.put("latitude", constants.getaddLatitude());
                    jsonObj_.put("longitude", constants.getaddLongitude());
                    jsonObj_.put("order_delivery_time", selectTime); */


                    startActivity(intent);
                    bottomSheetDialog.dismiss();
                } else {

                }
            }
        });
   /*     cb_online.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    cb_online.setChecked(true);
                    if (NetworkUtility.isNetAvailable(getApplicationContext())) {
//                        Constants.getTotalWithSeparater(String.valueOf(total));
//                        Constants.getTotalWithSeparater(result)
//                        Log.e("rahulraj", "rahulraj:::: " + tvTotal.getText().toString());
//                        Log.e("rahulraj", "rahulraj:::111: " + String.valueOf(total));

                        Log.e("rahulraj", "rahulraj:::22222: " + Constants.getTotalWithSeparater(result));
//                        startRazorPayPayment(""+Constants.getTotalWithSeparater(result));
//                        checkoutApi("ONLINE");

                    } else {
                        Toast.makeText(getApplicationContext(), "Check Network Connection", Toast.LENGTH_LONG).show();
                    }
                    bottomSheetDialog.dismiss();
                } else {

                }
            }
        });*/


        cb_onlineIdwallet.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
//                    Toast.makeText(OrderConfirmation_Activity.this, "now click", Toast.LENGTH_SHORT).show();
                    cb_onlineIdwallet.setChecked(true);
                    if (NetworkUtility.isNetAvailable(getApplicationContext())) {
//                        Log.e("rahulraj " ,"rahulraj " + result);
                        Log.e("rahulraj ", "rahulra::" + total_amount);
                        Log.e("rahulraj ", "rahulra:walletBalance:" + "" + walletBalance);
                        if (walletBalance == 0) {
                            Toast.makeText(OrderConfirmation_Activity.this, "Wallet amount not sufficient for this Order", Toast.LENGTH_LONG).show();
                            Intent intdata = new Intent(OrderConfirmation_Activity.this, PaymentMethodActivity.class);
                            startActivity(intdata);

                        } else {
                            if (Float.parseFloat(total_amount) > walletBalance) {
                                Toast.makeText(OrderConfirmation_Activity.this, "Wallet amount not sufficient for this Order", Toast.LENGTH_SHORT).show();
                                Intent intdat = new Intent(OrderConfirmation_Activity.this, PaymentMethodActivity.class);
                                startActivity(intdat);
                            } else {
                                Log.e("checlout ", "WALLET");
                                checkoutApi("WALLET");
                            }
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Check Network Connection", Toast.LENGTH_LONG).show();
                    }
                    bottomSheetDialog.dismiss();
                } else {

                }
            }
        });

    /*    tv_cod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetworkUtility.isNetAvailable(getApplicationContext())) {
                    checkoutApi();
                } else {
                    Toast.makeText(getApplicationContext(), "Check Network Connection", Toast.LENGTH_LONG).show();
                }
                bottomSheetDialog.dismiss();
            }
        });

        tv_onlinepayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OrderConfirmation_Activity.this, OnlinePayment_Activity.class);
                intent.putExtra("user_id", userid);
                intent.putExtra("delivery_charge", deliveryCharge);
                intent.putExtra("delivery_time", getCurrentDate() + " " + getCurrentTime());
                intent.putExtra("add_coupon", couponCode);
                intent.putExtra("payment_method", "ONLINE");
                intent.putExtra("total_amount", total_amount);
                intent.putExtra("delivery_address", id_address.getText().toString());
                intent.putExtra("outlets_pickup_location", "");
                intent.putExtra("total_items", "" + tasksSize.size());
                intent.putExtra("date", getCurrentDate());
                intent.putExtra("center_pickup_date", getCurrentDate());
                intent.putExtra("center_pickup_time", getCurrentTime());
                intent.putExtra("add_note", etAddNotes.getText().toString());
                intent.putExtra("products", jsonArray.toString());
                startActivity(intent);
                bottomSheetDialog.dismiss();

            }
        });
*/

        bottomSheetDialog.setContentView(view);
        bottomSheetDialog.show();
    }

    @Override
    public void onBackPressed() {
        if (goToScreen == null) {
            startActivity(new Intent(OrderConfirmation_Activity.this, HomeMainActivity.class));
            finish();
        } else if (goToScreen.equalsIgnoreCase("detail")) {
//            startActivity(new Intent(getApplicationContext(), Product_details_Activity.class));
            Intent intent = new Intent(OrderConfirmation_Activity.this, Product_details_Activity.class);
            intent.putExtra("userid", goToScreenId);
            startActivity(intent);

            Log.e("rahul ", "rahul111 " + goToScreenId);
//                onBackPressed();
            finish();
//            finish();
        } else {
            startActivity(new Intent(getApplicationContext(), HomeMainActivity.class));
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_send:
                if (size == 0) {
                    Toast.makeText(getApplicationContext(), "Add product to cart to use Voucher", Toast.LENGTH_LONG).show();
                } else {
                    if (Integer.parseInt(total_amount) > 1000) {
                        Intent intent = new Intent(getApplicationContext(), CoupanActivity.class);
                        startActivityForResult(intent, 2);
                    } else {
                        Toast.makeText(getApplicationContext(), "Voucher can be used above of Rp 1000", Toast.LENGTH_LONG).show();
                    }
                }
                break;
            case R.id.btn_paynow:
                if (/*constants.getAddress().equalsIgnoreCase("") && constants.getDeliveyaddress().equalsIgnoreCase("") &&*/ constants.getCityAddress().equalsIgnoreCase("Location") && delivery_address == null) {
                    Toast.makeText(getApplicationContext(), "Address Empty", Toast.LENGTH_LONG).show();
                } else if (selectTime.equals("")) {
                    Toast.makeText(getApplicationContext(), "Plz Select Time", Toast.LENGTH_LONG).show();
                } else if (size == 0) {
                    Toast.makeText(getApplicationContext(), "Cart Empty", Toast.LENGTH_LONG).show();
                } else {
                    showBottomSheetDialog();
                }
                break;

            case R.id.btn_cancel:
                onBackPressed();
                break;

            case R.id.id_back:
//                Toast.makeText(this, "now click", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(OrderConfirmation_Activity.this, Product_details_Activity.class);
                intent.putExtra("userid", goToScreenId);
                startActivity(intent);
                Log.e("rahul ", "rahul " + goToScreenId);
//                onBackPressed();
                finish();
                break;

            case R.id.id_notification:
                startActivity(new Intent(getApplicationContext(), Notification_Activity.class));
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                break;
            case R.id.iv_add_address:
                AppSession.setStringPreferences(getApplicationContext(), "addEntry", "confirmation");
                startActivity(new Intent(getApplicationContext(), AddressListActivity.class));
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                break;
            case R.id.timefirst:
                timefirst.setVisibility(View.VISIBLE);

                timeunselect.setVisibility(View.GONE);

                timesecond.setVisibility(View.GONE);
                timesecondunselet.setVisibility(View.VISIBLE);

                timethird.setVisibility(View.GONE);
                timethirdunselect.setVisibility(View.VISIBLE);
                selectTime = "0 - 3hr";
//                Toast.makeText(this, "Time :: " + selectTime, Toast.LENGTH_SHORT).show();
                break;
            case R.id.timesecond:


                selectTime = "3hr - 6hr";

                timesecond.setVisibility(View.VISIBLE);

                timesecondunselet.setVisibility(View.GONE);

                timefirst.setVisibility(View.GONE);
                timeunselect.setVisibility(View.VISIBLE);

                timethird.setVisibility(View.GONE);
                timethirdunselect.setVisibility(View.VISIBLE);


                break;
            case R.id.timethird:

                selectTime = "6hr - 9hr";

//                Toast.makeText(this, "Time  Third :: " +  timethird.isSelected(), Toast.LENGTH_SHORT).show();

                timethird.setVisibility(View.VISIBLE);

                timethirdunselect.setVisibility(View.GONE);

                timefirst.setVisibility(View.GONE);
                timeunselect.setVisibility(View.VISIBLE);

                timesecond.setVisibility(View.GONE);
                timesecondunselet.setVisibility(View.VISIBLE);
//                Toast.makeText(this, "Time  Second :: " +selectTime, Toast.LENGTH_SHORT).show();


                break;
            case R.id.timeunselect:
                selectTime = "0 - 3hr";
                timeunselect.setVisibility(View.GONE);

                timefirst.setVisibility(View.VISIBLE);

                timesecondunselet.setVisibility(View.VISIBLE);
                timethirdunselect.setVisibility(View.VISIBLE);

                timesecond.setVisibility(View.GONE);
                timethird.setVisibility(View.GONE);
//                Toast.makeText(this, "Time   :: " +selectTime, Toast.LENGTH_SHORT).show();

//                timeunselect.setVisibility(View.GONE);
//                timeunselect.setVisibility(View.GONE);


                break;
            case R.id.timesecondunselet:

                selectTime = "3hr - 6hr";

                timesecondunselet.setVisibility(View.GONE);

                timesecond.setVisibility(View.VISIBLE);

                timeunselect.setVisibility(View.VISIBLE);
                timethirdunselect.setVisibility(View.VISIBLE);

                timefirst.setVisibility(View.GONE);
                timethird.setVisibility(View.GONE);

//                Toast.makeText(this, "Time   :: " +selectTime, Toast.LENGTH_SHORT).show();

                break;
            case R.id.timethirdunselect:

                selectTime = "6hr - 9hr";
                timethirdunselect.setVisibility(View.GONE);

//                Toast.makeText(this, "Time :: ", Toast.LENGTH_SHORT).show();

                timethird.setVisibility(View.VISIBLE);

                timeunselect.setVisibility(View.VISIBLE);
                timesecondunselet.setVisibility(View.VISIBLE);

                timefirst.setVisibility(View.GONE);
                timesecond.setVisibility(View.GONE);
//                Toast.makeText(this, "Time   :: " +selectTime, Toast.LENGTH_SHORT).show();


//                timesecond.setVisibility(View.VISIBLE);
                break;


        }
    }

    private void getTasks() {
        class GetTasks extends AsyncTask<Void, Void, List<ProductDataTask>> {
            @Override
            protected List<ProductDataTask> doInBackground(Void... voids) {
                List<ProductDataTask> taskList = DatabaseClient
                        .getInstance(getApplicationContext())
                        .getAppDatabase()
                        .taskDao()
                        .getAll();
                return taskList;
            }

            @Override
            protected void onPostExecute(List<ProductDataTask> tasks) {
                super.onPostExecute(tasks);

                Log.e(TAG, "onPostExecute: task " + tasks);

                if (tasks.isEmpty()) {
                    size = tasks.size();
                    tasks.clear();
                    orderItem_adapter.notifyItemRangeRemoved(0, size);// clear list
                    rv_orderitem.setAdapter(null);
                } else {
                    size = tasks.size();

                    Log.e(TAG, "onPostExecute: task111 " + tasks.get(0).getQuantity());
                    orderItem_adapter.allproductlist(tasks);
                    tvSubTotalItems.setText("Subtotal (" + tasks.size() + "items)");
                }
            }
        }

        GetTasks gt = new GetTasks();
        gt.execute();
    }

    private void getTasks1() {
        class GetTasks extends AsyncTask<Void, Void, String> {
            @Override
            protected String doInBackground(Void... voids) {
                String taskList = DatabaseClient
                        .getInstance(getApplicationContext())
                        .getAppDatabase()
                        .taskDao()
                        .totalSum();
                return taskList;
            }

            @Override
            protected void onPostExecute(String tasks) {
                super.onPostExecute(tasks);
                if (tasks != null) {
                    result = String.valueOf(Integer.parseInt(tasks));
                    tvSubTotalItemsPrice.setText(getResources().getString(R.string.rupee) + " " + Constants.getTotalWithSeparater(result));
                    if (Integer.parseInt(result) > 1000) {
                        if (couponCodeDiscount == null) {
                            tvTotal.setText(getResources().getString(R.string.rupee) + " " + Constants.getTotalWithSeparater(" " + result));
                            total_amount = result;
                        } else {
                            int total = (Integer.parseInt(result) - Integer.parseInt(couponCodeDiscount));
                            tvTotal.setText(getResources().getString(R.string.rupee) + " " + Constants.getTotalWithSeparater(" " + total));
                            total_amount = "" + total;
                        }
                    } else {
                        tvTotal.setText(getResources().getString(R.string.rupee) + " " + Constants.getTotalWithSeparater(result));
                        total_amount = result;
                    }

                    if (CheckNetwork.isNetAvailable(getApplicationContext())) {
                        getDeliveryChargeApi();
                    } else {
                        Toast.makeText(getApplicationContext(), "Check Network Connection", Toast.LENGTH_LONG).show();
                    }
                } else {
                    tvTotal.setText(getResources().getString(R.string.rupee) + " " + " " + "00.00");
                    tvSubTotalItemsPrice.setText(getResources().getString(R.string.rupee) + " " + "00.00");
                    tvPromotionDisouct.setText(getResources().getString(R.string.rupee) + " " + "00.00");

                    tvDeliveryAmount.setText(getResources().getString(R.string.rupee) + " " + "00.00");
                }
            }
        }

        GetTasks gt = new GetTasks();
        gt.execute();
    }

    @Override
    public void addtotal(String as) {
        if (as != null) {
            tvTotal.setText(getResources().getString(R.string.rupee) + " " + Constants.getTotalWithSeparater(" " + as));
            tvSubTotalItemsPrice.setText(getResources().getString(R.string.rupee) + " " + Constants.getTotalWithSeparater(as));
            total_amount = as;
        } else {
            try {
                tvTotal.setText(getResources().getString(R.string.rupee) + " " + " " + "00.00");
                tvSubTotalItemsPrice.setText(getResources().getString(R.string.rupee) + " " + "00.00");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void carOnAmountRefresh() {
        getTasksAmuont();
    }

    @Override
    public void carOnDelete(int position, ProductDataTask datum) {
        deleteTask(datum.getProduct_id(), position);
    }

    private void getTasksAmuont() {
        class GetTasks extends AsyncTask<Void, Void, String> {
            @Override
            protected String doInBackground(Void... voids) {
                String taskList = DatabaseClient
                        .getInstance(getApplicationContext())
                        .getAppDatabase()
                        .taskDao()
                        .totalSum();
                return taskList;
            }

            @Override
            protected void onPostExecute(String tasks) {
                super.onPostExecute(tasks);
                tvSubTotalItemsPrice.setText(getResources().getString(R.string.rupee) + Constants.getTotalWithSeparater(tasks));
                if (tasks != null) {
                    result = String.valueOf(Integer.parseInt(tasks));
                    if (Integer.parseInt(result) > 1000) {
                        if (couponCodeDiscount == null) {
                            tvTotal.setText(getResources().getString(R.string.rupee) + " " + Constants.getTotalWithSeparater(result));
                            total_amount = result;
                        } else {

                            Log.e(TAG, "onPostExecute: result " + result);
                            int total = (Integer.parseInt(result) - Integer.parseInt(couponCodeDiscount));
                            tvTotal.setText(getResources().getString(R.string.rupee) + "" + Constants.getTotalWithSeparater(String.valueOf(total)));
                            total_amount = "" + total;
                            //tvTotal.setText(getResources().getString(R.string.rupee) + " " + Constants.getTotalWithSeparater(result));
                        }
                    } else {
                        tvTotal.setText(getResources().getString(R.string.rupee) + " " + Constants.getTotalWithSeparater(result));
                        total_amount = result;
                    }


                    if (CheckNetwork.isNetAvailable(getApplicationContext())) {
                        getDeliveryChargeApi();
                    } else {
                        Toast.makeText(getApplicationContext(), "Check Network Connection", Toast.LENGTH_LONG).show();
                    }
                } else {
                    tvSubTotalItemsPrice.setText(getResources().getString(R.string.rupee) + " " + "00.00");
                    tvTotal.setText(getResources().getString(R.string.rupee) + " " + "00.00");
                }

            }
        }

        GetTasks gt = new GetTasks();
        gt.execute();
    }


    private void deleteTask(final String id, final int postion) {

        Log.e(TAG, "deleteTask: ");
        class DeleteTask extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... voids) {
                ProductDataTask task = new ProductDataTask();
                DatabaseClient.getInstance(getApplicationContext()).getAppDatabase()
                        .taskDao()
                        .deleteByUserId(id);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                getTasks();
                getTasks1();
                // getCartCount();
            }
        }

        DeleteTask dt = new DeleteTask();
        dt.execute();

    }

    private void getCartCount() {
        class GetTasks extends AsyncTask<Void, Void, String> {
            @Override
            protected String doInBackground(Void... voids) {
                String taskList = DatabaseClient
                        .getInstance(getApplicationContext())
                        .getAppDatabase()
                        .taskDao()
                        .getCount();
                return taskList;
            }

            @Override
            protected void onPostExecute(String tasks) {
                super.onPostExecute(tasks);

            }
        }
        GetTasks gt = new GetTasks();
        gt.execute();
    }

    private void getCartDataTasks() {
        class GetTasks extends AsyncTask<Void, Void, List<ProductDataTask>> {
            @Override
            protected List<ProductDataTask> doInBackground(Void... voids) {
                List<ProductDataTask> taskList = DatabaseClient
                        .getInstance(getApplicationContext())
                        .getAppDatabase()
                        .taskDao()
                        .getAll();
                return taskList;
            }

            @Override
            protected void onPostExecute(List<ProductDataTask> tasks) {
                super.onPostExecute(tasks);
                tasksSize = tasks;
                jsonArray = new JSONArray();
                for (int i = 0; i < tasks.size(); i++) {
                    jsonObject = new JSONObject();
                    try {
                        jsonObject.put("product_name", tasks.get(i).getProduct_name());
                        jsonObject.put("product_description", tasks.get(i).getProduct_description());
                        jsonObject.put("product_id", tasks.get(i).getProduct_id());
                        jsonObject.put("quantity", tasks.get(i).getQuantity());
                        jsonObject.put("product_image", tasks.get(i).getProduct_image());
                        jsonObject.put("total_amount", tasks.get(i).getPrice());
                        jsonObject.put("price", tasks.get(i).getTotal());
                        jsonObject.put("net_weight", "");
                        jsonObject.put("in_stock", tasks.get(i).getIn_stock());
                        jsonObject.put("wishlist_status", "");
                        jsonObject.put("count_status", "");
                        jsonObject.put("off_percentage", "");
                        jsonObject.put("discount_price", tasks.get(i).getDiscount_price());
                        jsonObject.put("vendor_id", tasks.get(i).getVendorId());
                        jsonArray.put(jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
        }

        GetTasks gt = new GetTasks();
        gt.execute();
    }

    private void clearCart() {
        class GetTasks extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... voids) {
                DatabaseClient.getInstance(getApplicationContext())
                        .getAppDatabase()
                        .taskDao()
                        .delete();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

            }
        }
        GetTasks gt = new GetTasks();
        gt.execute();
    }

    private void checkoutApi(String paymethod) {
        if (deliveryCharge == null) {
            deliveryCharge = "0";
        }

        if (couponCode == null) {
            couponCode = "";
        }
        pd.setMessage("Loading");
        pd.show();
        JSONObject jsonObj_ = null;
        try {
            jsonObj_ = new JSONObject();
            jsonObj_.put("user_id", userid);
            jsonObj_.put("delivery_charge", deliveryCharge);
            jsonObj_.put("delivery_time", getCurrentDate() + " " + getCurrentTime());
            jsonObj_.put("add_coupon", couponCode);
            jsonObj_.put("payment_method", paymethod);
            jsonObj_.put("total_amount", total_amount);
            jsonObj_.put("delivery_address", id_address.getText().toString());
            jsonObj_.put("outlets_pickup_location", "");
            jsonObj_.put("total_items", "" + tasksSize.size());
            jsonObj_.put("date", getCurrentDate());
            jsonObj_.put("center_pickup_date", getCurrentDate());
            jsonObj_.put("center_pickup_time", getCurrentTime());
            jsonObj_.put("add_note", etAddNotes.getText().toString());

            jsonObj_.put("latitude", constants.getaddLatitude());
            jsonObj_.put("longitude", constants.getaddLongitude());
            jsonObj_.put("order_delivery_time", selectTime);


//            addressList.getLatitude()

            jsonObj_.put("products", jsonArray);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("currentime:", "Time" + getCurrentTime());

        Log.e("latitude:", "getLatitude" + constants.getaddLatitude());
        Log.e("longitude:", " constants.getLongitude()" + constants.getaddLongitude());

//        Log.e("userid:getLatitude::::", "getLatitude" + constants.getLatitude());
//        Log.e("userid:::getLongitude::", " constants.getLongitude()" +  constants.getLongitude());
        Log.e("userid:getLatitude::::", "getLatitude" + id_address.getText().toString());
        Log.e("userid:::getLongitude::", " constants.getLongitude()" + id_address.getText().toString());


//        Log.e("userid:::::", "getLatitude" + constants.getaddLatitude());
//        Log.e("userid:::::", " constants.getLongitude()" +  constants.getaddLongitude());


        Log.e("userid:::::", "userid" + userid);
        Log.e("userid:::::", "productID" + productID);
        Log.e("userid:::::", "productIkkkkD" + constants.getUserID());
        System.out.println("upload checkout order confirmation ======" + jsonObj_.toString());
//        pd.dismiss();
        (ApiClient.getClient().checkout(jsonObj_.toString())).enqueue(new Callback<CheckoutPozo>() {
            @Override
            public void onResponse(Call<CheckoutPozo> call, Response<CheckoutPozo> response) {
                if (response.isSuccessful()) {
                    Log.e("response", "checkout resposne" + new Gson().toJson(response.body()));
                    try {
                        pd.dismiss();
                        CheckoutPozo checkoutResponse = response.body();
//                        Toast.makeText(OrderConfirmation_Activity.this, ""+checkoutResponse.getMessage(), Toast.LENGTH_SHORT).show();
//                        Toast.makeText(OrderConfirmation_Activity.this, "" + checkoutResponse.getMessage(), Toast.LENGTH_SHORT).show();
//                        TextView text = (TextView) layout.findViewById(R.id.text);
                        text.setText("" + checkoutResponse.getMessage());

                        Toast toast = new Toast(getApplicationContext());
                        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                        toast.setDuration(Toast.LENGTH_LONG);
                        toast.setView(layout);
                        toast.show();
                        //                     Toast  toast =    Toast.makeText(OrderConfirmation_Activity.this, ""+checkoutResponse.getMessage(), Toast.LENGTH_SHORT);
//
////                      Toast.makeText(OrderConfirmation_Activity.this, ""+checkoutResponse.getMessage(),Toast.LENGTH_LONG);
//                        View view = toast.getView();
//
//                        //To change the Background of Toast
//                        view.setBackgroundColor(Color.TRANSPARENT);
//                        TextView text = (TextView) view.findViewById(android.R.id.message);
//
//                        //Shadow of the Of the Text Color
//                        text.setShadowLayer(0, 0, 0, Color.TRANSPARENT);
//                        text.setTextColor(Color.BLACK);
//                        text.setTextSize(Integer.valueOf(getResources().getString(R.string.text_size)));
//                        toast.show();


                        if (checkoutResponse.getStatus()) {
//                            clearCart();
//                           Toast.makeText(OrderConfirmation_Activity.this, "NNNNN::: ", Toast.LENGTH_SHORT).show();

                            Intent intent = new Intent(getApplicationContext(), OrderPlaced_Activity.class);
                            intent.putExtra("saleId", "" + checkoutResponse.getResult());
                            intent.putExtra("orderplaced", "" + checkoutResponse.getMessage());


                            startActivity(intent);

//                            toast.show();

                        } else {
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        pd.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(Call<CheckoutPozo> call, Throwable t) {
                pd.dismiss();
            }
        });

    }


    private String getCurrentDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
        String datetime = dateformat.format(c.getTime());
        return datetime;
    }


    private String getCurrentTime() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat dateformat = new SimpleDateFormat("hh:mm:ss a");
        String datetime = dateformat.format(c.getTime());
        return datetime;
    }

    private void getTotal() {
        class GetTasks extends AsyncTask<Void, Void, String> {
            @Override
            protected String doInBackground(Void... voids) {
                String taskList = DatabaseClient
                        .getInstance(getApplicationContext())
                        .getAppDatabase()
                        .taskDao()
                        .totalSum();
                return taskList;
            }

            @Override
            protected void onPostExecute(String tasks) {
                super.onPostExecute(tasks);
                if (tasks != null) {
                    //  total_amount = tasks;
                } else {

                }
            }
        }
        GetTasks gt = new GetTasks();
        gt.execute();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2) {
            try {
                couponCode = data.getStringExtra("couponCode");
                couponCodeDiscount = data.getStringExtra("couponOff");
                tvPromotionDisouct.setText(getResources().getString(R.string.rupee) + "" + couponCodeDiscount);
                int total = (Integer.parseInt(total_amount) - Integer.parseInt(couponCodeDiscount));
                tvTotal.setText(getResources().getString(R.string.rupee) + "" + Constants.getTotalWithSeparater(String.valueOf(total)));
                total_amount = "" + total;

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private void getDeliveryChargeApi() {
        // pd.setMessage("Loading");
        //  pd.show();
        JsonObject jsonObject = new JsonObject();
        try {
            jsonObject.addProperty("user_id", userid);
            jsonObject.addProperty("total_amount", total_amount);

        } catch (Exception e) {
            e.printStackTrace();
        }
        (ApiClient.getClient().showDeliveryCharges(jsonObject)).enqueue(new Callback<DeliveryChargePozo>() {
            @Override
            public void onResponse(Call<DeliveryChargePozo> call, Response<DeliveryChargePozo> response) {
                if (response.isSuccessful()) {
                    try {
                        //  pd.dismiss();
                        DeliveryChargePozo deliveryChargePozo = response.body();
                        if (deliveryChargePozo.getStatus()) {
                            deliveryCharge = "" + deliveryChargePozo.getDeliveryCharges();
                            if (deliveryCharge.equalsIgnoreCase("0")) {
                                tvDeliveryAmount.setText("Rs 00.00");
                            } else {
                                tvDeliveryAmount.setText("Rs " + deliveryChargePozo.getDeliveryCharges());
                                int total = Integer.parseInt(total_amount) + Integer.parseInt(deliveryCharge);
                                tvTotal.setText(getResources().getString(R.string.rupee) + " " + Constants.getTotalWithSeparater(String.valueOf(total)));
                                total_amount = "" + total;
                            }
                            //  tvDeliveryAmount.setText("Rp " + deliveryChargePozo.getDeliveryCharges());
                            //tvTotal.setText();
                        } else {
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        //  pd.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(Call<DeliveryChargePozo> call, Throwable t) {
                pd.dismiss();
            }
        });


    }

//    @Override
//    protected void onResume() {
//
////        getTasks();
////        getTasks1();
////        clearCart();
////        orderItem_adapter.notifyDataSetChanged();
////        orderItem_adapter = new OrderItem_Adapter(getApplicationContext(), this);
////        orderItem_adapter.notifyItemRangeRemoved(0, size);
////        getTasksAmuont();
//
//
//
//        super.onResume();
//    }


    public void getMyWalletList() {
//        pd.setMessage("Loading");
//        pd.show();
        JsonObject jsonObject = new JsonObject();
        try {
            jsonObject.addProperty("user_id", constants.getUserID());
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e("TAG", constants.getUserID() + "wallet list  request : " + new Gson().toJson(jsonObject));
        (ApiClient.getClient().getwallethistory(jsonObject)).enqueue(new Callback<WalletPozo>() {
            @Override
            public void onResponse(Call<WalletPozo> call, Response<WalletPozo> response) {
                pd.dismiss();

                Log.e("TAG", "wallet list response : " + new Gson().toJson(response.body()));
                if (response.isSuccessful()) {
                    try {
                        Log.e("TAG", "wallet list response : " + new Gson().toJson(response.body()));
                        WalletPozo walletPozo = response.body();

                        if (walletPozo.getWalletBalance().equals("")) {
                            tv_totalamount.setText("Rs." + 00);
                        } else {
                            tv_totalamount.setText("Rs." + walletPozo.getWalletBalance());
                        }

                        walletBalance = Float.parseFloat(walletPozo.getWalletBalance());
                        Log.e("wallet balance", "balance:: " + walletBalance);

                       /* if (walletPozo.getStatus() == 1) {
//                                    totalAmount = orderHistoryPozo.getWallet();
//                                    historyList = orderHistoryPozo.getData();
//                                    Collections.reverse(historyList);
                            arraylist = walletPozo.getData();

                            adapter = new WalletAdapter(getContext(), arraylist);


                            rvWallet.setAdapter(adapter);
//                                    tv_nodata.setVisibility(View.GONE);

//                                    constants.setWallet(totalAmount);

                        } else {
//                                    tv_nodata.setVisibility(View.VISIBLE);
//                                    tv_nodata.setText("Data not found");
                        }*/


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<WalletPozo> call, Throwable t) {
                pd.dismiss();
//                tv_nodata.setVisibility(View.VISIBLE);
                Toast.makeText(OrderConfirmation_Activity.this, getString(R.string.server_errors), Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void startRazorPayPayment(String amount) {
        final Checkout co = new Checkout();
        co.setKeyID("rzp_test_QrkS2B01UMFJ95");//pannli test key
//        co.setKeyID("rzp_test_GC7PRrOSR8GIQf");//oro test key

        co.setImage(R.drawable.launcher);
        final OrderConfirmation_Activity activity = this;
//        context = getApplicationContext() ;
//        final ProductFragment context = this;
        try {
            System.out.println(constants.getEmail() + "------add wallet amount------" + amount + "===name =====" + constants.getUsername());

            JSONObject options = new JSONObject();
            options.put("name", constants.getUsername());
            options.put("description", "");
            //You can omit the image option to fetch the image from dashboard
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png");
            options.put("currency", "INR");
            options.put("amount", Float.parseFloat(amount) * 100);

            JSONObject preFill = new JSONObject();
            preFill.put("email", constants.getEmail());
            preFill.put("contact", "91" + "");
            options.put("prefill", preFill);


            co.open(activity, options);
        } catch (Exception e) {
            Log.e("rahulraj ", "walletpage222");
            Toast.makeText(getApplicationContext(), "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }
    }


    @Override
    public void onPaymentSuccess(String razorpayPaymentID) {
        try {

//            Log.e("rahulraj ", "goToScreenId::: " + goToScreenId);
//            Log.e("rahulraj ", "fixamount::: " + fixamount);
//            Log.e("rahulraj ", "walletpage::: " + razorpayPaymentID);

            constants.getVendror();

            Log.e("rahul raj", "Useriddat::constants.getVendror()::" + constants.getVendror());


            String Useriddat = constants.getUserID();
            Log.e("rahul raj", "Useriddat::111111111111::" + Useriddat);


            getCoupanCount(Useriddat, fixamount);
//            https://alphawizztest.tk/PaaniLoo/api/Authentication/add_coupon_amount_vendor_wallet
            Toast.makeText(getApplicationContext(), "Payment Successful: ", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
        }
    }

    @Override
    public void onPaymentError(int code, String response) {
        try {
            Toast.makeText(getApplicationContext(), "Payment failed ", Toast.LENGTH_SHORT).show();
//            finish();
        } catch (Exception e) {
        }
    }


    /*private void addAmount_Wallet() {
//        Toast.makeText(this, "rrrrrrrr", Toast.LENGTH_SHORT).show();
        pd.setMessage("Loading");
        pd.show();
        JsonObject jsonObject = new JsonObject();
        try {
            jsonObject.addProperty("user_id", constants.getUserID());
            jsonObject.addProperty("amount", fixamount);
        } catch (Exception e) {
            e.printStackTrace();
        }
        (ApiClient.getClient().addWalletAmount(jsonObject)).enqueue(new Callback<WalletInfo>() {
            @Override
            public void onResponse(Call<WalletInfo> call, Response<WalletInfo> response) {
                pd.dismiss();
                if (response.isSuccessful()) {
                    try {
                        pd.dismiss();
                        WalletInfo walletInfo = response.body();
                        Log.e("TAG", "add wallet response : " + new Gson().toJson(response.body()));
                        if (walletInfo.getStatus() == 1) {
//                            Toast.makeText(getContext(), "Amount added" + walletInfo.getData().getWallet(), Toast.LENGTH_SHORT).show();

//                            totalamount = ""+walletInfo.getData().getWallet();

//                            tvTotalAmount.setText("Rs." + walletInfo.getData().getWallet());
                            if (walletInfo.getData().equals("")) {
                                tvCounponCount.setText("Rs." + 00);
                            } else {
                                tvCounponCount.setText("Rs." + walletInfo.getData().getWallet());

                                llcod.setVisibility(View.VISIBLE);
                            }


                            //     finish();

                        } else {
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        pd.dismiss();
                    }

//                    getMyWalletList();
//                    tvCoupanAMount.setText("");

                }
            }

            @Override
            public void onFailure(Call<WalletInfo> call, Throwable t) {
                pd.dismiss();
            }
        });
    }*/


    private void getCoupanCount(String vendid, String amount) {
//        Toast.makeText(this, "rrrrrrrr", Toast.LENGTH_SHORT).show();
        pd.setMessage("Loading");
        pd.show();
        JsonObject jsonObject = new JsonObject();
        try {
            jsonObject.addProperty("user_id", vendid);
            jsonObject.addProperty("amount", amount);
        } catch (Exception e) {
            e.printStackTrace();
        }
//        Log.e("TAG", "get coupan vendid : " + vendid);

//        Log.e("TAG", "get coupan amount : " + amount);

        (ApiClient.getClient().getCoupanCount(jsonObject)).enqueue(new Callback<CouponCountModle>() {
            @Override
            public void onResponse(Call<CouponCountModle> call, Response<CouponCountModle> response) {
                Log.e("TAG", "get coupan response : " + new Gson().toJson(response.body()));
                pd.dismiss();
                if (response.isSuccessful()) {
                    try {
                        pd.dismiss();
                        CouponCountModle couponCountModle = response.body();

                        if (couponCountModle.getStatus() == 1) {
//                            Toast.makeText(getContext(), "Amount added" + walletInfo.getData().getWallet(), Toast.LENGTH_SHORT).show();

//                            totalamount = ""+walletInfo.getData().getWallet();

//                            tvTotalAmount.setText("Rs." + walletInfo.getData().getWallet());
                           /* if (couponCountModle.getData().equals("")) {
                                tvCounponCount.setText("Rs." + 00);
                            } else {
                                tvCounponCount.setText("Rs." + couponCountModle.getData().getTotal_coupon());
                            }*/
                            getCoupanNumber();
                            //     finish();

                        } else {
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        pd.dismiss();
                    }

//                    getCoupanNumber();
//                    tvCoupanAMount.setText("");

                }
            }

            @Override
            public void onFailure(Call<CouponCountModle> call, Throwable t) {
                pd.dismiss();
            }
        });
    }


    private void getCoupanNumber() {
//        pbLoginId.setVisibility(View.VISIBLE);
        pd.setMessage("Loading");
        pd.show();
//        constants.setVendor(product.getFarmerId());
//        Log.e("rahul raj", "goToScreenId::" + goToScreenId);


        String Userid = constants.getVendror();
        Log.e("rahul raj", "Userid:::::" + Userid);
        Log.e("rahul raj", "Useriddat:111:constants.getVendror()::" + constants.getVendror());

        (ApiClient.getClient().getCoupanNumber(Userid)).enqueue(new Callback<CouponNumberModel>() {
            @Override
            public void onResponse(Call<CouponNumberModel> call, Response<CouponNumberModel> response) {
                pd.dismiss();
                Log.e("TAG", "coupon number : " + new Gson().toJson(response.body()));

                if (response.isSuccessful()) {
                    CouponNumberModel couponNumberModel = response.body();
                    if (couponNumberModel.getStatus() == true) {

//                        total_coupon
//                        COUPONNO = String.valueOf(couponNumberModel.getData().getTotalCoupon());
//                        tvCounponCount.setText("Counter " + COUPONNO);
//                        if (couponNumberModel.getData().getTotalCoupon() >= 10) {
//                            ivCoponbtn.setVisibility(View.GONE);
//                        } else {
//                            ivCoponbtn.setVisibility(View.VISIBLE);
//                        }
                        if (couponNumberModel.getData().getTotalCoupon() >= 1) {
                            llcod.setVisibility(View.VISIBLE);
                            cb_cod.setEnabled(true);
                            cb_cod.setOnClickListener(OrderConfirmation_Activity.this);
//                            cb_cod.setOnCheckedChangeListener(this);
//                            cb_cod.setOnClickListener();
//                            cb_cod.setChecked(true);
//                            if()
//                            cb_cod.isChecked() ==false ;
//                            cb_cod.isClickable()==false;
//                            cb_cod.setChecked(true);
                        } else {
                            cb_cod.setEnabled(false);
                            cb_cod.setChecked(false);
//                            cb_cod.setoncl;
//                            llcod.setVisibility(View.GONE);
                        }
//                        llcod.setVisibility(View.GONE);
//                        Log.e("rahulraj", "rahulraj COUPONNO ::::::: " + COUPONNO);
                    }

                }
            }

            @Override
            public void onFailure(Call<CouponNumberModel> call, Throwable t) {
//                pbLoginId.setVisibility(View.GONE);
                Log.e("message ", "message ::: " + t.getMessage());
                pd.dismiss();
            }
        });
    }

}

