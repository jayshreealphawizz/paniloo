package com.alpha.paanilo.cart_pkg;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.alpha.paanilo.HomeMainActivity;
import com.alpha.paanilo.R;
import com.alpha.paanilo.cart_pkg.showAddressModlePkg.AddressList;
import com.alpha.paanilo.utility.AppSession;
import com.alpha.paanilo.utility.Constants;

import java.util.List;

public class AddressListadapter extends RecyclerView.Adapter<AddressListadapter.ViewHolder> {
    private final Context context;
    private final AddressOnClick addressOnClick;
    private List<AddressList> arressLists;
    private final StringBuilder stringBuilder = new StringBuilder();
    private String deliveryAddress, landmark, area, city, state, pincode, mobilenumber, deliverttype, lati, longt;
    private final String addressEntry;
    private final String vendorId;
    private final Constants constants;


    public AddressListadapter(Context context, AddressOnClick addressOnClick) {
        this.context = context;
        this.addressOnClick = addressOnClick;
        addressEntry = AppSession.getStringPreferences(context, "addEntry");
        vendorId = AppSession.getStringPreferences(context, "vendorId");
        constants = new Constants(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.address_list_row, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        if (arressLists.get(position).getDeliveryAddress().isEmpty()) {
            deliveryAddress = "";
        } else {
            deliveryAddress = arressLists.get(position).getDeliveryAddress();
        }
        if (arressLists.get(position).getArea().isEmpty()) {
            area = "";
        } else {
            area = arressLists.get(position).getArea();
        }
        if (arressLists.get(position).getLandmark().isEmpty()) {
            landmark = "";
        } else {
            landmark = arressLists.get(position).getLandmark();
        }
        if (arressLists.get(position).getCity().isEmpty()) {
            city = "";
        } else {
            city = arressLists.get(position).getCity();
        }

        if (arressLists.get(position).getState().isEmpty()) {
            state = "";
        } else {
            state = arressLists.get(position).getState();
        }

        if (arressLists.get(position).getPincode().isEmpty()) {
            pincode = "";
        } else {
            pincode = arressLists.get(position).getPincode();
        }
        if (arressLists.get(position).getDelivery_type().isEmpty()) {
            deliverttype = "";
        } else {
            deliverttype = arressLists.get(position).getDelivery_type();
        }

        if (arressLists.get(position).getLatitude().isEmpty()) {
            lati = "";
        } else {
            lati = arressLists.get(position).getLatitude();
            constants.setaddLatitude(lati);
            Log.e("Rahulraj ", "lati" + lati);
        }


        if (arressLists.get(position).getLongitude().isEmpty()) {
            longt = "";
        } else {
            longt = arressLists.get(position).getLongitude();
            constants.setaddLongitude(longt);
            Log.e("Rahulraj ", "longt" + longt);
        }

//        if(holder.tvAddressName.getText().toString().isEmpty()){
//         holder.rbSelectAddress.setChecked(false);
//        }else {
//            holder.rbSelectAddress.setChecked(true);

        Log.e("Rahulraj ", "arressLists.size():::" + arressLists.size());
//        if(arressLists.size()<=0) {
//            holder.rbSelectAddress.setChecked(false);
//        }else {
//            holder.rbSelectAddress.setChecked(true);

        holder.tvAddressName.setText(deliveryAddress + "," + landmark + "," + area + "," + city + "," + state + "," + pincode + "," + deliverttype);
        holder.tvAddressNumbre.setText("Number: " + arressLists.get(position).getMobile());
//        }
//        constants.setDeliveyaddress(deliveryAddress + "," + landmark + "," + area + "," + city + "," + state + "," + pincode + "," + deliverttype + "," + arressLists.get(position).getMobile());


        holder.rbSelectAddress.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Activity mainAct = (Activity) context;
                    if (addressEntry.equalsIgnoreCase("home")) {
                        constants.setCity(arressLists.get(position).getDeliveryAddress());
                        Intent intent = new Intent(context, HomeMainActivity.class);
                        mainAct.startActivity(intent);
                        mainAct.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
                        mainAct.finish();
                    } else {

                        constants.setCityAddress(holder.tvAddressName.getText().toString());
                        constants.setaddLatitude(lati);
                        constants.setaddLongitude(longt);
                        Intent intent = new Intent(context, OrderConfirmation_Activity.class);
                        intent.putExtra("delivery_address", holder.tvAddressName.getText().toString());
                        intent.putExtra("pincode", arressLists.get(position).getPincode());


                        mainAct.startActivity(intent);
                        mainAct.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
                        mainAct.finish();
                    }

                } else {

                }
            }
        });
    }

    public void addAddressList(List<AddressList> arressLists) {
        this.arressLists = arressLists;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return arressLists == null ? 0 : arressLists.size();
    }

    public interface AddressOnClick {
        void addOnClick(View view, int position, AddressList addressList);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final AppCompatImageView ivDeleteAddress;
        private final AppCompatImageView ivEditAddress;
        private final AppCompatTextView tvAddressName;
        private final AppCompatTextView tvAddressNumbre;
        private final RadioButton rbSelectAddress;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ivDeleteAddress = itemView.findViewById(R.id.ivDeleteAddressid);
            ivEditAddress = itemView.findViewById(R.id.ivEditAddressId);
            tvAddressName = itemView.findViewById(R.id.tvAddressNameId);
            // tvUserForAddress = itemView.findViewById(R.id.tvUserForAddressId);
            tvAddressNumbre = itemView.findViewById(R.id.tvAddressNumbreId);
            rbSelectAddress = itemView.findViewById(R.id.rbSelectAddressId);
            ivDeleteAddress.setOnClickListener(this);
            ivEditAddress.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            addressOnClick.addOnClick(v, getAdapterPosition(), arressLists.get(getAdapterPosition()));
        }
    }


}
