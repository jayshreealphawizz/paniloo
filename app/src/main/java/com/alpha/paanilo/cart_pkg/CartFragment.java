package com.alpha.paanilo.cart_pkg;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.fragment.app.Fragment;

import com.alpha.paanilo.R;


public class CartFragment extends Fragment {
    View v;
    View toolbar; RelativeLayout rl_whole;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.cart_fragment, container, false);


        return v;
    }

}

