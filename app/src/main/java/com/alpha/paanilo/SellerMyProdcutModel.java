package com.alpha.paanilo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SellerMyProdcutModel {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private DatamyProduct data;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DatamyProduct getData() {
        return data;
    }

    public void setData(DatamyProduct data) {
        this.data = data;
    }
}
