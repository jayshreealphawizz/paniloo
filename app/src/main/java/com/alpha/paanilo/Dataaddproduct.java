package com.alpha.paanilo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Dataaddproduct {
    /*@SerializedName("product_id")
    @Expose
    private String product_id;

    @SerializedName("product_name")
    @Expose
    private String product_name;

    @SerializedName("product_description")
    @Expose
    private String product_description;


    @SerializedName("product_image")
    @Expose
    private String product_image;

    @SerializedName("category_id")
    @Expose
    private String category_id;

    @SerializedName("subcategory_id")
    @Expose
    private String subcategory_id;

    @SerializedName("childcat_id")
    @Expose
    private String childcat_id;


    @SerializedName("product_pincode")
    @Expose
    private String product_pincode;

    @SerializedName("in_stock")
    @Expose
    private String in_stock;

    @SerializedName("price")
    @Expose
    private String price;

    @SerializedName("availability")
    @Expose
    private String availability;


    @SerializedName("volume")
    @Expose
    private String volume;

    @SerializedName("quantity")
    @Expose
    private String quantity;


    @SerializedName("unit")
    @Expose
    private String unit;

    @SerializedName("thumbnails_image")
    @Expose
    private String thumbnails_image;

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_description() {
        return product_description;
    }

    public void setProduct_description(String product_description) {
        this.product_description = product_description;
    }

    public String getProduct_image() {
        return product_image;
    }

    public void setProduct_image(String product_image) {
        this.product_image = product_image;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getSubcategory_id() {
        return subcategory_id;
    }

    public void setSubcategory_id(String subcategory_id) {
        this.subcategory_id = subcategory_id;
    }

    public String getChildcat_id() {
        return childcat_id;
    }

    public void setChildcat_id(String childcat_id) {
        this.childcat_id = childcat_id;
    }

    public String getProduct_pincode() {
        return product_pincode;
    }

    public void setProduct_pincode(String product_pincode) {
        this.product_pincode = product_pincode;
    }

    public String getIn_stock() {
        return in_stock;
    }

    public void setIn_stock(String in_stock) {
        this.in_stock = in_stock;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getThumbnails_image() {
        return thumbnails_image;
    }

    public void setThumbnails_image(String thumbnails_image) {
        this.thumbnails_image = thumbnails_image;
    }*/


    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("product_description")
    @Expose
    private String productDescription;
    @SerializedName("product_image")
    @Expose
    private String productImage;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("subcategory_id")
    @Expose
    private String subcategoryId;
    @SerializedName("childcat_id")
    @Expose
    private String childcatId;
    @SerializedName("product_pincode")
    @Expose
    private String productPincode;
    @SerializedName("in_stock")
    @Expose
    private String inStock;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("availability")
    @Expose
    private String availability;
    @SerializedName("volume")
    @Expose
    private String volume;
    @SerializedName("mrp")
    @Expose
    private String mrp;
    @SerializedName("tax")
    @Expose
    private String tax;
    @SerializedName("unit_value")
    @Expose
    private String unitValue;
    @SerializedName("unit")
    @Expose
    private String unit;
    @SerializedName("increament")
    @Expose
    private String increament;
    @SerializedName("rewards")
    @Expose
    private String rewards;
    @SerializedName("loca_category")
    @Expose
    private String locaCategory;
    @SerializedName("quantity")
    @Expose
    private String quantity;
    @SerializedName("net_weight")
    @Expose
    private String netWeight;
    @SerializedName("State_code")
    @Expose
    private String stateCode;
    @SerializedName("Location_filter")
    @Expose
    private String locationFilter;
    @SerializedName("thumbnails_image")
    @Expose
    private String thumbnailsImage;
    @SerializedName("farmer_id")
    @Expose
    private String farmerId;
    @SerializedName("discount_price")
    @Expose
    private String discountPrice;
    @SerializedName("off_percentage")
    @Expose
    private String offPercentage;
    @SerializedName("weekend_promo")
    @Expose
    private String weekendPromo;
    @SerializedName("wishlist_status")
    @Expose
    private String wishlistStatus;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getSubcategoryId() {
        return subcategoryId;
    }

    public void setSubcategoryId(String subcategoryId) {
        this.subcategoryId = subcategoryId;
    }

    public String getChildcatId() {
        return childcatId;
    }

    public void setChildcatId(String childcatId) {
        this.childcatId = childcatId;
    }

    public String getProductPincode() {
        return productPincode;
    }

    public void setProductPincode(String productPincode) {
        this.productPincode = productPincode;
    }

    public String getInStock() {
        return inStock;
    }

    public void setInStock(String inStock) {
        this.inStock = inStock;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getUnitValue() {
        return unitValue;
    }

    public void setUnitValue(String unitValue) {
        this.unitValue = unitValue;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getIncreament() {
        return increament;
    }

    public void setIncreament(String increament) {
        this.increament = increament;
    }

    public String getRewards() {
        return rewards;
    }

    public void setRewards(String rewards) {
        this.rewards = rewards;
    }

    public String getLocaCategory() {
        return locaCategory;
    }

    public void setLocaCategory(String locaCategory) {
        this.locaCategory = locaCategory;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getNetWeight() {
        return netWeight;
    }

    public void setNetWeight(String netWeight) {
        this.netWeight = netWeight;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getLocationFilter() {
        return locationFilter;
    }

    public void setLocationFilter(String locationFilter) {
        this.locationFilter = locationFilter;
    }

    public String getThumbnailsImage() {
        return thumbnailsImage;
    }

    public void setThumbnailsImage(String thumbnailsImage) {
        this.thumbnailsImage = thumbnailsImage;
    }

    public String getFarmerId() {
        return farmerId;
    }

    public void setFarmerId(String farmerId) {
        this.farmerId = farmerId;
    }

    public String getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(String discountPrice) {
        this.discountPrice = discountPrice;
    }

    public String getOffPercentage() {
        return offPercentage;
    }

    public void setOffPercentage(String offPercentage) {
        this.offPercentage = offPercentage;
    }

    public String getWeekendPromo() {
        return weekendPromo;
    }

    public void setWeekendPromo(String weekendPromo) {
        this.weekendPromo = weekendPromo;
    }

    public String getWishlistStatus() {
        return wishlistStatus;
    }

    public void setWishlistStatus(String wishlistStatus) {
        this.wishlistStatus = wishlistStatus;
    }


}
