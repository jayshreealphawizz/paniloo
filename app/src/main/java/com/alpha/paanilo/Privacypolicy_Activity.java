package com.alpha.paanilo;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.alpha.paanilo.model.privacyPolicyPkg.PrivacyPolicyPozo;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.utility.CheckNetwork;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Privacypolicy_Activity extends AppCompatActivity implements View.OnClickListener {
    AppCompatImageView iv_back; RecyclerView rv_city;
    View id_toolbar; AppCompatTextView tv_title,tv_text;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        id_toolbar = findViewById(R.id.id_toolbar);
        iv_back = id_toolbar.findViewById(R.id.id_back);
        tv_title = id_toolbar.findViewById(R.id.tv_tittle);
        tv_text = findViewById(R.id.tv_text);

        iv_back.setOnClickListener(this);


        rv_city = findViewById(R.id.id_rv);
        tv_title.setText(getString(R.string.privacypolicy));


        pd = new ProgressDialog(Privacypolicy_Activity.this, R.style.AppCompatAlertDialogStyle);
        if (CheckNetwork.isNetAvailable(getApplicationContext())) {
            getPrivacyPolicy();
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.plz_check_your_intrenet_text), Toast.LENGTH_SHORT).show();
        }
    }

    public void getPrivacyPolicy() {
        pd.setMessage("Loading");
        pd.show();

        (ApiClient.getClient().privacy_policy()).enqueue(new Callback<PrivacyPolicyPozo>() {
            @Override
            public void onResponse(Call<PrivacyPolicyPozo> call, Response<PrivacyPolicyPozo> response) {
                pd.dismiss();
                if (response.isSuccessful()) {
                    try {
                        PrivacyPolicyPozo termsAndConditionPozo = response.body();
                        if (termsAndConditionPozo.getStatus()) {
                            tv_text.setText(Html.fromHtml(termsAndConditionPozo.getData().getPrivacyDesc()));
                        } else {
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<PrivacyPolicyPozo> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(getApplicationContext(), getString(R.string.server_errors), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.id_back:
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                break;
        }
    }
}
