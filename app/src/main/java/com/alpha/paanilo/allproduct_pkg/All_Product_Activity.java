package com.alpha.paanilo.allproduct_pkg;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.alpha.paanilo.R;
import com.alpha.paanilo.filterPkg.FilterActivity;
import com.alpha.paanilo.model.HistoryData;
import com.alpha.paanilo.model.productByCategory.ProductByCategoryPozo;
import com.alpha.paanilo.model.productByCategory.ProductData;
import com.alpha.paanilo.product_detail_pkg.Product_details_Activity;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.utility.AppSession;
import com.alpha.paanilo.utility.CheckNetwork;
import com.alpha.paanilo.utility.Constants;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class All_Product_Activity extends AppCompatActivity implements View.OnClickListener, All_Product_Adapter.ProductOnClickListener {
    AppCompatImageView iv_back;
    RecyclerView rv_allproduct;
    ArrayList<HistoryData> historyList;
    View id_toolbar;
    AppCompatTextView tv_catname, tvProductFound, tvProductUnavailable;
    TextView tv_nodata;
    RelativeLayout rl_notification;
    LinearLayout ll_filer,ll_filer_mainlay, ll_sort;
    All_Product_Adapter searchAdapter;
    ProgressDialog pd;
    private List<ProductData> productList;
    AppCompatSpinner sp_sort;
    private String[] sorting_list;
    private EditText etProductSearch;
    private Constants constants;
    private SwipeRefreshLayout srlProduct;
    String catID,catName, VENDOR_ID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setContentView(R.layout.all_product_rv);

        init();

        etProductSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                searchAdapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {
                searchAdapter.getFilter().filter(s);
            }
        });


    }


    private void init() {
        constants = new Constants(getApplicationContext());
        srlProduct = findViewById(R.id.srlProductId);
        tvProductFound = findViewById(R.id.tvProductFoundId);
        tvProductUnavailable = findViewById(R.id.tvProductUnavailableId);
        ll_filer = findViewById(R.id.ll_filerId);
        ll_sort = findViewById(R.id.ll_sort);
        id_toolbar = findViewById(R.id.id_toolbar_product);
        tv_nodata = findViewById(R.id.tv_nodata);
        ll_filer_mainlay = findViewById(R.id.ll_filer_mainlay);
        iv_back = id_toolbar.findViewById(R.id.id_back);
        tv_catname = id_toolbar.findViewById(R.id.tv_catname);
        etProductSearch = id_toolbar.findViewById(R.id.etProductSearchId);
        etProductSearch = id_toolbar.findViewById(R.id.etProductSearchId);

        iv_back.setOnClickListener(this);
        ll_filer.setOnClickListener(this);
        ll_sort.setOnClickListener(this);


        Intent intent =getIntent();
        catID = intent.getStringExtra("catID");
        VENDOR_ID = intent.getStringExtra("vendorID");

        rv_allproduct = findViewById(R.id.id_rv_allproduct);

        historyList = new ArrayList<>();
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
        rv_allproduct.setLayoutManager(gridLayoutManager);
        searchAdapter = new All_Product_Adapter(All_Product_Activity.this, this,VENDOR_ID);
        rv_allproduct.setAdapter(searchAdapter);

        pd = new ProgressDialog(All_Product_Activity.this, R.style.AppCompatAlertDialogStyle);

        tv_catname.setText(getString(R.string.product));

        if (CheckNetwork.isNetAvailable(getApplicationContext())) {
                getProductByCategory(catID,VENDOR_ID);
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.plz_check_your_intrenet_text), Toast.LENGTH_SHORT).show();
        }


        srlProduct.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (CheckNetwork.isNetAvailable(getApplicationContext())) {
                    getProductByCategory(catID,VENDOR_ID);
                } else {
                    Toast.makeText(getApplicationContext(), "Check Network Connection", Toast.LENGTH_LONG).show();
                }
                srlProduct.setRefreshing(false);
            }
        });

    }

    public void sording_dialog() {
        final Dialog dialog = new Dialog(All_Product_Activity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_sorting);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;

        AppCompatImageView rllogout = dialog.findViewById(R.id.ivlogoutCloseId);
        TextView tv_asending = dialog.findViewById(R.id.tv_asending);
        TextView tv_desending = dialog.findViewById(R.id.tv_desending);
        TextView tv_price_hl = dialog.findViewById(R.id.tv_price_hl);
        TextView tv_price_lh = dialog.findViewById(R.id.tv_price_lh);

        rllogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        tv_asending.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                productname_asending();
                dialog.dismiss();
            }
        });
        tv_desending.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                productname_desending();
                dialog.dismiss();
            }
        });
        tv_price_hl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                productprice_hl();
                dialog.dismiss();
            }
        });
        tv_price_lh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                productprice_lh();
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void productprice_hl() {
        if (productList.size() > 0) {
            Collections.sort(productList, new Comparator<ProductData>() {
                @Override
                public int compare(final ProductData object1, final ProductData object2) {
                    return object1.getPrice().compareTo(object2.getPrice());
                }
            });
            Collections.reverse(productList);
            searchAdapter.notifyDataSetChanged();
        }
    }

    public void productprice_lh() {
        if (productList.size() > 0) {
            Collections.sort(productList, new Comparator<ProductData>() {
                @Override
                public int compare(final ProductData object1, final ProductData object2) {
                    return object1.getPrice().compareTo(object2.getPrice());
                }
            });
            searchAdapter.notifyDataSetChanged();
        }
    }

    public void productname_desending() {
        if (productList.size() > 0) {
            Collections.sort(productList, new Comparator<ProductData>() {
                @Override
                public int compare(final ProductData object1, final ProductData object2) {
                    return object1.getProductName().toLowerCase().compareTo(object2.getProductName().toLowerCase());
                }
            });
            Collections.reverse(productList);
            searchAdapter.notifyDataSetChanged();
        }
    }

    public void productname_asending() {
        if (productList.size() > 0) {
            Collections.sort(productList, new Comparator<ProductData>() {
                @Override
                public int compare(final ProductData object1, final ProductData object2) {
                    return object1.getProductName().toLowerCase().compareTo(object2.getProductName().toLowerCase());
                }
            });
            searchAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.id_back:
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                break;
            case R.id.ll_filerId:
                startActivity(new Intent(getApplicationContext(), FilterActivity.class));
                finish();
                break;
            case R.id.ll_sort:
                sording_dialog();
                break;
        }
    }


    public void getProductByCategory(final String catId, final String vendor_id) {
        pd.setMessage("Loading");
        pd.show();
        JsonObject jsonObject = new JsonObject();

        try {
            jsonObject.addProperty("category_id", catId);
            jsonObject.addProperty("farmer_id", vendor_id);

            System.out.println(catId+"==ALL PROJECT =json object======"+vendor_id);

        } catch (Exception e) {
            e.printStackTrace();
        }

        (ApiClient.getClient().getProductByCategory(jsonObject)).enqueue(new Callback<ProductByCategoryPozo>() {
            @Override
            public void onResponse(Call<ProductByCategoryPozo> call, Response<ProductByCategoryPozo> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {
                        ProductByCategoryPozo productByCategoryPozo = response.body();
                        String resMessage = productByCategoryPozo.getMessage();
                        if (productByCategoryPozo.getStatus()) {
                            productList = productByCategoryPozo.getData();
                            tvProductFound.setText(productList.size() + " Products Found");
                            searchAdapter.addProductList(productList);
                            tvProductUnavailable.setVisibility(View.GONE);
                            ll_filer.setVisibility(View.VISIBLE);
                            ll_sort.setVisibility(View.VISIBLE);
                            ll_filer_mainlay.setVisibility(View.VISIBLE);

                            constants.setVendor(vendor_id);
                        } else {
                            tvProductFound.setVisibility(View.GONE);
                            tvProductUnavailable.setVisibility(View.VISIBLE);
                            ll_filer.setVisibility(View.GONE);
                            ll_sort.setVisibility(View.GONE);
                            ll_filer_mainlay.setVisibility(View.GONE);
                            tvProductUnavailable.setText(resMessage);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    tvProductUnavailable.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<ProductByCategoryPozo> call, Throwable t) {
                tvProductUnavailable.setVisibility(View.GONE);
                pd.dismiss();
                Toast.makeText(All_Product_Activity.this, getString(R.string.server_errors), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClick(View view, int position, ProductData productData) {
        switch (view.getId()) {
            case R.id.id_userpic:
                AppSession.setStringPreferences(getApplicationContext(), "productID", productData.getProductId());
                AppSession.setStringPreferences(getApplicationContext(), "entryFlag", "catProduct");
                startActivity(new Intent(getApplicationContext(), Product_details_Activity.class)
                        .putExtra("productID", productData.getProductId())
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                finish();
                break;
        }
    }

}

