
package com.alpha.paanilo.StallPkg.stallProfilePkg;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataVendor {

    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("user_name")
    @Expose
    private String userName;
    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("user_email")
    @Expose
    private String userEmail;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("user_phone")
    @Expose
    private String userPhone;
    @SerializedName("user_fullname")
    @Expose
    private String userFullname;
    @SerializedName("user_password")
    @Expose
    private String userPassword;
    @SerializedName("id_card_name")
    @Expose
    private String idCardName;
    @SerializedName("pan_card_number")
    @Expose
    private String panCardNumber;
    @SerializedName("user_type_id")
    @Expose
    private String userTypeId;
    @SerializedName("user_bdate")
    @Expose
    private String userBdate;
    @SerializedName("is_email_varified")
    @Expose
    private String isEmailVarified;
    @SerializedName("varified_token")
    @Expose
    private String varifiedToken;
    @SerializedName("varification_code")
    @Expose
    private String varificationCode;
    @SerializedName("user_gcm_code")
    @Expose
    private String userGcmCode;
    @SerializedName("user_ios_token")
    @Expose
    private String userIosToken;
    @SerializedName("user_image")
    @Expose
    private String userImage;
    @SerializedName("user_city")
    @Expose
    private String userCity;
    @SerializedName("user_state")
    @Expose
    private String userState;
    @SerializedName("pincode")
    @Expose
    private String pincode;
    @SerializedName("id_proof")
    @Expose
    private String idProof;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("street_address")
    @Expose
    private String streetAddress;
    @SerializedName("user_login_status")
    @Expose
    private String userLoginStatus;
    @SerializedName("user_status")
    @Expose
    private String userStatus;
    @SerializedName("is_status")
    @Expose
    private String isStatus;
    @SerializedName("role_status")
    @Expose
    private String roleStatus;
    @SerializedName("bankAcoountHolderName")
    @Expose
    private String bankAcoountHolderName;
    @SerializedName("bankName")
    @Expose
    private String bankName;
    @SerializedName("accountNo")
    @Expose
    private String accountNo;
    @SerializedName("ifscCode")
    @Expose
    private String ifscCode;
    @SerializedName("current_location")
    @Expose
    private String currentLocation;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("store_name")
    @Expose
    private String storeName;
    @SerializedName("firebaseToken")
    @Expose
    private String firebaseToken;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getUserFullname() {
        return userFullname;
    }

    public void setUserFullname(String userFullname) {
        this.userFullname = userFullname;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getIdCardName() {
        return idCardName;
    }

    public void setIdCardName(String idCardName) {
        this.idCardName = idCardName;
    }

    public String getPanCardNumber() {
        return panCardNumber;
    }

    public void setPanCardNumber(String panCardNumber) {
        this.panCardNumber = panCardNumber;
    }

    public String getUserTypeId() {
        return userTypeId;
    }

    public void setUserTypeId(String userTypeId) {
        this.userTypeId = userTypeId;
    }

    public String getUserBdate() {
        return userBdate;
    }

    public void setUserBdate(String userBdate) {
        this.userBdate = userBdate;
    }

    public String getIsEmailVarified() {
        return isEmailVarified;
    }

    public void setIsEmailVarified(String isEmailVarified) {
        this.isEmailVarified = isEmailVarified;
    }

    public String getVarifiedToken() {
        return varifiedToken;
    }

    public void setVarifiedToken(String varifiedToken) {
        this.varifiedToken = varifiedToken;
    }

    public String getVarificationCode() {
        return varificationCode;
    }

    public void setVarificationCode(String varificationCode) {
        this.varificationCode = varificationCode;
    }

    public String getUserGcmCode() {
        return userGcmCode;
    }

    public void setUserGcmCode(String userGcmCode) {
        this.userGcmCode = userGcmCode;
    }

    public String getUserIosToken() {
        return userIosToken;
    }

    public void setUserIosToken(String userIosToken) {
        this.userIosToken = userIosToken;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getUserCity() {
        return userCity;
    }

    public void setUserCity(String userCity) {
        this.userCity = userCity;
    }

    public String getUserState() {
        return userState;
    }

    public void setUserState(String userState) {
        this.userState = userState;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getIdProof() {
        return idProof;
    }

    public void setIdProof(String idProof) {
        this.idProof = idProof;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getUserLoginStatus() {
        return userLoginStatus;
    }

    public void setUserLoginStatus(String userLoginStatus) {
        this.userLoginStatus = userLoginStatus;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public String getIsStatus() {
        return isStatus;
    }

    public void setIsStatus(String isStatus) {
        this.isStatus = isStatus;
    }

    public String getRoleStatus() {
        return roleStatus;
    }

    public void setRoleStatus(String roleStatus) {
        this.roleStatus = roleStatus;
    }

    public String getBankAcoountHolderName() {
        return bankAcoountHolderName;
    }

    public void setBankAcoountHolderName(String bankAcoountHolderName) {
        this.bankAcoountHolderName = bankAcoountHolderName;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }

    public String getCurrentLocation() {
        return currentLocation;
    }

    public void setCurrentLocation(String currentLocation) {
        this.currentLocation = currentLocation;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getFirebaseToken() {
        return firebaseToken;
    }

    public void setFirebaseToken(String firebaseToken) {
        this.firebaseToken = firebaseToken;
    }

}
