package com.alpha.paanilo.StallPkg;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alpha.paanilo.R;
import com.alpha.paanilo.StallPkg.stallProfilePkg.DataProduct;
import com.alpha.paanilo.StallPkg.stallProfilePkg.DataVendor;
import com.alpha.paanilo.StallPkg.stallProfilePkg.StallProfilePozo;
import com.alpha.paanilo.product_detail_pkg.Product_details_Activity;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.utility.AppSession;
import com.alpha.paanilo.utility.CheckNetwork;
import com.alpha.paanilo.utility.Constants;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.JsonObject;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StallProfileActivity extends Fragment implements Stall_Product_Adapter.ProductOnClickListener {
    private AppCompatTextView tvStallNameid, tvStallEmailId, tvNumberId, tvStallAddress, tvProductFound, tvProductUnavailable;
    private CircleImageView civProfile;
    private RecyclerView rv_allproduct;
    private ApiClient apiClient;
    private ProgressBar pbStallPrifile;
    private ProgressDialog pd;
    private List<DataProduct> dataProductList;
    private List<DataVendor> dataVendorList;
    private String vendor_id = "55", imageUrl;
    private Stall_Product_Adapter stall_product_adapter;
    private Constants constants;
    View id_toolbar;
    AppCompatImageView iv_back;
    AppCompatTextView tv_title;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_stall_profile, container, false);
        imageUrl = getArguments().getString("url");
        init(v);
        if (CheckNetwork.isNetAvailable(getActivity())) {
            getStallProfile();
        } else {
            Toast.makeText(getActivity(), getString(R.string.plz_check_your_intrenet_text), Toast.LENGTH_SHORT).show();
        }

        return v;
    }


    private void init(View view) {

        id_toolbar = view.findViewById(R.id.id_toolbar);
        iv_back = id_toolbar.findViewById(R.id.id_back);
        tv_title = id_toolbar.findViewById(R.id.tv_tittle);
        tv_title.setText(getString(R.string.profile));

        constants = new Constants(getActivity());
        pbStallPrifile = view.findViewById(R.id.pbStallPrifileId);
        rv_allproduct = view.findViewById(R.id.id_rv_allproduct);
        civProfile = view.findViewById(R.id.civProfileId);
        tvStallNameid = view.findViewById(R.id.tvStallNameid);
        tvStallEmailId = view.findViewById(R.id.tvStallEmailId);
        tvNumberId = view.findViewById(R.id.tvNumberId);
        tvStallAddress = view.findViewById(R.id.tvStallAddressId);
        tvProductFound = view.findViewById(R.id.tvProductFoundId);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        rv_allproduct.setLayoutManager(gridLayoutManager);
        stall_product_adapter = new Stall_Product_Adapter(getActivity(), this);
        rv_allproduct.setAdapter(stall_product_adapter);

        tvProductUnavailable = view.findViewById(R.id.tvProductUnavailableId);
        pd = new ProgressDialog(getActivity(), R.style.AppCompatAlertDialogStyle);

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // onBackPressed();
                //  finish();
                getFragmentManager().popBackStack();
            }
        });

        try {
            if (imageUrl == null || imageUrl.equals("")) {
            } else {
                Glide.with(getActivity())
                        .load(ApiClient.VENDOR_PROFILE_URL + imageUrl)
                        .placeholder(R.drawable.placeholder)
                        .into(civProfile);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void getStallProfile() {
        pd.setMessage("Loading");
        pd.show();
        JsonObject jsonObject = new JsonObject();
        try {
            jsonObject.addProperty("vendor_id", constants.getVendror());

        } catch (Exception e) {
            e.printStackTrace();
        }

        (ApiClient.getClient().getVendorandproductbyid(jsonObject)).enqueue(new Callback<StallProfilePozo>() {
            @Override
            public void onResponse(Call<StallProfilePozo> call, Response<StallProfilePozo> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {
                        StallProfilePozo stallProfilePozo = response.body();
                        String resMessage = stallProfilePozo.getMessage();
                        if (stallProfilePozo.getStatus()) {
                            dataProductList = stallProfilePozo.getDataProduct();
                            dataVendorList = stallProfilePozo.getDataVendor();
                            tvProductFound.setText(dataProductList.size() + " Products Found");
                            stall_product_adapter.addProductList(dataProductList);
                            if (dataProductList.size() == 0) {
                                tvProductUnavailable.setVisibility(View.VISIBLE);
                                tvProductFound.setVisibility(View.GONE);
                            } else {
                                tvProductUnavailable.setVisibility(View.GONE);
                                tvProductFound.setVisibility(View.VISIBLE);
                            }
                            tvStallNameid.setText(dataVendorList.get(0).getFirstname() + " " + dataVendorList.get(0).getLastname());
                            tvStallEmailId.setText(dataVendorList.get(0).getUserEmail());
                            tvNumberId.setText(dataVendorList.get(0).getUserPhone());
                            tvStallAddress.setText(dataVendorList.get(0).getCurrentLocation());

                        } else {
                            tvProductUnavailable.setVisibility(View.VISIBLE);
                            tvProductFound.setVisibility(View.GONE);
                            tvProductUnavailable.setVisibility(View.VISIBLE);
                            tvProductUnavailable.setText(resMessage);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                } else {

                    tvProductUnavailable.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<StallProfilePozo> call, Throwable t) {
                tvProductUnavailable.setVisibility(View.GONE);
                pd.dismiss();
                Toast.makeText(getActivity(), getString(R.string.server_errors), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClick(View view, int position, DataProduct productData) {
        switch (view.getId()) {
            case R.id.id_userpic:
                AppSession.setStringPreferences(getActivity(), "productID", productData.getProductId());
                AppSession.setStringPreferences(getActivity(), "entryFlag", "profile");
                startActivity(new Intent(getActivity(), Product_details_Activity.class)
                        .putExtra("productID", productData.getProductId())
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                //finish();
                break;
        }
    }
}
