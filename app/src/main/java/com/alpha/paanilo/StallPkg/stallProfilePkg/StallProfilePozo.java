
package com.alpha.paanilo.StallPkg.stallProfilePkg;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StallProfilePozo {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data_vendor")
    @Expose
    private List<DataVendor> dataVendor = null;
    @SerializedName("data_product")
    @Expose
    private List<DataProduct> dataProduct = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataVendor> getDataVendor() {
        return dataVendor;
    }

    public void setDataVendor(List<DataVendor> dataVendor) {
        this.dataVendor = dataVendor;
    }

    public List<DataProduct> getDataProduct() {
        return dataProduct;
    }

    public void setDataProduct(List<DataProduct> dataProduct) {
        this.dataProduct = dataProduct;
    }

}
