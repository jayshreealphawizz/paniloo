package com.alpha.paanilo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AllOrderModel {
//    @SerializedName("status")
//    @Expose
//    private Boolean status;
//    @SerializedName("message")
//    @Expose
//    private String message;
//    @SerializedName("data")
//    @Expose
//    private List<DatumallOrder> data = null;
//
//    public Boolean getStatus() {
//        return status;
//    }
//
//    public void setStatus(Boolean status) {
//        this.status = status;
//    }
//
//    public String getMessage() {
//        return message;
//    }
//
//    public void setMessage(String message) {
//        this.message = message;
//    }
//
//    public List<DatumallOrder> getData() {
//        return data;
//    }
//
//    public void setData(List<DatumallOrder> data) {
//        this.data = data;
//    }

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("total_earn_money")
    @Expose
    private String total_earn_money;

    public String getTotal_earn_money() {
        return total_earn_money;
    }

    public void setTotal_earn_money(String total_earn_money) {
        this.total_earn_money = total_earn_money;
    }

    @SerializedName("data")
    @Expose
    private List<DatumallOrder> data = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DatumallOrder> getData() {
        return data;
    }

    public void setData(List<DatumallOrder> data) {
        this.data = data;
    }

}
