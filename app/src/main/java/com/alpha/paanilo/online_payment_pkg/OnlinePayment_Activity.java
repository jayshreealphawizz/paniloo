package com.alpha.paanilo.online_payment_pkg;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.app.ActivityCompat;

import com.alpha.paanilo.Notification_Activity;
import com.alpha.paanilo.R;
import com.alpha.paanilo.Roomdatabase.DatabaseClient;
import com.alpha.paanilo.Roomdatabase.ProductDataTask;
import com.alpha.paanilo.cart_pkg.OrderConfirmation_Activity;
import com.alpha.paanilo.model.PaymentSlipPozo;
import com.alpha.paanilo.model.bankDetailPkg.BankDetailPozo;
import com.alpha.paanilo.model.checkoutModlePkg.CheckoutPozo;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.utility.CheckNetwork;
import com.alpha.paanilo.utility.Constants;
import com.alpha.paanilo.utility.CustomToast;
import com.alpha.paanilo.utility.ImagePicker;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OnlinePayment_Activity extends AppCompatActivity implements View.OnClickListener , PaymentResultListener {
    AppCompatButton btn_continue;
    View toolbar;
    AppCompatTextView tv_tittle;
    AppCompatImageView id_notification, id_back;
    private ProgressDialog pd;
    private TextInputEditText tv_user_name,
            tv_bank_name, tv_account_number, tv_ifsc_code;

    private TextView tvSelectSlip;
    private AppCompatImageView ivCross, ivSlip;

    private static final int PERMISSION_READ_EXTERNAL_STORAGE = 100;
    private static final int SELECT_PICTURE = 101;
    String profilImgPath, userid;
    private Constants constants;
    String account_name,user_id, delivery_charge, delivery_time, add_coupon, payment_method,
            total_amount, delivery_address, outlets_pickup_location, total_items, date, center_pickup_date,
            center_pickup_time, add_note, products ,latitude ,longitude , orderdeliverytime;


    private JSONArray jsonArray;
    private JSONObject jsonObject;
    List<ProductDataTask> tasksSize;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.online_payment_fragment);

        toolbar = findViewById(R.id.id_toolbar);
        tv_tittle = toolbar.findViewById(R.id.tv_tittle);
        id_notification = toolbar.findViewById(R.id.id_notification);
        id_back = toolbar.findViewById(R.id.id_back);
        tv_tittle.setText(getString(R.string.myorder));

        jsonArray = new JSONArray();

        ivCross = findViewById(R.id.ivCrossId);
        ivSlip = findViewById(R.id.ivSlipid);
        tvSelectSlip = findViewById(R.id.tvSelectSlipid);
        tv_ifsc_code = findViewById(R.id.tv_ifsc_code);
        tv_account_number = findViewById(R.id.tv_account_number);
        tv_bank_name = findViewById(R.id.tv_bank_name);
        tv_user_name = findViewById(R.id.tv_user_name);
        btn_continue = findViewById(R.id.btn_continue);
        btn_continue.setOnClickListener(this);
        ivCross.setOnClickListener(this);
        id_back.setOnClickListener(this);
        id_notification.setOnClickListener(this);
        tvSelectSlip.setOnClickListener(this);
        constants = new Constants(getApplicationContext());
        userid = constants.getUserID();
        pd = new ProgressDialog(OnlinePayment_Activity.this, R.style.AppCompatAlertDialogStyle);

        System.out.println("vendore id online pay===="+constants.getVendror());

       /* if (CheckNetwork.isNetAvailable(getApplicationContext())) {
            getBankDetail(constants.getVendror());
        } else {

        }*/

        getCartDataTasks();
        user_id = getIntent().getStringExtra("user_id");
        delivery_charge = getIntent().getStringExtra("delivery_charge");
        delivery_time = getIntent().getStringExtra("delivery_time");
        add_coupon = getIntent().getStringExtra("add_coupon");
        payment_method = getIntent().getStringExtra("payment_method");
        total_amount = getIntent().getStringExtra("total_amount");
        delivery_address = getIntent().getStringExtra("delivery_address");
        outlets_pickup_location = getIntent().getStringExtra("outlets_pickup_location");
        total_items = getIntent().getStringExtra("total_items");
        date = getIntent().getStringExtra("date");
        center_pickup_date = getIntent().getStringExtra("center_pickup_date");
        center_pickup_time = getIntent().getStringExtra("center_pickup_time");
        add_note = getIntent().getStringExtra("add_note");
//        products = getIntent().getStringExtra("products");

        latitude = getIntent().getStringExtra("latitude");
        longitude = getIntent().getStringExtra("longitude");
        orderdeliverytime = getIntent().getStringExtra("order_delivery_time");


        Log.e("Rahulraj" , "latitude::: " +  latitude) ;
        Log.e("Rahulraj" , "longitude::: " +  longitude) ;

        if (CheckNetwork.isNetAvailable(getApplicationContext())) {
            startRazorPayPayment(total_amount);
        } else {

        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvSelectSlipid:
                askStoragePermission();
                break;
            case R.id.ivCrossId:
                profilImgPath = null;
                ivSlip.setImageResource(R.drawable.ic_user);
                break;
            case R.id.btn_continue:
                System.out.println("account name==="+account_name);
                if(account_name.isEmpty()) {
                    Toast.makeText(this, getResources().getString(R.string.sry_we_cant_procced), Toast.LENGTH_SHORT).show();
                }
                else{
                    if (profilImgPath == null) {
                        Toast.makeText(getApplicationContext(), "Please upload your transfer slip", Toast.LENGTH_LONG).show();
                    } else {
                        if (CheckNetwork.isNetAvailable(getApplicationContext())) {
                            uploadSlop(profilImgPath);
                        } else {
                            new CustomToast().Show_Toast(this, v, getResources().getString(R.string.plz_check_your_intrenet_text));
                        }
                    }
                }
                break;

            case R.id.id_back:
                finish();
//                getActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                break;

            case R.id.id_notification:
                startActivity(new Intent(getApplicationContext(), Notification_Activity.class));
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                break;
        }
    }

    private void getBankDetail(final String vendorId) {
        pd.setMessage("Loading");
        pd.show();
        JsonObject jsonObject = new JsonObject();
        try {
            jsonObject.addProperty("user_id", vendorId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        (ApiClient.getClient().getAdminDetails(jsonObject)).enqueue(new Callback<BankDetailPozo>() {
            @Override
            public void onResponse(Call<BankDetailPozo> call, Response<BankDetailPozo> response) {
                if (response.isSuccessful()) {
                    try {
                        pd.dismiss();
                        BankDetailPozo bankDetailPozo = response.body();
                        Log.e(vendorId+"TAG", "payment detail response : " + new Gson().toJson(response.body()));
                        if (bankDetailPozo.getStatus()) {
                                account_name = bankDetailPozo.getData().getAccountNo();
                                tv_user_name.setText(bankDetailPozo.getData().getBankAcoountHolderName());
                                tv_bank_name.setText(bankDetailPozo.getData().getBankName());
                                tv_account_number.setText(bankDetailPozo.getData().getAccountNo());
                                tv_ifsc_code.setText(bankDetailPozo.getData().getIfscCode());

                        } else {
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        pd.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(Call<BankDetailPozo> call, Throwable t) {
                pd.dismiss();
            }
        });


    }


    //    =================== image upload ======
    private void askStoragePermission() {
        if (ActivityCompat.checkSelfPermission(OnlinePayment_Activity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            {
                ActivityCompat.requestPermissions(OnlinePayment_Activity.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        PERMISSION_READ_EXTERNAL_STORAGE);
            }
        } else {
            chooseFromGallery();
        }
    }

    private void chooseFromGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    chooseFromGallery();
                } else {
                    Toast.makeText(getApplicationContext(), "Permission Denied", Toast.LENGTH_LONG).show();
                }
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SELECT_PICTURE) {
            if (resultCode == RESULT_OK) {
                try {
                    Uri imageUri = data.getData();
                    Bitmap bitmap = ImagePicker.getImageFromResult(getApplicationContext(), resultCode, data);
                    ivSlip.setImageBitmap(bitmap);
                    profilImgPath = getRealPathFromURI(getApplicationContext(), imageUri);
                    System.out.println("profile path===" + profilImgPath);

                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    //    ===============================

    private void uploadSlop(String profilImgPath) {
        pd.setMessage("Loading");
        pd.show();
        MultipartBody.Part imgFileStation = null;
        if (profilImgPath == null) {
        } else {
            File fileForImage = new File(profilImgPath);
            RequestBody requestFileOne = RequestBody.create(MediaType.parse("multipart/form-data"), fileForImage);
            imgFileStation = MultipartBody.Part.createFormData("file", fileForImage.getName(), requestFileOne);
        }

        MultipartBody.Part user_id = MultipartBody.Part.createFormData("user_id", constants.getVendror());
        (ApiClient.getClient().transeferPaySlip(user_id, imgFileStation)).enqueue(new Callback<PaymentSlipPozo>() {
            @Override
            public void onResponse(Call<PaymentSlipPozo> call, Response<PaymentSlipPozo> response) {
                pd.dismiss();
                if (response.isSuccessful()) {
                    try {
                        pd.dismiss();
                        Log.e("TAG", "UPLOAD SLIP  response : " + new Gson().toJson(response.body()));
                        PaymentSlipPozo paymentSlipPozo = response.body();
                        if (paymentSlipPozo.getStatus()) {
                            Toast.makeText(OnlinePayment_Activity.this, paymentSlipPozo.getMessage(), Toast.LENGTH_SHORT).show();
                            if (CheckNetwork.isNetAvailable(getApplicationContext())) {
                                checkoutApi();
                            } else {

                            }
                        } else {
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<PaymentSlipPozo> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(OnlinePayment_Activity.this, getString(R.string.server_errors), Toast.LENGTH_SHORT).show();
            }
        });

    }


    private void checkoutApi() {
        if (delivery_charge == null) {
            delivery_charge = "0";
        }

        if (add_coupon == null) {
            add_coupon = "";
        }
        pd.setMessage("Loading");
        pd.show();
        JSONObject jsonObj_ = null;
        try {
            jsonObj_ = new JSONObject();
            jsonObj_.put("user_id", userid);
            jsonObj_.put("delivery_charge", delivery_charge);
            jsonObj_.put("delivery_time", delivery_time);
            jsonObj_.put("add_coupon", add_coupon);
            jsonObj_.put("payment_method", "ONLINE");
            jsonObj_.put("total_amount", total_amount);
            jsonObj_.put("delivery_address", delivery_address);
            jsonObj_.put("outlets_pickup_location", "");
            jsonObj_.put("total_items", total_items);
            jsonObj_.put("date", getCurrentDate());
            jsonObj_.put("center_pickup_date", getCurrentDate());
            jsonObj_.put("center_pickup_time", getCurrentTime());
            jsonObj_.put("add_note", add_note);
            jsonObj_.put("latitude", latitude);
            jsonObj_.put("longitude", longitude);
            jsonObj_.put("order_delivery_time", orderdeliverytime);
            jsonObj_.put("products", jsonArray

            );






        } catch (JSONException e) {
            e.printStackTrace();
        }

        System.out.println("upload checkout ======"+jsonObj_.toString());

        (ApiClient.getClient().checkout(jsonObj_.toString())).enqueue(new Callback<CheckoutPozo>() {
            @Override
            public void onResponse(Call<CheckoutPozo> call, Response<CheckoutPozo> response) {
                Log.e("response ", "rsoaynebt " +  new Gson().toJson(response.body()));

                if (response.isSuccessful()) {
//                    Toast.makeText(OnlinePayment_Activity.this, "" + checkoutResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    try {
                        pd.dismiss();
                        CheckoutPozo checkoutResponse = response.body();
                        Toast.makeText(OnlinePayment_Activity.this, "" + checkoutResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        if (checkoutResponse.getStatus()) {

//                            clearCart();
                            Intent intent = new Intent(getApplicationContext(), OrderPlaced_Activity.class);
                            intent.putExtra("saleId", "" + checkoutResponse.getResult());
                            intent.putExtra("orderplaced", "" + checkoutResponse.getMessage());

                            startActivity(intent);

                        } else {
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        pd.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(Call<CheckoutPozo> call, Throwable t) {
                pd.dismiss();
            }
        });

    }


    private String getCurrentDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
        String datetime = dateformat.format(c.getTime());
        return datetime;
    }


    private String getCurrentTime() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat dateformat = new SimpleDateFormat("hh:mm");
        String datetime = dateformat.format(c.getTime());
        return datetime;
    }

    private void clearCart() {
        class GetTasks extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... voids) {
                DatabaseClient.getInstance(getApplicationContext())
                        .getAppDatabase()
                        .taskDao()
                        .delete();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

            }
        }
        GetTasks gt = new GetTasks();
        gt.execute();
    }

    private void getCartDataTasks() {
        class GetTasks extends AsyncTask<Void, Void, List<ProductDataTask>> {
            @Override
            protected List<ProductDataTask> doInBackground(Void... voids) {
                List<ProductDataTask> taskList = DatabaseClient
                        .getInstance(getApplicationContext())
                        .getAppDatabase()
                        .taskDao()
                        .getAll();
                return taskList;
            }

            @Override
            protected void onPostExecute(List<ProductDataTask> tasks) {
                super.onPostExecute(tasks);
                tasksSize = tasks;
                for (int i = 0; i < tasks.size(); i++) {
                    jsonObject = new JSONObject();
                    try {
                        jsonObject.put("product_name", tasks.get(i).getProduct_name());
                        jsonObject.put("product_description", tasks.get(i).getProduct_description());
                        jsonObject.put("product_id", tasks.get(i).getProduct_id());
                        jsonObject.put("quantity", tasks.get(i).getQuantity());
                        jsonObject.put("product_image", tasks.get(i).getProduct_image());
                        jsonObject.put("total_amount", tasks.get(i).getPrice());
                        jsonObject.put("price", tasks.get(i).getTotal());
                        jsonObject.put("net_weight", "");
                        jsonObject.put("in_stock", tasks.get(i).getIn_stock());
                        jsonObject.put("wishlist_status", "");
                        jsonObject.put("count_status", "");
                        jsonObject.put("off_percentage", "");
                        jsonObject.put("discount_price", tasks.get(i).getDiscount_price());
                        jsonObject.put("vendor_id", tasks.get(i).getVendorId());
                        jsonArray.put(jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
        }

        GetTasks gt = new GetTasks();
        gt.execute();
    }

    public void startRazorPayPayment(String amount) {
        final Checkout co = new Checkout();
//        rzp_test_QrkS2B01UMFJ95     //test key
        co.setKeyID("rzp_test_QrkS2B01UMFJ95");//paanilo test key
//        co.setKeyID("rzp_test_GC7PRrOSR8GIQf");//oro test key
        co.setImage(R.drawable.launcher);
        final OnlinePayment_Activity activity = this;

//        context = getApplicationContext() ;
//        final ProductFragment context = this;
        try {
            System.out.println(constants.getEmail() + "------add wallet amount------" + amount + "===name =====" + constants.getUsername());

            JSONObject options = new JSONObject();
            options.put("name", constants.getUsername());
            options.put("description", "");
            //You can omit the image option to fetch the image from dashboard
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png");
            options.put("currency", "INR");
            options.put("amount", Float.parseFloat(amount) * 100);

            JSONObject preFill = new JSONObject();
            preFill.put("email", constants.getEmail());
            preFill.put("contact", "91" + "");
            options.put("prefill", preFill);


            co.open(activity, options);
        } catch (Exception e) {
            Log.e("rahulraj ", "walletpage222");
            Toast.makeText(getApplicationContext(), "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }
    }


    @Override
    public void onPaymentSuccess(String razorpayPaymentID) {
        try {
            Log.e("rahulraj ", "walletpage");
            checkoutApi();

            Toast.makeText(getApplicationContext(), "Payment Successful: ", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
        }
    }

    @Override
    public void onPaymentError(int code, String response) {
        try {
            Toast.makeText(getApplicationContext(), "Payment failed ", Toast.LENGTH_SHORT).show();
//            finish();
        } catch (Exception e) {
        }
    }










}
