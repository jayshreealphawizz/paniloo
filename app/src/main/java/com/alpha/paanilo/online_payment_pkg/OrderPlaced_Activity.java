package com.alpha.paanilo.online_payment_pkg;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.alpha.paanilo.HomeMainActivity;
import com.alpha.paanilo.R;
import com.alpha.paanilo.Roomdatabase.DatabaseClient;
import com.alpha.paanilo.invoicePkg.Invoice_Activity;
import com.alpha.paanilo.utility.AppSession;

public class OrderPlaced_Activity extends AppCompatActivity {

    TextView tvplaced;
    private RelativeLayout rlViewOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_placed);
        rlViewOrder = findViewById(R.id.rlViewOrderId);
        tvplaced = findViewById(R.id.tvplaced);
        final String saleId = getIntent().getStringExtra("saleId");
        final String orderplaceddata = getIntent().getStringExtra("orderplaced");


        rlViewOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppSession.setStringPreferences(getApplicationContext(), "orderFlag", "OrderHistory");
                Intent intent = new Intent(getApplicationContext(), Invoice_Activity.class);
                intent.putExtra("saleId", saleId);

                intent.putExtra("orderplaced", orderplaceddata);


                startActivity(intent);
//                clearCart();
            }
        });

        tvplaced.setText("" + orderplaceddata);
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//
//            }
//        }, 2000);

       /* try {
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    onBackPressed();
                }
            }, 1000);
        } catch (Exception e) {
            e.printStackTrace();
        }*/

    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(OrderPlaced_Activity.this, HomeMainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
    }

    private void clearCart() {
        class GetTasks extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... voids) {
                DatabaseClient.getInstance(getApplicationContext())
                        .getAppDatabase()
                        .taskDao()
                        .delete();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

            }
        }
        GetTasks gt = new GetTasks();
        gt.execute();
    }
}
