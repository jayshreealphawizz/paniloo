package com.alpha.paanilo.online_payment_pkg;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;

import com.alpha.paanilo.R;
import com.google.android.material.textfield.TextInputEditText;

public class Online_payment_Fragment extends Fragment implements View.OnClickListener {
    View v;
    TextInputEditText tv_user_name,tv_bank_name,tv_account_number,tv_ifsc_code,ll_termcondition;
    AppCompatButton btn_continue;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.online_payment_fragment, container, false);
        tv_user_name = v.findViewById(R.id.tv_user_name);
        tv_bank_name = v.findViewById(R.id.tv_bank_name);
        tv_account_number = v.findViewById(R.id.tv_account_number);
        tv_ifsc_code = v.findViewById(R.id.tv_ifsc_code);
        btn_continue = v.findViewById(R.id.btn_continue);
        btn_continue.setOnClickListener(this);
        return v;
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_continue:
                break;
        }

    }
}
