package com.alpha.paanilo.invoicePkg;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alpha.paanilo.BuildConfig;
import com.alpha.paanilo.HomeMainActivity;
import com.alpha.paanilo.MyOrder_Activity;
import com.alpha.paanilo.R;
import com.alpha.paanilo.Roomdatabase.DatabaseClient;
import com.alpha.paanilo.cart_pkg.OrderConfirmation_Activity;
import com.alpha.paanilo.cart_pkg.ReOrderConfirmation_Activity;
import com.alpha.paanilo.downloadingPdf.FileDownloader;
import com.alpha.paanilo.model.DownloadInvoicePozo;
import com.alpha.paanilo.myOrderPkg.orderHistoryPkg.CancelOrderPozo;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.utility.AppSession;
import com.alpha.paanilo.utility.CheckNetwork;
import com.alpha.paanilo.utility.Constants;
import com.alpha.paanilo.utility.FileDownloading;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Invoice_Activity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "MainActivity";
    private static final String[] PERMISSIONS = {android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private static final int REQUEST_CODE_ASK_PERMISSIONS = 121;
    AppCompatImageView iv_back;
    RecyclerView rv_city;
    ArrayList<InvoiceInfo> invoiceList;
    View id_toolbar;
    AppCompatTextView tv_title, tv_nodata;
    RelativeLayout rl_notification;
    ProgressDialog pd;
    TextView tv_date, tv_payment, tvNotesId, tvVoucherDiscountId, tvDeliveryChargeId, tv_date1, tv_totalprice3, tv_totalprice2, tv_payment1, tv_invoiceno2, tv_subtotal_price1, tv_totalprice1, tv_invoiceno, tv_totalprice, tv_subtotal_price;
    AppCompatTextView tv_invoiceno1, tvDateInvoice;
    AppCompatImageView id_upload;
    TextView tv_shopname, tv_addressseller, tvContactNo, tvbtncancel, tvreorder;
    LinearLayout tvLatoutcall;
    String ContactNumber ,statusID;
    private String saleId ;
    private ScrollView svDownload;
    private FileDownloading fileDownloading;
    private String downloadUrl, back;
    private Constants constants;

    private static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice);
        ActivityCompat.requestPermissions(Invoice_Activity.this, PERMISSIONS, 112);
        back = AppSession.getStringPreferences(getApplicationContext(), "orderFlag");
        pd = new ProgressDialog(Invoice_Activity.this, R.style.AppCompatAlertDialogStyle);
        id_toolbar = findViewById(R.id.id_toolbar);
        iv_back = id_toolbar.findViewById(R.id.id_back);
        tv_title = id_toolbar.findViewById(R.id.tv_tittle);
        id_upload = id_toolbar.findViewById(R.id.id_upload);
        invoiceList = new ArrayList<>();
        id_upload.setOnClickListener(this);
        iv_back.setOnClickListener(this);
        tv_date = findViewById(R.id.tv_date);
        tvbtncancel = findViewById(R.id.tvbtncancel);

        tvreorder = findViewById(R.id.tvreorder);
        svDownload = findViewById(R.id.svDownloadId);

        tv_shopname = findViewById(R.id.tv_shopname);
        tv_addressseller = findViewById(R.id.tv_addressseller);
        tvContactNo = findViewById(R.id.tvContactNo);

        tvLatoutcall = findViewById(R.id.tvLatoutcall);
        tvVoucherDiscountId = findViewById(R.id.tvVoucherDiscountId);
        tvDeliveryChargeId = findViewById(R.id.tvDeliveryChargeId);
        tvNotesId = findViewById(R.id.tvNotesId);
        tvDateInvoice = findViewById(R.id.tvDateInvoice);
        tv_payment = findViewById(R.id.tv_payment);
        tv_invoiceno = findViewById(R.id.tv_invoiceno);
        tv_invoiceno1 = findViewById(R.id.tv_invoiceno1);
        tv_totalprice = findViewById(R.id.tv_totalprice);
        tv_subtotal_price = findViewById(R.id.tv_subtotal_price);
        tv_subtotal_price1 = findViewById(R.id.tv_subtotal_price1);
        tv_totalprice1 = findViewById(R.id.tv_totalamount);
        tv_totalprice2 = findViewById(R.id.tv_totalprice2);
        tv_totalprice3 = findViewById(R.id.tv_totalprice3);
        tv_invoiceno2 = findViewById(R.id.tv_invoiceno2);
        tv_date1 = findViewById(R.id.tv_date1);
        tv_payment1 = findViewById(R.id.tv_payment1);
        rv_city = findViewById(R.id.id_rv_product);
        rv_city.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        tv_title.setText(getString(R.string.invoice));
        saleId = getIntent().getStringExtra("saleId");

        statusID = getIntent().getStringExtra("status");
        tvLatoutcall.setOnClickListener(this);
        tvbtncancel.setOnClickListener(this);
        tvreorder.setOnClickListener(this);
        Log.e("rahul ::: ", "sdfdsf " + saleId);

        Log.e("rahul ::: ", "statusID " + statusID);
        constants = new Constants(getApplicationContext());



        getInvoiceDetail();

        if(statusID==null){
            tvbtncancel.setVisibility(View.VISIBLE);
            tvreorder.setVisibility(View.VISIBLE);
        }else
        if (statusID.equalsIgnoreCase("1") || statusID.equalsIgnoreCase("2") || statusID.equalsIgnoreCase("4")) {
            tvbtncancel.setVisibility(View.INVISIBLE);
            tvreorder.setVisibility(View.VISIBLE);
        }else if (statusID.equalsIgnoreCase("3") ) {
            tvbtncancel.setVisibility(View.INVISIBLE);
            tvreorder.setVisibility(View.INVISIBLE);
        } else {
            tvbtncancel.setVisibility(View.VISIBLE);
            tvreorder.setVisibility(View.VISIBLE);
        }
    }


    public void getInvoiceDetail() {
        pd.setMessage("Loading");
        pd.show();
        JsonObject jsonObject = new JsonObject();
        try {
            jsonObject.addProperty("sale_id", saleId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e("response ::: ", "response: invoice sale_id:: " + saleId);

        (ApiClient.getClient().getMyInvoice(jsonObject)).enqueue(new Callback<InvoicePojo>() {
            @Override
            public void onResponse(Call<InvoicePojo> call, Response<InvoicePojo> response) {
                pd.dismiss();

                Log.e("response ::: ", "response: invoice:: " + new Gson().toJson(response.body()));

                if (response.isSuccessful()) {
                    try {
                        InvoicePojo invoicePojo = response.body();
                        if (invoicePojo.getStatus()) {
                            invoiceList = invoicePojo.getData();
                            tv_date.setText(invoicePojo.getDate());
                            tv_date1.setText(invoicePojo.getDate());
                            tv_payment.setText(invoicePojo.getPayment_method());
                            tv_payment1.setText(invoicePojo.getPayment_method());
                            tvNotesId.setText(invoicePojo.getDelivery_address());
                            tvDateInvoice.setText("Date : " + invoicePojo.getDate());
                            //  double amount = Double.parseDouble(String.valueOf(invoicePojo.getTotal()));
                            // DecimalFormat formatter = new DecimalFormat("#,####");

//Log.e("rahulraj", "rahulraj" +  invoicePojo.get);



                            tv_shopname.setText(invoicePojo.getStore_name());
                            tv_addressseller.setText(invoicePojo.getSeller_address());
                            ContactNumber = invoicePojo.getSeller_phone();
                            tvContactNo.setText(ContactNumber);
                            tv_totalprice.setText(getString(R.string.rupee) + " " + Constants.getTotalWithSeparater(invoicePojo.getTotal()));
                            tv_totalprice1.setText(getString(R.string.rupee) + " " + Constants.getTotalWithSeparater(invoicePojo.getTotal()));
                            tv_totalprice2.setText(getString(R.string.rupee) + " " + Constants.getTotalWithSeparater(invoicePojo.getTotal()));

                            //   double subTotal = Double.parseDouble(String.valueOf((Integer.parseInt(invoicePojo.getTotal()) + Integer.parseInt(invoicePojo.getDiscount_amount()))));
                            //   DecimalFormat subTotalFormat = new DecimalFormat("#,####");
                            statusID = invoicePojo.getData().get(0).getStatus();
                            tv_subtotal_price.setText(getString(R.string.rupee) + " " + Constants.getTotalWithSeparater(String.valueOf((Integer.parseInt(invoicePojo.getTotal()) + Integer.parseInt(invoicePojo.getDiscount_amount()) - Integer.parseInt(invoicePojo.getDelivery_charge())))));
                            tv_subtotal_price1.setText(getString(R.string.rupee) + " " + Constants.getTotalWithSeparater(String.valueOf((Integer.parseInt(invoicePojo.getTotal()) + Integer.parseInt(invoicePojo.getDiscount_amount()) - Integer.parseInt(invoicePojo.getDelivery_charge())))));
                            // tv_subtotal_price1.setText(getString(R.string.rupee) + " " + (Integer.parseInt(invoicePojo.getTotal()) + Integer.parseInt(invoicePojo.getDiscount_amount())));
//                            holder.tv_product_price.setText(context.getString(R.string.rupee) + " " + Constants.getTotalWithSeparater(""+total)
                            // double total = Double.parseDouble(String.valueOf((invoicePojo.getTotal())));
                            // DecimalFormat totalFormat = new DecimalFormat("#,####");

                            tv_totalprice3.setText(getString(R.string.rupee) + " " + Constants.getTotalWithSeparater(invoicePojo.getTotal()));
                            tv_invoiceno.setText("Invoice No. : " + invoicePojo.getInvoice_number());
                            tv_invoiceno1.setText(invoicePojo.getInvoice_number());
                            tv_invoiceno2.setText(invoicePojo.getInvoice_number());
                            tvVoucherDiscountId.setText(getString(R.string.rupee) + " " + invoicePojo.getDiscount_amount());
                            tvDeliveryChargeId.setText(getString(R.string.rupee) + " " + invoicePojo.getDelivery_charge());
                            InvoiceAdapter searchAdapter = new InvoiceAdapter(getApplicationContext(), invoiceList);
                            rv_city.setAdapter(searchAdapter);

                        } else {
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<InvoicePojo> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(getApplicationContext(), getString(R.string.server_errors), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void downloadInvoice() {
        pd.setMessage("Loading");
        pd.show();
        JsonObject jsonObject = new JsonObject();
        try {
            jsonObject.addProperty("order_id", saleId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.e("rahul", "saleId:::" + saleId);

        (ApiClient.getClient().invoice(jsonObject)).enqueue(new Callback<DownloadInvoicePozo>() {
            @Override
            public void onResponse(Call<DownloadInvoicePozo> call, Response<DownloadInvoicePozo> response) {
                Log.e("response ::: ", "response:dddddd:: " + new Gson().toJson(response.body()));

                pd.dismiss();

                if (response.isSuccessful()) {
                    try {
                        DownloadInvoicePozo downloadInvoicePozo = response.body();
                        if (downloadInvoicePozo.getStatus()) {
                            downloadUrl = downloadInvoicePozo.getData();
                            download(downloadUrl);

                        } else {
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<DownloadInvoicePozo> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(getApplicationContext(), getString(R.string.server_errors), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.id_upload:
                if (CheckNetwork.isNetAvailable(getApplicationContext())) {
                    downloadInvoice();
                } else {
                    Toast.makeText(getApplicationContext(), "Check Network Connection", Toast.LENGTH_LONG).show();
                }

                break;
            case R.id.id_back:
                onBackPressed();
                break;


            case R.id.tvbtncancel:
                log_out();

                break;
            case R.id.tvreorder:
//                log_out();
//                statusID
//                Log.e("rahulaj" , "rahulraj" + invoiceList);
//                Intent intent = new Intent(Invoice_Activity.this, ReOrderConfirmation_Activity.class);


////                intent.putExtra("resdatumfood", invoiceList);
//                startActivity(intent);
//                clearCart();


               Gson gson = new Gson();
                String json = gson.toJson(invoiceList);

                startActivity(new Intent(Invoice_Activity.this, OrderConfirmation_Activity.class)
                        .putExtra("productlist",json)
                                .putExtra("lat",json)
                                .putExtra("long",json)
                        );



                break;


            case R.id.tvLatoutcall:


                if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.CALL_PHONE},
                            REQUEST_CODE_ASK_PERMISSIONS);
                } else {
//                    System.out.println("phone no===" + tv_phoneno.getText().toString());
//                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + tv_phoneno.getText().toString()));
//                    startActivity(intent);
                    String number = ContactNumber;
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + number));
                    startActivity(callIntent);
                }
//                onBackPressed();
                break;

        }
    }

    public void request(View view) {
        ActivityCompat.requestPermissions(Invoice_Activity.this, PERMISSIONS, 112);
    }

    public void view(View view) {
        Log.v(TAG, "view() Method invoked ");

        if (!hasPermissions(Invoice_Activity.this, PERMISSIONS)) {

            Log.v(TAG, "download() Method DON'T HAVE PERMISSIONS ");

            Toast t = Toast.makeText(getApplicationContext(), "You don't have read access !", Toast.LENGTH_LONG);
            t.show();

        } else {
            File d = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);  // -> filename = maven.pdf
            File pdfFile = new File(d, "maven.pdf");

            Log.v(TAG, "view() Method pdfFile " + pdfFile.getAbsolutePath());

            Uri path = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".fileprovider", pdfFile);


            Log.v(TAG, "view() Method path " + path);

            Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
            pdfIntent.setDataAndType(path, "application/pdf");
            pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            pdfIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

            try {
                startActivity(pdfIntent);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(Invoice_Activity.this, "No Application available to view PDF", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void download(String downloadUrl) {
        Log.v(TAG, "download() Method invoked ");
        if (!hasPermissions(Invoice_Activity.this, PERMISSIONS)) {
            Log.v(TAG, "download() Method DON'T HAVE PERMISSIONS ");
            Toast t = Toast.makeText(getApplicationContext(), "You don't have write access !", Toast.LENGTH_LONG);
            t.show();
        } else {
            Log.v(TAG, "download() Method HAVE PERMISSIONS ");
            String fileSuffix = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
            new DownloadFile().execute(downloadUrl, getString(R.string.app_name) + fileSuffix + ".pdf");
        }
        Log.v(TAG, "download() Method completed ");
    }

    @Override
    public void onBackPressed() {
        if (back.equalsIgnoreCase("MyOrder")) {
            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
        } else {
            Intent i = new Intent(Invoice_Activity.this, HomeMainActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Call Permission Granted..Please dial again.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Call permission not granted", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void log_out() {
        final Dialog dialog = new Dialog(Invoice_Activity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_cancelorder);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        ImageView ivlogoutClose = dialog.findViewById(R.id.ivlogoutCloseId);
        AppCompatImageView rllogout = dialog.findViewById(R.id.ivlogoutCloseId);
        CardView btn_logout = dialog.findViewById(R.id.btn_logout);

        ivlogoutClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(Invoice_Activity.this, "Under Development", Toast.LENGTH_SHORT).show();
                Log.e("rahuraj", "rahulraj::: saleId :::" + saleId);
                cancelOrder(saleId);

               /* updateOrderStatus(Userid, "" + saleid, "3");
                lllayout.setVisibility(View.INVISIBLE);
                ivdefaultimage.setVisibility(View.VISIBLE);*/
                dialog.dismiss();
            }
        });

        rllogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.cancel();
            }
        });

        dialog.show();
    }

    public void cancelOrder(String orderId) {
        pd.setMessage("Loading");
        pd.show();
        JsonObject jsonObject = new JsonObject();
        try {
            jsonObject.addProperty("user_id", constants.getUserID());
            jsonObject.addProperty("order_id", orderId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.e("TAG", "my order  request : " + new Gson().toJson(jsonObject));
        (ApiClient.getClient().cancelOrder(jsonObject)).enqueue(new Callback<CancelOrderPozo>() {
            @Override
            public void onResponse(Call<CancelOrderPozo> call, Response<CancelOrderPozo> response) {
                pd.dismiss();
                if (response.isSuccessful()) {
                    try {
                        Log.e("TAG", "my order  responsecancel : " + new Gson().toJson(response.body()));
                        CancelOrderPozo cancelOrderPozo = response.body();
                        if (cancelOrderPozo.getStatus()) {
                            Toast.makeText(getApplicationContext(), cancelOrderPozo.getMessage(), Toast.LENGTH_SHORT).show();
                            if (CheckNetwork.isNetAvailable(getApplicationContext())) {
                                Intent intent = new Intent(Invoice_Activity.this, MyOrder_Activity.class);
                                startActivity(intent);
//                                getMyOrder();

                            } else {
                                Toast.makeText(getApplicationContext(), "Check Network Connection", Toast.LENGTH_LONG).show();
                            }
                        } else {
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<CancelOrderPozo> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(getApplicationContext(), getString(R.string.server_errors), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private class DownloadFile extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... strings) {
            Log.v(TAG, "doInBackground() Method invoked ");
            String fileUrl = strings[0];   // -> http://maven.apache.org/maven-1.x/maven.pdf
            String fileName = strings[1];  // -> maven.pdf
            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            File folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
            File pdfFile = new File(folder, fileName);
            Log.v(TAG, "doInBackground() pdfFile invoked " + pdfFile.getAbsolutePath());
            Log.v(TAG, "doInBackground() pdfFile invoked " + pdfFile.getAbsoluteFile());
            try {
                pdfFile.createNewFile();
                Log.v(TAG, "doInBackground() file created" + pdfFile);

            } catch (IOException e) {
                e.printStackTrace();
                Log.e(TAG, "doInBackground() error" + e.getMessage());
                Log.e(TAG, "doInBackground() error" + e.getStackTrace());


            }
            FileDownloader.downloadFile(fileUrl, pdfFile);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(), "Invoice Downloaded", Toast.LENGTH_LONG).show();
                }
            });
            Log.v(TAG, "doInBackground() file download completed");

            return null;
        }
    }



  /*  private void saveTask(final List<InvoiceInfo> invoiceList, final int position) {
        class SaveTask extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... voids) {
                Task task = new Task();
                task.setProduct_id(productList.get(position).getProductId());
                task.setProduct_name(productList.get(position).getProductName());
                task.setProduct_description(productList.get(position).getProductDescription());
                task.setProduct_image(productList.get(position).getProductImage());
                task.setCategory_id(productList.get(position).getCategoryId());
                task.setProduct_pincode(productList.get(position).getProductPincode());
                task.setIn_stock(productList.get(position).getQuantity());
                task.setPrice(productList.get(position).getDiscountPrice());
                task.setQuantity(productList.get(position).getQty());
                task.setTotal(String.valueOf(Float.parseFloat(productList.get(position).getDiscountPrice())*Float.parseFloat(productList.get(position).getQuantity())));
                task.setFinished(false);
                DatabaseClient.getInstance(getApplicationContext()).getAppDatabase()
                        .taskDao()
                        .insert(task);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

            }
        }
        SaveTask st = new SaveTask();
        st.execute();
    }*/
    private void clearCart() {
        class GetTasks extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... voids) {
                DatabaseClient.getInstance(getApplicationContext())
                        .getAppDatabase()
                        .taskDao()
                        .delete();
                return null;
            }
            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                for (int i = 0; i < invoiceList.size(); i++) {

                    Log.e(TAG, invoiceList.get(i).getQuantity()+"onPostExecute: orderrrrrrrr "+invoiceList.get(i).getQuantity() );
//                    saveTask(invoiceList,i);
                }
                startActivity(new Intent(Invoice_Activity.this, OrderConfirmation_Activity.class));
            }
        }
        GetTasks gt = new GetTasks();
        gt.execute();
    }
}
