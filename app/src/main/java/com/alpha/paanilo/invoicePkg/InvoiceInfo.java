package com.alpha.paanilo.invoicePkg;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class InvoiceInfo {

    @SerializedName("sale_item_id")
    @Expose
    private String saleItemId;
    @SerializedName("sale_id")
    @Expose
    private String saleId;
    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("vendor_id")
    @Expose
    private String vendorId;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("product_description")
    @Expose
    private String productDescription;
    @SerializedName("product_image")
    @Expose
    private String productImage;
    @SerializedName("quantity")
    @Expose
    private String quantity;
    @SerializedName("unit")
    @Expose
    private String unit;
    @SerializedName("unit_value")
    @Expose
    private String unitValue;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("single_product_price")
    @Expose
    private String singleProductPrice;
    @SerializedName("total_product_price")
    @Expose
    private String totalProductPrice;
    @SerializedName("net_weight")
    @Expose
    private String netWeight;
    @SerializedName("rewards")
    @Expose
    private String rewards;
    @SerializedName("discount_price")
    @Expose
    private String discountPrice;
    @SerializedName("in_stock")
    @Expose
    private String inStock;
    @SerializedName("off_percentage")
    @Expose
    private String offPercentage;
    @SerializedName("wishlist_status")
    @Expose
    private String wishlistStatus;
    @SerializedName("count_status")
    @Expose
    private String countStatus;
    @SerializedName("datetime")
    @Expose
    private String datetime;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("order_id")
    @Expose
    private String orderId;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("delivery_time")
    @Expose
    private String deliveryTime;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("note")
    @Expose
    private String note;
    @SerializedName("is_paid")
    @Expose
    private String isPaid;
    @SerializedName("shipping_charge")
    @Expose
    private String shippingCharge;
    @SerializedName("coupon_discount")
    @Expose
    private String couponDiscount;
    @SerializedName("total_amount")
    @Expose
    private String totalAmount;
    @SerializedName("total_rewards")
    @Expose
    private String totalRewards;
    @SerializedName("total_kg")
    @Expose
    private String totalKg;
    @SerializedName("total_items")
    @Expose
    private String totalItems;
    @SerializedName("socity_id")
    @Expose
    private String socityId;
    @SerializedName("delivery_address")
    @Expose
    private String deliveryAddress;
    @SerializedName("delivery_charge")
    @Expose
    private String deliveryCharge;
    @SerializedName("new_store_id")
    @Expose
    private String newStoreId;
    @SerializedName("assign_to")
    @Expose
    private String assignTo;
    @SerializedName("payment_method")
    @Expose
    private String paymentMethod;
    @SerializedName("add_coupon")
    @Expose
    private String addCoupon;
    @SerializedName("First_name")
    @Expose
    private String firstName;
    @SerializedName("Last_name")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("zip_code")
    @Expose
    private String zipCode;
    @SerializedName("center_pickup_date")
    @Expose
    private String centerPickupDate;
    @SerializedName("center_pickup_time")
    @Expose
    private String centerPickupTime;
    @SerializedName("outlets_pickup_location")
    @Expose
    private String outletsPickupLocation;
    @SerializedName("location_id")
    @Expose
    private String locationId;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("product_pincode")
    @Expose
    private String productPincode;
    @SerializedName("availability")
    @Expose
    private String availability;
    @SerializedName("volume")
    @Expose
    private String volume;
    @SerializedName("mrp")
    @Expose
    private String mrp;
    @SerializedName("tax")
    @Expose
    private String tax;
    @SerializedName("increament")
    @Expose
    private String increament;
    @SerializedName("loca_category")
    @Expose
    private String locaCategory;
    @SerializedName("State_code")
    @Expose
    private String stateCode;
    @SerializedName("Location_filter")
    @Expose
    private String locationFilter;
    @SerializedName("thumbnails_image")
    @Expose
    private String thumbnailsImage;
    @SerializedName("farmer_id")
    @Expose
    private String farmerId;
    @SerializedName("weekend_promo")
    @Expose
    private String weekendPromo;



    @SerializedName("images")
    @Expose
    private ArrayList<String> images = null;

    public String getSaleItemId() {
        return saleItemId;
    }

    public void setSaleItemId(String saleItemId) {
        this.saleItemId = saleItemId;
    }

    public String getSaleId() {
        return saleId;
    }

    public void setSaleId(String saleId) {
        this.saleId = saleId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getUnitValue() {
        return unitValue;
    }

    public void setUnitValue(String unitValue) {
        this.unitValue = unitValue;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSingleProductPrice() {
        return singleProductPrice;
    }

    public void setSingleProductPrice(String singleProductPrice) {
        this.singleProductPrice = singleProductPrice;
    }

    public String getTotalProductPrice() {
        return totalProductPrice;
    }

    public void setTotalProductPrice(String totalProductPrice) {
        this.totalProductPrice = totalProductPrice;
    }

    public String getNetWeight() {
        return netWeight;
    }

    public void setNetWeight(String netWeight) {
        this.netWeight = netWeight;
    }

    public String getRewards() {
        return rewards;
    }

    public void setRewards(String rewards) {
        this.rewards = rewards;
    }

    public String getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(String discountPrice) {
        this.discountPrice = discountPrice;
    }

    public String getInStock() {
        return inStock;
    }

    public void setInStock(String inStock) {
        this.inStock = inStock;
    }

    public String getOffPercentage() {
        return offPercentage;
    }

    public void setOffPercentage(String offPercentage) {
        this.offPercentage = offPercentage;
    }

    public String getWishlistStatus() {
        return wishlistStatus;
    }

    public void setWishlistStatus(String wishlistStatus) {
        this.wishlistStatus = wishlistStatus;
    }

    public String getCountStatus() {
        return countStatus;
    }

    public void setCountStatus(String countStatus) {
        this.countStatus = countStatus;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(String deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getIsPaid() {
        return isPaid;
    }

    public void setIsPaid(String isPaid) {
        this.isPaid = isPaid;
    }

    public String getShippingCharge() {
        return shippingCharge;
    }

    public void setShippingCharge(String shippingCharge) {
        this.shippingCharge = shippingCharge;
    }

    public String getCouponDiscount() {
        return couponDiscount;
    }

    public void setCouponDiscount(String couponDiscount) {
        this.couponDiscount = couponDiscount;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getTotalRewards() {
        return totalRewards;
    }

    public void setTotalRewards(String totalRewards) {
        this.totalRewards = totalRewards;
    }

    public String getTotalKg() {
        return totalKg;
    }

    public void setTotalKg(String totalKg) {
        this.totalKg = totalKg;
    }

    public String getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(String totalItems) {
        this.totalItems = totalItems;
    }

    public String getSocityId() {
        return socityId;
    }

    public void setSocityId(String socityId) {
        this.socityId = socityId;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getDeliveryCharge() {
        return deliveryCharge;
    }

    public void setDeliveryCharge(String deliveryCharge) {
        this.deliveryCharge = deliveryCharge;
    }

    public String getNewStoreId() {
        return newStoreId;
    }

    public void setNewStoreId(String newStoreId) {
        this.newStoreId = newStoreId;
    }

    public String getAssignTo() {
        return assignTo;
    }

    public void setAssignTo(String assignTo) {
        this.assignTo = assignTo;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getAddCoupon() {
        return addCoupon;
    }

    public void setAddCoupon(String addCoupon) {
        this.addCoupon = addCoupon;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCenterPickupDate() {
        return centerPickupDate;
    }

    public void setCenterPickupDate(String centerPickupDate) {
        this.centerPickupDate = centerPickupDate;
    }

    public String getCenterPickupTime() {
        return centerPickupTime;
    }

    public void setCenterPickupTime(String centerPickupTime) {
        this.centerPickupTime = centerPickupTime;
    }

    public String getOutletsPickupLocation() {
        return outletsPickupLocation;
    }

    public void setOutletsPickupLocation(String outletsPickupLocation) {
        this.outletsPickupLocation = outletsPickupLocation;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getProductPincode() {
        return productPincode;
    }

    public void setProductPincode(String productPincode) {
        this.productPincode = productPincode;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getIncreament() {
        return increament;
    }

    public void setIncreament(String increament) {
        this.increament = increament;
    }

    public String getLocaCategory() {
        return locaCategory;
    }

    public void setLocaCategory(String locaCategory) {
        this.locaCategory = locaCategory;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getLocationFilter() {
        return locationFilter;
    }

    public void setLocationFilter(String locationFilter) {
        this.locationFilter = locationFilter;
    }

    public String getThumbnailsImage() {
        return thumbnailsImage;
    }

    public void setThumbnailsImage(String thumbnailsImage) {
        this.thumbnailsImage = thumbnailsImage;
    }

    public String getFarmerId() {
        return farmerId;
    }

    public void setFarmerId(String farmerId) {
        this.farmerId = farmerId;
    }

    public String getWeekendPromo() {
        return weekendPromo;
    }

    public void setWeekendPromo(String weekendPromo) {
        this.weekendPromo = weekendPromo;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }

}