package com.alpha.paanilo.invoicePkg;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.alpha.paanilo.R;
import com.alpha.paanilo.utility.Constants;

import java.util.ArrayList;

public class InvoiceAdapter extends RecyclerView.Adapter<InvoiceAdapter.MyViewHolder> {
    public ArrayList<InvoiceInfo> searchList;
    Context context;
    private LayoutInflater mInflater;

    public InvoiceAdapter(Context activity, ArrayList<InvoiceInfo> List) {
        searchList = List;
        context = activity;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        AppCompatTextView tv_productdes, tv_product_qty, tv_product_item, tv_product_subprice, tv_product_price;

        public MyViewHolder(View view) {
            super(view);
            tv_productdes = view.findViewById(R.id.tv_productdes);
            tv_product_qty = view.findViewById(R.id.tv_product_qty);
//            tv_product_item = view.findViewById(R.id.tv_product_item);
            tv_product_subprice = view.findViewById(R.id.tv_product_subprice);
            tv_product_price = view.findViewById(R.id.tv_product_price);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.invoice_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        try {
            final InvoiceInfo detail = searchList.get(position);
//            holder.tv_productdes.setText(detail.getProductName());
            holder.tv_product_qty.setText(detail.getQuantity());
//            holder.tv_product_item.setText(detail.getQuantity() + " " + detail.getUnit());

            //   double amount = Double.parseDouble(String.valueOf(detail.getPrice()));
            //  DecimalFormat formatter = new DecimalFormat("#,####");

            holder.tv_product_subprice.setText(context.getString(R.string.rupee) + " " + Constants.getTotalWithSeparater(detail.getPrice()));
            // holder.tv_product_subprice.setText(context.getString(R.string.rupee) + " " + detail.getPrice());


            //   double amountQty = Double.parseDouble(String.valueOf(Float.parseFloat(detail.getPrice()) * Float.parseFloat(detail.getQuantity())));
            //  DecimalFormat Qtyformatter = new DecimalFormat("#,####");

            // holder.tv_product_price.setText(context.getString(R.string.rupee) + " " + Constants.getTotalWithSeparater(String.valueOf(Double.parseDouble(String.valueOf(Float.parseFloat(detail.getPrice()) * Float.parseFloat(detail.getQuantity()))))));

            int total =Integer.parseInt(detail.getPrice()) * Integer.parseInt(detail.getQuantity());

            holder.tv_product_price.setText(context.getString(R.string.rupee) + " " + Constants.getTotalWithSeparater(""+total));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return searchList.size();
    }
}