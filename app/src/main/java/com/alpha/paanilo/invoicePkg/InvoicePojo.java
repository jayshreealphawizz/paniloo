package com.alpha.paanilo.invoicePkg;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class InvoicePojo {


    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("invoice_number")
    @Expose
    private String invoice_number;

    @SerializedName("payment_method")
    @Expose
    private String payment_method;

    @SerializedName("total")
    @Expose
    private String total;

    @SerializedName("cashback")
    @Expose
    private String cashback;

    @SerializedName("additional")
    @Expose
    private String additional;

    @SerializedName("date")
    @Expose
    private String date;

    @SerializedName("note")
    @Expose
    private String note;

    @SerializedName("discount_amount")
    @Expose
    private String discount_amount;


    @SerializedName("delivery_charge")
    @Expose
    private String delivery_charge;

    @SerializedName("delivery_address")
    @Expose
    private String delivery_address;


    @SerializedName("seller_phone")
    @Expose
    private String seller_phone;

    @SerializedName("store_name")
    @Expose
    private String store_name;


    @SerializedName("seller_address")
    @Expose
    private String seller_address;


    public String getSeller_phone() {
        return seller_phone;
    }

    public void setSeller_phone(String seller_phone) {
        this.seller_phone = seller_phone;
    }

    public String getStore_name() {
        return store_name;
    }

    public void setStore_name(String store_name) {
        this.store_name = store_name;
    }

    public String getSeller_address() {
        return seller_address;
    }

    public void setSeller_address(String seller_address) {
        this.seller_address = seller_address;
    }

    @SerializedName("data")
    @Expose
    private ArrayList<InvoiceInfo> data = null;

    public String getDelivery_address() {
        return delivery_address;
    }

    public void setDelivery_address(String delivery_address) {
        this.delivery_address = delivery_address;
    }

    public String getDiscount_amount() {
        return discount_amount;
    }

    public void setDiscount_amount(String discount_amount) {
        this.discount_amount = discount_amount;
    }

    public String getDelivery_charge() {
        return delivery_charge;
    }

    public void setDelivery_charge(String delivery_charge) {
        this.delivery_charge = delivery_charge;
    }


    public Boolean getStatus() {
        return status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<InvoiceInfo> getData() {
        return data;
    }

    public void setData(ArrayList<InvoiceInfo> data) {
        this.data = data;
    }


    public String getInvoice_number() {
        return invoice_number;
    }

    public void setInvoice_number(String invoice_number) {
        this.invoice_number = invoice_number;
    }

    public String getPayment_method() {
        return payment_method;
    }

    public void setPayment_method(String payment_method) {
        this.payment_method = payment_method;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getCashback() {
        return cashback;
    }

    public void setCashback(String cashback) {
        this.cashback = cashback;
    }

    public String getAdditional() {
        return additional;
    }

    public void setAdditional(String additional) {
        this.additional = additional;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }


}
