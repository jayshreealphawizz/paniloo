package com.alpha.paanilo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.alpha.paanilo.cart_pkg.OrderConfirmation_Activity;
import com.alpha.paanilo.product_detail_pkg.Product_details_Activity;
import com.alpha.paanilo.product_pkg.WalletAdapter;
import com.alpha.paanilo.product_pkg.WalletGetDatum;
import com.alpha.paanilo.product_pkg.WalletInfo;
import com.alpha.paanilo.product_pkg.WalletPozo;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.utility.CheckNetwork;
import com.alpha.paanilo.utility.Constants;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.FacebookSdk.getApplicationContext;

public class PaymentMethodActivity extends AppCompatActivity implements PaymentResultListener, View.OnClickListener {
    View toolbar;
    AppCompatTextView tv_tittle;
    AppCompatImageView id_notification, id_back;
    List<WalletGetDatum> arraylist = new ArrayList<>();
    WalletAdapter adapter;
    RecyclerView rvWallet;
    EditText tvCoupanAMount;
    CardView btn_addmoney;
    TextView tvTotalAmount;
    String totalamount;
    String goToScreen;
    private ProgressDialog pd;
    private Constants constants;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_method);
        init();
    }

    @SuppressLint("SetTextI18n")
    private void init() {
        toolbar = findViewById(R.id.id_toolbar);
        tv_tittle = toolbar.findViewById(R.id.tv_tittle);
        id_notification = toolbar.findViewById(R.id.id_notification);
        id_back = toolbar.findViewById(R.id.id_back);
        pd = new ProgressDialog(PaymentMethodActivity.this, R.style.AppCompatAlertDialogStyle);
        rvWallet = findViewById(R.id.rvWallet);
        btn_addmoney = findViewById(R.id.btn_addmoney);
        tvCoupanAMount = findViewById(R.id.tvCoupanAMount);
        constants = new Constants(getApplicationContext());

        tvTotalAmount = findViewById(R.id.tvTotalAmount);
        //  tvTotalAmountrewart = findViewById(R.id.tvTotalAmountrewart);
//        id_back.setVisibility(View.INVISIBLE);
        id_notification.setVisibility(View.INVISIBLE);
        btn_addmoney.setOnClickListener(this);
//        id_notification.setVisibility(View.GONE);
        tv_tittle.setText("Wallet");

//        goToScreen = getIntent().getStringExtra("entry");

        id_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), HomeMainActivity.class);
                startActivity(intent);
            }
        });
//        Collections.reverse(arraylist);
//        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false);
//        rvWallet.setLayoutManager(layoutManager);


        Collections.reverse(arraylist);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        rvWallet.setHasFixedSize(true);
        rvWallet.setLayoutManager(layoutManager);

        //        adapter = new WalletAdapter(getActivity(), arraylist);
//        rvWallet.setAdapter(adapter);
//        getdata();

        getMyWalletList();

    }

//    public void getdata() {
//        WalletModel model = new WalletModel("E-Wallet Top Up", "12 July 2020");
//        arraylist.add(model);
//
//        model = new WalletModel("E-Wallet Top Up", "12 July 2020");
//        arraylist.add(model);
//
//        model = new WalletModel("E-Wallet Top Up", "12 July 2020");
//        arraylist.add(model);
//
//        model = new WalletModel("E-Wallet Top Up", "12 July 2020");
//        arraylist.add(model);
//
//
//        adapter.notifyDataSetChanged();
//    }

    public void startRazorPayPayment(String amount) {
        final Checkout co = new Checkout();
        co.setKeyID("rzp_test_QrkS2B01UMFJ95");//pannli test key
//        co.setKeyID("rzp_test_GC7PRrOSR8GIQf");//oro test key

        co.setImage(R.drawable.launcher);
        final PaymentMethodActivity activity = this;

//        context = getApplicationContext() ;
//        final ProductFragment context = this;
        try {
            System.out.println(constants.getEmail() + "------add wallet amount------" + amount + "===name =====" + constants.getUsername());

            JSONObject options = new JSONObject();
            options.put("name", constants.getUsername());
            options.put("description", "");
            //You can omit the image option to fetch the image from dashboard
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png");
            options.put("currency", "INR");
            options.put("amount", Float.parseFloat(amount) * 100);

            JSONObject preFill = new JSONObject();
            preFill.put("email", constants.getEmail());
            preFill.put("contact", "91" + "");
            options.put("prefill", preFill);


            co.open(activity, options);
        } catch (Exception e) {
            Log.e("rahulraj ", "walletpage222");
            Toast.makeText(getApplicationContext(), "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }
    }


    @Override
    public void onPaymentSuccess(String razorpayPaymentID) {
        try {
            Log.e("rahulraj ", "walletpage");
            addAmount_Wallet();

            Toast.makeText(getApplicationContext(), "Payment Successful: ", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
        }
    }

    @Override
    public void onPaymentError(int code, String response) {
        try {
            Toast.makeText(getApplicationContext(), "Payment failed ", Toast.LENGTH_SHORT).show();
//            finish();
        } catch (Exception e) {
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_addmoney:
//                Toast.makeText(getContext(), "Add Money lick" , Toast.LENGTH_SHORT).show();
                if (CheckNetwork.isNetAvailable(getApplicationContext())) {
                    if (tvCoupanAMount.getText().toString().isEmpty()) {
                        Toast.makeText(getApplicationContext(), "Enter Amount", Toast.LENGTH_SHORT).show();
                    } else {
//                        addAmount_Wallet();
                        startRazorPayPayment(tvCoupanAMount.getText().toString());
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Check Network Connection", Toast.LENGTH_LONG).show();
                }

                break;
        }
    }

    private void addAmount_Wallet() {
//        Toast.makeText(this, "rrrrrrrr", Toast.LENGTH_SHORT).show();
        pd.setMessage("Loading");
        pd.show();
        JsonObject jsonObject = new JsonObject();
        try {
            jsonObject.addProperty("user_id", constants.getUserID());
            jsonObject.addProperty("amount", tvCoupanAMount.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        (ApiClient.getClient().addWalletAmount(jsonObject)).enqueue(new Callback<WalletInfo>() {
            @Override
            public void onResponse(Call<WalletInfo> call, Response<WalletInfo> response) {
                pd.dismiss();
                if (response.isSuccessful()) {
                    try {
                        pd.dismiss();
                        WalletInfo walletInfo = response.body();
                        Log.e("TAG", "add wallet response : " + new Gson().toJson(response.body()));
                        if (walletInfo.getStatus() == 1) {
//                            Toast.makeText(getContext(), "Amount added" + walletInfo.getData().getWallet(), Toast.LENGTH_SHORT).show();
//                            totalamount = ""+walletInfo.getData().getWallet();
//                            tvTotalAmount.setText("Rs." + walletInfo.getData().getWallet());
                            if (walletInfo.getData().equals("")) {
                                tvTotalAmount.setText("Rs." + 00);
                            } else {
                                tvTotalAmount.setText("Rs." + walletInfo.getData().getWallet());
                            }
                            //     finish();

                        } else {
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        pd.dismiss();
                    }

                    getMyWalletList();
                    tvCoupanAMount.setText("");

                }
            }

            @Override
            public void onFailure(Call<WalletInfo> call, Throwable t) {
                pd.dismiss();
            }
        });
    }


    public void getMyWalletList() {
//        pd.setMessage("Loading");
//        pd.show();
        JsonObject jsonObject = new JsonObject();
        try {
            jsonObject.addProperty("user_id", constants.getUserID());
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e("TAG", constants.getUserID() + "wallet list  request : " + new Gson().toJson(jsonObject));
        (ApiClient.getClient().getwallethistory(jsonObject)).enqueue(new Callback<WalletPozo>() {
            @Override
            public void onResponse(Call<WalletPozo> call, Response<WalletPozo> response) {
                pd.dismiss();
                Log.e("TAG", "wallet list response : " + new Gson().toJson(response.body()));
                if (response.isSuccessful()) {
                    try {
                        Log.e("TAG", "wallet list response : " + new Gson().toJson(response.body()));
                        WalletPozo walletPozo = response.body();

                        if (walletPozo.getWalletBalance().equals("")) {
                            tvTotalAmount.setText("Rs." + 00);
                        } else {
                            tvTotalAmount.setText("Rs." + walletPozo.getWalletBalance());
                        }
                        if (walletPozo.getStatus() == 1) {
//                                    totalAmount = orderHistoryPozo.getWallet();
//                                    historyList = orderHistoryPozo.getData();
//                                    Collections.reverse(historyList);
                            arraylist = walletPozo.getData();
                            adapter = new WalletAdapter(getApplicationContext(), arraylist);
                            rvWallet.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
//                                    tv_nodata.setVisibility(View.GONE);
//                                    constants.setWallet(totalAmount);

                        } else {
//                                    tv_nodata.setVisibility(View.VISIBLE);
//                                    tv_nodata.setText("Data not found");
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<WalletPozo> call, Throwable t) {
                pd.dismiss();
//                tv_nodata.setVisibility(View.VISIBLE);
                Toast.makeText(getApplicationContext(), getString(R.string.server_errors), Toast.LENGTH_SHORT).show();
            }
        });
    }


//    @Override
//    public void onBackPressed() {
//        if (goToScreen == null) {
//            startActivity(new Intent(PaymentMethodActivity.this, HomeMainActivity.class));
//            finish();
//        } else if (goToScreen.equalsIgnoreCase("detail")) {
////            startActivity(new Intent(getApplicationContext(), Product_details_Activity.class));
////            Intent intent = new Intent(PaymentMethodActivity.this, Product_details_Activity.class);
////            intent.putExtra("userid", goToScreenId);
////            startActivity(intent);
//
////            Log.e("rahul ", "rahul111 " + goToScreenId);
////                onBackPressed();
//            finish();
////            finish();
//        } else {
//            startActivity(new Intent(getApplicationContext(), HomeMainActivity.class));
//            finish();
//        }
//    }


      /*public void getSubCategoryChild(final SubCatDatum subCatDatum, String catID) {
        System.out.println("sub cat id ===="+catID);
        pd.setMessage("Loading");
        pd.show();
        (ApiClient.getClient().getAllSubChildCategory(catID)).enqueue(new Callback<SubCategoryChildPozo>() {
            @Override
            public void onResponse(Call<SubCategoryChildPozo> call, Response<SubCategoryChildPozo> response) {
                pd.dismiss();
                if (response.isSuccessful()) {
                    SubCategoryChildPozo subCategoryChildPozo = response.body();
                    Log.e("TAG", "all sub category child : " + new Gson().toJson(response.body()));
                    if (response.isSuccessful()) {
                        subcategorydatumList_child = subCategoryChildPozo.getData();
                        stringParentCategorySectionHashMap.put(subCatDatum, subcategorydatumList_child);
                        sectionedExpandableLayoutHelper.addParentCategory(stringParentCategorySectionHashMap);
                    }
                    sectionedExpandableLayoutHelper.notifyDataSetChanged();
                }
                else {
                    if (response.code() == 400) {
                        if (!false) {
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(response.errorBody().string());
//                                pbLoginId.setVisibility(View.GONE);
//                                tvProduct.setVisibility(View.VISIBLE);
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<SubCategoryChildPozo> call, Throwable t) {
                pd.dismiss();
                System.out.println("cslllll"+t.getMessage());
                Toast.makeText(getApplicationContext(), getString(R.string.server_errors), Toast.LENGTH_SHORT).show();
            }
        });
    }*/


}