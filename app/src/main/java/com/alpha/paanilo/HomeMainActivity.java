package com.alpha.paanilo;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.alpha.paanilo.MPremission.PermissionUtils;
import com.alpha.paanilo.Roomdatabase.DatabaseClient;
import com.alpha.paanilo.account_pkg.AccountFragment;
import com.alpha.paanilo.cart_pkg.OrderConfirmation_Activity;
import com.alpha.paanilo.home_pkg.HomeFragment;

import com.alpha.paanilo.utility.AddRefreshModel;
import com.alpha.paanilo.utility.Constants;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class HomeMainActivity extends AppCompatActivity implements View.OnClickListener,
        AddRefreshModel.OnCustomCartListener {

    final static int REQUEST_LOCATION = 199;
    private static final int REQUEST_CODE = 200;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 5445;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    public ArrayList<String> alForPermission;
    public boolean isFirst = true;
    LinearLayout ll_home, ll_product, ll_cart, ll_acc, id_notification;
    ImageView iv_home, iv_wallet, iv_cart, iv_acc;
    TextView tv_home, iv_counter, tv_product, tv_cart, tv_acc, iv_tittle;
    FrameLayout framcontainer;
    View toolbar;
    AppCompatTextView tv_location;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    PendingResult<LocationSettingsResult> result;
    PermissionUtils permissionUtils;
    Constants constants;
    LocationManager locationManager;
    private ProgressBar pbHome;
    private Location currentLocation;
    //    =========================
    private final LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            super.onLocationResult(locationResult);
            if (locationResult.getLastLocation() == null)
                return;
            currentLocation = locationResult.getLastLocation();

            showMarker(currentLocation);
            System.out.println("current location --------" + currentLocation);
        }
    };
    private ProgressDialog pd;
    private FusedLocationProviderClient fusedLocationProviderClient;

    //================================
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setContentView(R.layout.activity_main);
        AddRefreshModel.getInstance().setListener(this);


        framcontainer = findViewById(R.id.container);
        ll_home = findViewById(R.id.ll_home);
        ll_product = findViewById(R.id.ll_product);
        ll_cart = findViewById(R.id.ll_cart);
        ll_acc = findViewById(R.id.ll_acc);

        pbHome = findViewById(R.id.pbHomeId);
        iv_home = findViewById(R.id.iv_home);
        iv_wallet = findViewById(R.id.iv_wallet);
        iv_cart = findViewById(R.id.iv_cart);
        iv_acc = findViewById(R.id.iv_acc);

        tv_home = findViewById(R.id.tv_home);
        tv_product = findViewById(R.id.tv_product);
        tv_cart = findViewById(R.id.tv_cart);
        tv_acc = findViewById(R.id.tv_acc);

        pd = new ProgressDialog(getApplicationContext(), R.style.AppCompatAlertDialogStyle);

        // id_notification.setOnClickListener(this);
        ll_home.setOnClickListener(this);
        ll_product.setOnClickListener(this);
        ll_cart.setOnClickListener(this);
        ll_acc.setOnClickListener(this);


        // tv_location.setText(constants.getCity());

        constants = new Constants(getApplicationContext());

        constants.setHomeVendrorID("");

//====================================
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

        } else {
            getLocationPermission();
        }
//        clearCart();
    }

    //        =============================

//    dfdf


    @Override
    protected void onStop() {
        super.onStop();
        if (fusedLocationProviderClient != null)
            fusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (isGooglePlayServicesAvailable()) {
            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
            startCurrentLocationUpdates();
        }
    }

    private void getLocationPermission() {
        if (ContextCompat.checkSelfPermission(HomeMainActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
        } else {
            ActivityCompat.requestPermissions(HomeMainActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        fusedLocationProviderClient = null;
    }

    private void startCurrentLocationUpdates() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(3000);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(HomeMainActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
                return;
            }
        }
        fusedLocationProviderClient.requestLocationUpdates(locationRequest, mLocationCallback, Looper.myLooper());
    }

    private boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int status = googleApiAvailability.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status)
            return true;
        else {
            if (googleApiAvailability.isUserResolvableError(status))
                Toast.makeText(this, "Please Install google play services to use this application", Toast.LENGTH_LONG).show();
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION) {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED)
                Toast.makeText(this, "Permission denied by uses", Toast.LENGTH_SHORT).show();
            else if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                startCurrentLocationUpdates();
        }
    }


    private void showMarker(@NonNull Location currentLocation) {
        LatLng latLng = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
        Log.d("latitude", String.valueOf(currentLocation.getLatitude()));
        Log.d("latitude", String.valueOf(currentLocation.getLongitude()));

        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName();
            String sublocality = addresses.get(0).getSubLocality();

            System.out.println(sublocality + city + state + country + "========order detail====address=====" + address);


            System.out.println("iiiiii" + isFirst);
            if (isFirst) {
                constants.setCurrentLocation(address);
                constants.setCity(city);
                constants.setLatitude(String.valueOf(addresses.get(0).getLatitude()));
                constants.setLongitude(String.valueOf(addresses.get(0).getLongitude()));

                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, new HomeFragment())
                        .commit();
                isFirst = false;
            } else {

            }


        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    //       ================================

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_home:
                iv_home.setImageResource(R.drawable.ic_home);
                tv_home.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.textcolorgreen));
                iv_wallet.setImageResource(R.drawable.ic_wallet);
                tv_product.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorGray));
                iv_cart.setImageResource(R.drawable.ic_cart_gray);
                tv_cart.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorGray));
                iv_acc.setImageResource(R.drawable.ic_user_gray);
                tv_acc.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorGray));
                pbHome.setVisibility(View.GONE);
                constants.setHomeVendrorID("");
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, new HomeFragment())
                        .commit();
                break;

            case R.id.ll_cart:
//                clearCart();
                iv_cart.setImageResource(R.drawable.ic_cart);
                tv_cart.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.textcolorgreen));

                iv_home.setImageResource(R.drawable.ic_home_gray);
                tv_home.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorGray));

                iv_wallet.setImageResource(R.drawable.ic_wallet);
                tv_product.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorGray));

                iv_acc.setImageResource(R.drawable.ic_user_gray);
                tv_acc.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorGray));
                Intent intent = new Intent(HomeMainActivity.this, OrderConfirmation_Activity.class);
                intent.putExtra("entry", "home");
                startActivity(intent);
                finish();

                break;


            case R.id.ll_product:
                iv_wallet.setImageResource(R.drawable.ic_walletfill);
                tv_product.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.textcolorgreen));

                iv_home.setImageResource(R.drawable.ic_home_gray);
                tv_home.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorGray));

                iv_cart.setImageResource(R.drawable.ic_cart_gray);
                tv_cart.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorGray));

                iv_acc.setImageResource(R.drawable.ic_user_gray);
                tv_acc.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorGray));

//                System.out.println("con========="+constants.getHomeVendrorID());
//                if(constants.getHomeVendrorID().equals("") || constants.getHomeVendrorID().isEmpty()){
//                    Toast.makeText(this, "Please select stall first", Toast.LENGTH_SHORT).show();
//                 }
//                else{

//                getSupportFragmentManager().beginTransaction()
//                        .replace(R.id.container, new ProductFragment())
//                        .commit();
                Intent intentnew = new Intent(this, PaymentMethodActivity.class);
//                intentnew.putExtra("entry", "home");
                startActivity(intentnew);
                finish();
//                }

                break;


            case R.id.ll_acc:
                iv_acc.setImageResource(R.drawable.ic_user);
                tv_acc.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.textcolorgreen));

                iv_home.setImageResource(R.drawable.ic_home_gray);
                tv_home.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorGray));

                iv_cart.setImageResource(R.drawable.ic_cart_gray);
                tv_cart.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorGray));

                iv_wallet.setImageResource(R.drawable.ic_wallet);
                tv_product.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorGray));
                // toolbar.setVisibility(View.GONE);
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, new AccountFragment())
                        .commit();
                break;

        }
    }

//    ====================================

//    ===============================================

    @Override
    public void changeCartState() {
        iv_wallet.setImageResource(R.drawable.ic_product);
        tv_product.setTextColor(getResources().getColor(R.color.colorAccent));

        iv_home.setImageResource(R.drawable.ic_home_gray);
        tv_home.setTextColor(getResources().getColor(R.color.colorGray));

        iv_cart.setImageResource(R.drawable.ic_cart_gray);
        tv_cart.setTextColor(getResources().getColor(R.color.colorGray));

        iv_acc.setImageResource(R.drawable.ic_user_gray);
        tv_acc.setTextColor(getResources().getColor(R.color.colorGray));

//        /// toolbar.setVisibility(View.VISIBLE);
//        getSupportFragmentManager().beginTransaction()
//                .replace(R.id.container, new ProductFragment())
//                .commit();

        Intent intent = new Intent(this, PaymentMethodActivity.class);
        startActivity(intent);

    }
    private void clearCart() {
        class GetTasks extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... voids) {
                DatabaseClient.getInstance(getApplicationContext())
                        .getAppDatabase()
                        .taskDao()
                        .delete();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

            }
        }
        GetTasks gt = new GetTasks();
        gt.execute();
    }

}
