package com.alpha.paanilo.account_pkg;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.alpha.paanilo.BuildConfig;
import com.alpha.paanilo.Help_Activity;
import com.alpha.paanilo.MyOrder_Activity;
import com.alpha.paanilo.Privacypolicy_Activity;
import com.alpha.paanilo.R;
import com.alpha.paanilo.Term_condition_Activity;
import com.alpha.paanilo.authenticationModule.Login_Activity;
import com.alpha.paanilo.authenticationModule.Loginoption;
import com.alpha.paanilo.cart_pkg.RewartActivity;
import com.alpha.paanilo.editprofile_pkg.EditProfile_Activity;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.utility.AppSession;
import com.alpha.paanilo.utility.Constants;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import de.hdodenhof.circleimageview.CircleImageView;


public class AccountFragment extends Fragment implements View.OnClickListener {
    View v;
    LinearLayout ll_myorder, ll_invite_friend, ll_help, ll_privacypolicy, ll_termcondition, ll_rewart;
    AppCompatButton btn_logout;
    AppCompatImageView iv_editprofile;
    Constants constants;
    AppCompatTextView tv_username, tv_useremail;
    CircleImageView ivuserProfile;
    String userImage;
    GoogleSignInClient mGoogleSignInClient;
    CardView cardedit;

    /* @Override
     public void setUserVisibleHint(boolean isVisibleToUser) {
         super.setUserVisibleHint(isVisibleToUser);
         System.out.println("call==============");
         getFragmentManager().beginTransaction().detach(this).attach(this).commit();
     }
 */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.account_fragment, container, false);

        tv_username = v.findViewById(R.id.tv_username);
        tv_useremail = v.findViewById(R.id.tv_useremail);
        ivuserProfile = v.findViewById(R.id.ivuserProfile);
//        ll_rewart = v.findViewById(R.id.ll_rewart);
        ll_myorder = v.findViewById(R.id.ll_myorder);
        ll_invite_friend = v.findViewById(R.id.ll_invite_friend);
        ll_help = v.findViewById(R.id.ll_help);
        ll_privacypolicy = v.findViewById(R.id.ll_privacypolicy);
        ll_termcondition = v.findViewById(R.id.ll_termcondition);
        btn_logout = v.findViewById(R.id.btn_logout);
        iv_editprofile = v.findViewById(R.id.iv_editprofile);
        cardedit = v.findViewById(R.id.cardedit);
        ll_myorder.setOnClickListener(this);
        ll_invite_friend.setOnClickListener(this);
        ll_privacypolicy.setOnClickListener(this);
        ll_help.setOnClickListener(this);
        ll_termcondition.setOnClickListener(this);
        btn_logout.setOnClickListener(this);
        iv_editprofile.setOnClickListener(this);
//        ll_rewart.setOnClickListener(this);
        constants = new Constants(getContext());

        System.out.println(constants.getEmail() + "===user name 123===" + constants.getUsername());

        tv_useremail.setText(constants.getEmail());

        userImage = constants.getImage();

        Log.e("rahul::Bana: ", "rahul:nn:: " + userImage);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(getContext(), gso);
        cardedit.setOnClickListener(this);
//        if (userImage.equals("0")) {
//
//        } else if (TextUtils.isEmpty(userImage)) {
//
//        } else {
//
//            Glide.with(getContext())
//                    .load(ApiClient.USER_PROFILE_URL + userImage)
//                    .placeholder(R.drawable.ic_account_user)
//                    .into(ivuserProfile);
//        }

        if (userImage.equals("0")) {

        } else if (TextUtils.isEmpty(userImage)) {

        } else {

            Glide.with(getContext())
                    .load(ApiClient.USER_PROFILE_URL + userImage)
                    .placeholder(R.drawable.ic_account_user)
                    .into(ivuserProfile);

            Log.e("rahul::Banfdfdsfsdfa: ", "rahul:dfsdf:: " + ApiClient.USER_PROFILE_URL + userImage);

        }


        if (constants.getUsername().equals("0")) {

        } else {
            tv_username.setText(constants.getUsername());
        }


        if (constants.getAddress().equals("0")) {

        } else {
            Log.e("CIty ", " City " + constants.getAddress());
//            tv_username.setText(constants.getUsername());
        }
        if (constants.getdob().equals("0")) {

        } else {
            Log.e("dob ", " dob " + constants.getdob());
//            tv_username.setText(constants.getUsername());
        }

        if (constants.getMobile().equals("0")) {

        } else {
            Log.e("Mobile ", " Mobile " + constants.getMobile());
//            tv_username.setText(constants.getUsername());
        }

        return v;
    }


    public void log_out() {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_log_out);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        ImageView ivlogoutClose = dialog.findViewById(R.id.ivlogoutCloseId);
        AppCompatImageView rllogout = dialog.findViewById(R.id.ivlogoutCloseId);
        CardView btn_logout = dialog.findViewById(R.id.btn_logout);

        ivlogoutClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String loginstatus = constants.getIsLoginStatus("");

                Log.e("login status ", "status::::  " + loginstatus);

                if (loginstatus != null) {
                    if (loginstatus.equals("1")) {
                        mGoogleSignInClient.signOut().addOnCompleteListener(
                                new OnCompleteListener<Void>() {  //signout Google
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        FirebaseAuth.getInstance().signOut(); //signout firebase
                                        Log.e("loginstatus ", "gmail:::: ");
                                        AppSession.clearAllSharedPreferences(getActivity());
                                        dialog.dismiss();
                                        constants.setIsLogin("no");
                                        constants.setUsername("");
                                        constants.setEmail("");
                                        constants.setLoginAs("");
                                        constants.setUserID("");
                                        constants.setVendorClick("");
                                        constants.setIsLoginStatus("");
                                        constants.clearAllSharedPreferences(getContext());
                                        Intent intent = new Intent(getContext(), Loginoption.class);
                                        startActivity(intent);

                                        getActivity().finishAffinity();
                                    }
                                });

                    } else if (loginstatus.equals("2")) {

                        Log.e("fff facebook::: ", "fffffffff ::" + loginstatus);

                        if (AccessToken.getCurrentAccessToken() == null) {
                            new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest
                                    .Callback() {
                                @Override
                                public void onCompleted(GraphResponse graphResponse) {

                                    LoginManager.getInstance().logOut();
                                    dialog.dismiss();
                                    Log.e("fff faceboo Logout::: ", "fffffffff ::");
                                    AccessToken.setCurrentAccessToken(null);
                                    AppSession.clearAllSharedPreferences(getActivity());
                                    constants.setIsLogin("no");
                                    constants.setUsername("");
                                    constants.setEmail("");
                                    constants.setLoginAs("");
                                    constants.setUserID("");
                                    constants.setVendorClick("");
                                    constants.setIsLoginStatus("0");
//                                constants.clearAllSharedPreferences(getActivity());
//                                Intent intent = new Intent(getActivity(), Loginoption.class);
//                                startActivity(intent);

                                    Intent logoutint = new Intent(getActivity(), Loginoption.class);
                                    logoutint.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(logoutint);



                                    getActivity().finishAffinity();

                                }
                            }).executeAsync();
                            return; // already logged out
                        }


//                        ProfileActivity.super.onBackPressed();
                    } else if (loginstatus.equals("0")) {
                        dialog.dismiss();
//                        AppSession.clearAllSharedPreferences(getContext());
                        constants.setIsLogin("no");
                        constants.setUsername("");
                        constants.setEmail("");
                        constants.setLoginAs("");
                        constants.setUserID("");
                        constants.setVendorClick("");
                        constants.setIsLoginStatus("");
                        constants.clearAllSharedPreferences(getContext());
                        Intent intent = new Intent(getContext(), Loginoption.class);
                        startActivity(intent);

                        getActivity().finishAffinity();
                    }
                } else {
                    dialog.dismiss();
//try {
//    AppSession.clearAllSharedPreferences(getContext());
                    constants.setIsLogin("no");
                    constants.setUsername("");
                    constants.setEmail("");
                    constants.setLoginAs("");
                    constants.setUserID("");
                    constants.setVendorClick("");
                    constants.setIsLoginStatus("");
                    constants.clearAllSharedPreferences(getContext());
                    Intent intent = new Intent(getContext(), Loginoption.class);
                    startActivity(intent);

                    getActivity().finishAffinity();
//}catch (NullPointerException e){
//
//}


                }


            }
        });

        rllogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        dialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_myorder:
                startActivity(new Intent(getContext(), MyOrder_Activity.class));
                break;

            case R.id.ll_invite_friend:
                try {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
                    String shareMessage = "\nLet me recommend you this application\n\n";
                    shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID + "\n\n";
                    shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                    startActivity(Intent.createChooser(shareIntent, "choose one"));
                } catch (Exception e) {
                }
                break;

            case R.id.ll_help:
                startActivity(new Intent(getContext(), Help_Activity.class));
                break;
            case R.id.ll_privacypolicy:
                startActivity(new Intent(getContext(), Privacypolicy_Activity.class));
                break;

            case R.id.ll_termcondition:
                startActivity(new Intent(getContext(), Term_condition_Activity.class));
                break;

            case R.id.cardedit:
                startActivity(new Intent(getContext(), EditProfile_Activity.class));
                break;

            case R.id.btn_logout:
                log_out();
                break;

//            case R.id.ll_rewart:
//                startActivity(new Intent(getContext(), RewartActivity.class));
//                break;


        }
    }
}

