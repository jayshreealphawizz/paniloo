package com.alpha.paanilo.nearby_stall_pkg;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.alpha.paanilo.R;
import com.alpha.paanilo.model.AllStallPkg.All_Stall_Info;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.utility.Constants;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

public class All_nearby_stall_Adapter extends RecyclerView.Adapter<All_nearby_stall_Adapter.MyViewHolder> implements Filterable {
    public ArrayList<All_Stall_Info> searchList;
    public ArrayList<All_Stall_Info> searchList1;
    Context context;
    private LayoutInflater mInflater;
    private Constants constants;
    private StallOnClickListener stallOnClickListener;

    public All_nearby_stall_Adapter(Context activity) {
        context = activity;
        constants = new Constants(activity);

    }

    public All_nearby_stall_Adapter(Context activity,StallOnClickListener stall) {
        context = activity;
        constants = new Constants(activity);
        this.stallOnClickListener = stall;
    }

    public void addStallList(ArrayList<All_Stall_Info> searchList) {
        this.searchList = searchList;
        this.searchList1 = searchList;
        notifyDataSetChanged();
    }

    public interface StallOnClickListener {
        void onClick(View view, int position, All_Stall_Info stall_info);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    searchList = searchList1;
                } else {
                    ArrayList<All_Stall_Info> filteredList = new ArrayList<>();
                    for (All_Stall_Info row : searchList1) {
                        if (row.getUserFullname().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }
                    searchList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = searchList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                searchList = (ArrayList<All_Stall_Info>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tv_stall_name, tv_stall_des,tv_stall_distance;
        AppCompatImageView id_stallpic;
        LinearLayout ll_wholebox;

        public MyViewHolder(View view) {
            super(view);
            tv_stall_name = view.findViewById(R.id.tv_stall_name);
            tv_stall_des = view.findViewById(R.id.tv_stall_des);
            id_stallpic = view.findViewById(R.id.id_stallpic);
            tv_stall_distance = view.findViewById(R.id.tv_stall_distance);
            ll_wholebox = view.findViewById(R.id.ll_wholebox);

            ll_wholebox.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {
            stallOnClickListener.onClick(v, getAdapterPosition(), searchList.get(getAdapterPosition()));
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_fragment_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        final All_Stall_Info detail = searchList.get(position);

        holder.tv_stall_name.setText(detail.getUserFullname());
        holder.tv_stall_des.setText(detail.getCurrentLocation());

        String valuuu = String.format("%.2f", Double.valueOf(detail.getDistance()));
        System.out.println("=========="+valuuu);
        holder.tv_stall_distance.setText(valuuu+" Km");

        System.out.println("current imaege =====" + detail.getUserImage());

        if (detail.getUserImage().equals("")) {
        } else {
            Glide.with(context)
                    .load(ApiClient.VENDOR_PROFILE_URL + detail.getUserImage())
                    .placeholder(R.drawable.placeholder)
                    .into(holder.id_stallpic);
        }

        constants.setHomeVendrorID("");


    }
    @Override
    public int getItemCount() {
        return searchList == null ? 0 : searchList.size();
    }
}