package com.alpha.paanilo.nearby_stall_pkg;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.alpha.paanilo.R;
import com.alpha.paanilo.Roomdatabase.DatabaseClient;
import com.alpha.paanilo.Roomdatabase.ProductDataTask;
import com.alpha.paanilo.allproduct_pkg.All_Product_Activity;
import com.alpha.paanilo.model.AllStallPkg.All_StallDataPojo;
import com.alpha.paanilo.model.AllStallPkg.All_Stall_Info;
import com.alpha.paanilo.model.categoryPkg.Datum;
import com.alpha.paanilo.product_pkg.Explor_Cat_Adapter;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.utility.AppSession;
import com.alpha.paanilo.utility.CheckNetwork;
import com.alpha.paanilo.utility.Constants;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class All_NearBy_StallActivity extends AppCompatActivity implements View.OnClickListener, All_nearby_stall_Adapter.StallOnClickListener {

    private AppCompatTextView tvNotFound;
    AppCompatImageView iv_back;
    RecyclerView rv_city;
    View id_toolbar;
    AppCompatTextView tv_title, tv_nodata;
    private ProgressDialog pd;
    private List<Datum> categorydatumList;
    private Explor_Cat_Adapter explor_cat_adapter;
    private SwipeRefreshLayout srlCategory;
    Constants constants;
    String latitude,longi, CatID, SubcatID, SubCatChildID,ComesFrom;
    All_nearby_stall_Adapter all_nearby_stall_adapter;
    public ArrayList<All_Stall_Info> stallInfo_list;
    int taskList_size;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_all_category);
        init();

        constants = new Constants(getApplicationContext());
       latitude = constants.getLatitude();
      longi = constants.getLongitude();

        System.out.println("===all nearby stall===="+latitude+longi);

        Intent intent =getIntent();
//        if(intent != null){
            CatID = intent.getStringExtra("catid");
            SubcatID = intent.getStringExtra("subcatid");
            SubCatChildID = intent.getStringExtra("subcatchildid");
            ComesFrom = intent.getStringExtra("comes_from");
//        }

        if (CheckNetwork.isNetAvailable(getApplicationContext())) {
            getCartDataTasks();
            getStallNearBy(latitude,longi, CatID, SubcatID, SubCatChildID);
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.plz_check_your_intrenet_text), Toast.LENGTH_SHORT).show();
        }

        srlCategory.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (CheckNetwork.isNetAvailable(getApplicationContext())) {
                    getStallNearBy(latitude,longi,CatID,SubcatID,SubCatChildID);
                } else {
                    Toast.makeText(getApplicationContext(), "Check Network Connection", Toast.LENGTH_LONG).show();
                }
                srlCategory.setRefreshing(false);
            }
        });
    }

    private void init() {
        id_toolbar = findViewById(R.id.id_toolbar);
        iv_back = id_toolbar.findViewById(R.id.id_back);
        tv_title = id_toolbar.findViewById(R.id.tv_tittle);
        iv_back.setOnClickListener(this);
        rv_city = findViewById(R.id.id_rv);
        tvNotFound = findViewById(R.id.tvNotFoundId);
        srlCategory = findViewById(R.id.srlCategoryId);

        tv_title.setText(getString(R.string.stalls_near_you));
        stallInfo_list = new ArrayList<>();

        pd = new ProgressDialog(All_NearBy_StallActivity.this, R.style.AppCompatAlertDialogStyle);


        rv_city.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        all_nearby_stall_adapter = new All_nearby_stall_Adapter(getApplicationContext(),this);
        rv_city.setAdapter(all_nearby_stall_adapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.id_back:
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                break;
        }
    }

    public void getStallNearBy(final String latitude, final String longitute, String catID, String subcatID, String subCatChildID) {
        pd.setMessage("Loading");
        pd.show();

        JsonObject jsonObject = new JsonObject();
        try {
//            jsonObject.addProperty("latitude", "-6.4901067");
//            jsonObject.addProperty("longitude", "106.8306951");

            jsonObject.addProperty("latitude", latitude);
            jsonObject.addProperty("longitude", longitute);
            jsonObject.addProperty("cat_id", catID);
            jsonObject.addProperty("subcat_id", subcatID);
            jsonObject.addProperty("childsubcat_id", subCatChildID);

            Log.e("TAG", "stall near by you request all stalllllll: " +jsonObject.toString());

        } catch (Exception e) {
            e.printStackTrace();
        }

        (ApiClient.getClient().getstallnearbyme_bycat(jsonObject)).enqueue(new Callback<All_StallDataPojo>() {
            @Override
            public void onResponse(Call<All_StallDataPojo> call, Response<All_StallDataPojo> response) {
                pd.dismiss();
                if (response.isSuccessful()) {
                    try {
                        Log.e("TAG", "stall near by you response all stalllllll : " + new Gson().toJson(response.body()));
                        All_StallDataPojo stallDataPojo = response.body();
                        String resMessage = stallDataPojo.getMessage();
                        if (stallDataPojo.getStatus()) {
                            tvNotFound.setVisibility(View.GONE);
                            stallInfo_list = stallDataPojo.getData();
                            all_nearby_stall_adapter.addStallList(stallInfo_list);
                        } else {
                            pd.dismiss();
                            tvNotFound.setVisibility(View.VISIBLE);
                            stallInfo_list.clear();
                            all_nearby_stall_adapter.addStallList(stallInfo_list);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        pd.dismiss();
                    }
                } else {
                    pd.dismiss();
                }
            }

            @Override
            public void onFailure(Call<All_StallDataPojo> call, Throwable t) {
                 pd.dismiss();
                Toast.makeText(All_NearBy_StallActivity.this, getString(R.string.server_errors), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getCartDataTasks() {
        class GetTasks extends AsyncTask<Void, Void, List<ProductDataTask>> {
            @Override
            protected List<ProductDataTask> doInBackground(Void... voids) {
                List<ProductDataTask> taskList = DatabaseClient
                        .getInstance(All_NearBy_StallActivity.this)
                        .getAppDatabase()
                        .taskDao()
                        .getAll();
                return taskList;
            }

            @Override
            protected void onPostExecute(List<ProductDataTask> tasks) {
                super.onPostExecute(tasks);
                System.out.println("======task list size====="+tasks.size());
                taskList_size = tasks.size();
            }
        }

        GetTasks gt = new GetTasks();
        gt.execute();
    }

    @Override
    public void onClick(View view, int position, All_Stall_Info detail) {
        switch (view.getId()) {
            case R.id.ll_wholebox:
                String FINALVENDORID =  AppSession.getStringPreferences(getApplicationContext(), "final_vendorID");
                System.out.println(detail.getUserId()+"==all near stall activity==vendorID id===="+FINALVENDORID);

                if(FINALVENDORID !=null ) {
                    if (FINALVENDORID.equals(detail.getUserId())) {
                        AppSession.setStringPreferences(getApplicationContext(), "final_vendorID", detail.getUserId());
                        startActivity(new Intent(getApplicationContext(), All_Product_Activity.class)
                                .putExtra("catID", detail.getCategory_id())
                                .putExtra("vendorID", detail.getUserId())
                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                    } else {
                        if( FINALVENDORID.isEmpty()){
                            AppSession.setStringPreferences(getApplicationContext(), "final_vendorID", detail.getUserId());
                            startActivity(new Intent(getApplicationContext(), All_Product_Activity.class)
                                    .putExtra("catID", detail.getCategory_id())
                                    .putExtra("vendorID", detail.getUserId())
                                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                        }
                        else{
                            if(taskList_size == 0) {
                                AppSession.setStringPreferences(getApplicationContext(), "final_vendorID", detail.getUserId());
                                startActivity(new Intent(getApplicationContext(), All_Product_Activity.class)
                                        .putExtra("catID", detail.getCategory_id())
                                        .putExtra("vendorID", detail.getUserId())
                                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                            }
                            else{
                                cartdialoge();
                            }
                            }
                    }
                }
                else {
                    AppSession.setStringPreferences(getApplicationContext(), "final_vendorID", detail.getUserId());
                    startActivity(new Intent(getApplicationContext(), All_Product_Activity.class)
                            .putExtra("catID", detail.getCategory_id())
                            .putExtra("vendorID", detail.getUserId())
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }
        }
    }

    private void cartdialoge() {
        final Dialog dialog = new Dialog(All_NearBy_StallActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.clear_cart_dialoge);
        AppCompatButton no = dialog.findViewById(R.id.mbtnNoId);
        AppCompatButton mbtnYes = dialog.findViewById(R.id.mbtnYesId);

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        mbtnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearCart();
                dialog.dismiss();
                AppSession.setStringPreferences(getApplicationContext(), "final_vendorID","");
            }
        });
        dialog.show();
    }


    private void clearCart() {
        class GetTasks extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... voids) {
                DatabaseClient.getInstance(getApplicationContext())
                        .getAppDatabase()
                        .taskDao()
                        .delete();
                return null;
            }
            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
            }
        }
        GetTasks gt = new GetTasks();
        gt.execute();
    }
}
