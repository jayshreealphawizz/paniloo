package com.alpha.paanilo.coupanPkg;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.alpha.paanilo.R;
import com.alpha.paanilo.coupanPkg.couponModlePkg.Coupon;
import com.alpha.paanilo.coupanPkg.couponModlePkg.CouponListPozo;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.utility.CheckNetwork;
import com.alpha.paanilo.utility.Constants;
import com.google.gson.Gson;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CoupanActivity extends AppCompatActivity implements View.OnClickListener, CoupanAdapter.CouponOnClick {
    private static Animation shakeAnimation;
    View id_toolbar;
    private String userId;
    private RecyclerView rvCoupon;
    private CoupanAdapter coupanAdapter;
    private LinearLayoutManager linearLayoutManager;
    private SwipeRefreshLayout srlCoupon;
    private AppCompatTextView tvApplyCoupon, tvCoupontTag;
    private AppCompatEditText etCouponInputBox;
    private List<Coupon> couponList;
    private final int entryForToast = 0;
    private RelativeLayout rlAvailableSearch;
    private Constants constants;
    private ProgressDialog pd;
    private AppCompatEditText etSearchStall;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.coupan_activity);
        id_toolbar = findViewById(R.id.toolbar);
        etSearchStall = id_toolbar.findViewById(R.id.etSearchStallId);
        init();
        if (CheckNetwork.isNetAvailable(getApplicationContext())) {
            getCoupon();
        } else {
            Toast.makeText(getApplicationContext(), "Check Network Connection", Toast.LENGTH_LONG).show();
        }
        srlCoupon.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (CheckNetwork.isNetAvailable(getApplicationContext())) {
                    getCoupon();
                } else {
                    Toast.makeText(getApplicationContext(), "Check Network Connection", Toast.LENGTH_LONG).show();
                }
                srlCoupon.setRefreshing(false);
            }
        });


        etSearchStall.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                coupanAdapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {
                coupanAdapter.getFilter().filter(s);
            }
        });
    }


    private void init() {
        tvCoupontTag = findViewById(R.id.tvCoupontTagId);
        srlCoupon = findViewById(R.id.srlCouponId);
        rvCoupon = findViewById(R.id.rvCouponId);
        linearLayoutManager = new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false);
        rvCoupon.setLayoutManager(linearLayoutManager);
        coupanAdapter = new CoupanAdapter(getApplicationContext(), this);
        rvCoupon.setAdapter(coupanAdapter);
        shakeAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake);
        constants = new Constants(getApplicationContext());
        pd = new ProgressDialog(CoupanActivity.this, R.style.AppCompatAlertDialogStyle);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        }
    }

    public void getCoupon() {
        pd.setMessage("Loading");
        pd.show();

        (ApiClient.getClient().couponListing()).enqueue(new Callback<CouponListPozo>() {
            @Override
            public void onResponse(Call<CouponListPozo> call, Response<CouponListPozo> response) {
                pd.dismiss();

                Log.e("response:: ", "response :: " + new Gson().toJson(response.body()));
                if (response.isSuccessful()) {
                    try {
                        pd.dismiss();
                        CouponListPozo couponListPozo = response.body();
                        if (couponListPozo.getStatus()) {
                            couponList = couponListPozo.getCoupon();
                            coupanAdapter.addCoupon(couponList);
                            tvCoupontTag.setVisibility(View.GONE);
                        } else {
                            tvCoupontTag.setVisibility(View.VISIBLE);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<CouponListPozo> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(getApplicationContext(), getString(R.string.server_errors), Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void couponOnClick(View view, int position, Coupon mostViewList) {
        switch (view.getId()) {
            case R.id.ivCouponClickId:
                Intent intent = new Intent();
                intent.putExtra("couponOff", mostViewList.getDiscount());
                intent.putExtra("couponCode", mostViewList.getCouponCode());
                setResult(2, intent);
                finish();

                break;
        }
    }
}
