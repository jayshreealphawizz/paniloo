package com.alpha.paanilo.coupanPkg;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.alpha.paanilo.R;
import com.alpha.paanilo.coupanPkg.couponModlePkg.Coupon;

import java.util.ArrayList;
import java.util.List;

public class CoupanAdapter extends RecyclerView.Adapter<CoupanAdapter.ViewHolder>implements Filterable {
    private Context context;
    private CouponOnClick cartClick;
    List<Coupon> couponList;
    List<Coupon> couponList1;

    public CoupanAdapter(Context context, CouponOnClick cartClick) {
        this.context = context;
        this.cartClick = cartClick;
    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    couponList = couponList1;
                } else {
                    List<Coupon> filteredList = new ArrayList<>();
                    for (Coupon row : couponList1) {
                        if (row.getCouponCode().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }
                    couponList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = couponList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                couponList = (ArrayList<Coupon>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.coupon_row, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.tvCouponCode.setText(couponList.get(position).getCouponCode());
        holder.tvOffCoupon.setText(couponList.get(position).getDiscount()+" Off");
    }

    public void addCoupon(List<Coupon> couponList) {
        this.couponList = couponList;
        this.couponList1 = couponList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return couponList == null ? 0 : couponList.size();
    }

    public interface CouponOnClick {
        void couponOnClick(View view, int position,Coupon  mostViewList);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private AppCompatTextView tvCouponCode, tvOffCoupon, tvValidityCoupon;
        private AppCompatImageView ivCouponClick;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ivCouponClick = itemView.findViewById(R.id.ivCouponClickId);
            tvCouponCode = itemView.findViewById(R.id.tvCouponCodeId);
            tvOffCoupon = itemView.findViewById(R.id.tvOffCouponId);
            tvValidityCoupon = itemView.findViewById(R.id.tvValidityCouponId);
            ivCouponClick.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            cartClick.couponOnClick(v, getAdapterPosition(),couponList.get(getAdapterPosition()));
        }
    }
}
