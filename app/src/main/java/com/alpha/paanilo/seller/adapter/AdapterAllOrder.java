package com.alpha.paanilo.seller.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.alpha.paanilo.DatumallOrder;
import com.alpha.paanilo.R;
import com.alpha.paanilo.seller.SellerAllOrderDetailsActivity;

import java.util.List;


public class AdapterAllOrder extends RecyclerView.Adapter<AdapterAllOrder.MyViewHolder> {
//    private final List<DatumallOrder> productList11;
    Context context;
    private List<DatumallOrder> productList;
//    private AdapterAllOrder listener;

    public AdapterAllOrder(Context context, List<DatumallOrder> productList) {
        this.context = context;

        this.productList = productList;
//        this.productList11 = productList;
        notifyDataSetChanged();

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_all_allorder_adapter, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        final  DatumallOrder completeOrderDatum = productList.get(position);

        holder.tvOrderId.setText("Order Id: "+ completeOrderDatum.getOrderId());
        holder.tvTAmount.setText(completeOrderDatum.getTotalAmount());
        holder.tvAdress.setText(completeOrderDatum.getDeliveryAddress());
        holder.tvDate.setText(completeOrderDatum.getDate());

        Log.e("productstatus", "productstatus complete :::" + completeOrderDatum);

//      Log.e("Status", "data::  " + completeOrderDatum.getStatus());

        if(completeOrderDatum.getStatus().equalsIgnoreCase("0")){
            holder.tvStatus.setVisibility(View.INVISIBLE);
//            holder.tvStatus.setText("Delivered");
        }else if(completeOrderDatum.getStatus().equalsIgnoreCase("1")){
            holder.tvStatus.setVisibility(View.VISIBLE);
            holder.tvStatus.setText("Pending");
        }else if(completeOrderDatum.getStatus().equalsIgnoreCase("2")){
            holder.tvStatus.setVisibility(View.VISIBLE);
            holder.tvStatus.setText("Delivered");
        }else if(completeOrderDatum.getStatus().equalsIgnoreCase("3")){
            holder.tvStatus.setVisibility(View.VISIBLE);
            holder.tvStatus.setText("Cancel");
        }else if(completeOrderDatum.getStatus().equalsIgnoreCase("4")){
            holder.tvStatus.setVisibility(View.VISIBLE);
            holder.tvStatus.setText("Complete");
        }else{
            holder.tvStatus.setVisibility(View.INVISIBLE);
        }


        holder.carditem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, SellerAllOrderDetailsActivity.class);
                intent.putExtra("prodcutname", completeOrderDatum.getProductName());
                intent.putExtra("productquantity", completeOrderDatum.getQuantity());
                intent.putExtra("producttotamount", completeOrderDatum.getTotalAmount());
                intent.putExtra("productimage", completeOrderDatum.getProductImage());
                intent.putExtra("productuserid", completeOrderDatum.getUserId());
                intent.putExtra("productSaleid", completeOrderDatum.getSaleId());
                intent.putExtra("productstatus", completeOrderDatum.getStatus());
                intent.putExtra("getDeliveryAddress", completeOrderDatum.getDeliveryAddress());
                intent.putExtra("productdate", completeOrderDatum.getDeliveryTime());
                intent.putExtra("userfullname", completeOrderDatum.getUserFullname());
                intent.putExtra("OrderId" ,  completeOrderDatum.getOrderId());
                intent.putExtra("paymentmethod" ,  completeOrderDatum.getPaymentMethod());

                //                Log.e("rahul", "rahul Userid " + openOrderDatum.getUserId());
//                Log.e("rahul", "rahul getSaleId " + openOrderDatum.getSaleId());

                context.startActivity(intent);

//                Intent intent = new Intent(context, SellerProdcutDetailActivity.class);
//                context.startActivity(intent);
//                maplistitemClickListener.maplistitemClickListener(v, position);
            }
        });
    }

//    @Override
//    public int getItemCount() {
//        return completeOrderDatumArrayList.size();
//    }

//    @Override
//    public Filter getFilter() {
//        return exampleFilter;
//    }

    @Override
    public int getItemCount() {
        return productList == null ? 0 : productList.size();
    }





    public class MyViewHolder extends RecyclerView.ViewHolder {
        //        public View cardview;
//        ImageView img_bottle;
//        TextView tv_bottlename, tv_date, tv_time, tv_qlt, tv_liter, tv_rupee, tv_order, tv_name;
//        CardView carditem;
        CardView carditem;
//        TextView tv_bottlename, tv_date, tv_time, tv_qty, tv_liter, tv_amount, tv_rupee, tv_order, tv_name;
//        AppCompatButton btn_decline, btn_accept;

//        ImageView img_bottle;
//        CardView carditem;
//        TextView tv_bottlename, tv_Status,tv_date, tv_time, tv_qty, tv_liter, tv_amount, tv_rupee, tv_order, tv_name;
//        AppCompatButton btn_decline, btn_accept;

        TextView tvOrderId, tvTAmount, tvAdress, tvStatus, tvDate;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

//            img_bottle = itemView.findViewById(R.id.img_bottle);
//
//            tv_bottlename = itemView.findViewById(R.id.tv_bottlename);
//            tv_date = itemView.findViewById(R.id.tv_date);
//            tv_time = itemView.findViewById(R.id.tv_time);
//            tv_qlt = itemView.findViewById(R.id.tv_qty);
//            tv_liter = itemView.findViewById(R.id.tv_liter);
//
//            carditem = itemView.findViewById(R.id.carditem);
//            tv_rupee = itemView.findViewById(R.id.tv_rupee);
//            tv_order = itemView.findViewById(R.id.tv_order);
//            tv_name = itemView.findViewById(R.id.tv_name);

            carditem = itemView.findViewById(R.id.carditem);
            tvStatus = itemView.findViewById(R.id.tvStatus);
            tvOrderId = itemView.findViewById(R.id.tvOrderId);
            tvTAmount = itemView.findViewById(R.id.tvTAmount);
            tvAdress = itemView.findViewById(R.id.tvAdress);
            tvDate = itemView.findViewById(R.id.tvDate);
        }
    }

}