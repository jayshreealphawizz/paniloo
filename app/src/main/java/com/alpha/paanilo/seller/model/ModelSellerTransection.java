package com.alpha.paanilo.seller.model;

public class ModelSellerTransection {
    String transactionhtitle;
    String transactionhdate;

    public ModelSellerTransection(String transactionhtitle) {
        this.transactionhtitle = transactionhtitle;
        this.transactionhdate = transactionhdate;
    }

    public String getTransactionhtitle() {
        return transactionhtitle;
    }

    public void setTransactionhtitle(String transactionhtitle) {
        this.transactionhtitle = transactionhtitle;
    }

    public String getTransactionhdate() {
        return transactionhdate;
    }

    public void setTransactionhdate(String transactionhdate) {
        this.transactionhdate = transactionhdate;
    }
}
