
package com.alpha.paanilo.seller.model.openorder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OpenOrderDatum {

//    @SerializedName("sale_id")
//    @Expose
//    private String saleId;
//    @SerializedName("user_id")
//    @Expose
//    private String userId;
//    @SerializedName("product_id")
//    @Expose
//    private String productId;
//    @SerializedName("order_id")
//    @Expose
//    private String orderId;
//    @SerializedName("date")
//    @Expose
//    private String date;
//    @SerializedName("delivery_time")
//    @Expose
//    private String deliveryTime;
//    @SerializedName("status")
//    @Expose
//    private String status;
//    @SerializedName("note")
//    @Expose
//    private String note;
//    @SerializedName("is_paid")
//    @Expose
//    private String isPaid;
//    @SerializedName("coupon_discount")
//    @Expose
//    private String couponDiscount;
//    @SerializedName("total_amount")
//    @Expose
//    private String totalAmount;
//    @SerializedName("shipping_charge")
//    @Expose
//    private String shippingCharge;
//    @SerializedName("total_rewards")
//    @Expose
//    private String totalRewards;
//    @SerializedName("total_kg")
//    @Expose
//    private String totalKg;
//    @SerializedName("total_items")
//    @Expose
//    private String totalItems;
//    @SerializedName("socity_id")
//    @Expose
//    private String socityId;
//    @SerializedName("delivery_address")
//    @Expose
//    private String deliveryAddress;
//    @SerializedName("delivery_charge")
//    @Expose
//    private String deliveryCharge;
//    @SerializedName("new_store_id")
//    @Expose
//    private String newStoreId;
//    @SerializedName("assign_to")
//    @Expose
//    private String assignTo;
//    @SerializedName("payment_method")
//    @Expose
//    private String paymentMethod;
//    @SerializedName("add_coupon")
//    @Expose
//    private String addCoupon;
//    @SerializedName("First_name")
//    @Expose
//    private String firstName;
//    @SerializedName("Last_name")
//    @Expose
//    private String lastName;
//    @SerializedName("email")
//    @Expose
//    private String email;
//    @SerializedName("zip_code")
//    @Expose
//    private String zipCode;
//    @SerializedName("center_pickup_date")
//    @Expose
//    private String centerPickupDate;
//    @SerializedName("center_pickup_time")
//    @Expose
//    private String centerPickupTime;
//    @SerializedName("outlets_pickup_location")
//    @Expose
//    private String outletsPickupLocation;
//    @SerializedName("location_id")
//    @Expose
//    private String locationId;
//    @SerializedName("add_note")
//    @Expose
//    private String addNote;
//    @SerializedName("address_id")
//    @Expose
//    private String addressId;
//    @SerializedName("order_delivery_time")
//    @Expose
//    private String orderDeliveryTime;
//    @SerializedName("user_fullname")
//    @Expose
//    private String userFullname;
//    @SerializedName("user_phone")
//    @Expose
//    private String userPhone;
//    @SerializedName("pincode")
//    @Expose
//    private String pincode;
//    @SerializedName("product_image")
//    @Expose
//    private String productImage;
//    @SerializedName("user_email")
//    @Expose
//    private String userEmail;
//    @SerializedName("house_no")
//    @Expose
//    private String houseNo;
//
//    public String getSaleId() {
//        return saleId;
//    }
//
//    public void setSaleId(String saleId) {
//        this.saleId = saleId;
//    }
//
//    public String getUserId() {
//        return userId;
//    }
//
//    public void setUserId(String userId) {
//        this.userId = userId;
//    }
//
//    public String getProductId() {
//        return productId;
//    }
//
//    public void setProductId(String productId) {
//        this.productId = productId;
//    }
//
//    public String getOrderId() {
//        return orderId;
//    }
//
//    public void setOrderId(String orderId) {
//        this.orderId = orderId;
//    }
//
//    public String getDate() {
//        return date;
//    }
//
//    public void setDate(String date) {
//        this.date = date;
//    }
//
//    public String getDeliveryTime() {
//        return deliveryTime;
//    }
//
//    public void setDeliveryTime(String deliveryTime) {
//        this.deliveryTime = deliveryTime;
//    }
//
//    public String getStatus() {
//        return status;
//    }
//
//    public void setStatus(String status) {
//        this.status = status;
//    }
//
//    public String getNote() {
//        return note;
//    }
//
//    public void setNote(String note) {
//        this.note = note;
//    }
//
//    public String getIsPaid() {
//        return isPaid;
//    }
//
//    public void setIsPaid(String isPaid) {
//        this.isPaid = isPaid;
//    }
//
//    public String getCouponDiscount() {
//        return couponDiscount;
//    }
//
//    public void setCouponDiscount(String couponDiscount) {
//        this.couponDiscount = couponDiscount;
//    }
//
//    public String getTotalAmount() {
//        return totalAmount;
//    }
//
//    public void setTotalAmount(String totalAmount) {
//        this.totalAmount = totalAmount;
//    }
//
//    public String getShippingCharge() {
//        return shippingCharge;
//    }
//
//    public void setShippingCharge(String shippingCharge) {
//        this.shippingCharge = shippingCharge;
//    }
//
//    public String getTotalRewards() {
//        return totalRewards;
//    }
//
//    public void setTotalRewards(String totalRewards) {
//        this.totalRewards = totalRewards;
//    }
//
//    public String getTotalKg() {
//        return totalKg;
//    }
//
//    public void setTotalKg(String totalKg) {
//        this.totalKg = totalKg;
//    }
//
//    public String getTotalItems() {
//        return totalItems;
//    }
//
//    public void setTotalItems(String totalItems) {
//        this.totalItems = totalItems;
//    }
//
//    public String getSocityId() {
//        return socityId;
//    }
//
//    public void setSocityId(String socityId) {
//        this.socityId = socityId;
//    }
//
//    public String getDeliveryAddress() {
//        return deliveryAddress;
//    }
//
//    public void setDeliveryAddress(String deliveryAddress) {
//        this.deliveryAddress = deliveryAddress;
//    }
//
//    public String getDeliveryCharge() {
//        return deliveryCharge;
//    }
//
//    public void setDeliveryCharge(String deliveryCharge) {
//        this.deliveryCharge = deliveryCharge;
//    }
//
//    public String getNewStoreId() {
//        return newStoreId;
//    }
//
//    public void setNewStoreId(String newStoreId) {
//        this.newStoreId = newStoreId;
//    }
//
//    public String getAssignTo() {
//        return assignTo;
//    }
//
//    public void setAssignTo(String assignTo) {
//        this.assignTo = assignTo;
//    }
//
//    public String getPaymentMethod() {
//        return paymentMethod;
//    }
//
//    public void setPaymentMethod(String paymentMethod) {
//        this.paymentMethod = paymentMethod;
//    }
//
//    public String getAddCoupon() {
//        return addCoupon;
//    }
//
//    public void setAddCoupon(String addCoupon) {
//        this.addCoupon = addCoupon;
//    }
//
//    public String getFirstName() {
//        return firstName;
//    }
//
//    public void setFirstName(String firstName) {
//        this.firstName = firstName;
//    }
//
//    public String getLastName() {
//        return lastName;
//    }
//
//    public void setLastName(String lastName) {
//        this.lastName = lastName;
//    }
//
//    public String getEmail() {
//        return email;
//    }
//
//    public void setEmail(String email) {
//        this.email = email;
//    }
//
//    public String getZipCode() {
//        return zipCode;
//    }
//
//    public void setZipCode(String zipCode) {
//        this.zipCode = zipCode;
//    }
//
//    public String getCenterPickupDate() {
//        return centerPickupDate;
//    }
//
//    public void setCenterPickupDate(String centerPickupDate) {
//        this.centerPickupDate = centerPickupDate;
//    }
//
//    public String getCenterPickupTime() {
//        return centerPickupTime;
//    }
//
//    public void setCenterPickupTime(String centerPickupTime) {
//        this.centerPickupTime = centerPickupTime;
//    }
//
//    public String getOutletsPickupLocation() {
//        return outletsPickupLocation;
//    }
//
//    public void setOutletsPickupLocation(String outletsPickupLocation) {
//        this.outletsPickupLocation = outletsPickupLocation;
//    }
//
//    public String getLocationId() {
//        return locationId;
//    }
//
//    public void setLocationId(String locationId) {
//        this.locationId = locationId;
//    }
//
//    public String getAddNote() {
//        return addNote;
//    }
//
//    public void setAddNote(String addNote) {
//        this.addNote = addNote;
//    }
//
//    public String getAddressId() {
//        return addressId;
//    }
//
//    public void setAddressId(String addressId) {
//        this.addressId = addressId;
//    }
//
//    public String getOrderDeliveryTime() {
//        return orderDeliveryTime;
//    }
//
//    public void setOrderDeliveryTime(String orderDeliveryTime) {
//        this.orderDeliveryTime = orderDeliveryTime;
//    }
//
//    public String getUserFullname() {
//        return userFullname;
//    }
//
//    public void setUserFullname(String userFullname) {
//        this.userFullname = userFullname;
//    }
//
//    public String getUserPhone() {
//        return userPhone;
//    }
//
//    public void setUserPhone(String userPhone) {
//        this.userPhone = userPhone;
//    }
//
//    public String getPincode() {
//        return pincode;
//    }
//
//    public void setPincode(String pincode) {
//        this.pincode = pincode;
//    }
//
//    public String getProductImage() {
//        return productImage;
//    }
//
//    public void setProductImage(String productImage) {
//        this.productImage = productImage;
//    }
//
//    public String getUserEmail() {
//        return userEmail;
//    }
//
//    public void setUserEmail(String userEmail) {
//        this.userEmail = userEmail;
//    }
//
//    public String getHouseNo() {
//        return houseNo;
//    }
//
//    public void setHouseNo(String houseNo) {
//        this.houseNo = houseNo;
//    }

    @SerializedName("sale_id")
    @Expose
    private String saleId;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("order_id")
    @Expose
    private String orderId;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("delivery_time")
    @Expose
    private String deliveryTime;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("note")
    @Expose
    private String note;
    @SerializedName("is_paid")
    @Expose
    private String isPaid;


    @SerializedName("distance")
    @Expose
    private String distance;
    @SerializedName("order_delivery_time")
    @Expose
    private String order_delivery_time;

    public String getOrder_delivery_time() {
        return order_delivery_time;
    }

    public void setOrder_delivery_time(String order_delivery_time) {
        this.order_delivery_time = order_delivery_time;
    }

    @SerializedName("coupon_discount")
    @Expose
    private String couponDiscount;
    @SerializedName("total_amount")
    @Expose
    private String totalAmount;
    @SerializedName("shipping_charge")
    @Expose
    private String shippingCharge;
    @SerializedName("total_rewards")
    @Expose
    private String totalRewards;
    @SerializedName("total_kg")
    @Expose
    private String totalKg;
    @SerializedName("total_items")
    @Expose
    private String totalItems;
    @SerializedName("socity_id")
    @Expose
    private String socityId;
    @SerializedName("delivery_address")
    @Expose
    private String deliveryAddress;
    @SerializedName("delivery_charge")
    @Expose
    private String deliveryCharge;
    @SerializedName("new_store_id")
    @Expose
    private String newStoreId;
    @SerializedName("assign_to")
    @Expose
    private String assignTo;
    @SerializedName("payment_method")
    @Expose
    private String paymentMethod;
    @SerializedName("add_coupon")
    @Expose
    private String addCoupon;
    @SerializedName("First_name")
    @Expose
    private String firstName;
    @SerializedName("Last_name")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("zip_code")
    @Expose
    private String zipCode;
    @SerializedName("center_pickup_date")
    @Expose
    private String centerPickupDate;
    @SerializedName("center_pickup_time")
    @Expose
    private String centerPickupTime;
    @SerializedName("outlets_pickup_location")
    @Expose
    private String outletsPickupLocation;
    @SerializedName("location_id")
    @Expose
    private String locationId;
    @SerializedName("add_note")
    @Expose
    private String addNote;
    @SerializedName("address_id")
    @Expose
    private String addressId;
//    @SerializedName("order_delivery_time")
//    @Expose
//    private String orderDeliveryTime;
    @SerializedName("user_fullname")
    @Expose
    private String userFullname;
    @SerializedName("user_phone")
    @Expose
    private String userPhone;
    @SerializedName("pincode")
    @Expose
    private String pincode;
    @SerializedName("product_image")
    @Expose
    private String productImage;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("quantity")
    @Expose
    private String quantity;
    @SerializedName("unit")
    @Expose
    private String unit;
    @SerializedName("user_email")
    @Expose
    private String userEmail;
    @SerializedName("house_no")
    @Expose
    private String houseNo;


    @SerializedName("unit_value")
    @Expose
    private String unit_value;

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getUnit_value() {
        return unit_value;
    }

    public void setUnit_value(String unit_value) {
        this.unit_value = unit_value;
    }

    public String getSaleId() {
        return saleId;
    }

    public void setSaleId(String saleId) {
        this.saleId = saleId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(String deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getIsPaid() {
        return isPaid;
    }

    public void setIsPaid(String isPaid) {
        this.isPaid = isPaid;
    }

    public String getCouponDiscount() {
        return couponDiscount;
    }

    public void setCouponDiscount(String couponDiscount) {
        this.couponDiscount = couponDiscount;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getShippingCharge() {
        return shippingCharge;
    }

    public void setShippingCharge(String shippingCharge) {
        this.shippingCharge = shippingCharge;
    }

    public String getTotalRewards() {
        return totalRewards;
    }

    public void setTotalRewards(String totalRewards) {
        this.totalRewards = totalRewards;
    }

    public String getTotalKg() {
        return totalKg;
    }

    public void setTotalKg(String totalKg) {
        this.totalKg = totalKg;
    }

    public String getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(String totalItems) {
        this.totalItems = totalItems;
    }

    public String getSocityId() {
        return socityId;
    }

    public void setSocityId(String socityId) {
        this.socityId = socityId;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getDeliveryCharge() {
        return deliveryCharge;
    }

    public void setDeliveryCharge(String deliveryCharge) {
        this.deliveryCharge = deliveryCharge;
    }

    public String getNewStoreId() {
        return newStoreId;
    }

    public void setNewStoreId(String newStoreId) {
        this.newStoreId = newStoreId;
    }

    public String getAssignTo() {
        return assignTo;
    }

    public void setAssignTo(String assignTo) {
        this.assignTo = assignTo;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getAddCoupon() {
        return addCoupon;
    }

    public void setAddCoupon(String addCoupon) {
        this.addCoupon = addCoupon;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCenterPickupDate() {
        return centerPickupDate;
    }

    public void setCenterPickupDate(String centerPickupDate) {
        this.centerPickupDate = centerPickupDate;
    }

    public String getCenterPickupTime() {
        return centerPickupTime;
    }

    public void setCenterPickupTime(String centerPickupTime) {
        this.centerPickupTime = centerPickupTime;
    }

    public String getOutletsPickupLocation() {
        return outletsPickupLocation;
    }

    public void setOutletsPickupLocation(String outletsPickupLocation) {
        this.outletsPickupLocation = outletsPickupLocation;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getAddNote() {
        return addNote;
    }

    public void setAddNote(String addNote) {
        this.addNote = addNote;
    }

    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

//    public String getOrderDeliveryTime() {
//        return orderDeliveryTime;
//    }
//
//    public void setOrderDeliveryTime(String orderDeliveryTime) {
//        this.orderDeliveryTime = orderDeliveryTime;
//    }

    public String getUserFullname() {
        return userFullname;
    }

    public void setUserFullname(String userFullname) {
        this.userFullname = userFullname;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getHouseNo() {
        return houseNo;
    }

    public void setHouseNo(String houseNo) {
        this.houseNo = houseNo;
    }


}
