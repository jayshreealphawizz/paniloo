package com.alpha.paanilo.seller.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.alpha.paanilo.DatumallOrder;
import com.alpha.paanilo.R;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.seller.model.ModelAllOrder;
import com.alpha.paanilo.seller.model.completeorder.CompleteOrderDatum;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;


public class AdapterSellerReport extends RecyclerView.Adapter<AdapterSellerReport.MyViewHolder> {
    Context context;
    List<DatumallOrder> arraylistpicture;
    int counter = 0;

    public AdapterSellerReport(Context context, List<DatumallOrder> arraylistpicture) {
        this.context = context;
        this.arraylistpicture = arraylistpicture;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_all_order_adapter, parent, false);
        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        DatumallOrder datumallOrder = arraylistpicture.get(position);

        Log.e("rahul ", "rahul " + datumallOrder.getProductImage());
        Log.e("rahul ", "tv_bottlename " + datumallOrder.getDeliveryTime());

//        Toast.makeText(context, "totl earing ", Toast.LENGTH_SHORT).show();
//dfdfd
        Log.e("Product image ", "product image " + datumallOrder.getProductImage());
//        if (completeOrderDatum.getProductImage().equals("") || completeOrderDatum.getProductImage() == null) {
//        } else {
//            Glide.with(context)
//                    .load(ApiClient.VENDOR_PROFILE_URL + completeOrderDatum.getProductImage())
//                    .placeholder(R.drawable.placeholder)
//                    .into(holder.img_bottle);
//        }
//        Log.e("Product image ", "product image " + modelPicture.getProductImage());
//        if (datumallOrder.getProductImage() == null) {
//
//        } else {
//            String imgName = datumallOrder.getProductImage().substring((datumallOrder.getProductImage().lastIndexOf(",") + 1));
//
//            Log.e("pro", "imgName" + imgName);
//
//            Glide.with(context)
//                    .load(ApiClient.CATEGORY_PRODUCT_URL + imgName)
//                    .placeholder(R.drawable.placeholder)
//                    .into(holder.img_bottle);
//        }


        holder.tv_date.setText("" + datumallOrder.getDate());
        holder.tv_rupee.setText("Rs." + datumallOrder.getTotalAmount());
        holder.tv_liter.setText("" + datumallOrder.getQuantity());
//        holder.tv_bottlename.setText("" + datumallOrder.getProductName());
        holder.tv_name.setText("" + datumallOrder.getUserFullname());

        if(datumallOrder.getStatus().equalsIgnoreCase("0")){
            holder.tvStatus.setVisibility(View.INVISIBLE);
//            holder.tvStatus.setText("Delivered");
        }else if(datumallOrder.getStatus().equalsIgnoreCase("1")){
            holder.tvStatus.setVisibility(View.VISIBLE);
            holder.tvStatus.setText("Pending");
        }else if(datumallOrder.getStatus().equalsIgnoreCase("2")){
            holder.tvStatus.setVisibility(View.VISIBLE);
            holder.tvStatus.setText("Delivered");
        }else if(datumallOrder.getStatus().equalsIgnoreCase("3")){
            holder.tvStatus.setVisibility(View.VISIBLE);
            holder.tvStatus.setText("Cancel");
        }else if(datumallOrder.getStatus().equalsIgnoreCase("4")){
            holder.tvStatus.setVisibility(View.VISIBLE);
            holder.tvStatus.setText("Complete");
        }else{
            holder.tvStatus.setVisibility(View.INVISIBLE);
        }
       /* CompleteOrderDatum completeOrderDatum = arraylistpicture.get(position);
//tvStatus



        if (completeOrderDatum.getProductImage().equals("")) {
        } else {
            Glide.with(context)
                    .load(ApiClient.VENDOR_PROFILE_URL + completeOrderDatum.getProductImage())
                    .placeholder(R.drawable.placeholder)
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .dontTransform()
                    .into(holder.img_bottle);
        }

        holder.tv_date.setText(completeOrderDatum.getDate());
        holder.tv_liter.setText(completeOrderDatum.getTotalItems()+" Liter");

        holder.tv_rupee.setText(Integer.parseInt(completeOrderDatum.getTotalAmount())/Integer.parseInt(completeOrderDatum.getTotalItems())+"/Liter");
*/
//        holder.carditem.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(position==0) {
//                    context.startActivity(new Intent(context, com.alpha.paanilo.seller.SellerOrderDetailsActivity.class));
//                }
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return arraylistpicture.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public View cardview;
        ImageView img_bottle;
        TextView tvStatus ,tv_bottlename, tv_date, tv_time, tv_qlt, tv_liter, tv_rupee, tv_order, tv_name;
        CardView carditem;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            img_bottle = itemView.findViewById(R.id.img_bottle);

            tv_bottlename = itemView.findViewById(R.id.tv_bottlename);
            tv_date = itemView.findViewById(R.id.tv_date);
            tv_time = itemView.findViewById(R.id.tv_time);
            tv_qlt = itemView.findViewById(R.id.tv_qty);
            tv_liter = itemView.findViewById(R.id.tv_liter);
            tvStatus = itemView.findViewById(R.id.tvStatus);
            carditem=itemView.findViewById(R.id.carditem);
            tv_rupee = itemView.findViewById(R.id.tv_rupee);
            tv_order = itemView.findViewById(R.id.tv_order);
            tv_name = itemView.findViewById(R.id.tv_name);



        }
    }
}
