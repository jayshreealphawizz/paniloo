package com.alpha.paanilo.seller;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.OpenableColumns;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alpha.paanilo.FileUtils;
import com.alpha.paanilo.R;

import com.alpha.paanilo.SellerMyProdcutActiveModel;
import com.alpha.paanilo.SellerMyProdcutModel;
import com.alpha.paanilo.VendorAddProdcutModel;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.retrofit.RestService;
import com.alpha.paanilo.seller.adapter.AdapterProductUpload;

import com.alpha.paanilo.seller.model.ModelProductUpload;

import com.alpha.paanilo.utility.CheckNetwork;
import com.alpha.paanilo.utility.Constants;

import com.alpha.paanilo.utility.CustomProgressbar;
import com.alpha.paanilo.utility.CustomToast;
import com.bumptech.glide.Glide;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SellerProductUploadActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int PICK_FROM_GALLERY = 101;
    private final ArrayList<ModelProductUpload> newAttachmentList = new ArrayList<>();
    String TAG = getClass().getSimpleName();
    //    ArrayList<ModelProductUpload> newAttachmentList = new ArrayList<>();
//    AdapterProductUpload adapterpicture;
    AdapterProductUpload attachmentListAdapter;
    AppCompatImageView iv_back, iv_notification;
    View id_toolbar;
    AppCompatTextView tv_title;
   TextView etProdcutName ,etMeasurement ;
    TextInputEditText   etPrice, etQuantity, etDescription;
    CardView btn_signup, btn_signupsubmitprodct;
    Constants constants;
    RelativeLayout rlayoutid;
    RestService apiInterface;
    Uri returnUri;
    ArrayList<String> arrayListimageuri = new ArrayList<>();
    private RecyclerView recyclerUploadProduct;
    private ProgressDialog pd;
    private String imagePath, imagePathDoc;
    private File fileForImage, fileForImageDoc;
    private String imagePathlist;
    private List<String> imagePathList;
    String ProdcutIdEdit ;
    String Productid;
    //    private String imagePath;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller_product_upload);

        init();

        id_toolbar = findViewById(R.id.id_toolbar);
        iv_back = id_toolbar.findViewById(R.id.id_back);
        iv_notification = id_toolbar.findViewById(R.id.id_notification);
        tv_title = id_toolbar.findViewById(R.id.tv_tittle);

        iv_back.setOnClickListener(this);
        tv_title.setText("Product Edit");
        iv_notification.setOnClickListener(this);
    }

    private void init() {

        constants = new Constants(getApplicationContext());
        pd = new ProgressDialog(SellerProductUploadActivity.this, R.style.AppCompatAlertDialogStyle);

        rlayoutid = findViewById(R.id.rlayoutid);
        apiInterface = ApiClient.getClient();
        etProdcutName = findViewById(R.id.etProdcutName);
        etMeasurement = findViewById(R.id.etMeasurement);
        etPrice = findViewById(R.id.etPrice);
        etQuantity = findViewById(R.id.etQuantity);
        etDescription = findViewById(R.id.etDescription);

        btn_signup = findViewById(R.id.btn_signup);
        btn_signupsubmitprodct = findViewById(R.id.btn_signupsubmitprodct);
        rlayoutid.setOnClickListener(this);
        btn_signup.setOnClickListener(this);
        btn_signupsubmitprodct.setOnClickListener(this);
        recyclerUploadProduct = findViewById(R.id.recyclerUploadProduct);

        LinearLayoutManager layoutMa = new LinearLayoutManager(com.alpha.paanilo.seller.SellerProductUploadActivity.this, RecyclerView.HORIZONTAL, false);
        recyclerUploadProduct.setLayoutManager(layoutMa);
//        adapterpicture = new AdapterProductUpload(com.alpha.paanilo.seller.SellerProductUploadActivity.this, arraylistpicture);
//        recyclerUploadProduct.setAdapter(adapterpicture);
//        getDataPicture();
//        getaddvendorProduct("");


//        constants.setProductName(etProdcutName.getText().toString());
//        if (etProdcutName.getText().toString().equals(constants.getProductName())) {
////            Log.e("intent::: ", "intent:gfgfg:::" +constants.getProductName() );
////            Log.e("intent::: ", "intent:gfgfgfgghfgggfg:::" + etProdcutName.getText().toString() );
//            btn_signup.setVisibility(View.GONE);
//            btn_signupsubmitprodct.setVisibility(View.GONE);
//        }
        if (CheckNetwork.isNetAvailable(getApplicationContext())) {
            getMyProduct(constants.getUserID());
        } else {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.network), Toast.LENGTH_LONG).show();
        }




        Intent intent = getIntent();


        Log.e("intent::: ", "intent::::" + intent);


         ProdcutIdEdit =  intent.getStringExtra("productid");
        Log.e("intent::: ", "ProdcutIdEdit::::" + ProdcutIdEdit);


        if(ProdcutIdEdit==null){
//    Productid = ProdcutIdEdit;
            btn_signup.setVisibility(View.VISIBLE);
            btn_signupsubmitprodct.setVisibility(View.GONE);
}else {
            btn_signup.setVisibility(View.GONE);
            btn_signupsubmitprodct.setVisibility(View.VISIBLE);
}

//        String ProductName = intent.getStringExtra("productname");
//        String ProductMeasurement = intent.getStringExtra("productmes");
        String ProductPrice = intent.getStringExtra("productprice");
//        String ProductQty = intent.getStringExtra("productqty");
        String ProductDes = intent.getStringExtra("productdes");
//        String ProductImage = intent.getStringExtra("productimage");

//        Log.e("ProductImage::: ", "ProductImage::::" + ProductImage);


//        if (ProductName != null) {
//
//
//            btn_signup.setVisibility(View.GONE);
//            btn_signupsubmitprodct.setVisibility(View.VISIBLE);
//            etProdcutName.setText("" + ProductName);
//        }
//        if (ProductMeasurement != null) {
//            etMeasurement.setText("" + ProductMeasurement);
//        }
        if (ProductPrice != null) {
//            btn_signup.setVisibility(View.GONE);
//            btn_signupsubmitprodct.setVisibility(View.VISIBLE);
//            etProdcutName.setText("" + ProductName);
            etPrice.setText("" + ProductPrice);
        }
//        if (ProductQty != null) {
//            etQuantity.setText("" + ProductQty);
//        }
        if (ProductDes != null) {
//            btn_signup.setVisibility(View.GONE);
//            btn_signupsubmitprodct.setVisibility(View.VISIBLE);
            etDescription.setText("" + ProductDes);
        }
//        if (ProductImage != null) {
//
//        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.id_back:
                Intent intet = new Intent(com.alpha.paanilo.seller.SellerProductUploadActivity.this, SellerHomeActivity.class);
                startActivity(intet);
                finish();
                break;

            case R.id.id_notification:
                Intent noti = new Intent(com.alpha.paanilo.seller.SellerProductUploadActivity.this, SellerNotificationActivity.class);
                startActivity(noti);
                finish();
                break;

            case R.id.btn_signup:

                validation(v);

                break;
            case R.id.btn_signupsubmitprodct:

                validationEdit(v);


                break;
            case R.id.rlayoutid:
                newAttachmentList.clear();
                openFolder();
                break;

        }

    }

    public void openFolder() {
        Uri uri = Uri.parse(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath());
        Intent intent = new Intent();
        intent.setType("*/*");
        intent.putExtra(Intent.EXTRA_STREAM, uri);
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_FROM_GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
      /*  if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                resultCrop(resultUri);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Toast.makeText(this, "Cropping failed: " + error, Toast.LENGTH_LONG).show();
            }
        }*/
        if (requestCode == PICK_FROM_GALLERY && resultCode == Activity.RESULT_OK) {
            if (data.getClipData() != null) {
//                int count = data.getClipData().getItemCount(); //evaluate the count before the for loop --- otherwise, the count is evaluated every loop.
                int count = 1;

                for (int i = 0; i < count; i++) {
//                    newAttachmentList.clear();
                    returnUri = data.getClipData().getItemAt(i).getUri();

                    com.alpha.paanilo.FileUtils fileUtils = new FileUtils(getApplicationContext());

                    arrayListimageuri.add(fileUtils.getPath(returnUri));

                    Log.e("TAG", "onActivityResult: uri " + fileUtils.getPath(returnUri));

                    Cursor returnCursor = getContentResolver().query(returnUri, null, null, null, null);
                    int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                    int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
                    returnCursor.moveToFirst();
                    System.out.println("PIYUSH NAME IS::::" + returnCursor.getString(nameIndex));
                    System.out.println("PIYUSH SIZE IS::::" + returnCursor.getLong(sizeIndex));
                    ModelProductUpload attachmentListData = new ModelProductUpload();
                    attachmentListData.setImageName(returnCursor.getString(nameIndex));
                    attachmentListData.setImageID(returnUri.toString());
                    newAttachmentList.add(attachmentListData);
//                    arrayListimageuri.notify();
                }

            } else if (data.getData() != null) {
//                newAttachmentList.clear();
                returnUri = data.getData();

                FileUtils fileUtils = new FileUtils(getApplicationContext());

                arrayListimageuri.add(fileUtils.getPath(returnUri));

                Cursor returnCursor =
                        getContentResolver().query(returnUri, null, null, null, null);
                int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
                returnCursor.moveToFirst();
                System.out.println("PIYUSH NAME IS::" + returnCursor.getString(nameIndex));
                System.out.println("PIYUSH SIZE IS::" + returnCursor.getLong(sizeIndex));
                ModelProductUpload attachmentListData = new ModelProductUpload();
                attachmentListData.setImageName(returnCursor.getString(nameIndex));
                attachmentListData.setImageID(returnUri.toString());
                newAttachmentList.add(attachmentListData);

            }
            generateNewAttachmentList(newAttachmentList);

        }
    }

    private void generateNewAttachmentList(ArrayList<ModelProductUpload> newAttachmentList) {
        recyclerUploadProduct.setHasFixedSize(true);
        LinearLayoutManager MyLayoutManager = new LinearLayoutManager(SellerProductUploadActivity.this);
        MyLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerUploadProduct.setLayoutManager(MyLayoutManager);
        attachmentListAdapter = new AdapterProductUpload(newAttachmentList, SellerProductUploadActivity.this);
        recyclerUploadProduct.setAdapter(attachmentListAdapter);
//        recyclerUploadProduct.notify();
    }


    private void validation(View v) {
        if (etPrice.getText().toString().isEmpty()) {
            new CustomToast().Show_Toast(SellerProductUploadActivity.this, v, "Price Can't Empty");
            etPrice.getBackground().mutate().setColorFilter(ContextCompat.getColor(SellerProductUploadActivity.this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        } else if (etDescription.getText().toString().isEmpty() || etDescription.getText().equals("null") || etDescription.getText().equals("")) {
            new CustomToast().Show_Toast(SellerProductUploadActivity.this, v, "Product Description Can't Empty");
            etDescription.getBackground().mutate().setColorFilter(ContextCompat.getColor(SellerProductUploadActivity.this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        } else if (CheckNetwork.isNetAvailable(SellerProductUploadActivity.this)) {

//            Toast.makeText(this, "hetfddfdfd", Toast.LENGTH_SHORT).show();
            getaddvendorProductnew();

        } else {
            new CustomToast().Show_Toast(SellerProductUploadActivity.this, v, "Check Network Connection");
        }
    }

    private void validationEdit(View v) {
       /* if (etProdcutName.getText().toString().isEmpty() || etProdcutName.getText().toString().equalsIgnoreCase("0")) {
            new CustomToast().Show_Toast(SellerProductUploadActivity.this, v, "Name Can't Empty");
            etProdcutName.getBackground().mutate().setColorFilter(ContextCompat.getColor(SellerProductUploadActivity.this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        } else if (etMeasurement.getText().toString().isEmpty() || etMeasurement.getText().toString().equalsIgnoreCase("0")) {
            new CustomToast().Show_Toast(SellerProductUploadActivity.this, v, "Measurement Can't Empty");
            etMeasurement.getBackground().mutate().setColorFilter(ContextCompat.getColor(SellerProductUploadActivity.this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        } else*/

            if (etPrice.getText().toString().isEmpty() || etPrice.getText().toString().equalsIgnoreCase("0")) {
            new CustomToast().Show_Toast(SellerProductUploadActivity.this, v, "Price Can't Empty");
            etPrice.getBackground().mutate().setColorFilter(ContextCompat.getColor(SellerProductUploadActivity.this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        } /*else if (etQuantity.getText().toString().isEmpty() || etQuantity.getText().toString().equalsIgnoreCase("0")) {
            new CustomToast().Show_Toast(SellerProductUploadActivity.this, v, "Quantity Can't Empty");
            etQuantity.getBackground().mutate().setColorFilter(ContextCompat.getColor(SellerProductUploadActivity.this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        } */else if (etDescription.getText().toString().isEmpty() || etDescription.getText().toString().equalsIgnoreCase("0")) {
            new CustomToast().Show_Toast(SellerProductUploadActivity.this, v, "Product Description Can't Empty");
            etDescription.getBackground().mutate().setColorFilter(ContextCompat.getColor(SellerProductUploadActivity.this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        } else if (CheckNetwork.isNetAvailable(SellerProductUploadActivity.this)) {

//            getaddvendorProduct(tv_product_name.getText().toString().trim(),
//                    tv_measurement_product.getText().toString().trim(),
//                    tv_measurement_product.getText().toString().trim(),
//                    tv_price_product.getText().toString().trim(),
//                    tv_quantity_product.getText().toString().trim(),
//                    etAddress.getText().toString().trim());
//            SellerProductUploadActivity();

            getaddvendorProductEdit();
//                getProductactive(constants.getProductAddId(),constants.getUserID(),"1");

//            getVendor();
        } else {
            new CustomToast().Show_Toast(SellerProductUploadActivity.this, v, "Check Network Connection");
        }
    }

    private void getaddvendorProductnew() {
        pd.setMessage("Loading");
        pd.show();
        MultipartBody.Part bnnerimgFileStation = null;
//        String tv_prodcutname = etProdcutName.getText().toString().trim();
//        String tv_measurementproduct = etMeasurement.getText().toString().trim();
        String tv_price = etPrice.getText().toString().trim();
//        String quantity_product = etQuantity.getText().toString().trim();
        String tv_Descc = etDescription.getText().toString().trim();
        String vendoid = constants.getUserID();

//        RequestBody mProductname = RequestBody.create(MediaType.parse("text/plain"), tv_prodcutname);
//        RequestBody mProductMeas = RequestBody.create(MediaType.parse("text/plain"), tv_measurementproduct);
        RequestBody mProductPrice = RequestBody.create(MediaType.parse("text/plain"), tv_price);
//        RequestBody mProductQuantity = RequestBody.create(MediaType.parse("text/plain"), quantity_product);
        RequestBody mProuctDes = RequestBody.create(MediaType.parse("text/plain"), tv_Descc);
        RequestBody vendorid = RequestBody.create(MediaType.parse("text/plain"), vendoid);


//        Log.e(TAG, arrayListimageuri.size() + "   getaddvendorProductnew: " + newAttachmentList.size());
//        MultipartBody.Part[] array = new MultipartBody.Part[newAttachmentList.size()];
//        if (returnUri == null) {
//        } else {
//            array = new MultipartBody.Part[newAttachmentList.size()];
//            for (int index = 0; index < newAttachmentList.size(); index++) {
//
//                Log.e("TAG", newAttachmentList.get(index).getImageID() + "    signUpSellernew: image name " + newAttachmentList.get(index).getImageName());
//                File file = new File(arrayListimageuri.get(index));
//                RequestBody requestFileOne = RequestBody.create(MediaType.parse("image/*"), file);
////                array[index] = MultipartBody.Part.createFormData("user_document[" + index + "]", file.getName(), requestFileOne);
//                array[index] = MultipartBody.Part.createFormData("thumbnails_image[]", file.getName(), requestFileOne);
//                Log.e("raj ", "rahul:::::: " + array[index]);
//            }
//
//            //Log.e(TAG, "getaddvendorProductnew: arrrrrr "+array.length );
//        }

        apiInterface.getaddvendorProductnew( mProductPrice,  mProuctDes,  vendorid).enqueue(new Callback<VendorAddProdcutModel>() {
            @Override
            public void onResponse(Call<VendorAddProdcutModel> call, Response<VendorAddProdcutModel> response) {
                pd.dismiss();
                Log.e("TAG", "onResponse Add Product response  " + new Gson().toJson(response.body()));
                VendorAddProdcutModel vendorAddProdcutModel = response.body();
                if (response.isSuccessful()) {
                    try {
                        pd.dismiss();
//                  if(vendorAddProdcutModel.getData());
                        vendorAddProdcutModel.getData();


                       btn_signup.setVisibility(View.GONE);
                       btn_signupsubmitprodct.setVisibility(View.VISIBLE);

//                        constants.setProductName(vendorAddProdcutModel.getData().getProductName());
//                        constants.setProductMeasurement(vendorAddProdcutModel.getData().getUnit());
//                        constants.setProductPrice(vendorAddProdcutModel.getData().getPrice());
//                        constants.setProductQuantity(vendorAddProdcutModel.getData().getQuantity());
//                        constants.setProductDest(vendorAddProdcutModel.getData().getProductDescription());
//                        constants.setProductImage(vendorAddProdcutModel.getData().getProductImage());
//                        constants.setProductAddId(vendorAddProdcutModel.getData().getProductId());


//                        Log.e("TAG", "Add prodcut response : " + new Gson().toJson(response.body()));
//                        Log.e("data", "data Name " + vendorAddProdcutModel.getData().getProductName());
//
//                        Log.e("data", "data Mesas " + vendorAddProdcutModel.getData().getUnit());
//
//                        Log.e("data", "data Price " + vendorAddProdcutModel.getData().getPrice());
//                        Log.e("data", "data Qauntity " + vendorAddProdcutModel.getData().getQuantity());
//
//                        Log.e("data", "data Des " + vendorAddProdcutModel.getData().getProductDescription());
//                        Log.e("data", "data Image " + vendorAddProdcutModel.getData().getProductImage());
//                        Log.e("data", "data Image " + vendorAddProdcutModel.getData().getProductId());


                        Intent intent = new Intent(SellerProductUploadActivity.this, SellerHomeActivity.class);
                        startActivity(intent);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    System.out.println("error==========");
                }

//                LoginSellerPojo profileUpdateModelRes = response.body();
            }

            @Override
            public void onFailure(Call<VendorAddProdcutModel> call, Throwable t) {
//                pbProfile.setVisibility(View.GONE);
                pd.dismiss();
                Log.e("Message", "Message " + t.getMessage());
                Log.e("Message", "Rahul " + t.getMessage());
                Toast.makeText(SellerProductUploadActivity.this, "Message " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void getaddvendorProductEdit() {
        pd.setMessage("Loading");
        pd.show();
        MultipartBody.Part bnnerimgFileStation = null;
//if(ProdcutIdEdit.equalsIgnoreCase("null")){
////    Productid = ProdcutIdEdit;
//}else {
//    Productid =  ProdcutIdEdit;
//}

         Productid = ProdcutIdEdit;
        String tv_prodcutname = etProdcutName.getText().toString().trim();
        String tv_measurementproduct = etMeasurement.getText().toString().trim();
        String tv_price = etPrice.getText().toString().trim();
        String quantity_product = etQuantity.getText().toString().trim();
        String tv_Descc = etDescription.getText().toString().trim();
        String vendoid = constants.getUserID();

        Log.e("TAG", "constants.getProductAddId()11 : " + ProdcutIdEdit);
//        Log.e("TAG", "Productidim11 : " + Productidim.toString());
        Log.e("TAG", "mProductname11 : " + etProdcutName.getText().toString().trim());
        Log.e("TAG", "mProductMeas11 : " + etMeasurement.getText().toString().trim());
        Log.e("TAG", "mProductPrice11 : " +etPrice.getText().toString().trim());
        Log.e("TAG", "mProductQuantity11 : " + etQuantity.getText().toString().trim());
        Log.e("TAG", "mProuctDes11 : " + etDescription.getText().toString().trim());
        Log.e("TAG", "vendorid11 : " + constants.getUserID());



        MultipartBody.Part imgFileStation = null;

        if (imagePath == null) {
        } else {
            fileForImage = new File(imagePath);
//            File fileForImage = file;
            RequestBody requestFileOne = RequestBody.create(MediaType.parse("multipart/form-data"), fileForImage);
            imgFileStation = MultipartBody.Part.createFormData("product_image", fileForImage.getName(), requestFileOne);
        }

        RequestBody Productidim = RequestBody.create(MediaType.parse("text/plain"), Productid);

        RequestBody mProductname = RequestBody.create(MediaType.parse("text/plain"), tv_prodcutname);
        RequestBody mProductMeas = RequestBody.create(MediaType.parse("text/plain"), tv_measurementproduct);
        RequestBody mProductPrice = RequestBody.create(MediaType.parse("text/plain"), tv_price);
        RequestBody mProductQuantity = RequestBody.create(MediaType.parse("text/plain"), quantity_product);
        RequestBody mProuctDes = RequestBody.create(MediaType.parse("text/plain"), tv_Descc);
        RequestBody vendorid = RequestBody.create(MediaType.parse("text/plain"), vendoid);


        Log.e(TAG, arrayListimageuri.size() + "   getaddvendorProductnew: " + newAttachmentList.size());
        MultipartBody.Part[] array = new MultipartBody.Part[newAttachmentList.size()];
        if (returnUri == null) {
        } else {
            array = new MultipartBody.Part[newAttachmentList.size()];
            for (int index = 0; index < newAttachmentList.size(); index++) {

                Log.e("TAG", newAttachmentList.get(index).getImageID() + "    signUpSellernew: image name " + newAttachmentList.get(index).getImageName());
                File file = new File(arrayListimageuri.get(index));
                RequestBody requestFileOne = RequestBody.create(MediaType.parse("image/*"), file);
//                array[index] = MultipartBody.Part.createFormData("user_document[" + index + "]", file.getName(), requestFileOne);
                array[index] = MultipartBody.Part.createFormData("thumbnails_image[]", file.getName(), requestFileOne);
                Log.e("raj ", "rahul:::::: " + array[index]);
            }

            //Log.e(TAG, "getaddvendorProductnew: arrrrrr "+array.length );
//            System.out.println("","");
        }
//        Log.e("TAG", "constants.getProductAddId() : " + constants.getProductAddId());

        Log.e("TAG", "constants.getProductAddId() : " + ProdcutIdEdit);
        Log.e("TAG", "Productidim : " + Productidim.toString());
        Log.e("TAG", "mProductname : " + mProductname.toString());
        Log.e("TAG", "mProductMeas : " + mProductMeas.toString());
        Log.e("TAG", "mProductPrice : " + mProductPrice.toString());
        Log.e("TAG", "mProductQuantity : " + mProductQuantity.toString());
        Log.e("TAG", "mProuctDes : " + mProuctDes.toString());
        Log.e("TAG", "vendorid : " + vendorid.toString());
        Log.e("TAG", "imgFileStation : " + imgFileStation);
        Log.e("TAG", "array : " + array);




        apiInterface.getaddvendorProductEdit(Productidim,  mProductPrice,  mProuctDes, vendorid).enqueue(new Callback<VendorAddProdcutModel>() {
            @Override
            public void onResponse(Call<VendorAddProdcutModel> call, Response<VendorAddProdcutModel> response) {
                pd.dismiss();
                Log.e("TAG", "onResponse Edit Product response  " + new Gson().toJson(response.body()));
                VendorAddProdcutModel vendorAddProdcutModel = response.body();
                if (response.isSuccessful()) {
                    try {
                        pd.dismiss();
                        Log.e("TAG", "Add prodcut response : " + new Gson().toJson(response.body()));
//                        LoginSellerPojo loginSellerPojo = response.body();
                        Toast.makeText(SellerProductUploadActivity.this, "" + vendorAddProdcutModel.getMessage(), Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(SellerProductUploadActivity.this, SellerHomeActivity.class);
                        startActivity(intent);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    System.out.println("error==========");
                }

//                LoginSellerPojo profileUpdateModelRes = response.body();
            }

            @Override
            public void onFailure(Call<VendorAddProdcutModel> call, Throwable t) {
//                pbProfile.setVisibility(View.GONE);
                pd.dismiss();
                Log.e("Message", "Message " + t.getMessage());
                Log.e("Message", "Rahul " + t.getMessage());
                Toast.makeText(SellerProductUploadActivity.this, "Message " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void getMyProduct(String userId) {
        CustomProgressbar.showProgressBar(this, false);
        ApiClient.getClient().getMyProduct(userId).enqueue(new Callback<SellerMyProdcutModel>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<SellerMyProdcutModel> call, Response<SellerMyProdcutModel> response) {

                Log.e("TAG", "my Product onResponse: " + new Gson().toJson(response.body()));
                CustomProgressbar.hideProgressBar();
                if(response.body()==null ){
                    Log.e("pro", "verndorReportPojogggg");
                    btn_signup.setVisibility(View.VISIBLE);
                    btn_signupsubmitprodct.setVisibility(View.GONE);
                }else {
                    btn_signup.setVisibility(View.GONE);
//                    btn_signupsubmitprodct.setVisibility(View.GONE);

                }
                SellerMyProdcutModel        verndorReportPojo = response.body();
                if (response.isSuccessful()) {
                    CustomProgressbar.hideProgressBar();

                    if (verndorReportPojo.getStatus() == true) {

                        if(verndorReportPojo.getData().getProductName()==null){
                            btn_signup.setVisibility(View.VISIBLE);
                            btn_signupsubmitprodct.setVisibility(View.GONE);
                       Log.e("rahulraj " , "rahulrajbana  ");
                        }else{
                            btn_signup.setVisibility(View.GONE);
                            btn_signupsubmitprodct.setVisibility(View.VISIBLE);
                        }


            } else {
                    }


                } else {
                    if (response.code() == 400) {
                        if (!false) {
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(response.errorBody().string());
                                CustomProgressbar.hideProgressBar();
                                String message = jsonObject.getString("message");
                                // Toast.makeText(SignupActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }

            }

            @Override
            public void onFailure(Call<SellerMyProdcutModel> call, Throwable t) {
                CustomProgressbar.hideProgressBar();
            }
        });

    }




}