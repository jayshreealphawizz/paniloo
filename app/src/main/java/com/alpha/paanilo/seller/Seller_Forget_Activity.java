package com.alpha.paanilo.seller;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import com.alpha.paanilo.R;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.utility.CheckNetwork;
import com.alpha.paanilo.utility.Constants;
import com.alpha.paanilo.utility.CustomToast;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Seller_Forget_Activity extends AppCompatActivity implements View.OnClickListener {

    private static Animation shakeAnimation;
    ProgressDialog pd;
    private CardView mbtnSendEmail;
    private AppCompatImageView ivBackForgetId;
    private TextInputEditText tv_email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setContentView(R.layout.activity_seller_forget);

        tv_email = findViewById(R.id.tv_email);
        mbtnSendEmail = findViewById(R.id.btn_send);
        ivBackForgetId = findViewById(R.id.ivBackForgetId);
        pd = new ProgressDialog(com.alpha.paanilo.seller.Seller_Forget_Activity.this, R.style.AppCompatAlertDialogStyle);

        mbtnSendEmail.setOnClickListener(this);
        ivBackForgetId.setOnClickListener(this);
        shakeAnimation = AnimationUtils.loadAnimation(this, R.anim.shake);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivBackForgetId:
                CheckNetwork.backScreenWithFinis(com.alpha.paanilo.seller.Seller_Forget_Activity.this);
                finish();
                break;
            case R.id.btn_send:
                validation(v);
//                Intent intent = new Intent(com.alpha.paanilo.seller.Seller_Forget_Activity.this, com.alpha.paanilo.seller.SellerLoginActivity.class);
//                startActivity(intent);
//                finish();

                break;
        }
    }

    private void validation(View v) {
        if (tv_email.getText().toString().isEmpty()) {
            //new CustomToast().Show_Toast(this, v, getString(R.string.email_error));
            tv_email.startAnimation(shakeAnimation);
            tv_email.getBackground().mutate().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            tv_email.requestFocus();
            tv_email.setError(getString(R.string.email_error));

        } else if (!Constants.isValidEmail(tv_email.getText().toString())) {
            //new CustomToast().Show_Toast(this, v, getString(R.string.invalid_email_error));
            tv_email.startAnimation(shakeAnimation);
            tv_email.getBackground().mutate().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            tv_email.requestFocus();
            tv_email.setError(getString(R.string.invalid_email_error));

        } else if (CheckNetwork.isNetAvailable(this)) {
            forgetPassword(tv_email.getText().toString().trim(),"2");
        } else {
            new CustomToast().Show_Toast(this, v, getString(R.string.plz_check_your_intrenet_text));
        }
    }

    public void forgetPassword(final String email,String type) {
        if (CheckNetwork.isNetAvailable(getApplicationContext())) {
            pd.setMessage("Loading");
            pd.show();
            JsonObject jsonObject = new JsonObject();
            try {
                jsonObject.addProperty("user_email", email);
                jsonObject.addProperty("type", type);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.e("TAG", "FORGET PASS request : " + new Gson().toJson(jsonObject));
            (ApiClient.getClient().getForgetPassword(jsonObject)).enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    pd.dismiss();
                    if (response.isSuccessful()) {
                        try {
                            Log.e("TAG", "FORGET PASS response : " + new Gson().toJson(response.body()));
                            String repmsg = String.valueOf(response.body().get("message"));
                            Boolean status = response.body().get("status").getAsBoolean();
                            String withour = repmsg.replace("\"", "");

                            if (status) {
                                Toast.makeText(com.alpha.paanilo.seller.Seller_Forget_Activity.this, withour, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(com.alpha.paanilo.seller.Seller_Forget_Activity.this, withour, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    pd.dismiss();
                    Toast.makeText(com.alpha.paanilo.seller.Seller_Forget_Activity.this, getString(R.string.server_errors), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(com.alpha.paanilo.seller.Seller_Forget_Activity.this, getString(R.string.plz_check_your_intrenet_text), Toast.LENGTH_SHORT).show();
        }
    }
}
