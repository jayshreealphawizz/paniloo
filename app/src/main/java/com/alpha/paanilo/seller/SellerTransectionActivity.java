package com.alpha.paanilo.seller;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alpha.paanilo.R;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.seller.adapter.AdapterSellerTransection;
import com.alpha.paanilo.seller.model.ModelSellerTransection;
import com.alpha.paanilo.utility.CheckNetwork;
import com.alpha.paanilo.utility.Constants;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SellerTransectionActivity extends AppCompatActivity implements View.OnClickListener {

    List<DatumTranHis> arraylistpicture = new ArrayList<>();
    AdapterSellerTransection adapterpicture;
    AppCompatImageView iv_back, iv_notification;
    View id_toolbar;
    AppCompatTextView tv_title;
    Constants constants;
    private RecyclerView recyclerSellerTransaction;
    private Object ModelSellerTransection;
    private ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller_transection);


        init();

        id_toolbar = findViewById(R.id.id_toolbar);
        iv_back = id_toolbar.findViewById(R.id.id_back);
        iv_notification = id_toolbar.findViewById(R.id.id_notification);
        tv_title = id_toolbar.findViewById(R.id.tv_tittle);

        iv_back.setOnClickListener(this);
        tv_title.setText("Transaction History");
        iv_notification.setOnClickListener(this);
    }

    private void init() {

        constants = new Constants(getApplicationContext());
        pd = new ProgressDialog(SellerTransectionActivity.this, R.style.AppCompatAlertDialogStyle);

        recyclerSellerTransaction = findViewById(R.id.recyclerSellerTransaction);
//        LinearLayoutManager layoutMa = new LinearLayoutManager(com.alpha.paanilo.seller.SellerTransectionActivity.this, RecyclerView.VERTICAL, false);
//        layoutMa.setReverseLayout(true);
//        recyclerSellerTransaction.setLayoutManager(layoutMa);

        Collections.reverse(arraylistpicture);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        recyclerSellerTransaction.setHasFixedSize(true);
        recyclerSellerTransaction.setLayoutManager(layoutManager);



//        adapterpicture = new AdapterSellerTransection(com.alpha.paanilo.seller.SellerTransectionActivity.this, arraylistpicture);
//        recyclerSellerTransaction.setAdapter(adapterpicture);
//        getData();

//        constants.getUserID()

        Log.e("rahul ", "rahuldsdsd :::" + constants.getresponseUserID());

        Log.e("rahul ", "rahuldsdsduserid  :::" + constants.getUserID());
        if (CheckNetwork.isNetAvailable(getApplicationContext())) {
            walletHistory(constants.getUserID());
        } else {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.network), Toast.LENGTH_LONG).show();
        }

    }

   /* private void getData() {
        com.alpha.paanilo.seller.model.ModelSellerTransection modelSellerTransection = new ModelSellerTransection("a");
        arraylistpicture.add(modelSellerTransection);

        modelSellerTransection = new ModelSellerTransection("b");
        arraylistpicture.add(modelSellerTransection);

        modelSellerTransection = new ModelSellerTransection("b");
        arraylistpicture.add(modelSellerTransection);

        modelSellerTransection = new ModelSellerTransection("b");
        arraylistpicture.add(modelSellerTransection);

        modelSellerTransection = new ModelSellerTransection("b");
        arraylistpicture.add(modelSellerTransection);

        modelSellerTransection = new ModelSellerTransection("b");
        arraylistpicture.add(modelSellerTransection);

        adapterpicture.notifyDataSetChanged();
    }*/

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.id_back:
                Intent intet = new Intent(com.alpha.paanilo.seller.SellerTransectionActivity.this, SellerHomeActivity.class);
                startActivity(intet);
                finish();
                break;

            case R.id.id_notification:
                Intent noti = new Intent(com.alpha.paanilo.seller.SellerTransectionActivity.this, SellerNotificationActivity.class);
                startActivity(noti);
                finish();
                break;
        }

    }


    public void walletHistory(String userid) {
        pd.setMessage("Loading");
        pd.show();

        JsonObject jsonObject = new JsonObject();
        try {
//            jsonObject.addProperty("latitude", "-6.4901067");
//            jsonObject.addProperty("longitude", "106.8306951");
            jsonObject.addProperty("user_id", userid);
//            jsonObject.addProperty("amount", amount);
//            Log.e("TAG", "stall near by you request all stalllllll: " + jsonObject.toString());

        } catch (Exception e) {
            e.printStackTrace();
        }

        (ApiClient.getClient().walletHistory(jsonObject)).enqueue(new Callback<WithdrawPozo>() {
            @Override
            public void onResponse(Call<WithdrawPozo> call, Response<WithdrawPozo> response) {
                pd.dismiss();
                Log.e("TAG", "response Wall History : " + new Gson().toJson(response.body()));
                WithdrawPozo withdrawPozo = response.body();
                if (response.isSuccessful()) {
                    try {
                        pd.dismiss();
                        Log.e("TAG", "stall near by you response all stalllllll : " + new Gson().toJson(response.body()));


//                        if (withdrawPozo.getStatus().equals("1")) {


                        arraylistpicture = withdrawPozo.getData();
                        for (int i = 0; i < arraylistpicture.size(); i++) {

                        }
                        Log.e("rahul", "rahul::::: " + arraylistpicture.size());
                        adapterpicture = new AdapterSellerTransection(SellerTransectionActivity.this, arraylistpicture);
                        recyclerSellerTransaction.setAdapter(adapterpicture);
                        adapterpicture.notifyDataSetChanged();

//                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                        pd.dismiss();
                    }
                } else {
                    Toast.makeText(SellerTransectionActivity.this, ""+ withdrawPozo.getMessage(), Toast.LENGTH_SHORT).show();

                    pd.dismiss();
                }
            }

            @Override
            public void onFailure(Call<WithdrawPozo> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(SellerTransectionActivity.this, getString(R.string.server_errors), Toast.LENGTH_SHORT).show();
            }
        });
    }
}