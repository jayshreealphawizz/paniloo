package com.alpha.paanilo.seller.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.alpha.paanilo.R;
import com.alpha.paanilo.cart_pkg.showAddressModlePkg.AddressList;
import com.alpha.paanilo.cart_pkg.showAddressModlePkg.deleteAddressPkg.DeleteAddressModle;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.seller.SellerOpenOrderActivity;
import com.alpha.paanilo.seller.SellerProdcutDetailActivity;
import com.alpha.paanilo.seller.model.ModelOpenOrder;
import com.alpha.paanilo.seller.model.openorder.OpenOrderDatum;
import com.alpha.paanilo.utility.Constants;
import com.alpha.paanilo.utility.CustomProgressbar;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AdapterOpenOrder extends RecyclerView.Adapter<AdapterOpenOrder.MyViewHolder> {
    Context context;
    List<OpenOrderDatum> arraylistpicture;
    int counter = 0;
    String selectedItemstatus;
    OpenOrderAcceptDecline openOrderAcceptDecline;
    int spinnerPosition;
    String dateToStr;
    String[] status = {"accept/confirm", "out of deliver", "cancel", "complete"};
    Spinner spinnerstatus;
    ArrayAdapter spinnerArrayAdapter;
    Constants constants;
    TextView t;
    String[] country;
    int sp_position;

    TextView tv_Status;
    String selected, spinner_item;

    public AdapterOpenOrder(Context context, List<OpenOrderDatum> arraylistpicture, OpenOrderAcceptDecline openOrderAcceptDecline) {
        this.context = context;
        this.arraylistpicture = arraylistpicture;
        this.openOrderAcceptDecline = openOrderAcceptDecline;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_my_order_adapter, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        final OpenOrderDatum openOrderDatum = arraylistpicture.get(position);

        holder.tvOrderId.setText("Order Id: "+ openOrderDatum.getOrderId());
        holder.tvTAmount.setText(openOrderDatum.getTotalAmount());
        holder.tvAdress.setText(openOrderDatum.getDeliveryAddress());
        holder.tvDate.setText(openOrderDatum.getDate());
        Log.e("productstatus", "productstatus::openorder:" + openOrderDatum.getStatus());

//        Log.e("Status", "data::  " + openOrderDatum.getStatus());

        if(openOrderDatum.getStatus().equalsIgnoreCase("0")){
            holder.tvStatus.setVisibility(View.INVISIBLE);
//            holder.tvStatus.setText("Delivered");

        }else if(openOrderDatum.getStatus().equalsIgnoreCase("1")){
            holder.tvStatus.setVisibility(View.VISIBLE);
            holder.tvStatus.setText("Pending");
        }else if(openOrderDatum.getStatus().equalsIgnoreCase("2")){
            holder.tvStatus.setVisibility(View.VISIBLE);
            holder.tvStatus.setText("Delivered");
        }else if(openOrderDatum.getStatus().equalsIgnoreCase("3")){
            holder.tvStatus.setVisibility(View.VISIBLE);
            holder.tvStatus.setText("Cancel");
        }else if(openOrderDatum.getStatus().equalsIgnoreCase("4")){
            holder.tvStatus.setVisibility(View.VISIBLE);
            holder.tvStatus.setText("Complete");
        }else{
            holder.tvStatus.setVisibility(View.INVISIBLE);
        }

        holder.carditem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, SellerProdcutDetailActivity.class);
                intent.putExtra("prodcutname", openOrderDatum.getProductName());
                intent.putExtra("userno", openOrderDatum.getUserPhone());
                intent.putExtra("productquantity", openOrderDatum.getQuantity());
                intent.putExtra("producttotamount", openOrderDatum.getTotalAmount());
                intent.putExtra("productimage", openOrderDatum.getProductImage());
                intent.putExtra("productuserid", openOrderDatum.getUserId());
                intent.putExtra("productSaleid", openOrderDatum.getSaleId());
                intent.putExtra("productstatus", openOrderDatum.getStatus());
                intent.putExtra("getDeliveryAddress", openOrderDatum.getDeliveryAddress());
                intent.putExtra("productdate", openOrderDatum.getDeliveryTime());
                intent.putExtra("userfullname", openOrderDatum.getUserFullname());
                intent.putExtra("OrderId" ,  openOrderDatum.getOrderId());
                intent.putExtra("paymentmethod" ,  openOrderDatum.getPaymentMethod());
                intent.putExtra("distance" ,  openOrderDatum.getDistance());
                intent.putExtra("orderdeliverttime" ,  openOrderDatum.getOrder_delivery_time());
                context.startActivity(intent);
            }
        });
//        holder.carditem.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(context, SellerProdcutDetailActivity.class);
//                intent.putExtra("prodcutname", openOrderDatum.getProductName());
//                intent.putExtra("productquantity", openOrderDatum.getQuantity());
//                intent.putExtra("producttotamount", openOrderDatum.getTotalAmount());
//                intent.putExtra("productimage", openOrderDatum.getProductImage());
//                intent.putExtra("productuserid", openOrderDatum.getUserId());
//                intent.putExtra("productSaleid", openOrderDatum.getSaleId());
//                intent.putExtra("productstatus", openOrderDatum.getStatus());
////                Log.e("rahul", "rahul Userid " + openOrderDatum.getUserId());
////                Log.e("rahul", "rahul getSaleId " + openOrderDatum.getSaleId());
//
//                context.startActivity(intent);
//
////                Intent intent = new Intent(context, SellerProdcutDetailActivity.class);
////                context.startActivity(intent);
////                maplistitemClickListener.maplistitemClickListener(v, position);
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return arraylistpicture.size();
    }

    private void updateOrderStatus(String user_id, String sale_id, final String status) {
        CustomProgressbar.showProgressBar(context, false);
        ApiClient.getClient().updateOrderStatus(user_id, sale_id, status).enqueue(new Callback<DeleteAddressModle>() {
            @Override
            public void onResponse(Call<DeleteAddressModle> call, Response<DeleteAddressModle> response) {

                Log.e("TAG", "onResponse: Update order " + new Gson().toJson(response.body()));
                CustomProgressbar.hideProgressBar();

                if (response.isSuccessful()) {

                    DeleteAddressModle addAddressResponseModle = response.body();
                    if (addAddressResponseModle.getStatus()) {
//                        accept/confirm=1,out of deliver=2,cancel=3,complete=4
                        if (status.equalsIgnoreCase("1")) {
//                            Toast.makeText(context, "Order Accepted Successfully", Toast.LENGTH_SHORT).show();
                        } else if (status.equalsIgnoreCase("4")) {
//                            Toast.makeText(context, "Order Complete Successfully", Toast.LENGTH_SHORT).show();
                        } else if (status.equalsIgnoreCase("2")) {
                            //Toast.makeText(context, "Order Complete Successfully", Toast.LENGTH_SHORT).show();
                        }
//                        Toast.makeText(getContext(), addAddressResponseModle.getMessage(), Toast.LENGTH_LONG).show();
                        // getOPenOrders(constants.getUserID());
                    } else {
                        Toast.makeText(context, addAddressResponseModle.getMessage(), Toast.LENGTH_LONG).show();
                    }


                } else {
                    if (response.code() == 400) {
                        if (!false) {
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(response.errorBody().string());
                                CustomProgressbar.hideProgressBar();
                                String message = jsonObject.getString("message");
                                // Toast.makeText(SignupActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }


            }

            @Override
            public void onFailure(Call<DeleteAddressModle> call, Throwable t) {
                CustomProgressbar.hideProgressBar();
            }
        });

    }

    public interface OpenOrderAcceptDecline {
        void openOrderAcceptDecline(View view, int position, OpenOrderDatum openOrderDatum);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
//        ImageView img_bottle;
//        CardView carditem;
//        TextView tv_bottlename, tv_date, tv_time, tv_qty, tv_liter, tv_amount, tv_rupee, tv_order, tv_name;
//        AppCompatButton btn_decline, btn_accept;
//
//        public MyViewHolder(@NonNull View itemView) {
//            super(itemView);
//            carditem = itemView.findViewById(R.id.carditem);
//            img_bottle = itemView.findViewById(R.id.img_bottle);
//            tv_Status = itemView.findViewById(R.id.tv_Status);
//            tv_bottlename = itemView.findViewById(R.id.tv_bottlename);
//            tv_date = itemView.findViewById(R.id.tv_date);
//            tv_time = itemView.findViewById(R.id.tv_time);
//            tv_qty = itemView.findViewById(R.id.tv_qty);
//            tv_liter = itemView.findViewById(R.id.tv_liter);
//            tv_amount = itemView.findViewById(R.id.tv_amount);
//            tv_rupee = itemView.findViewById(R.id.tv_rupee);
//            tv_order = itemView.findViewById(R.id.tv_order);
//            tv_name = itemView.findViewById(R.id.tv_name);
//
//            btn_decline = itemView.findViewById(R.id.btn_decline);
//            btn_accept = itemView.findViewById(R.id.btn_accept);
//            spinnerstatus = itemView.findViewById(R.id.spinnerstatus);
//
//            Date today = new Date();
//            SimpleDateFormat format = new SimpleDateFormat("hh:mm:ss a");
//            dateToStr = format.format(today);
//
//            Log.e("rahulraj ", "time " + dateToStr);
//            System.out.println(dateToStr);
//
//
//        }
CardView carditem;
        TextView tvOrderId ,tvTAmount, tvAdress,  tvStatus,   tvDate;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            carditem = itemView.findViewById(R.id.carditem);
            tvStatus  = itemView.findViewById(R.id.tvStatus);
            tvOrderId = itemView.findViewById(R.id.tvOrderId);
            tvTAmount = itemView.findViewById(R.id.tvTAmount);
            tvAdress = itemView.findViewById(R.id.tvAdress);
            tvDate = itemView.findViewById(R.id.tvDate);

        }
    }
}
