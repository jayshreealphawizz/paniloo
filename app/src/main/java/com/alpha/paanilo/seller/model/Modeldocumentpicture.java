package com.alpha.paanilo.seller.model;

public class Modeldocumentpicture {
    String uploadpicture ;

    public Modeldocumentpicture(String uploadpicture) {
        this.uploadpicture = uploadpicture;
    }

    public String getUploadpicture() {
        return uploadpicture;
    }

    public void setUploadpicture(String uploadpicture) {
        this.uploadpicture = uploadpicture;
    }
}