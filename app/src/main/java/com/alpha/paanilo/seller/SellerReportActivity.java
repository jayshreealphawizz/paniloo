package com.alpha.paanilo.seller;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alpha.paanilo.AllOrderModel;
import com.alpha.paanilo.DatumallOrder;
import com.alpha.paanilo.R;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.seller.adapter.AdapterAllOrder;
import com.alpha.paanilo.seller.adapter.AdapterSellerReport;
import com.alpha.paanilo.seller.model.ModelAllOrder;
import com.alpha.paanilo.seller.model.VerndorReportPojo;
import com.alpha.paanilo.utility.CheckNetwork;
import com.alpha.paanilo.utility.Constants;
import com.alpha.paanilo.utility.CustomProgressbar;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SellerReportActivity extends AppCompatActivity implements View.OnClickListener {

    List<DatumallOrder> arraylistpicture = new ArrayList<>();
    AdapterSellerReport adapterpicture;
    SimpleDateFormat dateFormatter;
    AppCompatImageView iv_back, iv_notification, ivCalendar;
    View id_toolbar;
    AppCompatTextView tv_title;
    Constants constants;
    TextView tvTOrder, tvCOrder, tvTEMoney;
    TextView tvOrederLength;
    String currentDateandTime ;
    private AppCompatTextView tv_Calendar;
    private RecyclerView recyclerAllOrder;
    Calendar newDate ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller_report);

        init();

        id_toolbar = findViewById(R.id.id_toolbar);
        iv_back = id_toolbar.findViewById(R.id.id_back);
        iv_notification = id_toolbar.findViewById(R.id.id_notification);
        tv_title = id_toolbar.findViewById(R.id.tv_tittle);

        iv_back.setOnClickListener(this);
        tv_title.setText("Report");
        iv_notification.setOnClickListener(this);

    }

    private void init() {
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        tv_Calendar = findViewById(R.id.tv_Calendar);
        ivCalendar = findViewById(R.id.ivCalendar);
        tvTOrder = findViewById(R.id.tvTOrder);
        tvCOrder = findViewById(R.id.tvCOrder);
        tvTEMoney = findViewById(R.id.tvTEMoney);

        tvOrederLength = findViewById(R.id.tvOrederLength);
        recyclerAllOrder = findViewById(R.id.recyclerAllOrder);
        constants = new Constants(getApplicationContext());

//        Collections.reverse(arraylistpicture);
//        LinearLayoutManager layoutMa = new LinearLayoutManager(com.alpha.paanilo.seller.SellerReportActivity.this, RecyclerView.VERTICAL, false);
//        layoutMa.setReverseLayout(true);
//        recyclerAllOrder.setLayoutManager(layoutMa);


        Collections.reverse(arraylistpicture);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        recyclerAllOrder.setHasFixedSize(true);
        recyclerAllOrder.setLayoutManager(layoutManager);



//        adapterpicture = new AdapterSellerReport(com.alpha.paanilo.seller.SellerReportActivity.this, arraylistpicture);
//        recyclerAllOrder.setAdapter(adapterpicture);
//        getDataPicture();
//        constants.getUserID()

        Log.e("Userid ", "UserId" + constants.getUserID());

        if (CheckNetwork.isNetAvailable(getApplicationContext())) {
            getVenderReport(constants.getUserID());
        } else {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.network), Toast.LENGTH_LONG).show();
        }
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
         currentDateandTime = sdf.format(new Date());
//        textView.setText(currentDateandTime);
        tv_Calendar.setText(currentDateandTime);

        if (CheckNetwork.isNetAvailable(getApplicationContext())) {

            Log.e("currentDateandTime" , "dt" +constants.getUserID());
            Log.e("currentDateandTime" , "dt" + tv_Calendar.getText().toString());


            getAllOrderList(constants.getUserID(),tv_Calendar.getText().toString());
        } else {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.network), Toast.LENGTH_LONG).show();
        }


        ivCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance(Locale.getDefault());
                DatePickerDialog datePickerDialog;
                datePickerDialog = new DatePickerDialog(com.alpha.paanilo.seller.SellerReportActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
                                //todo
                                 newDate = Calendar.getInstance();
                                newDate.set(year, month, dayOfMonth);

                                currentDateandTime = dateFormatter.format(newDate.getTime());
                                tv_Calendar.setText(currentDateandTime);
//                                tv_Calendar.setText(dateFormatter.format(newDate.getTime()));
                                Log.e("currentDateandTime" , "dt" +currentDateandTime);
                                if (CheckNetwork.isNetAvailable(getApplicationContext())) {

                                    Log.e("currentDateandTimeMM" , "dt" +constants.getUserID());
                                    Log.e("MMMMM" , "dt" + tv_Calendar.getText().toString());


                                    getAllOrderList(constants.getUserID(),tv_Calendar.getText().toString());
                                } else {
                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.network), Toast.LENGTH_LONG).show();
                                }
                      Log.e("dtate" , "dt" +dateFormatter.format(newDate.getTime()));
                            }
                        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();
                datePickerDialog.getDatePicker().setMaxDate(setMaxDate());

            }
        });
//        ivCalendar.setOnClickListener(new View.OnClickListener() {
//            @RequiresApi(api = Build.VERSION_CODES.N)
//            @Override
//            public void onClick(View v) {
//                Calendar calendar = Calendar.getInstance(Locale.getDefault());
//                DatePickerDialog datePickerDialog;
//                datePickerDialog = new DatePickerDialog(com.alpha.paanilo.seller.SellerReportActivity.this,
//                        new DatePickerDialog.OnDateSetListener() {
//                            @Override
//                            public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
//                                //todo
//                                 newDate = Calendar.getInstance();
//                                newDate.set(year, month, dayOfMonth);
//                                tv_Calendar.setText(dateFormatter.format(newDate.getTime()));
//                      Log.e("dtate" , "dt" +dateFormatter.format(newDate.getTime()));
//                            }
//                        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
//                datePickerDialog.show();
//                datePickerDialog.getDatePicker().setMaxDate(setMaxDate());
//            }
//        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.id_back:
                Intent intet = new Intent(com.alpha.paanilo.seller.SellerReportActivity.this, SellerHomeActivity.class);
                startActivity(intet);
                finish();
                break;

            case R.id.id_notification:
                Intent noti = new Intent(com.alpha.paanilo.seller.SellerReportActivity.this, SellerNotificationActivity.class);
                startActivity(noti);
                finish();
                break;
        }

    }


    private long setMaxDate() {
        long oldMillis;
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int updateyear = year - 14;
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        formatter.setLenient(false);
        Date curDate = new Date();
        long curMillis = curDate.getTime();
        String curTime = formatter.format(curDate);
        String oldTime = currentdateOfTheYear();
        Date oldDate = null;
        try {
            oldDate = formatter.parse(oldTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        oldMillis = oldDate.getTime();
        return oldMillis;
    }

    private String currentdateOfTheYear() {
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String formattedDate = simpleDateFormat.format(c);
        return formattedDate;
    }


    private void getVenderReport(String userId) {
        CustomProgressbar.showProgressBar(this, false);
        ApiClient.getClient().getVenderReport(userId).enqueue(new Callback<VerndorReportPojo>() {
            @Override
            public void onResponse(Call<VerndorReportPojo> call, Response<VerndorReportPojo> response) {

                Log.e("TAG", "getVenderReport onResponseupper: " + new Gson().toJson(response.body()));
                CustomProgressbar.hideProgressBar();

                VerndorReportPojo verndorReportPojo = response.body();
                if (response.isSuccessful()) {
                    CustomProgressbar.hideProgressBar();

                    if (verndorReportPojo.getStatus() == true) {
//                        Log.e("bana::::", "bana");
                        tvTOrder.setText("" + verndorReportPojo.getData().getTotalOrders());
                        tvCOrder.setText("" + verndorReportPojo.getData().getCancelOrders());
                        tvTEMoney.setText("Rs." + verndorReportPojo.getData().getTotalEarnMoney());
                    } else {
                        Toast.makeText(SellerReportActivity.this, "" + verndorReportPojo.getMessage(), Toast.LENGTH_SHORT).show();

                    }


                } else {
                    if (response.code() == 400) {
                        if (!false) {
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(response.errorBody().string());
                                CustomProgressbar.hideProgressBar();
                                String message = jsonObject.getString("message");
                                // Toast.makeText(SignupActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }

            }

            @Override
            public void onFailure(Call<VerndorReportPojo> call, Throwable t) {
                CustomProgressbar.hideProgressBar();
            }
        });

    }

    private void getAllOrderList(String userId, String date) {
//        contactList.clear();
        CustomProgressbar.showProgressBar(this, false);
        arraylistpicture.clear();

        Log.e("TAG", "onResponse All report order responseuserId : " + userId);

        Log.e("TAG", "onResponse All report order responsedate: " + date);


        ApiClient.getClient().getAllOrderList(userId,date).enqueue(new Callback<AllOrderModel>() {
            @Override
            public void onResponse(Call<AllOrderModel> call, Response<AllOrderModel> response) {
                Log.e("TAG", "onResponse All report order response: " + new Gson().toJson(response.body()));
                CustomProgressbar.hideProgressBar();



                AllOrderModel allOrderModel = response.body();
                if (response.isSuccessful()) {

                    try {

                        CustomProgressbar.hideProgressBar();

                        if(allOrderModel.getStatus()==true) {
                            arraylistpicture = allOrderModel.getData();
                            for (int count = 0; count < arraylistpicture.size(); count++) {

                            }
                            Log.e("rahul", "rahul::::: " + arraylistpicture.size());
                            tvOrederLength.setText("" + arraylistpicture.size() + " order");

                            adapterpicture = new AdapterSellerReport(com.alpha.paanilo.seller.SellerReportActivity.this, arraylistpicture);
                            recyclerAllOrder.setAdapter(adapterpicture);
                            adapterpicture.notifyDataSetChanged();
                        }else if(allOrderModel.getStatus()==false) {
                            tvOrederLength.setText("" + arraylistpicture.size() + " order");

                            adapterpicture = new AdapterSellerReport(com.alpha.paanilo.seller.SellerReportActivity.this, arraylistpicture);
                            recyclerAllOrder.setAdapter(adapterpicture);
                            adapterpicture.notifyDataSetChanged();
                        }
//                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        CustomProgressbar.hideProgressBar();

                    }
                }else if(allOrderModel.getStatus()==false) {
                    adapterpicture = new AdapterSellerReport(com.alpha.paanilo.seller.SellerReportActivity.this, arraylistpicture);
                    recyclerAllOrder.setAdapter(adapterpicture);
                    tvOrederLength.setText("" + arraylistpicture.size() + " order");

                    adapterpicture.notifyDataSetChanged();
                } else {
                    Toast.makeText(SellerReportActivity.this, "Message " + allOrderModel.getMessage(), Toast.LENGTH_SHORT).show();

                    CustomProgressbar.hideProgressBar();
                    if (response.code() == 400) {
                        if (!false) {
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(response.errorBody().string());
                                CustomProgressbar.hideProgressBar();
                                String message = jsonObject.getString("message");
                                // Toast.makeText(SignupActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<AllOrderModel> call, Throwable t) {
                CustomProgressbar.hideProgressBar();
            }
        });

    }


}