package com.alpha.paanilo.seller.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.alpha.paanilo.DatumallOrder;
import com.alpha.paanilo.R;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.seller.SellerCompleteOrderActivity;
import com.alpha.paanilo.seller.SellerOpenOrderActivity;
import com.alpha.paanilo.seller.SellerOrderDetailsActivity;
import com.alpha.paanilo.seller.model.completeorder.CompleteOrderDatum;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;


public class AdapterAllOrdernew extends RecyclerView.Adapter<AdapterAllOrdernew.MyViewHolder> {
    private final CompleteorderClickListener completeorderClickListener;
    Context context;
    List<CompleteOrderDatum> completeOrderDatumArrayList;
    List<CompleteOrderDatum> exampleListFull;

  /*  private final Filter exampleFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<CompleteOrderDatum> filteredList = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(exampleListFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();
                for (CompleteOrderDatum item : exampleListFull) {
                    if (item.getProductName().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
//            completeOrderDatumArrayList.clear();
//            completeOrderDatumArrayList.addAll((List) results.values);
//            notifyDataSetChanged();

            if (results.count > 0) {
                Log.println(Log.INFO, "Results", "FOUND");
                notifyDataSetChanged();
            } else {
                Log.println(Log.INFO, "Results", "-");
//                notifyDataSetInvalidated();
            }
        }
    };
    int counter = 0;*/

    public AdapterAllOrdernew(Context context, List<CompleteOrderDatum> completeOrderDatumArrayList, CompleteorderClickListener completeorderClickListener) {
        this.context = context;
//        this.completeOrderDatumArrayList = completeOrderDatumArrayList;

//        this.completeOrderDatumArrayList = completeOrderDatumArrayList;
//        exampleListFull = new ArrayList<>(completeOrderDatumArrayList);
        this.completeorderClickListener = completeorderClickListener;

        this.completeOrderDatumArrayList = completeOrderDatumArrayList;
        this.exampleListFull = completeOrderDatumArrayList;
        notifyDataSetChanged();

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_all_order_adapternew, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        final CompleteOrderDatum completeOrderDatum = completeOrderDatumArrayList.get(position);

//
////        Toast.makeText(context, "complete order dataum", Toast.LENGTH_SHORT).show();
//        Log.e("Product image ", "product image " + completeOrderDatum.getProductImage());
//
//
//        if (completeOrderDatum.getProductImage().equals("") || completeOrderDatum.getProductImage() == null) {
//        } else {
////            jhjhjhj
//            String imgName = completeOrderDatum.getProductImage().substring((completeOrderDatum.getProductImage().lastIndexOf(",") + 1));
//            Log.e("pro", "imgName" + imgName);
//            Glide.with(context)
//                    .load(ApiClient.CATEGORY_PRODUCT_URL + imgName)
//                    .placeholder(R.drawable.placeholder)
//                    .into(holder.img_bottle);
//        }
//
//        holder.tv_bottlename.setText("" + completeOrderDatum.getProductName());
//        holder.tv_liter.setText("" + completeOrderDatum.getQuantity());
//        holder.tv_date.setText("" + completeOrderDatum.getDate());
//        holder.tv_name.setText("" + completeOrderDatum.getUserFullname());
//        holder.tv_rupee.setText("Rs." + completeOrderDatum.getTotalAmount());
////        holder.tv_liter.setText(completeOrderDatum.getTotalItems() + " Liter");
//
//
//        holder.carditem.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(context, SellerOrderDetailsActivity.class);
//                intent.putExtra("prodcutname", completeOrderDatum.getProductName());
//                intent.putExtra("productquantity", completeOrderDatum.getQuantity());
//                intent.putExtra("producttotamount", completeOrderDatum.getTotalAmount());
//                intent.putExtra("productimage", completeOrderDatum.getProductImage());
//                intent.putExtra("productuserid", completeOrderDatum.getUserId());
//                intent.putExtra("productSaleid", completeOrderDatum.getSaleId());
//                intent.putExtra("productDate", completeOrderDatum.getDate());
//                intent.putExtra("productorderid", completeOrderDatum.getOrderId());
//
//                intent.putExtra("productuserfullname", completeOrderDatum.getUserFullname());
//
//                intent.putExtra("productpaymentmethod", completeOrderDatum.getPaymentMethod());
//                intent.putExtra("productdaddress", completeOrderDatum.getDeliveryAddress());
//
//                intent.putExtra("status", completeOrderDatum.getTotalItems());
//
//
////                Toast.makeText(context, "click event", Toast.LENGTH_SHORT).show();
//
//
//                Log.e("rahul", "rahul Userid " + completeOrderDatum.getUserId());
//                Log.e("rahul", "rahul getSaleId " + completeOrderDatum.getSaleId());
//
//
//                Log.e("Name ", "Name1 " + completeOrderDatum.getProductName());
//                Log.e("Name ", "Name2 " + completeOrderDatum.getQuantity());
//                Log.e("Name ", "Name3 " + completeOrderDatum.getTotalAmount());
//                Log.e("Name ", "Name4 " + completeOrderDatum.getProductImage());
//                Log.e("Name ", "Name5 " + completeOrderDatum.getUserId());
//                Log.e("Name ", "Name6 " + completeOrderDatum.getSaleId());
//                Log.e("Name ", "Name7 " + completeOrderDatum.getDate());
//                Log.e("Name ", "Name8 " + completeOrderDatum.getUserFullname());
//                Log.e("Name ", "Name9 " + completeOrderDatum.getOrderId());
//
//                Log.e("Name ", "Name10 " + completeOrderDatum.getPaymentMethod());
//
//                Log.e("Name ", "Name11 " + completeOrderDatum.getDeliveryAddress());
//
//                context.startActivity(intent);
////                openOrderAcceptDecline.openOrderAcceptDeclineOnClick(v, position, openOrderDatum);
//            }
//        });

//        constant = new Constants(context);
        holder.tvOrderId.setText("Order Id: "+ completeOrderDatum.getOrderId());
        holder.tvTAmount.setText(completeOrderDatum.getTotalAmount());
        holder.tvAdress.setText(completeOrderDatum.getDeliveryAddress());
        holder.tvDate.setText(completeOrderDatum.getDate());

        Log.e("productstatus", "productstatus complete :::" + completeOrderDatum);

//        Log.e("Status", "data::  " + completeOrderDatum.getStatus());

        if(completeOrderDatum.getStatus().equalsIgnoreCase("0")){
            holder.tvStatus.setVisibility(View.INVISIBLE);
//            holder.tvStatus.setText("Delivered");

        }else if(completeOrderDatum.getStatus().equalsIgnoreCase("1")){
            holder.tvStatus.setVisibility(View.VISIBLE);
            holder.tvStatus.setText("Pending");
        }else if(completeOrderDatum.getStatus().equalsIgnoreCase("2")){
            holder.tvStatus.setVisibility(View.VISIBLE);
            holder.tvStatus.setText("Delivered");
        }else if(completeOrderDatum.getStatus().equalsIgnoreCase("3")){
            holder.tvStatus.setVisibility(View.VISIBLE);
            holder.tvStatus.setText("Cancel");
        }else if(completeOrderDatum.getStatus().equalsIgnoreCase("4")){
            holder.tvStatus.setVisibility(View.VISIBLE);
            holder.tvStatus.setText("Complete");
        }else{
            holder.tvStatus.setVisibility(View.INVISIBLE);
        }

       /* Log.e("pro", "imgNameeimage " + openOrderDatum.getProductImage());
        if (openOrderDatum.getProductImage() == null) {
            Log.e("empty image", "empty");
        } else {
            String imgName = openOrderDatum.getProductImage().substring((openOrderDatum.getProductImage().lastIndexOf(",") + 1));
            Log.e("pro", "imgName" + imgName);

            Glide.with(context)
                    .load(ApiClient.CATEGORY_PRODUCT_URL + imgName)
                    .placeholder(R.drawable.placeholder)
                    .into(holder.img_bottle);
        }
//        Toast.makeText(context, "now get open data", Toast.LENGTH_SHORT).show();

        holder.tv_bottlename.setText("" + openOrderDatum.getProductName());
        holder.tv_qty.setText("Qty. " + openOrderDatum.getQuantity());
        holder.tv_amount.setText("Rs." + openOrderDatum.getTotalAmount());
        holder.tv_date.setText("" + openOrderDatum.getDate());
        holder.tv_name.setText("" + openOrderDatum.getUserFullname());


        Log.e("pro", "openOrderDatum.getDate()" + openOrderDatum.getDate());
        Log.e("pro", "openOrderDatum.dfdfdf ffsgdfgdfgdf()" + openOrderDatum.getDeliveryTime());
        Log.e("pro", "openOrderDatum.dfdfdf()" + openOrderDatum.getOrderDeliveryTime());
        Log.e("pro", "openOrderDatum.fffff()" + openOrderDatum.getCenterPickupTime());


        if(openOrderDatum.getStatus().equalsIgnoreCase("0")){
            holder.tv_Status.setText("");
        }else if(openOrderDatum.getStatus().equalsIgnoreCase("1")){
            holder.tv_Status.setText("Accept");
        }else if(openOrderDatum.getStatus().equalsIgnoreCase("2")){
            holder.tv_Status.setText("Delivered");
        }else if(openOrderDatum.getStatus().equalsIgnoreCase("3")){
            holder.tv_Status.setText("Cancle");
        }else if(openOrderDatum.getStatus().equalsIgnoreCase("4")){
            holder.tv_Status.setText("Complete");
        }

        holder.carditem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, SellerProdcutDetailActivity.class);
                intent.putExtra("prodcutname", openOrderDatum.getProductName());
                intent.putExtra("productquantity", openOrderDatum.getQuantity());
                intent.putExtra("producttotamount", openOrderDatum.getTotalAmount());
                intent.putExtra("productimage", openOrderDatum.getProductImage());
                intent.putExtra("productuserid", openOrderDatum.getUserId());
                intent.putExtra("productSaleid", openOrderDatum.getSaleId());
                intent.putExtra("productstatus", openOrderDatum.getStatus());
//                Log.e("rahul", "rahul Userid " + openOrderDatum.getUserId());
//                Log.e("rahul", "rahul getSaleId " + openOrderDatum.getSaleId());

                context.startActivity(intent);

//                Intent intent = new Intent(context, SellerProdcutDetailActivity.class);
//                context.startActivity(intent);
//                maplistitemClickListener.maplistitemClickListener(v, position);
            }
        });*/
        holder.carditem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, SellerOrderDetailsActivity.class);
                intent.putExtra("prodcutname", completeOrderDatum.getProductName());
                intent.putExtra("userphone", completeOrderDatum.getUserPhone());
                intent.putExtra("productquantity", completeOrderDatum.getQuantity());
                intent.putExtra("producttotamount", completeOrderDatum.getTotalAmount());
                intent.putExtra("productimage", completeOrderDatum.getProductImage());
                intent.putExtra("productuserid", completeOrderDatum.getUserId());
                intent.putExtra("productSaleid", completeOrderDatum.getSaleId());
                intent.putExtra("productstatus", completeOrderDatum.getStatus());
                intent.putExtra("getDeliveryAddress", completeOrderDatum.getDeliveryAddress());
                intent.putExtra("productdate", completeOrderDatum.getDeliveryTime());
                intent.putExtra("userfullname", completeOrderDatum.getUserFullname());
                intent.putExtra("OrderId" ,  completeOrderDatum.getOrderId());
                intent.putExtra("paymentmethod" ,  completeOrderDatum.getPaymentMethod());

                //                Log.e("rahul", "rahul Userid " + openOrderDatum.getUserId());
//                Log.e("rahul", "rahul getSaleId " + openOrderDatum.getSaleId());

                context.startActivity(intent);

//                Intent intent = new Intent(context, SellerProdcutDetailActivity.class);
//                context.startActivity(intent);
//                maplistitemClickListener.maplistitemClickListener(v, position);
            }
        });
    }

//    @Override
//    public int getItemCount() {
//        return completeOrderDatumArrayList.size();
//    }

//    @Override
//    public Filter getFilter() {
//        return exampleFilter;
//    }

    @Override
    public int getItemCount() {
        return completeOrderDatumArrayList == null ? 0 : completeOrderDatumArrayList.size();
//        if(mFilteredList != null){
//            return mFilteredList.size();
//
//        } else {
//            return 0;
//        }
//        return mFilteredList.size();
    }

//    @Override
//    public Filter getFilter() {
//        return new Filter() {
//            @Override
//            protected FilterResults performFiltering(CharSequence charSequence) {
//                String charString = charSequence.toString();
//
//
//                if (charString.isEmpty()) {
//                    completeOrderDatumArrayList = exampleListFull;
//                } else {
//
//                    List<CompleteOrderDatum> filteredList = new ArrayList<>();
//
//                    for (CompleteOrderDatum row : exampleListFull) {
//                        try {
//                            if (row.getProductName().toLowerCase().contains(charString.toLowerCase())) {
//                                Log.e("rahulraj ", "rahulraj naem " + row.getProductName());
//
//                                filteredList.add(row);
//                            }
//                        } catch (Exception e) {
//
//                        }
//
//
//                    }
//                    completeOrderDatumArrayList = filteredList;
//                }
//
//
//                FilterResults filterResults = new FilterResults();
//                filterResults.values = completeOrderDatumArrayList;
//                Log.e("rahulraj ", "rahulraj size " + completeOrderDatumArrayList.size());
//                return filterResults;
//            }
//
//            @Override
//            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
//                completeOrderDatumArrayList = (ArrayList<CompleteOrderDatum>) filterResults.values;
//                notifyDataSetChanged();
//            }
//        };
//    }


    public interface CompleteorderClickListener {
        void completeorderClickListener(View v, int position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
//        public View cardview;
//        ImageView img_bottle;
//        TextView tv_bottlename, tv_date, tv_time, tv_qlt, tv_liter, tv_rupee, tv_order, tv_name;
//        CardView carditem;
CardView carditem;
//        TextView tv_bottlename, tv_date, tv_time, tv_qty, tv_liter, tv_amount, tv_rupee, tv_order, tv_name;
//        AppCompatButton btn_decline, btn_accept;

//        ImageView img_bottle;
//        CardView carditem;
//        TextView tv_bottlename, tv_Status,tv_date, tv_time, tv_qty, tv_liter, tv_amount, tv_rupee, tv_order, tv_name;
//        AppCompatButton btn_decline, btn_accept;

        TextView tvOrderId, tvTAmount, tvAdress, tvStatus, tvDate;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

//            img_bottle = itemView.findViewById(R.id.img_bottle);
//
//            tv_bottlename = itemView.findViewById(R.id.tv_bottlename);
//            tv_date = itemView.findViewById(R.id.tv_date);
//            tv_time = itemView.findViewById(R.id.tv_time);
//            tv_qlt = itemView.findViewById(R.id.tv_qty);
//            tv_liter = itemView.findViewById(R.id.tv_liter);
//
//            carditem = itemView.findViewById(R.id.carditem);
//            tv_rupee = itemView.findViewById(R.id.tv_rupee);
//            tv_order = itemView.findViewById(R.id.tv_order);
//            tv_name = itemView.findViewById(R.id.tv_name);

            carditem = itemView.findViewById(R.id.carditem);
            tvStatus = itemView.findViewById(R.id.tvStatus);
            tvOrderId = itemView.findViewById(R.id.tvOrderId);
            tvTAmount = itemView.findViewById(R.id.tvTAmount);
            tvAdress = itemView.findViewById(R.id.tvAdress);
            tvDate = itemView.findViewById(R.id.tvDate);
        }
    }

}
