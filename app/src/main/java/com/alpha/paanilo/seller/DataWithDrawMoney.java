package com.alpha.paanilo.seller;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataWithDrawMoney {
    @SerializedName("wallet_amount")
    @Expose
    private Integer walletAmount;

    public Integer getWalletAmount() {
        return walletAmount;
    }

    public void setWalletAmount(Integer walletAmount) {
        this.walletAmount = walletAmount;
    }
}
