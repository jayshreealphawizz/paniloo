package com.alpha.paanilo.seller;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alpha.paanilo.R;
import com.alpha.paanilo.cart_pkg.showAddressModlePkg.deleteAddressPkg.DeleteAddressModle;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.seller.adapter.AdapterOpenOrder;
import com.alpha.paanilo.seller.model.openorder.OpenOrderDatum;
import com.alpha.paanilo.seller.model.openorder.OpenOrderPojo;
import com.alpha.paanilo.utility.CheckNetwork;
import com.alpha.paanilo.utility.Constants;
import com.alpha.paanilo.utility.CustomProgressbar;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import java.util.Collections;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SellerOpenOrderActivity extends AppCompatActivity implements View.OnClickListener, AdapterOpenOrder.OpenOrderAcceptDecline {

    String TAG = getClass().getSimpleName();
    List<OpenOrderDatum> arrayList = new ArrayList<>();
    AdapterOpenOrder adapterpicture;
    AppCompatImageView iv_back, iv_notification;
    View id_toolbar;
    AppCompatTextView tv_title, tv_notfound;
    Constants constants;
    String selectedItemstatus;
    ArrayAdapter spinnerArrayAdapter;
    String[] status = {"accept/confirm", "out of deliver", "cancel", "complete"};
    String dateToStr;
    private RecyclerView recyclerOpenOrder;
    private Object ModelOpenOrder;
AppCompatImageView id_refresh ;
    ImageView ivdefaultimage ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller_open_order);
        constants = new Constants(getApplicationContext());

        init();

        id_toolbar = findViewById(R.id.id_toolbar);
        iv_back = id_toolbar.findViewById(R.id.id_back);
        iv_notification = id_toolbar.findViewById(R.id.id_notification);
        tv_title = id_toolbar.findViewById(R.id.tv_tittle);
        tv_notfound = findViewById(R.id.tv_notfound);
        id_refresh = id_toolbar.findViewById(R.id.id_refresh);
        iv_back.setOnClickListener(this);
        id_refresh.setOnClickListener(this);
        tv_title.setText("Open Order");
        id_refresh.setVisibility(View.VISIBLE);
        iv_notification.setVisibility(View.GONE);
        iv_notification.setOnClickListener(this);


    }

    private void init() {
        recyclerOpenOrder = findViewById(R.id.recyclerOpenOrder);
        ivdefaultimage = findViewById(R.id.ivdefaultimage);
//        Collections.reverse(arrayList);
//        LinearLayoutManager layoutMa = new LinearLayoutManager(com.alpha.paanilo.seller.SellerOpenOrderActivity.this, RecyclerView.VERTICAL, false);
//        layoutMa.setReverseLayout(true);
//        recyclerOpenOrder.setLayoutManager(layoutMa);

        Collections.reverse(arrayList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        recyclerOpenOrder.setHasFixedSize(true);
        recyclerOpenOrder.setLayoutManager(layoutManager);


        if (CheckNetwork.isNetAvailable(getApplicationContext())) {
            getOPenOrders(constants.getUserID());
        } else {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.network), Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.id_back:
                Intent intet = new Intent(com.alpha.paanilo.seller.SellerOpenOrderActivity.this, SellerHomeActivity.class);
                startActivity(intet);
                finish();
                break;

            case R.id.id_notification:
                Intent noti = new Intent(com.alpha.paanilo.seller.SellerOpenOrderActivity.this, SellerNotificationActivity.class);
                startActivity(noti);
                finish();
                break;
            case R.id.id_refresh:
                if (CheckNetwork.isNetAvailable(getApplicationContext())) {
                    getOPenOrders(constants.getUserID());
                } else {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.network), Toast.LENGTH_LONG).show();
                }
                break;


        }

    }


    private void getOPenOrders(String userId) {
        CustomProgressbar.showProgressBar(this, false);
        ApiClient.getClient().getVenderOpenOrders(userId).enqueue(new Callback<OpenOrderPojo>() {
            @Override
            public void onResponse(Call<OpenOrderPojo> call, Response<OpenOrderPojo> response) {

                Log.e(TAG, "getSeller Open onResponse: " + new Gson().toJson(response.body()));
                CustomProgressbar.hideProgressBar();

                if (response.isSuccessful()) {
                    ivdefaultimage.setVisibility(View.GONE);
                    CustomProgressbar.hideProgressBar();
                    OpenOrderPojo sellerNotificationPojo = response.body();
                    if (sellerNotificationPojo.getStatus() == true) {
                        arrayList = sellerNotificationPojo.getData();
                        for (int i = 0; i < arrayList.size(); i++) {

                        }
                        Log.e("rahul ", "rahulk " + arrayList.size());
                        adapterpicture = new AdapterOpenOrder(com.alpha.paanilo.seller.SellerOpenOrderActivity.this, arrayList, SellerOpenOrderActivity.this);
                        recyclerOpenOrder.setAdapter(adapterpicture);
                        adapterpicture.notifyDataSetChanged();
                    } else {
                        Log.e("rahul" ," NO any order");
                        ivdefaultimage.setVisibility(View.VISIBLE);
//                        Toast.makeText(SellerOpenOrderActivity.this, "message " + sellerNotificationPojo.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (response.code() == 400) {
                        if (!false) {
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(response.errorBody().string());
                                CustomProgressbar.hideProgressBar();
                                String message = jsonObject.getString("message");
                                // Toast.makeText(SignupActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }

            }

            @Override
            public void onFailure(Call<OpenOrderPojo> call, Throwable t) {
                CustomProgressbar.hideProgressBar();
            }
        });


    }




    @Override
    public void openOrderAcceptDecline(View view, int position, OpenOrderDatum openOrderDatum) {
        switch (view.getId()) {
            case R.id.spinnerstatus:
//                Toast.makeText(this, "nnnnn", Toast.LENGTH_SHORT).show();
                break;
        }


    }

    @Override
    public void onBackPressed() {
//        Toast.makeText(this, "now backcccc", Toast.LENGTH_SHORT).show();
        Intent intet = new Intent(com.alpha.paanilo.seller.SellerOpenOrderActivity.this, SellerHomeActivity.class);
        startActivity(intet);
        finish();


        super.onBackPressed();
    }
}