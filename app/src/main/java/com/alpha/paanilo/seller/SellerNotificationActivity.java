package com.alpha.paanilo.seller;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alpha.paanilo.R;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.seller.adapter.AdapterSellerNotification;
import com.alpha.paanilo.seller.model.notification.SellerNotificationDatum;
import com.alpha.paanilo.seller.model.notification.SellerNotificationPojo;
import com.alpha.paanilo.utility.CheckNetwork;
import com.alpha.paanilo.utility.Constants;
import com.alpha.paanilo.utility.CustomProgressbar;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.FacebookSdk.getApplicationContext;

public class SellerNotificationActivity extends AppCompatActivity implements View.OnClickListener, AdapterSellerNotification.Sellernotification {

    String TAG = getClass().getSimpleName();

    List<SellerNotificationDatum> arrayList = new ArrayList<>();
    AdapterSellerNotification adapterpicture;
    AppCompatImageView iv_back, iv_notification;
    View id_toolbar;
    AppCompatTextView tv_title, tv_notfound;
    Constants constants;
    private RecyclerView recyclerSellerNotification;
    private Object ModelSellerNotification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller_notification);

        constants = new Constants(getApplicationContext());

        Log.e(TAG, "onCreate: " + constants.getUserID());
        init();


        id_toolbar = findViewById(R.id.id_toolbar);
        iv_back = id_toolbar.findViewById(R.id.id_back);
        iv_notification = id_toolbar.findViewById(R.id.id_notification);
        tv_title = id_toolbar.findViewById(R.id.tv_tittle);
        tv_notfound = findViewById(R.id.tv_notfound);

        iv_back.setOnClickListener(this);
        tv_title.setText("Notification");
        iv_notification.setVisibility(View.GONE);
    }

    private void init() {
        recyclerSellerNotification = findViewById(R.id.recyclerSellerNotification);


        Collections.reverse(arrayList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        recyclerSellerNotification.setHasFixedSize(true);
        recyclerSellerNotification.setLayoutManager(layoutManager);

//        recyclerSellerNotification.scrollToPosition(0);

        // 1. First, clear the array of data

// 2. Notify the adapter of the update

// 3. Reset endless scroll listener when performing a new search

//        recyclerSellerNotification.smoothScrollToPosition(0);

//        recyclerSellerNotification.smoothScrollToPosition(0);

//        Collections.reverse(arrayList);
//        LinearLayoutManager layoutMa = new LinearLayoutManager(com.alpha.paanilo.seller.SellerNotificationActivity.this, RecyclerView.VERTICAL, false);
//        layoutMa.setReverseLayout(true);
//        recyclerSellerNotification.setLayoutManager(layoutMa);
//        adapterpicture = new AdapterSellerNotification(com.alpha.paanilo.seller.SellerNotificationActivity.this, arrayList);
//        recyclerSellerNotification.setAdapter(adapterpicture);

//        constants.getUserID()
        Log.e("Userid 16 ", "UserId" + constants.getUserID());

        if (CheckNetwork.isNetAvailable(getApplicationContext())) {
            getSellerNotificationList(constants.getUserID());
        } else {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.network), Toast.LENGTH_LONG).show();
        }
    }

  /*  private void getDataPicture() {
        ModelSellerNotification modelSellerNotification = new ModelSellerNotification("a");
        arraylistpicture.add(modelSellerNotification);

        modelSellerNotification = new ModelSellerNotification("b");
        arraylistpicture.add(modelSellerNotification);

        modelSellerNotification = new ModelSellerNotification("b");
        arraylistpicture.add(modelSellerNotification);

        modelSellerNotification = new ModelSellerNotification("b");
        arraylistpicture.add(modelSellerNotification);

        modelSellerNotification = new ModelSellerNotification("b");
        arraylistpicture.add(modelSellerNotification);

        modelSellerNotification = new ModelSellerNotification("b");
        arraylistpicture.add(modelSellerNotification);

        adapterpicture.notifyDataSetChanged();
    }*/

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.id_back:
                Intent intet = new Intent(com.alpha.paanilo.seller.SellerNotificationActivity.this, com.alpha.paanilo.seller.SellerHomeActivity.class);
                startActivity(intet);
                finish();
                break;
        }
    }


    private void getSellerNotificationList(String userId) {
        CustomProgressbar.showProgressBar(SellerNotificationActivity.this, false);
        ApiClient.getClient().getSellerNotificationList(userId).enqueue(new Callback<SellerNotificationPojo>() {
            @Override
            public void onResponse(Call<SellerNotificationPojo> call, Response<SellerNotificationPojo> response) {

                Log.e(TAG, "getSellerNotificationList onResponse: " + new Gson().toJson(response.body()));
                CustomProgressbar.hideProgressBar();

                SellerNotificationPojo sellerNotificationPojo = response.body();

                if (response.isSuccessful()) {
                    CustomProgressbar.hideProgressBar();
                    if (sellerNotificationPojo.getStatus() == true) {
                        arrayList = sellerNotificationPojo.getData();
                        for (int i = 0; i < arrayList.size(); i++) {

                        }
                        Log.e("rahul", "rahul::::: " + arrayList.size());
                        adapterpicture = new AdapterSellerNotification(com.alpha.paanilo.seller.SellerNotificationActivity.this, arrayList, SellerNotificationActivity.this);
                        recyclerSellerNotification.setAdapter(adapterpicture);
                        adapterpicture.notifyDataSetChanged();
//                        recyclerSellerNotification.scrollToPosition(0);
//                        arrayList.clear();
//                        adapterpicture.notifyItemRemoved(0);
//                        adapterpicture.getItemId(0);
                    }
                } else {
                    if (response.code() == 400) {
                        if (!false) {
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(response.errorBody().string());
                                CustomProgressbar.hideProgressBar();
                                String message = jsonObject.getString("message");
                                // Toast.makeText(SignupActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }

            }

            @Override
            public void onFailure(Call<SellerNotificationPojo> call, Throwable t) {
                CustomProgressbar.hideProgressBar();
            }
        });


    }


    @Override
    public void openOrderAcceptDecline(View view, int position, SellerNotificationDatum openOrderDatum) {
        switch (view.getId()) {
            case R.id.cardclick:
//                Toast.makeText(this, "click "  + position, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(SellerNotificationActivity.this, SellerAllOrderNotificationDetailsActivity.class);
                intent.putExtra("orderid", arrayList.get(position).getOrderId());
//        intent.putExtra("prodcutname", arrayList.get(position).getp);
//                intent.putExtra("productquantity", completeOrderDatum.getQuantity());
//                intent.putExtra("producttotamount", completeOrderDatum.getTotalAmount());
//                intent.putExtra("productimage", completeOrderDatum.getProductImage());
//                intent.putExtra("productuserid", completeOrderDatum.getUserId());
//                intent.putExtra("productSaleid", completeOrderDatum.getSaleId());
//                intent.putExtra("productstatus", completeOrderDatum.getStatus());
//                intent.putExtra("getDeliveryAddress", completeOrderDatum.getDeliveryAddress());
//                intent.putExtra("productdate", completeOrderDatum.getDeliveryTime());
//                intent.putExtra("userfullname", completeOrderDatum.getUserFullname());
//                intent.putExtra("OrderId" ,  completeOrderDatum.getOrderId());
//                intent.putExtra("paymentmethod" ,  completeOrderDatum.getPaymentMethod());
//                //                Log.e("rahul", "rahul Userid " + openOrderDatum.getUserId());
////                Log.e("rahul", "rahul getSaleId " + openOrderDatum.getSaleId());
                startActivity(intent);

                Log.e("rajbana", "rahul getSaleId::: " + openOrderDatum.getOrderId());
                break;
        }
    }
}