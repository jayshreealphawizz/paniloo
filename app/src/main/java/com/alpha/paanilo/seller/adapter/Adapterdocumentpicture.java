package com.alpha.paanilo.seller.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.alpha.paanilo.R;
import com.alpha.paanilo.seller.model.Modeldocumentpicture;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

public class Adapterdocumentpicture extends RecyclerView.Adapter<Adapterdocumentpicture.MyViewHolder> {
    Context context;
    List<Modeldocumentpicture> arraylistpicture;
    int counter = 0;

    public Adapterdocumentpicture(Context context, List<Modeldocumentpicture> arraylistpicture) {
        this.context = context;
        this.arraylistpicture = arraylistpicture;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_uploadpicture, parent, false);
        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull com.alpha.paanilo.seller.adapter.Adapterdocumentpicture.MyViewHolder holder, int position) {
        Modeldocumentpicture modelPicture = arraylistpicture.get(position);



        /*if (position == 0) {
            Glide.with(context)
                    .load("" + arraylistpicture.get(position))
                    .placeholder(R.drawable.pancard)
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .dontTransform()
                    .into(holder.ivImagePicture);


        } else if (position == 1) {
            Glide.with(context)
                    .load("" + arraylistpicture.get(position))
                    .placeholder(R.drawable.adhar)
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .dontTransform()
                    .into(holder.ivImagePicture);


        }*/

    }

    @Override
    public int getItemCount() {
        return arraylistpicture.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView ivImagePicture, iv_cross;
        ;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            ivImagePicture = itemView.findViewById(R.id.ivImagePicture);
            iv_cross = itemView.findViewById(R.id.iv_cross);

        }
    }
}
