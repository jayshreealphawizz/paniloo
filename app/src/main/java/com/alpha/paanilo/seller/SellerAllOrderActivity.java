package com.alpha.paanilo.seller;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alpha.paanilo.AllOrderModel;
import com.alpha.paanilo.DatumallOrder;
import com.alpha.paanilo.R;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.seller.adapter.AdapterAllOrder;
import com.alpha.paanilo.utility.CheckNetwork;
import com.alpha.paanilo.utility.Constants;
import com.alpha.paanilo.utility.CustomProgressbar;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SellerAllOrderActivity extends AppCompatActivity implements View.OnClickListener {

    String TAG = getClass().getSimpleName();


    List<DatumallOrder> productList = new ArrayList<>();
    //    List<DatumallOrder> completeOrderDatumArrayList = new ArrayList<>();
    AdapterAllOrder adapterpicture;
    AppCompatImageView iv_back, iv_notification;
    View id_toolbar;
    AppCompatTextView tv_title, tv_ordernotfound;
    LinearLayout ll_top;
    Constants constants;
    AppCompatEditText etProductSearchId;
    private RecyclerView recyclerAllOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller_all_order);
        constants = new Constants(getApplicationContext());

        init();

        id_toolbar = findViewById(R.id.id_toolbar);
        iv_back = id_toolbar.findViewById(R.id.id_back);
        iv_notification = id_toolbar.findViewById(R.id.id_notification);
        tv_title = id_toolbar.findViewById(R.id.tv_tittle);


        tv_ordernotfound = findViewById(R.id.tv_ordernotfound);
        ll_top = findViewById(R.id.ll_top);

        iv_back.setOnClickListener(this);
        tv_title.setText("All Order");
        iv_notification.setOnClickListener(this);

    }

    private void init() {
        recyclerAllOrder = findViewById(R.id.recyclerAllOrder);
        etProductSearchId = findViewById(R.id.etProductSearchId);
//        Collections.reverse(productList);
//        LinearLayoutManager layoutMa = new LinearLayoutManager(com.alpha.paanilo.seller.SellerAllOrderActivity.this, RecyclerView.VERTICAL, false);
//        layoutMa.setReverseLayout(true);
//        recyclerAllOrder.setLayoutManager(layoutMa);


        Collections.reverse(productList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        recyclerAllOrder.setHasFixedSize(true);
        recyclerAllOrder.setLayoutManager(layoutManager);



//        adapterpicture = new AdapterAllOrder(com.alpha.paanilo.seller.SellerAllOrderActivity.this, completeOrderDatumArrayList);
//        recyclerAllOrder.setAdapter(adapterpicture);
//constants.getUserID()


       /* etProductSearchId.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {
                adapterpicture.getFilter().filter(s);
            }
        });*/

//        etProductSearchId.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
////                ivOkBtn.setVisibility(View.GONE);
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
////                if (s.length() >= 1) {
////                    Toast.makeText(SellerAllOrderActivity.this, "Toast Data", Toast.LENGTH_SHORT).show();
//                    adapterpicture.getFilter().filter(s);
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                adapterpicture.getFilter().filter(s);
//            }
//        });


        Log.e("Userid 2::", "UserId " + constants.getUserID());

        if (CheckNetwork.isNetAvailable(getApplicationContext())) {
            getAllOrderList(constants.getUserID(),"");
        } else {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.network), Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.id_back:
                Intent intet = new Intent(com.alpha.paanilo.seller.SellerAllOrderActivity.this, SellerHomeActivity.class);
                startActivity(intet);
                finish();
                break;

            case R.id.id_notification:
                Intent noti = new Intent(com.alpha.paanilo.seller.SellerAllOrderActivity.this, SellerNotificationActivity.class);
                startActivity(noti);
                finish();
                break;
        }

    }


    private void getAllOrderList(String userId,String date) {
//        contactList.clear();
        CustomProgressbar.showProgressBar(this, false);
        ApiClient.getClient().getAllOrderList(userId ,date).enqueue(new Callback<AllOrderModel>() {
            @Override
            public void onResponse(Call<AllOrderModel> call, Response<AllOrderModel> response) {
                Log.e(TAG, "onResponse All order response: " + new Gson().toJson(response.body()));
                CustomProgressbar.hideProgressBar();
                AllOrderModel allOrderModel = response.body();
                if (response.isSuccessful()) {
                    try {
                        CustomProgressbar.hideProgressBar();
//                        Log.e("TAG", "stall near by you response all stalllllll : " + new Gson().toJson(response.body()));
//                          allOrderModel.body();
//                        if (withdrawPozo.getStatus().equals("1")) {

                        productList = allOrderModel.getData();
                        for (int i = 0; i < productList.size(); i++) {

                        }


                        Log.e("rahul", "rahul::::: " + productList.size());
//                        adapterpicture = new AdapterSellerTransection(SellerTransectionActivity.this, arraylistpicture);
//                        recyclerSellerTransaction.setAdapter(adapterpicture);
                        adapterpicture = new AdapterAllOrder(com.alpha.paanilo.seller.SellerAllOrderActivity.this, productList);
                        recyclerAllOrder.setAdapter(adapterpicture);
                        adapterpicture.notifyDataSetChanged();
//                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        CustomProgressbar.hideProgressBar();

                    }
                } else {
                    Toast.makeText(SellerAllOrderActivity.this, "Message " +allOrderModel.getMessage(), Toast.LENGTH_SHORT).show();

                    CustomProgressbar.hideProgressBar();
                    if (response.code() == 400) {
                        if (!false) {
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(response.errorBody().string());
                                CustomProgressbar.hideProgressBar();
                                String message = jsonObject.getString("message");
                                // Toast.makeText(SignupActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<AllOrderModel> call, Throwable t) {
                CustomProgressbar.hideProgressBar();
            }
        });

    }


}