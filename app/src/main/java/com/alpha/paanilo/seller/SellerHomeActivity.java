package com.alpha.paanilo.seller;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;

import com.alpha.paanilo.R;
import com.alpha.paanilo.Roomdatabase.DatabaseClient;
import com.alpha.paanilo.authenticationModule.Loginoption;
import com.alpha.paanilo.editprofile_pkg.SellerEditProfile_Activity;
import com.alpha.paanilo.model.notificationPkg.NotificationClearPozo;
import com.alpha.paanilo.model.notificationPkg.NotificationCountModel;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.seller.adapter.AdapterSellerNotification;
import com.alpha.paanilo.seller.model.notification.SellerNotificationPojo;
import com.alpha.paanilo.utility.AppSession;
import com.alpha.paanilo.utility.Constants;
import com.alpha.paanilo.utility.CustomProgressbar;
import com.bumptech.glide.Glide;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.FacebookSdk.getApplicationContext;

public class SellerHomeActivity extends AppCompatActivity implements View.OnClickListener {

    View toolbar_seller_home;
    DrawerLayout drawer;
    ImageView ivNavClose;
    NavigationView navigationView;
    AppCompatImageView id_drawerMenu;
    DrawerLayout drawer_layout;
    Constants constants;
    AppCompatTextView username, useremail;
    CircleImageView ivuserProfile;
    String UserImage, UserName, UserEmail;
    AppCompatImageView iv_editprofile;
    LinearLayout llcountlayout;
    TextView tvCountnotification;
    RelativeLayout id_notification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller_home);
        constants = new Constants(getApplicationContext());

        toolbar_seller_home = findViewById(R.id.toolbar_seller_home);
        id_drawerMenu = toolbar_seller_home.findViewById(R.id.id_drawerMenu_btn);
        id_notification = toolbar_seller_home.findViewById(R.id.id_notification);

        llcountlayout = toolbar_seller_home.findViewById(R.id.llcountlayout);
        tvCountnotification = toolbar_seller_home.findViewById(R.id.tvCountnotification);


        replaceFragement(new com.alpha.paanilo.seller.SellerHomeFragment());

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setItemIconTintList(null);
        navigationView.getMenu().getItem(0).setChecked(true);

        username = navigationView.getHeaderView(0).findViewById(R.id.username);
        useremail = navigationView.getHeaderView(0).findViewById(R.id.useremail);

        ivuserProfile = navigationView.getHeaderView(0).findViewById(R.id.ivuserProfile);
        iv_editprofile = navigationView.getHeaderView(0).findViewById(R.id.iv_editprofile);

        Log.e("User Id id:::: ", "UserId::: " + constants.getImage());

        Log.e("Name get ", "Name::: " + constants.getUsername());
        Log.e("Email id:::: ", "Email::: " + constants.getEmail());
        Log.e("User Id id:::: ", "UserId::: " + constants.getUserID());

        UserEmail = constants.getEmail();
        UserName = constants.getUsername();
        UserImage = constants.getImage();
        if (UserImage.equals("0")) {

        } else if (TextUtils.isEmpty(UserImage)) {

        } else {

            Glide.with(SellerHomeActivity.this)
                    .load(ApiClient.USER_PROFILE_URL + UserImage)
                    .placeholder(R.drawable.ic_account_user)
                    .into(ivuserProfile);

            Log.e("rahul::user Image: ", "rahul:dfsdf:: " + ApiClient.USER_PROFILE_URL + UserImage);

        }

        if (UserName != null) {
            username.setText(constants.getUsername());
        }

        if (UserEmail != null) {
            useremail.setText(constants.getEmail());
        }

        if (constants.getdob().equals("0")) {

        } else {
            Log.e("dob", "dob" + constants.getdob());
//            tv_dob.setText(constants.getdob());
        }

        drawer_layout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);

        id_drawerMenu.setOnClickListener(this);
        id_notification.setOnClickListener(this);
        iv_editprofile.setOnClickListener(this);
        ivNavClose = navigationView.getHeaderView(0).findViewById(R.id.ivNavClose);
        ivNavClose.setOnClickListener(this);
        ivNavClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
                    drawer_layout.closeDrawer(Gravity.LEFT);
                } else {
                    drawer_layout.openDrawer(Gravity.LEFT);
                }
            }
        });
        getSellerNotificationListdata(constants.getUserID());

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                int id = item.getItemId();
                if (id == R.id.nav_home) {
//                title.setText("Home");
//                loadFragment(new HomeFragment());
                } else if (id == R.id.nav_all_order) {
                    Intent intent = new Intent(com.alpha.paanilo.seller.SellerHomeActivity.this, com.alpha.paanilo.seller.SellerAllOrderActivity.class);
                    startActivity(intent);
                    drawer_layout.closeDrawer(Gravity.LEFT);
//                    finish();
                }/* else if (id == R.id.nav_product_upload) {
                    Intent intent = new Intent(com.alpha.paanilo.seller.SellerHomeActivity.this, com.alpha.paanilo.seller.SellerProductUploadActivity.class);
                    startActivity(intent);
                    drawer_layout.closeDrawer(Gravity.LEFT);
//                    finish();
                } */ else if (id == R.id.nav_product_myproduct) {
                    Intent intent = new Intent(com.alpha.paanilo.seller.SellerHomeActivity.this, com.alpha.paanilo.seller.SellerMyProductActivity.class);
                    startActivity(intent);
                    drawer_layout.closeDrawer(Gravity.LEFT);
//                    finish();
                } /*else if (id == R.id.nav_notifications) {
                    Intent intent = new Intent(com.alpha.paanilo.seller.SellerHomeActivity.this, SellerNotificationActivity.class);
                    startActivity(intent);
                    drawer_layout.closeDrawer(Gravity.LEFT);
//                    finish();
                } */ else if (id == R.id.nav_transaction_history) {
                    Intent intent = new Intent(com.alpha.paanilo.seller.SellerHomeActivity.this, com.alpha.paanilo.seller.SellerTransectionActivity.class);
                    startActivity(intent);
                    drawer_layout.closeDrawer(Gravity.LEFT);
//                    finish();
                } else if (id == R.id.nav_wallet) {
                    Intent intent = new Intent(com.alpha.paanilo.seller.SellerHomeActivity.this, SellerWalletActivity.class);
                    startActivity(intent);
                    drawer_layout.closeDrawer(Gravity.LEFT);
//                    finish();
                } else if (id == R.id.nav_report) {
                    Intent intent = new Intent(com.alpha.paanilo.seller.SellerHomeActivity.this, com.alpha.paanilo.seller.SellerReportActivity.class);
                    startActivity(intent);
                    drawer_layout.closeDrawer(Gravity.LEFT);
//                    finish();
                } else if (id == R.id.nav_support) {
                    Intent intent = new Intent(com.alpha.paanilo.seller.SellerHomeActivity.this, com.alpha.paanilo.seller.SellerSupportActivity.class);
                    startActivity(intent);
                    drawer_layout.closeDrawer(Gravity.LEFT);
//                    finish();
                } else if (id == R.id.nav_coupon) {
                    Intent intent = new Intent(com.alpha.paanilo.seller.SellerHomeActivity.this, SellerCouponWalletActivity.class);
                    startActivity(intent);
                    drawer_layout.closeDrawer(Gravity.LEFT);
//                    finish();
                } else if (id == R.id.nav_logout) {
                    log_out();

                    /*AlertDialog.Builder builder = new AlertDialog.Builder(com.alpha.paanilo.seller.SellerHomeActivity.this);
                    builder.setMessage("Are you sure you want to exit ?")
                            .setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {



                                   *//* AppSession.clearAllSharedPreferences(getApplicationContext());
                                    constants.setIsLogin("no");
                                    constants.setUsername("");
                                    constants.setEmail("");
                                    constants.setLoginAs("");
                                    constants.setUserID("");
                                    constants.setVendorClick("");
                                    constants.setIsLoginStatus("");
                                    constants.clearAllSharedPreferences(getApplicationContext());
                                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                                    startActivity(new Intent(getApplicationContext(), Loginoption.class));

                                    deleteTask();

                                    dialog.dismiss();*//*


                                    AppSession.clearAllSharedPreferences(SellerHomeActivity.this);
                                    constants.setIsLogin("no");
                                    constants.setUsername("");
                                    constants.setEmail("");
                                    constants.setLoginAs("");
                                    constants.setUserID("");
                                    constants.setVendorClick("");
                                    constants.setIsLoginStatus("");
                                    constants.clearAllSharedPreferences(SellerHomeActivity.this);
                                    Intent intent = new Intent(SellerHomeActivity.this, Loginoption.class);
                                    startActivity(intent);
                                    dialog.dismiss();
                                    finishAffinity();

//                                    finishAffinity();










                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();*/
//
                }


                drawer_layout.closeDrawer(GravityCompat.START);
                return true;
            }
        });

    }

    private void replaceFragement(com.alpha.paanilo.seller.SellerHomeFragment homeFragment) {
        FragmentTransaction home = getSupportFragmentManager().beginTransaction();
        home.replace(R.id.flHomeId, homeFragment);
        home.commit();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.id_drawerMenu_btn:
                if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
                    drawer_layout.closeDrawer(Gravity.LEFT);
                } else {
                    drawer_layout.openDrawer(Gravity.LEFT);
                }
                break;
            case R.id.id_notification:

                Intent intent = new Intent(com.alpha.paanilo.seller.SellerHomeActivity.this, SellerNotificationActivity.class);
                startActivity(intent);
                clearNotificationFarmerData(constants.getUserID());
//                finish();
                break;
            case R.id.iv_editprofile:
//                Toast.makeText(this, "cccc", Toast.LENGTH_SHORT).show();
                Intent intentedit = new Intent(com.alpha.paanilo.seller.SellerHomeActivity.this, SellerEditProfile_Activity.class);
                startActivity(intentedit);
//                finish();
                break;


        }

    }

    public void log_out() {
        final Dialog dialog = new Dialog(SellerHomeActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_log_out);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        ImageView ivlogoutClose = dialog.findViewById(R.id.ivlogoutCloseId);
        AppCompatImageView rllogout = dialog.findViewById(R.id.ivlogoutCloseId);
        CardView btn_logout = dialog.findViewById(R.id.btn_logout);

        ivlogoutClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                tvStatus.setText("Offline");
//                constants.getOnline().equals("online")
                constants.setOnline("Offline");
                constants.setIsLogin("no");
                constants.setUsername("");
                constants.setEmail("");
                constants.setLoginAs("");
                constants.setUserID("");
                constants.setVendorClick("");
                constants.setIsLoginStatus("");
                constants.clearAllSharedPreferences(SellerHomeActivity.this);
                Intent intent = new Intent(SellerHomeActivity.this, Loginoption.class);
                startActivity(intent);
                dialog.dismiss();
                finishAffinity();
                drawer_layout.closeDrawer(Gravity.LEFT);
            }
        });

        rllogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                drawer_layout.closeDrawer(Gravity.LEFT);
            }
        });

        dialog.show();
    }

    private void deleteTask() {
        class DeleteTask extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... voids) {
                DatabaseClient.getInstance(getApplicationContext()).getAppDatabase()
                        .taskDao()
                        .delete();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

            }
        }
        DeleteTask dt = new DeleteTask();
        dt.execute();
    }


    private void getSellerNotificationListdata(String userId) {
        CustomProgressbar.showProgressBar(SellerHomeActivity.this, false);
        ApiClient.getClient().getSellerNotificationList(userId).enqueue(new Callback<SellerNotificationPojo>() {
            @Override
            public void onResponse(Call<SellerNotificationPojo> call, Response<SellerNotificationPojo> response) {

                Log.e("TAG", "getSellerNotificationList onResponsedata: " + new Gson().toJson(response.body()));
                CustomProgressbar.hideProgressBar();

                SellerNotificationPojo sellerNotificationPojo = response.body();

                if (response.isSuccessful()) {
                    CustomProgressbar.hideProgressBar();
                    if (sellerNotificationPojo.getStatus() == true) {

                        llcountlayout.setVisibility(View.VISIBLE);
                        tvCountnotification.setText("" + sellerNotificationPojo.getCount());

                    }
                } else {
                    tvCountnotification.setText("" + sellerNotificationPojo.getCount());

                    llcountlayout.setVisibility(View.GONE);
                    if (response.code() == 400) {
                        if (!false) {
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(response.errorBody().string());
                                CustomProgressbar.hideProgressBar();
                                String message = jsonObject.getString("message");
                                // Toast.makeText(SignupActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }


            }

            @Override
            public void onFailure(Call<SellerNotificationPojo> call, Throwable t) {
                CustomProgressbar.hideProgressBar();
            }
        });


    }


    private void clearNotificationFarmerData(String farmerid) {
//        CustomProgressbar.showProgressBar(getApplicationContext(), false);
        ApiClient.getClient().clearNotificationFarmer(farmerid).enqueue(new Callback<NotificationCountModel>() {
            @Override
            public void onResponse(Call<NotificationCountModel> call, Response<NotificationCountModel> response) {

                Log.e("TAG", "getSellerNotificationList onResponse: " + new Gson().toJson(response.body()));
                CustomProgressbar.hideProgressBar();

                NotificationCountModel sellerNotificationPojo = response.body();

                if (response.isSuccessful()) {
                    CustomProgressbar.hideProgressBar();
                    if (sellerNotificationPojo.getStatus()) {
//                            searchAdapter.notifyDataSetChanged();
                        llcountlayout.setVisibility(View.VISIBLE);
                        tvCountnotification.setText("" + sellerNotificationPojo.getCount());

                    } else {
                        llcountlayout.setVisibility(View.GONE);
                        Toast.makeText(getApplicationContext(), sellerNotificationPojo.getCount(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    llcountlayout.setVisibility(View.GONE);
                    if (response.code() == 400) {
                        if (!false) {
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(response.errorBody().string());
                                CustomProgressbar.hideProgressBar();
                                String message = jsonObject.getString("message");
                                // Toast.makeText(SignupActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }


            }

            @Override
            public void onFailure(Call<NotificationCountModel> call, Throwable t) {
                CustomProgressbar.hideProgressBar();
            }
        });


    }

}