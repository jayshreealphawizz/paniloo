package com.alpha.paanilo.seller.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.alpha.paanilo.R;
import com.alpha.paanilo.seller.model.ModelSellerNotification;
import com.alpha.paanilo.seller.model.notification.SellerNotificationDatum;
import com.alpha.paanilo.seller.model.openorder.OpenOrderDatum;

import java.util.List;


public class AdapterSellerNotification extends RecyclerView.Adapter<AdapterSellerNotification.MyViewHolder> {
    Context context;
    List<SellerNotificationDatum> arraylistpicture;
    int counter = 0;
Sellernotification openOrderAcceptDecline;
    public AdapterSellerNotification(Context context, List<SellerNotificationDatum> arraylistpicture, Sellernotification openOrderAcceptDecline) {
        this.context = context;
        this.arraylistpicture = arraylistpicture;
        this.openOrderAcceptDecline = openOrderAcceptDecline;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_seller_notification, parent, false);
        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        SellerNotificationDatum modelPicture = arraylistpicture.get(position);


//        Toast.makeText(context, "sellnotification datum ", Toast.LENGTH_SHORT).show();



        holder.discription.setText(modelPicture.getNotification());

        holder.tv_orderid.setText("OrderId : "+modelPicture.getInvoiceId());



        holder.cardclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openOrderAcceptDecline.openOrderAcceptDecline(v, position, arraylistpicture.get(position));
            }
        });
//modelPicture.notify();
//        arraylistpicture.get(position);
//        arraylistpicture.clear();

    }

    @Override
    public int getItemCount() {
        return arraylistpicture.size();

    }


    public interface Sellernotification {
        void openOrderAcceptDecline(View view, int position, SellerNotificationDatum openOrderDatum);
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
       CardView cardclick ;

        TextView name, discription,tv_orderid;
        RelativeLayout rlRow_notification;

        public MyViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name_notification);
            discription = itemView.findViewById(R.id.discription_notification);

            rlRow_notification = itemView.findViewById(R.id.rlRow_notification);

            tv_orderid = itemView.findViewById(R.id.tv_orderid);
            cardclick = itemView.findViewById(R.id.cardclick);
        }
    }
}
