package com.alpha.paanilo.seller.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.alpha.paanilo.R;
import com.alpha.paanilo.seller.model.Modelshoppicture;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;


public class Adaptershoppicture extends RecyclerView.Adapter<Adaptershoppicture.MyViewHolder> {
    Context context;
    List<Modelshoppicture> arraylistpicture;
    int counter = 0;

    public Adaptershoppicture(Context context, List<Modelshoppicture> arraylistpicture) {
        this.context = context;
        this.arraylistpicture = arraylistpicture;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_uploadpicturenew, parent, false);
        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Modelshoppicture modelPicture = arraylistpicture.get(position);

        if (position == 0) {
            Glide.with(context)
                    .load("" + arraylistpicture.get(position))
                    .placeholder(R.drawable.image1)
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .dontTransform()
                    .into(holder.ivImagePicture);


        }

    }

    @Override
    public int getItemCount() {
        return arraylistpicture.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView ivImagePicture, iv_cross;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            ivImagePicture = itemView.findViewById(R.id.ivImagePicture);
            iv_cross = itemView.findViewById(R.id.iv_cross);

        }
    }
}
