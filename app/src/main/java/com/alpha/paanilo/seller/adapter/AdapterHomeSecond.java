package com.alpha.paanilo.seller.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.alpha.paanilo.R;

import com.alpha.paanilo.seller.model.ModelTwo;
import com.alpha.paanilo.utility.Constants;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.List;

public class AdapterHomeSecond extends RecyclerView.Adapter<AdapterHomeSecond.MyViewHolder> {
    Context context;
    //    List<ModelTwo> arraylist;
    List<ModelTwo> arraylist;
    int counter = 0;
    private final MaplistitemClickListener maplistitemClickListener;
    Constants constants;

    public AdapterHomeSecond(Context context, List<ModelTwo> arraylist, MaplistitemClickListener maplistitemClickListener) {
        this.context = context;
        this.arraylist = arraylist;
        this.maplistitemClickListener = maplistitemClickListener;
    }

//    public AdapterHomeSecond(Context context, List<ModelTwo> arraylist) {
//        this.context = context;
//        this.arraylist = arraylist;
//
//    }
    /*public AdapterHomeSecond(com.alpha.paanilo.seller.SellerHomeFragment sellerHomeFragment, List<ModelTwo> arraylist) {
        this.context = context;
        this.arraylist = arraylist;
    }*/

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_row, parent, false);
        constants = new Constants(context);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        ModelTwo modelTwo = arraylist.get(position);


//        Toast.makeText(context, "ssssssssssss ", Toast.LENGTH_SHORT).show();
        if (position == 0) {
            holder.tvInvest.setText("Open Order");
//            holder.idview.setVisibility(View.VISIBLE);
            Glide.with(context)
                    .load("" + arraylist.get(position).getImage())
                    .placeholder(R.drawable.ic_open_order)
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .dontTransform()
                    .into(holder.ivImage);
            holder.llcountlayout.setVisibility(View.VISIBLE);
//            dfdfd



//            constants.setCountno(""+openOrderPojo.getCount());

          Log.e("ram" , "ram:::: " + constants.getCountno()) ;
            holder.tvCountno.setText(""+constants.getCountno());

        } else if (position == 1) {
//            holder.idview.setVisibility(View.VISIBLE);
            holder.tvInvest.setText("Complete Order");
            holder.tvInvest.setTextColor(Color.parseColor("#8E9092"));
            Glide.with(context)
                    .load("" + arraylist.get(position).getImage())
                    .placeholder(R.drawable.complete_order)
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .dontTransform()
                    .into(holder.ivImage);
            holder.llcountlayout.setVisibility(View.INVISIBLE);
        } else if (position == 2) {
//            holder.idview.setVisibility(View.VISIBLE);
            holder.tvInvest.setText("Total Earning");
            holder.tvInvest.setTextColor(Color.parseColor("#8E9092"));
            Glide.with(context)
                    .load("" + arraylist.get(position).getImage())
                    .placeholder(R.drawable.total_earning)
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .dontTransform()
                    .into(holder.ivImage);
            holder.llcountlayout.setVisibility(View.INVISIBLE);
        }

        holder.cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                maplistitemClickListener.maplistitemClickListener(v, position);
//                if(position==0) {
//                    context.startActivity(new Intent(context, com.alpha.paanilo.seller.SellerOpenOrderActivity.class));
//                }
//                else if(position==1){
//                    context.startActivity(new Intent(context, com.alpha.paanilo.seller.SellerCompleteOrderActivity.class));
//                }
//                else{
//                    context.startActivity(new Intent(context, com.alpha.paanilo.seller.SellerTotalEarningActivity.class));
//                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return arraylist.size();
    }

    public interface MaplistitemClickListener {
        void maplistitemClickListener(View v, int position);
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        static TextView tvCountno;
        TextView tvInvest;
        ImageView ivImage;
        CardView cardview;
        View idview;
        LinearLayout llcountlayout;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvInvest = itemView.findViewById(R.id.tvInvest);
            cardview = itemView.findViewById(R.id.cardview);
            tvCountno = itemView.findViewById(R.id.tvCountno);
//            idview = itemView.findViewById(R.id.idview);
            ivImage = itemView.findViewById(R.id.ivImage);
            llcountlayout = itemView.findViewById(R.id.llcountlayout);
        }
    }
}
