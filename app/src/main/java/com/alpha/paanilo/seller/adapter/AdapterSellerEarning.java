package com.alpha.paanilo.seller.adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.alpha.paanilo.DatumallOrder;
import com.alpha.paanilo.R;
import com.alpha.paanilo.retrofit.ApiClient;
import com.bumptech.glide.Glide;
import java.util.List;

public class AdapterSellerEarning extends RecyclerView.Adapter<AdapterSellerEarning.MyViewHolder> {
    Context context;
    List<DatumallOrder> arraylistpicture;

    public AdapterSellerEarning(Context context, List<DatumallOrder> arraylistpicture) {
        this.context = context;
        this.arraylistpicture = arraylistpicture;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_seller_total_earning, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        DatumallOrder modelPicture = arraylistpicture.get(position);
//        Toast.makeText(context, "totl earing ", Toast.LENGTH_SHORT).show();
        Log.e("Product image ", "product image " + modelPicture.getProductImage());

//        if(modelPicture.getStatus().equalsIgnoreCase("4") || modelPicture.getStatus().equalsIgnoreCase("complete")) {
        holder.tvProductName.setText("Bottle Jar");
            holder.tv_date.setText("" + modelPicture.getDate());
            holder.tv_rupee.setText("Rs." + modelPicture.getTotalAmount());
        if (modelPicture.getProductImage() == null) {

        } else {
            String imgName = modelPicture.getProductImage().substring((modelPicture.getProductImage().lastIndexOf(",") + 1));

            Log.e("pro", "imgName" + imgName);

            Glide.with(context)
                    .load(ApiClient.CATEGORY_PRODUCT_URL + imgName)
                    .placeholder(R.drawable.proimage)
                    .into(holder.Img);

        }

        if (modelPicture.getStatus().equals("4")) {
            holder.tv_rupee.setTextColor(Color.parseColor("#16A085"));
            holder.ivSign.setImageResource(R.drawable.ic_plushistory);
        } else {
            holder.ivSign.setImageResource(R.drawable.ic_minushistory);
            holder.tv_rupee.setTextColor(Color.parseColor("#E96E60"));
        }
//            notifyItemRemoved(position);
//        }

//        holder.tv1.setText("" + modelPicture.getQuantity());
//        holder.tvProductName.setText("" + modelPicture.getProductName());
    }

    @Override
    public int getItemCount() {
        return arraylistpicture.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView Img,ivSign;
        TextView tv1, tv_date, tv_time, tv_rupee, tvProductName;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            Img = itemView.findViewById(R.id.Img);
            tv_date = itemView.findViewById(R.id.tv_date);
            tv_time = itemView.findViewById(R.id.tv_time);
            tv_rupee = itemView.findViewById(R.id.tv_rupee);
            tvProductName = itemView.findViewById(R.id.tvProductName);
            tv1 = itemView.findViewById(R.id.tv1);
            ivSign = itemView.findViewById(R.id.ivSign);
        }
    }
}
