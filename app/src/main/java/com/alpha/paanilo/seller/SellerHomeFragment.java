package com.alpha.paanilo.seller;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.SwitchCompat;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.alpha.paanilo.R;

import com.alpha.paanilo.authenticationModule.Loginoption;
import com.alpha.paanilo.cart_pkg.showAddressModlePkg.deleteAddressPkg.DeleteAddressModle;
import com.alpha.paanilo.editprofile_pkg.EditProfile_Activity;
import com.alpha.paanilo.model.banner_pkg.BannerInfo;

import com.alpha.paanilo.retrofit.ApiClient;

import com.alpha.paanilo.seller.adapter.AdapterHomeFirst;
import com.alpha.paanilo.seller.adapter.AdapterHomeSecond;

import com.alpha.paanilo.seller.model.ModelTwo;

import com.alpha.paanilo.seller.model.openorder.OnlineStatusmodel;
import com.alpha.paanilo.seller.model.openorder.OpenOrderPojo;

import com.alpha.paanilo.utility.CheckNetwork;
import com.alpha.paanilo.utility.Constants;
import com.alpha.paanilo.utility.CustomProgressbar;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.FacebookSdk.getApplicationContext;

public class SellerHomeFragment extends Fragment implements View.OnClickListener, AdapterHomeSecond.MaplistitemClickListener {

    View v;

    List<ModelTwo> arraylist = new ArrayList<>();
    AdapterHomeSecond adapterHomeSecond;


    AppCompatButton btn_accept, btn_Decline;


    Constants constants;

    String saleid, Userid;
    SwitchCompat SwitchCompat;
    int count;
    TextView tvStatus;

    String selectedItem;

    ImageView ivdefaultimage;
    RecyclerView recycler_home;
    LinearLayout lllayout, Latoutll;
    ImageView ivProductImage;
    TextView tvDistance, tvDeliveryTime, tvOrderId, tvUserName, tvAdress, tvDate, tvOrderTAmount, tvQuantity, tvPaymentMethod;

    int timerdelay = 50000;


    OpenOrderPojo openOrderPojo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        SwitchCompat.setChecked(true);
        v = inflater.inflate(R.layout.activity_seller_home_page, container, false);
        constants = new Constants(getContext());
        init();
//        banner_first_list = new ArrayList<>();
//        vpHomeProduct = v.findViewById(R.id.vp_homecreen);
//        tl_HomeProdcut = v.findViewById(R.id.tl_homescren);

        return v;
    }

    private void init() {
//        rrLayout = v.findViewById(R.id.rrLayout);
//        lllayoutbtn = v.findViewById(R.id.lllayoutbtn);
//        clayout = v.findViewById(R.id.clayout);
//        llOrderDetails = v.findViewById(R.id.llOrderDetails);

//        spinner = v.findViewById(R.id.spinner);

//        spinnerstatus = v.findViewById(R.id.spinnerstatus);
        tvStatus = v.findViewById(R.id.tvStatus);
//        CardSpinner = v.findViewById(R.id.cardSpinner);
        recycler_home = v.findViewById(R.id.recycler_home);
//        btnaccept = v.findViewById(R.id.btn_accept);
//        btndecline = v.findViewById(R.id.btn_decline);
        SwitchCompat = v.findViewById(R.id.switchCompat);

        ivProductImage = v.findViewById(R.id.ivProductImage);
        tvOrderId = v.findViewById(R.id.tvOrderId);
        lllayout = v.findViewById(R.id.lllayout);

        ivdefaultimage = v.findViewById(R.id.ivdefaultimage);


        tvAdress = v.findViewById(R.id.tvAdress);
        tvDate = v.findViewById(R.id.tvDate);
        tvOrderTAmount = v.findViewById(R.id.tvOrderTAmount);
        tvQuantity = v.findViewById(R.id.tvQuantity);
        tvPaymentMethod = v.findViewById(R.id.tvPaymentMethod);
        btn_accept = v.findViewById(R.id.btn_accept);
        btn_Decline = v.findViewById(R.id.btn_Decline);
        tvUserName = v.findViewById(R.id.tvUserName);
        tvDistance = v.findViewById(R.id.tvDistance);

        tvDeliveryTime = v.findViewById(R.id.tvDeliveryTime);
        Latoutll = v.findViewById(R.id.Latoutll);

        lllayout.setVisibility(View.INVISIBLE);
        Log.e("constants", "constants::111:" + constants.getUserID());


        Glide.with(getApplicationContext())
                .load("")
                .placeholder(R.drawable.proimage)
                .into(ivProductImage);
        getDataPicture();
//        getOPenOrders(constants.getUserID());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);
        recycler_home.setLayoutManager(layoutManager);
        adapterHomeSecond = new AdapterHomeSecond(getContext(), arraylist, this);
        recycler_home.setAdapter(adapterHomeSecond);
        adapterHomeSecond.notifyDataSetChanged();

        if (constants.getOnline().equals("online")) {
            SwitchCompat.setChecked(true);
            tvStatus.setText("Online");
//            Toast.makeText(getApplicationContext(), "yyyy2222222", Toast.LENGTH_SHORT).show();

            if (CheckNetwork.isNetAvailable(getApplicationContext())) {
                getOPenOrders(constants.getUserID());
                try {
                    new CountDownTimer(timerdelay, 100) {
                        public void onTick(long millisUntilFinished) {
                        }

                        public void onFinish() {
                            timerdelay = 50000;
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    getOPenOrders(constants.getUserID());

                                    start();
                                }
                            }, 1200);


                        }
                    }.start();


                    Log.e("rahulrajba:::: ","rahural::::" + constants.getUserID());
                    vendoronlineoffline(constants.getUserID(), "Online");
                } catch (NullPointerException e) {

                }


            } else {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.network), Toast.LENGTH_LONG).show();
            }
        } else {
//            Toast.makeText(getApplicationContext(), "yyyy11111", Toast.LENGTH_SHORT).show();
            SwitchCompat.setChecked(false);
            tvStatus.setText("Offline");
            vendoronlineoffline(constants.getUserID(), "Offline");
        }

        btn_accept.setOnClickListener(this);
        btn_Decline.setOnClickListener(this);

        SwitchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
//                    Toast.makeText(getApplicationContext(), "yyyy", Toast.LENGTH_SHORT).show();
//                    Toast.makeText(getApplicationContext(), "is checked  show", Toast.LENGTH_SHORT).show();
                    tvStatus.setText("Online");
//                    isCheckedclick = true;
                    constants.setOnline("online");
                    Log.e("constants", "constants:::" + constants.getUserID());

                    if (CheckNetwork.isNetAvailable(getApplicationContext())) {
                        getOPenOrders(constants.getUserID());
                        new CountDownTimer(timerdelay, 100) {
                            public void onTick(long millisUntilFinished) {
                            }

                            public void onFinish() {
                                timerdelay = 50000;
                                CustomProgressbar.showProgressBar(getApplicationContext(), false);

                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        getOPenOrders(constants.getUserID());

//                                Toast.makeText(getApplicationContext(), "YYYYY", Toast.LENGTH_SHORT).show();
//                                CustomProgressbar.hideProgressBar();

                                        start();
                                    }
                                }, 1200);

                            }
                        }.start();


//                        getOPenOrders(constants.getUserID());

//                        Toast.makeText(getApplicationContext(), "yyyy555", Toast.LENGTH_SHORT).show();
                        vendoronlineoffline(constants.getUserID(), "Online");

                    } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.network), Toast.LENGTH_LONG).show();
                    }

                } else {
//                    Toast.makeText(getApplicationContext(), "yyy66666", Toast.LENGTH_SHORT).show();
                    constants.setOnline("Offline");
                    tvStatus.setText("Offline");
                    vendoronlineoffline(constants.getUserID(), "Offline");
                    lllayout.setVisibility(View.INVISIBLE);
//                    rrLayout.setVisibility(View.INVISIBLE);
//                    lllayoutbtn.setVisibility(View.INVISIBLE);
////                    clayout.setVisibility(View.INVISIBLE);
//                    CardSpinner.setVisibility(View.INVISIBLE);
//                    spinner.setVisibility(View.INVISIBLE);
//                    llOrderDetails.setVisibility(View.INVISIBLE);
                }
            }
        });
        Log.e("banasa", "bana::::: " + constants.getUserID());

    }


    private void getDataPicture() {
        ModelTwo modelTwo = new ModelTwo("a");
        arraylist.add(modelTwo);
        arraylist.add(modelTwo);
        arraylist.add(modelTwo);
//        modelTwo = new ModelTwo("a","Completle Order");
//        arraylist.add(modelTwo);
//
//
//        modelTwo = new ModelTwo("a","Total Earning");
//        arraylist.add(modelTwo);

//        arraylist.add(modelTwo);
//        arraylist.add(modelTwo);
    }


    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_accept:

                Log.e("rahul ", "saleidaaa " + saleid);
                Log.e("rahul ", "Userid aaa" + Userid);
                Log.e("selectedItem ", "deliverytimeaa " + selectedItem);
//                if (selectedItem.equals("10-15 Min") || selectedItem.equals("30-1 Hour") || selectedItem.equals("3-4 Hours")) {


                updateOrderStatus(Userid, "" + saleid, "1");
//                    CardSpinner.setVisibility(View.GONE);
                lllayout.setVisibility(View.INVISIBLE);
                ivdefaultimage.setVisibility(View.VISIBLE);
//                } else {
//                    Toast.makeText(getContext(), "plz select time", Toast.LENGTH_SHORT).show();
//                }
                break;

            case R.id.btn_Decline:
//                Log.e("rahul ", "saleid dd" + saleid);
//                Log.e("rahul ", "Userid ddd" + Userid);
//                Log.e("rahul ", "deliverytime dd" + selectedItem);

                log_out();


//                updateOrderStatus(Userid, "" + saleid, "3");
//                lllayout.setVisibility(View.INVISIBLE);
//                ivdefaultimage.setVisibility(View.VISIBLE);


                break;
//
//            case R.id.iv_minus:
//                if (deliverytime != 1) {
//                    deliverytime--;
//                    tv_delivery_time.setText(deliverytime + " Min");
//                }
//                break;
//            case R.id.iv_plus:
//                deliverytime++;
//                tv_delivery_time.setText(deliverytime + " Min");
//                break;

        }
    }


    public void maplistitemClickListener(View v, int position) {
        switch (v.getId()) {

            case R.id.cardview:
                if (position == 0) {
                    getContext().startActivity(new Intent(getContext(), com.alpha.paanilo.seller.SellerOpenOrderActivity.class));

//                    tvCountno

                } else if (position == 1) {
                    getContext().startActivity(new Intent(getContext(), com.alpha.paanilo.seller.SellerCompleteOrderActivity.class));
                } else {
                    getContext().startActivity(new Intent(getContext(), com.alpha.paanilo.seller.SellerTotalEarningActivity.class));
                }
                break;

        }

    }

    private void updateOrderStatus(String user_id, String sale_id, final String status) {
        CustomProgressbar.showProgressBar(getContext(), false);
        ApiClient.getClient().updateOrderStatus(user_id, sale_id, status).enqueue(new Callback<DeleteAddressModle>() {
            @Override
            public void onResponse(Call<DeleteAddressModle> call, Response<DeleteAddressModle> response) {

                Log.e("TAG", "onResponse: Update order " + new Gson().toJson(response.body()));
                CustomProgressbar.hideProgressBar();

                if (response.isSuccessful()) {

                    DeleteAddressModle addAddressResponseModle = response.body();
                    if (addAddressResponseModle.getStatus()) {
//                        accept/confirm=1,out of deliver=2,cancel=3,complete=4
                        if (status.equalsIgnoreCase("1")) {
                            Toast.makeText(getApplicationContext(), "Order Accepted Successfully", Toast.LENGTH_SHORT).show();
//                            CardSpinner.setVisibility(View.GONE);
                            lllayout.setVisibility(View.INVISIBLE);
                            ivdefaultimage.setVisibility(View.VISIBLE);
                        } else if (status.equalsIgnoreCase("3")) {
                            Toast.makeText(getApplicationContext(), "Order Cancel Successfully", Toast.LENGTH_SHORT).show();
//                            CardSpinner.setVisibility(View.GONE);
                            lllayout.setVisibility(View.INVISIBLE);
                            ivdefaultimage.setVisibility(View.VISIBLE);
                        }
//                        Toast.makeText(getContext(), addAddressResponseModle.getMessage(), Toast.LENGTH_LONG).show();
                        getOPenOrders(constants.getUserID());
//                        Toast.makeText(getApplicationContext(), "Thrid", Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(getContext(), addAddressResponseModle.getMessage(), Toast.LENGTH_LONG).show();
                    }


                } else {
                    if (response.code() == 400) {
                        if (!false) {
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(response.errorBody().string());
                                CustomProgressbar.hideProgressBar();
                                String message = jsonObject.getString("message");
                                // Toast.makeText(SignupActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }


            }

            @Override
            public void onFailure(Call<DeleteAddressModle> call, Throwable t) {
                CustomProgressbar.hideProgressBar();
            }
        });

    }

    private void getOPenOrders(String userId) {
//        arraylist.clear();
//        CustomProgressbar.showProgressBar(getActivity(), false);
        ApiClient.getClient().getVenderOpenOrders(userId).enqueue(new Callback<OpenOrderPojo>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<OpenOrderPojo> call, Response<OpenOrderPojo> response) {
                Log.e("TAG", "get Seller Open Order List onResponse: " + new Gson().toJson(response.body()));
//                CustomProgressbar.hideProgressBar();
                openOrderPojo = response.body();
                if (response.isSuccessful()) {
                    CustomProgressbar.hideProgressBar();
//                    OpenOrderPojo openOrderPojo = response.body();
                    if (openOrderPojo.getStatus() == true) {
                        Log.e("ss", "size" + openOrderPojo.getData());

                        ivdefaultimage.setVisibility(View.GONE);

                        for (count = 0; count < openOrderPojo.getData().size(); count++) {
                            Log.e("rahulraj ", "rahulraj " + openOrderPojo.getData().size());
                        }

                        Log.e("rahulraj ", "count " + openOrderPojo.getCount());
//                        count
                        constants.setCountno("" + openOrderPojo.getCount());
                        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);
                        recycler_home.setLayoutManager(layoutManager);
//                        adapterHomeSecond = new AdapterHomeSecond(getContext(), arraylist, SellerHomeActivity.this);
                        recycler_home.setAdapter(adapterHomeSecond);

                        tvUserName.setText("" + openOrderPojo.getData().get(count - 1).getUserFullname());
                        tvOrderId.setText("" + openOrderPojo.getData().get(count - 1).getOrderId());
                        tvAdress.setText("" + openOrderPojo.getData().get(count - 1).getDeliveryAddress());
                        tvDate.setText("" + openOrderPojo.getData().get(count - 1).getUserPhone());
                        tvOrderTAmount.setText("" + openOrderPojo.getData().get(count - 1).getTotalAmount());
                        tvQuantity.setText("" + openOrderPojo.getData().get(count - 1).getQuantity());
                        tvPaymentMethod.setText("" + openOrderPojo.getData().get(count - 1).getPaymentMethod());
//                        tvPrice.setText("Rs." + openOrderPojo.getData().get(count - 1).getTotalAmount());
//                        tvQuantity.setText("Qty." + openOrderPojo.getData().get(count - 1).getQuantity());
//dsdds
//                        userPhone
                        if (openOrderPojo.getData().get(count - 1).getDistance().isEmpty() || openOrderPojo.getData().get(count - 1).getDistance().equalsIgnoreCase("") || openOrderPojo.getData().get(count - 1).getDistance().equalsIgnoreCase("null")) {
                            tvDistance.setText("" + "00 KM");
                        } else {
                            tvDistance.setText("" + openOrderPojo.getData().get(count - 1).getDistance());
                        }


                        if (openOrderPojo.getData().get(count - 1).getOrder_delivery_time().isEmpty() || openOrderPojo.getData().get(count - 1).getOrder_delivery_time().equalsIgnoreCase("") || openOrderPojo.getData().get(count - 1).getOrder_delivery_time().equalsIgnoreCase("null")) {
                            tvDeliveryTime.setText("" + "00.00");
                        } else {
                            tvDeliveryTime.setText("" + openOrderPojo.getData().get(count - 1).getOrder_delivery_time());
                        }
//                        Toast.makeText(getContext(), "dfdf " + openOrderPojo.getData().get(count - 1).getOrder_delivery_time(), Toast.LENGTH_SHORT).show();
                        saleid = openOrderPojo.getData().get(count - 1).getSaleId();
                        Userid = openOrderPojo.getData().get(count - 1).getUserId();
//                        constants.setresponseUserID(openOrderPojo.getData().get(count - 1).getUserId());
//                        order_delivery_time = openOrderPojo.getData().get(count - 1).getOrderDeliveryTime();

                        Log.e("Distance ", "GetDelivery Time::" + openOrderPojo.getData().get(count - 1).getDistance());
                        Glide.with(getApplicationContext())
                                .load("")
                                .placeholder(R.drawable.proimage)
                                .into(ivProductImage);

                        Log.e("rahul ", "salieid::" + openOrderPojo.getData().get(count - 1).getSaleId());
                        Log.e("rahul ", "Userid::" + openOrderPojo.getData().get(count - 1).getUserId());
                        Log.e("rahul ", "GetDelivery Time::" + openOrderPojo.getData().get(count - 1).getOrder_delivery_time());
////                            mViewPagerAdapter = new AdapterHomeFirst(getContext(), list);
////                            vpHomeProduct.setAdapter(mViewPagerAdapter);
//                      openOrderPojo.getData().get(count - 1).getProductImage();

//                        if (openOrderPojo.getData().get(count - 1).getProductImage() == null) {
//                            Log.e("Null ", "Null::");
//                        } else {
//                            String string = openOrderPojo.getData().get(count - 1).getProductImage();
//                            Log.e("rahul ", "string::" + string);
//                            String[] parts = string.split(",");
//                            String part1 = parts[0]; // 004
////                            String part2 = parts[1];
//                            Log.e("rahul ", "part1 Time::" + part1);
//                            list.add("" + part1);
////                            list.add("" + part2);
////                            list = new
//                            Log.e("rahullist ", "part1 list::" + list);
////                            mViewPagerAdapter = new AdapterHomeFirst(getContext(), list);
////                            vpHomeProduct.setAdapter(mViewPagerAdapter);
//
//
////                            ivProductImage

//
////                            tl_HomeProdcut.setupWithViewPager(vpHomeProduct);
////                            Log.e("rahul ", "part2 Time::" + part2);
//                        }

                        lllayout.setVisibility(View.VISIBLE);

//                        rrLayout.setVisibility(View.VISIBLE);
//                        lllayoutbtn.setVisibility(View.VISIBLE);

                        if (openOrderPojo.getData().get(count - 1).getStatus().equals("0")) {

                            btn_Decline.setVisibility(View.VISIBLE);
                            btn_accept.setVisibility(View.VISIBLE);

                        } else if (openOrderPojo.getData().get(count - 1).getStatus().equals("1")) {
                            btn_accept.setClickable(false);
                            btn_accept.setVisibility(View.GONE);
                            btn_Decline.setVisibility(View.GONE);
                            lllayout.setVisibility(View.INVISIBLE);
                            ivdefaultimage.setVisibility(View.VISIBLE);
                        } else if (openOrderPojo.getData().get(count - 1).getStatus().equals("3")) {
                            btn_Decline.setClickable(false);
                            btn_Decline.setVisibility(View.GONE);
                            btn_accept.setVisibility(View.GONE);
                            lllayout.setVisibility(View.INVISIBLE);
                            ivdefaultimage.setVisibility(View.VISIBLE);
                        } else if (openOrderPojo.getData().get(count - 1).getStatus().equals("2")) {
                            btn_Decline.setClickable(false);
                            btn_Decline.setVisibility(View.GONE);
                            btn_accept.setVisibility(View.GONE);
                            lllayout.setVisibility(View.INVISIBLE);
                            ivdefaultimage.setVisibility(View.VISIBLE);
                        }
//                    clayout.setVisibility(View.VISIBLE);

//                        CardSpinner.setVisibility(View.VISIBLE);
//                        spinnerstatus.setVisibility(View.VISIBLE);
//                        llOrderDetails.setVisibility(View.VISIBLE);

                    } else {
//                        Toast.makeText(getApplicationContext(), "" + openOrderPojo.getMessage(), Toast.LENGTH_SHORT).show();
//                        Toast.makeText(getApplicationContext(), "message " + sellerNotificationPojo.getMessage(), Toast.LENGTH_SHORT).show();

                        lllayout.setVisibility(View.INVISIBLE);
                        ivdefaultimage.setVisibility(View.VISIBLE);
//                        rrLayout.setVisibility(View.INVISIBLE);
//
//                        lllayoutbtn.setVisibility(View.INVISIBLE);
////                        clayout.setVisibility(View.INVISIBLE);
//                        CardSpinner.setVisibility(View.INVISIBLE);
//
//                        spinner.setVisibility(View.INVISIBLE);
//                        llOrderDetails.setVisibility(View.INVISIBLE);

                    }
                } else {
//                    Toast.makeText(getApplicationContext(), "message " + openOrderPojo.getMessage(), Toast.LENGTH_SHORT).show();
                    if (response.code() == 400) {
                        if (!false) {
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(response.errorBody().string());
                                CustomProgressbar.hideProgressBar();
                                String message = jsonObject.getString("message");
                                // Toast.makeText(SignupActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }


            }

            @Override
            public void onFailure(Call<OpenOrderPojo> call, Throwable t) {
                CustomProgressbar.hideProgressBar();
                Toast.makeText(getApplicationContext(), "Failed Response", Toast.LENGTH_SHORT).show();
            }
        });


    }


    private void vendoronlineoffline(String userId, String status) {
//        arraylist.clear();
        CustomProgressbar.showProgressBar(getContext(), false);
        ApiClient.getClient().vendoronlineoffline(userId, status).enqueue(new Callback<OnlineStatusmodel>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<OnlineStatusmodel> call, Response<OnlineStatusmodel> response) {
                Log.e("TAG", "Online OffLine: " + new Gson().toJson(response.body()));
                CustomProgressbar.hideProgressBar();
                OnlineStatusmodel openOrderPojo = response.body();

            }

            @Override
            public void onFailure(Call<OnlineStatusmodel> call, Throwable t) {
                CustomProgressbar.hideProgressBar();
                Toast.makeText(getApplicationContext(), "Failed Response", Toast.LENGTH_SHORT).show();
            }
        });


    }


    public void log_out() {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_cancelorder);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        ImageView ivlogoutClose = dialog.findViewById(R.id.ivlogoutCloseId);
        AppCompatImageView rllogout = dialog.findViewById(R.id.ivlogoutCloseId);
        CardView btn_logout = dialog.findViewById(R.id.btn_logout);

        ivlogoutClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateOrderStatus(Userid, "" + saleid, "3");
                lllayout.setVisibility(View.INVISIBLE);
                ivdefaultimage.setVisibility(View.VISIBLE);
                dialog.dismiss();
            }
        });

        rllogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.cancel();
            }
        });

        dialog.show();
    }


}