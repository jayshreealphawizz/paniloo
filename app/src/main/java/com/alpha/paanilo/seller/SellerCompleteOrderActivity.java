package com.alpha.paanilo.seller;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alpha.paanilo.R;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.seller.adapter.AdapterAllOrdernew;
import com.alpha.paanilo.seller.model.completeorder.CompleteOrderDatum;
import com.alpha.paanilo.seller.model.completeorder.CompleteOrderPojo;
import com.alpha.paanilo.utility.CheckNetwork;
import com.alpha.paanilo.utility.Constants;
import com.alpha.paanilo.utility.CustomProgressbar;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SellerCompleteOrderActivity extends AppCompatActivity implements View.OnClickListener, AdapterAllOrdernew.CompleteorderClickListener {

    String TAG = getClass().getSimpleName();

    List<CompleteOrderDatum> completeOrderDatumArrayList = new ArrayList<>();
    AdapterAllOrdernew adapterpicture;
    AppCompatImageView iv_back, iv_notification;
    View id_toolbar;
    AppCompatTextView tv_title, tv_ordernotfound;
    LinearLayout ll_top;
    Constants constants;
    AppCompatEditText etProductSearchId;
    private RecyclerView recyclerAllOrder;
    ImageView ivdefaultimage ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller_all_order);
        constants = new Constants(getApplicationContext());

        init();

        id_toolbar = findViewById(R.id.id_toolbar);
        iv_back = id_toolbar.findViewById(R.id.id_back);
        iv_notification = id_toolbar.findViewById(R.id.id_notification);
        tv_title = id_toolbar.findViewById(R.id.tv_tittle);
        tv_ordernotfound = findViewById(R.id.tv_ordernotfound);
//        ll_top = findViewById(R.id.ll_top);
        iv_notification.setVisibility(View.GONE);
        iv_back.setOnClickListener(this);
        tv_title.setText("Complete Order");
        iv_notification.setOnClickListener(this);

    }

    private void init() {
//        etProductSearchId = findViewById(R.id.etProductSearchId);
        recyclerAllOrder = findViewById(R.id.recyclerAllOrder);
        ivdefaultimage = findViewById(R.id.ivdefaultimage);
//        etProductSearchId.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                adapterpicture.getFilter().filter(s);
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                adapterpicture.getFilter().filter(s);
//
//            }
//        });
//        Collections.reverse(completeOrderDatumArrayList);
//        LinearLayoutManager layoutMa = new LinearLayoutManager(SellerCompleteOrderActivity.this, RecyclerView.VERTICAL, false);
//        layoutMa.setReverseLayout(true);
//        recyclerAllOrder.setLayoutManager(layoutMa);


        Collections.reverse(completeOrderDatumArrayList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        recyclerAllOrder.setHasFixedSize(true);
        recyclerAllOrder.setLayoutManager(layoutManager);



        if (CheckNetwork.isNetAvailable(getApplicationContext())) {
            completeOrderList(constants.getUserID());
        } else {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.network), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.id_back:
                Intent intet = new Intent(SellerCompleteOrderActivity.this, SellerHomeActivity.class);
                startActivity(intet);
                finish();
                break;

            case R.id.id_notification:
                Intent noti = new Intent(SellerCompleteOrderActivity.this, SellerNotificationActivity.class);
                startActivity(noti);
                finish();
                break;
        }

    }


    private void completeOrderList(String userId) {
        completeOrderDatumArrayList.clear();
        CustomProgressbar.showProgressBar(this, false);
        ApiClient.getClient().getCompleteOrderList(userId).enqueue(new Callback<CompleteOrderPojo>() {
            @Override
            public void onResponse(Call<CompleteOrderPojo> call, Response<CompleteOrderPojo> response) {

                Log.e(TAG, "onResponse: Compplete order " + new Gson().toJson(response.body()));
                CustomProgressbar.hideProgressBar();
                CompleteOrderPojo completeOrderPojo = response.body();
                if (response.isSuccessful()) {
                    CustomProgressbar.hideProgressBar();

                    if (completeOrderPojo.getStatus() == true) {
                        ivdefaultimage.setVisibility(View.GONE);
                        completeOrderDatumArrayList = completeOrderPojo.getData();
                        for (int i = 0; i < completeOrderDatumArrayList.size(); i++) {

                        }
                        Log.e("rahul Comptelte ", "rahulk " + completeOrderDatumArrayList.size());
                        adapterpicture = new AdapterAllOrdernew(SellerCompleteOrderActivity.this, completeOrderDatumArrayList, SellerCompleteOrderActivity.this);
                        recyclerAllOrder.setAdapter(adapterpicture);
                        adapterpicture.notifyDataSetChanged();
                        Log.e("rahul Comptelte ", "UserId++ " +completeOrderPojo.getData().get(0).getUserId());

                    } else {
                        Log.e("rahul" ,"complete any order");
                        ivdefaultimage.setVisibility(View.VISIBLE);
//                        Toast.makeText(SellerCompleteOrderActivity.this, completeOrderPojo.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    if (response.code() == 400) {
                        if (!false) {
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(response.errorBody().string());
                                CustomProgressbar.hideProgressBar();
                                String message = jsonObject.getString("message");
                                // Toast.makeText(SignupActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }

            }

            @Override
            public void onFailure(Call<CompleteOrderPojo> call, Throwable t) {
                CustomProgressbar.hideProgressBar();
            }
        });


    }


    @Override
    public void completeorderClickListener(View v, int position) {
        switch (v.getId()) {
            case R.id.carditem:
                Intent intent = new Intent(SellerCompleteOrderActivity.this, SellerOrderDetailsActivity.class);
                startActivity(intent);
           break;
        }
    }
}