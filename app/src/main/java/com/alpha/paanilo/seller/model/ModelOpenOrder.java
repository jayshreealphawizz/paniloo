package com.alpha.paanilo.seller.model;

public class ModelOpenOrder {
    String uploadpicture ;

    public ModelOpenOrder(String uploadpicture) {
        this.uploadpicture = uploadpicture;
    }

    public String getUploadpicture() {
        return uploadpicture;
    }

    public void setUploadpicture(String uploadpicture) {
        this.uploadpicture = uploadpicture;
    }
}