package com.alpha.paanilo.seller;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginSellerPojo {

  /*  @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private DataSellerRegistation data;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataSellerRegistation getData() {
        return data;
    }

    public void setData(DataSellerRegistation data) {
        this.data = data;
    }*/

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private DataSellerRegistation data;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataSellerRegistation getData() {
        return data;
    }

    public void setData(DataSellerRegistation data) {
        this.data = data;
    }

}
