package com.alpha.paanilo.seller;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alpha.paanilo.R;
import com.alpha.paanilo.cart_pkg.OrderConfirmation_Activity;
import com.alpha.paanilo.coupanPkg.couponModlePkg.CouponListPozo;
import com.alpha.paanilo.product_detail_pkg.CouponCountModle;
import com.alpha.paanilo.product_detail_pkg.CouponNotificationModel;
import com.alpha.paanilo.product_detail_pkg.CouponNumberModel;
import com.alpha.paanilo.product_detail_pkg.GetPriceModel;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.seller.adapter.AdapterSellerTransectionNew;
import com.alpha.paanilo.utility.CheckNetwork;
import com.alpha.paanilo.utility.Constants;
import com.alpha.paanilo.utility.CustomProgressbar;
import com.alpha.paanilo.utility.CustomToast;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SellerCouponWalletActivity extends AppCompatActivity implements View.OnClickListener, PaymentResultListener {

    //    final String fixamount = "200";
    List<DatumTranHis> arraylistpicture = new ArrayList<>();
    AdapterSellerTransectionNew adapterpicture;
    AppCompatImageView iv_back, iv_notification;
    View id_toolbar;
    AppCompatTextView tv_title;
    Constants constants;
    TextView tvTotalMoney;
    EditText tvCoupanAMount;
    CardView btn_send;
    String COUPONNO = "";
    String CopuonPrice;
    String descriptiondata;
    String TotalAMount;
    TextView tvDescription;
    private RecyclerView recyclerSellerTransaction;
    private Object ModelSellerTransection;
    private ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sellercopuon_wallet);

        init();
        constants = new Constants(getApplicationContext());
        id_toolbar = findViewById(R.id.id_toolbar);
        iv_back = id_toolbar.findViewById(R.id.id_back);
        iv_notification = id_toolbar.findViewById(R.id.id_notification);
        tv_title = id_toolbar.findViewById(R.id.tv_tittle);

        pd = new ProgressDialog(SellerCouponWalletActivity.this, R.style.AppCompatAlertDialogStyle);

        iv_back.setOnClickListener(this);
        tv_title.setText("Counter");
        iv_notification.setOnClickListener(this);

        Log.e("rahul ", "rahul111111111  " + constants.getUserID());
        Log.e("rahul ", "rahul11222  " + constants.getresponseUserID());
//        walletHistory(constants.getUserID(), "40");
//        constants.getUserID()
//        getCoupanNumber();
//        descriptiondata

        if (CheckNetwork.isNetAvailable(getApplicationContext())) {

            getCouponPrice();
            getCoupanNumber();
//            startRazorPayPayment(fixamount);
//            walletHistory(constants.getUserID());

        } else {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.network), Toast.LENGTH_LONG).show();
        }


//                if (CheckNetwork.isNetAvailable(getApplicationContext())) {
//walletHistory(constants.getresponseUserID(), ""+tvCoupanAMount.getText().toString().isEmpty());
//                } else {

//                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.network), Toast.LENGTH_LONG).show();
//                }

    }

    private void init() {
        tvCoupanAMount = findViewById(R.id.tvCoupanAMount);

        btn_send = findViewById(R.id.btn_send);
        tvTotalMoney = findViewById(R.id.tvTotalMoney);
//        recyclerSellerTransaction = findViewById(R.id.recyclerSellerTransaction);
        tvDescription = findViewById(R.id.tvDescription);

//        LinearLayoutManager layoutMa = new LinearLayoutManager(com.alpha.paanilo.seller.SellerWalletActivity.this, RecyclerView.VERTICAL, false);
//        layoutMa.setReverseLayout(true);
//        recyclerSellerTransaction.setLayoutManager(layoutMa);


//        Collections.reverse(arraylistpicture);
//        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
//        layoutManager.setReverseLayout(true);
//        layoutManager.setStackFromEnd(true);
//        recyclerSellerTransaction.setHasFixedSize(true);
//        recyclerSellerTransaction.setLayoutManager(layoutManager);


//        adapterpicture = new AdapterSellerTransectionNew(com.alpha.paanilo.seller.SellerWalletActivity.this, arraylistpicture);
//        recyclerSellerTransaction.setAdapter(adapterpicture);
//        getData();
        btn_send.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.id_back:
                Intent intet = new Intent(SellerCouponWalletActivity.this, SellerHomeActivity.class);
                startActivity(intet);
                finish();
                break;

            case R.id.id_notification:
                Intent noti = new Intent(SellerCouponWalletActivity.this, SellerNotificationActivity.class);
                startActivity(noti);
                finish();
                break;

            case R.id.btn_send:
//                Toast.makeText(this, "click withdraw btn", Toast.LENGTH_SHORT).show();
                if (CheckNetwork.isNetAvailable(SellerCouponWalletActivity.this)) {
                    if (tvCoupanAMount.getText().toString().isEmpty()) {
//
                        new CustomToast().Show_Toast(getApplicationContext(), v, "Amount Can't Empty");
////                        tvCoupanAMount.getBackground().mutate().setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
////                        tvCoupanAMount.requestFocus();
                    }

//                    if (COUPONNO.isEmpty() || COUPONNO.equals("") || COUPONNO.equals("0") || COUPONNO.equals("null")) {
//                        new CustomToast().Show_Toast(getApplicationContext(), v, "Coupon Can't Empty");
//                    }
                    else {
//                        walletHistory(constants.getresponseUserID(), "" + tvCoupanAMount.getText().toString().isEmpty());

//                        if (CheckNetwork.isNetAvailable(getApplicationContext())) {

                        Log.e("TAG", "BBB " + constants.getUserID());
                        Log.e("TAG", "BBBB 11111  " + constants.getresponseUserID());

                        Log.e("TAG", "CopuonPrice  " + CopuonPrice);
                        TotalAMount = String.valueOf(Float.parseFloat(CopuonPrice) * Float.parseFloat(tvCoupanAMount.getText().toString()));

                        Log.e("TAG", "TotalAMount::  " + TotalAMount);
                        startRazorPayPayment(TotalAMount);
//                        withdrawMoney(constants.getUserID(), tvCoupanAMount.getText().toString());
//                        } else {
//                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.network), Toast.LENGTH_LONG).show();
//                        }
//
//                        addAmount_Wallet();
//                        startRazorPayPayment(tvCoupanAMount.getText().toString());
                    }
                } else {

                    Toast.makeText(SellerCouponWalletActivity.this, "Check Network Connection", Toast.LENGTH_LONG).show();
                }


                break;


        }

    }


    public void startRazorPayPayment(String amount) {
        final Checkout co = new Checkout();
        co.setKeyID("rzp_test_QrkS2B01UMFJ95");//pannli test key
//        co.setKeyID("rzp_test_GC7PRrOSR8GIQf");//oro test key

        co.setImage(R.drawable.launcher);
        final SellerCouponWalletActivity activity = this;
//        context = getApplicationContext() ;
//        final ProductFragment context = this;
        try {
            System.out.println(constants.getEmail() + "------add wallet amount------" + amount + "===name =====" + constants.getUsername());

            JSONObject options = new JSONObject();
            options.put("name", constants.getUsername());
            options.put("description", "");
            //You can omit the image option to fetch the image from dashboard
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png");
            options.put("currency", "INR");
            options.put("amount", Float.parseFloat(amount) * 100);

            JSONObject preFill = new JSONObject();
            preFill.put("email", constants.getEmail());
            preFill.put("contact", "91" + "");
            options.put("prefill", preFill);


            co.open(activity, options);
        } catch (Exception e) {
            Log.e("rahulraj ", "walletpage222");
            Toast.makeText(getApplicationContext(), "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }
    }


    @Override
    public void onPaymentSuccess(String razorpayPaymentID) {
        try {

//            Log.e("rahulraj ", "goToScreenId::: " + goToScreenId);
//            Log.e("rahulraj ", "fixamount::: " + fixamount);
//            Log.e("rahulraj ", "walletpage::: " + razorpayPaymentID);


            String Useriddat = constants.getUserID();
            Log.e("rahul raj", "Useriddat::111111111111::" + Useriddat);

            Log.e("rahul raj", "CopuonPrice::111111111111::" + CopuonPrice);

            getCoupanCount(Useriddat, TotalAMount);
//            https://alphawizztest.tk/PaaniLoo/api/Authentication/add_coupon_amount_vendor_wallet
            Toast.makeText(getApplicationContext(), "Payment Successful: ", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
        }
    }

    @Override
    public void onPaymentError(int code, String response) {
        try {
            Toast.makeText(getApplicationContext(), "Payment failed ", Toast.LENGTH_SHORT).show();
//            finish();
        } catch (Exception e) {
        }
    }


    private void getCoupanCount(String vendid, String amount) {
//        Toast.makeText(this, "rrrrrrrr", Toast.LENGTH_SHORT).show();
        pd.setMessage("Loading");
        pd.show();
        JsonObject jsonObject = new JsonObject();
        try {
            jsonObject.addProperty("user_id", vendid);
            jsonObject.addProperty("amount", amount);
        } catch (Exception e) {
            e.printStackTrace();
        }
//        Log.e("TAG", "get coupan vendid : " + vendid);

//        Log.e("TAG", "get coupan amount : " + amount);

        (ApiClient.getClient().getCoupanCount(jsonObject)).enqueue(new Callback<CouponCountModle>() {
            @Override
            public void onResponse(Call<CouponCountModle> call, Response<CouponCountModle> response) {
                Log.e("TAG", "get coupan response : " + new Gson().toJson(response.body()));
                pd.dismiss();
                if (response.isSuccessful()) {
                    try {
                        pd.dismiss();
                        CouponCountModle couponCountModle = response.body();

                        if (couponCountModle.getStatus() == 1) {


//                            Toast.makeText(getContext(), "Amount added" + walletInfo.getData().getWallet(), Toast.LENGTH_SHORT).show();

//                            totalamount = ""+walletInfo.getData().getWallet();

//                            tvTotalAmount.setText("Rs." + walletInfo.getData().getWallet());
                           /* if (couponCountModle.getData().equals("")) {
                                tvCounponCount.setText("Rs." + 00);
                            } else {
                                tvCounponCount.setText("Rs." + couponCountModle.getData().getTotal_coupon());
                            }*/
                            getCoupanNumber();
                            //     finish();

                        } else {
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        pd.dismiss();
                    }

//                    getCoupanNumber();
//                    tvCoupanAMount.setText("");

                }
            }

            @Override
            public void onFailure(Call<CouponCountModle> call, Throwable t) {
                pd.dismiss();
            }
        });
    }


    private void getCoupanNumber() {
//        pbLoginId.setVisibility(View.VISIBLE);
        pd.setMessage("Loading");
        pd.show();
//        constants.setVendor(product.getFarmerId());
//        Log.e("rahul raj", "goToScreenId::" + goToScreenId);


        String Userid = constants.getUserID();
        Log.e("rahul raj", "Userid:::::" + Userid);

        (ApiClient.getClient().getCoupanNumber(Userid)).enqueue(new Callback<CouponNumberModel>() {
            @Override
            public void onResponse(Call<CouponNumberModel> call, Response<CouponNumberModel> response) {
                pd.dismiss();
                Log.e("TAG", "coupon number : " + new Gson().toJson(response.body()));

                if (response.isSuccessful()) {
                    CouponNumberModel couponNumberModel = response.body();
                    if (couponNumberModel.getStatus() == true) {

                        COUPONNO = String.valueOf(couponNumberModel.getData().getTotalCoupon());
                        if (couponNumberModel.getData().getTotalCoupon().equals("")) {
                            tvTotalMoney.setText("Counter " + 00);
                        } else {
                            tvTotalMoney.setText("Counter " + COUPONNO);
                        }
                        tvCoupanAMount.setText("");
                        if (couponNumberModel.getData().getTotalCoupon() <= 10) {
                            btn_send.setVisibility(View.VISIBLE);
                            getNotificationCounter();

                        } else {
                            btn_send.setVisibility(View.VISIBLE);
                        }

                        if (couponNumberModel.getData().getTotalCoupon() >= 1) {
//                            llcod.setVisibility(View.VISIBLE);
//                            cb_cod.setEnabled(true);
//                            cb_cod.setOnClickListener(OrderConfirmation_Activity.this);
//                            cb_cod.setOnCheckedChangeListener(this);
//                            cb_cod.setOnClickListener();
//                            cb_cod.setChecked(true);
//                            if()
//                            cb_cod.isChecked() ==false ;
//                            cb_cod.isClickable()==false;
//                            cb_cod.setChecked(true);
                        } else {
//                            cb_cod.setEnabled(false);
//                            cb_cod.setChecked(false);
//                            cb_cod.setoncl;
//                            llcod.setVisibility(View.GONE);
                        }
//                        llcod.setVisibility(View.GONE);
//                        Log.e("rahulraj", "rahulraj COUPONNO ::::::: " + COUPONNO);
                    }

                }
            }

            @Override
            public void onFailure(Call<CouponNumberModel> call, Throwable t) {
//                pbLoginId.setVisibility(View.GONE);
                Log.e("message ", "message ::: " + t.getMessage());
                pd.dismiss();
            }
        });
    }


    public void getCouponPrice() {
        pd.setMessage("Loading");
        pd.show();

        (ApiClient.getClient().getCouponPrice()).enqueue(new Callback<GetPriceModel>() {
            @Override
            public void onResponse(Call<GetPriceModel> call, Response<GetPriceModel> response) {
                pd.dismiss();

                Log.e("response:: ", "response data:: " + new Gson().toJson(response.body()));
                if (response.isSuccessful()) {
                    try {
                        pd.dismiss();
                        GetPriceModel couponListPozo = response.body();
                        if (couponListPozo.getStatus()) {

                            if (!couponListPozo.getData().getAmount().equals("null"))
                                CopuonPrice = couponListPozo.getData().getAmount();

                            descriptiondata = couponListPozo.getData().getDescription();
                            Log.e("raahulraj", "rahulraj " + CopuonPrice);
                            Log.e("raahulraj", "descriptiondata " + descriptiondata);
                            tvDescription.setText("" + descriptiondata);

//                            couponList = couponListPozo.getCoupon();
//                            coupanAdapter.addCoupon(couponList);
//                            tvCoupontTag.setVisibility(View.GONE);
                        } else {
//                            tvCoupontTag.setVisibility(View.VISIBLE);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<GetPriceModel> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(getApplicationContext(), getString(R.string.server_errors), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void getNotificationCounter() {
//        pbLoginId.setVisibility(View.VISIBLE);
        pd.setMessage("Loading");
        pd.show();
//        constants.setVendor(product.getFarmerId());
//        Log.e("rahul raj", "goToScreenId::" + goToScreenId);


        String Userid = constants.getUserID();
        Log.e("rahul raj", "Userid::::dta:" + Userid);

        (ApiClient.getClient().getNotificationCounter(Userid)).enqueue(new Callback<CouponNotificationModel>() {
            @Override
            public void onResponse(Call<CouponNotificationModel> call, Response<CouponNotificationModel> response) {
                pd.dismiss();
                Log.e("TAG", "notification counter number : " + new Gson().toJson(response.body()));

                if (response.isSuccessful()) {
                    CouponNotificationModel couponNumberModel = response.body();
                    if (couponNumberModel.getStatus() == true) {

                       /* COUPONNO = String.valueOf(couponNumberModel.getData().getTotalCoupon());
                        if (couponNumberModel.getData().getTotalCoupon().equals("")) {
                            tvTotalMoney.setText("Counter " + 00);
                        } else {
                            tvTotalMoney.setText("Counter " + COUPONNO);
                        }
                        tvCoupanAMount.setText("");
                        if (couponNumberModel.getData().getTotalCoupon() >= 11) {
                            btn_send.setVisibility(View.VISIBLE);

                        } else {
                            btn_send.setVisibility(View.VISIBLE);
                        }

                        if (couponNumberModel.getData().getTotalCoupon() >= 1) {
//                            llcod.setVisibility(View.VISIBLE);
//                            cb_cod.setEnabled(true);
//                            cb_cod.setOnClickListener(OrderConfirmation_Activity.this);
//                            cb_cod.setOnCheckedChangeListener(this);
//                            cb_cod.setOnClickListener();
//                            cb_cod.setChecked(true);
//                            if()
//                            cb_cod.isChecked() ==false ;
//                            cb_cod.isClickable()==false;
//                            cb_cod.setChecked(true);
                        } else {
//                            cb_cod.setEnabled(false);
//                            cb_cod.setChecked(false);
//                            cb_cod.setoncl;
//                            llcod.setVisibility(View.GONE);
                        }*/
//                        llcod.setVisibility(View.GONE);
//                        Log.e("rahulraj", "rahulraj COUPONNO ::::::: " + COUPONNO);
                    }

                }
            }

            @Override
            public void onFailure(Call<CouponNotificationModel> call, Throwable t) {
//                pbLoginId.setVisibility(View.GONE);
                Log.e("message ", "message ::: " + t.getMessage());
                pd.dismiss();
            }
        });
    }


}