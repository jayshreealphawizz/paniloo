package com.alpha.paanilo.seller.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.alpha.paanilo.R;
import com.alpha.paanilo.seller.SellerProductUploadActivity;
import com.alpha.paanilo.seller.Seller_SignUp_Activity;
import com.alpha.paanilo.seller.model.AttachmentListData;
import com.alpha.paanilo.seller.model.ModelProductUpload;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.List;


public class AdapterProductUpload extends RecyclerView.Adapter<AdapterProductUpload.AttachmentListViewHolder> {
    public ArrayList<ModelProductUpload> newAttachmentList;
    public Activity mActivity;


    public AdapterProductUpload(ArrayList<ModelProductUpload> list, SellerProductUploadActivity activity) {
        newAttachmentList = list;
        mActivity = activity;
    }

    @Override
    public AttachmentListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_uploadpicture, parent, false);

        AttachmentListViewHolder holder = new AttachmentListViewHolder(view, mActivity, newAttachmentList);

        return holder;
    }

    @Override
    public void onBindViewHolder(final AttachmentListViewHolder holder, int position) {
//        holder.attachedImageName.setText((newAttachmentList.get(position).getImageName()));

        String userImage = newAttachmentList.get(position).getImageID();



        if (userImage.isEmpty()||userImage.equals(null)||userImage.equals("")) {
        } else {
          Glide.with(mActivity)
                    .load(userImage)
                    .into(holder.attachedImageId);

        }
    }

    @Override
    public int getItemCount() {
        return newAttachmentList.size();
    }

    class AttachmentListViewHolder extends RecyclerView.ViewHolder {

        //        @BindView(R.id.attachedImageId)
        ImageView attachedImageId;
        //        @BindView(R.id.attachedImageName)
//        TextView attachedImageName;
//        @BindView(R.id.cancelAttachment)
        ImageView cancelAttachment;



        public AttachmentListViewHolder(View view, final Activity activity, final ArrayList<ModelProductUpload> attachmentList) {
            super(view);

//            ButterKnife.bind(this, view);
            attachedImageId = view.findViewById(R.id.attachedImageId);

//            attachedImageName = view.findViewById(R.id.attachedImageName);
            cancelAttachment = view.findViewById(R.id.cancelAttachment);


            cancelAttachment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = getAdapterPosition();
                    attachmentList.remove(pos);
                    notifyDataSetChanged();
                }
            });
        }
    }
}
