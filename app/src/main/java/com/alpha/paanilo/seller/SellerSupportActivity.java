package com.alpha.paanilo.seller;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import com.alpha.paanilo.R;

public class SellerSupportActivity extends AppCompatActivity implements View.OnClickListener {

    AppCompatImageView iv_back, iv_notification;
    View id_toolbar;
    AppCompatTextView tv_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller_support);

        id_toolbar = findViewById(R.id.id_toolbar);
        iv_back = id_toolbar.findViewById(R.id.id_back);
        iv_notification = id_toolbar.findViewById(R.id.id_notification);
        tv_title = id_toolbar.findViewById(R.id.tv_tittle);

        iv_back.setOnClickListener(this);
        tv_title.setText("Support");
        iv_notification.setVisibility(View.GONE);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.id_back:
                Intent intet = new Intent(com.alpha.paanilo.seller.SellerSupportActivity.this, SellerHomeActivity.class);
                startActivity(intet);
                finish();
                break;
        }

    }
}