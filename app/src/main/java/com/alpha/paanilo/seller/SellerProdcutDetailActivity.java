package com.alpha.paanilo.seller;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.alpha.paanilo.AllOrderModel;
import com.alpha.paanilo.FetchLocationActivity;
import com.alpha.paanilo.R;
import com.alpha.paanilo.authenticationModule.OTPActivity;
import com.alpha.paanilo.cart_pkg.showAddressModlePkg.deleteAddressPkg.DeleteAddressModle;
import com.alpha.paanilo.model.banner_pkg.BannerInfo;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.seller.adapter.AdapterSellerOrderDetail;
import com.alpha.paanilo.seller.model.openorder.OpenOrderPojo;
import com.alpha.paanilo.utility.Constants;
import com.alpha.paanilo.utility.CustomProgressbar;
import com.bumptech.glide.Glide;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import in.aabhasjindal.otptextview.OtpTextView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.FacebookSdk.getApplicationContext;

public class SellerProdcutDetailActivity extends AppCompatActivity implements View.OnClickListener {
    private static final int REQUEST_CODE_ASK_PERMISSIONS = 121;

    AppCompatImageView iv_back;
    View id_toolbar;
    AppCompatTextView tv_title;
    ImageView ivProductImage;
    TextView tvDistance, tvDeliverTime, tvOrderId, tvUserName, tvAdress, tvDate, tvOrderTAmount, tvQuantity, tvPaymentMethod;
    //    List<DatumOrderDetials> arraylist = new ArrayList<>();
//    AdapterHomeVendorAll adapterHomeVendorAll;
    RecyclerView recycler_viewdetails;
    Constants constants;
    String productImage, OrderDeliveryTime, productname, productuserid, productQuantity, producttotamount, productSaleid, productstatus, getDeliveryAddress, productdate, userfullname, OrderId, paymentmethod;
    AppCompatButton btn_accept, btn_reject;
    LinearLayout Latoutll;
    LinearLayout cardSpinner;
    String selectedItemstatus;
    Spinner spinnerstatus;
    ArrayAdapter spinnerArrayAdapter;
    AppCompatImageView id_notification;

    OtpTextView otp_view_Id;
    String[] status = {"Select status", "Out for Delivery","Complete"};
//    String[] Hours = {"Select Time", "10-15 Min", "30-1 Hour", "3-4 Hours"};

    //    String selectedItem = "10-15 Min";
    SimpleDateFormat format;
    Date today;
    String dateToStr;
    String distance, UserContactNo;
    //    Spinner spinnerstatustime;
//    LinearLayout cardSpinnertime;
    RelativeLayout tvCallLayout;
    TextView tv_phoneno;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller_prodcut_detail);
        init();

        id_toolbar = findViewById(R.id.id_toolbar);
        iv_back = id_toolbar.findViewById(R.id.id_back);
        id_notification = id_toolbar.findViewById(R.id.id_notification);
        tv_title = id_toolbar.findViewById(R.id.tv_tittle);

        iv_back.setOnClickListener(this);
        tv_title.setText("Product Details");

        id_notification.setVisibility(View.GONE);
    }

    private void init() {
        constants = new Constants(SellerProdcutDetailActivity.this);

        today = new Date();
        format = new SimpleDateFormat("hh:mm:ss a");

        dateToStr = format.format(today);
        System.out.println(dateToStr);

        cardSpinner = findViewById(R.id.cardSpinner);
        Latoutll = findViewById(R.id.Latoutll);
        btn_accept = findViewById(R.id.btn_accept);
        btn_reject = findViewById(R.id.btn_reject);
        spinnerstatus = findViewById(R.id.spinnerstatus);
        tvDistance = findViewById(R.id.tvDistance);

        tvDeliverTime = findViewById(R.id.tvDeliverTime);

        ivProductImage = findViewById(R.id.ivProductImage);
        tvOrderId = findViewById(R.id.tvOrderId);
        tvUserName = findViewById(R.id.tvUserName);
        tvAdress = findViewById(R.id.tvAdress);
        tvDate = findViewById(R.id.tvDate);

        tvCallLayout = findViewById(R.id.tvCallLayout);
        tv_phoneno = findViewById(R.id.tv_phoneno);

        tvOrderTAmount = findViewById(R.id.tvOrderTAmount);
        tvQuantity = findViewById(R.id.tvQuantity);
        tvPaymentMethod = findViewById(R.id.tvPaymentMethod);
//        cardSpinnertime = findViewById(R.id.cardSpinnertime);

//        spinnerstatustime = findViewById(R.id.spinnerstatustime);

        Intent intent = getIntent();
        OrderDeliveryTime = intent.getStringExtra("orderdeliverttime");
        distance = intent.getStringExtra("distance");
        UserContactNo = intent.getStringExtra("userno");
        productImage = intent.getStringExtra("productimage");
        productname = intent.getStringExtra("prodcutname");
        productQuantity = intent.getStringExtra("productquantity");
        producttotamount = intent.getStringExtra("producttotamount");
        productuserid = intent.getStringExtra("productuserid");
        productSaleid = intent.getStringExtra("productSaleid");
        productstatus = intent.getStringExtra("productstatus");
        getDeliveryAddress = intent.getStringExtra("getDeliveryAddress");

        productdate = intent.getStringExtra("productdate");
        userfullname = intent.getStringExtra("userfullname");

        OrderId = intent.getStringExtra("OrderId");
        paymentmethod = intent.getStringExtra("paymentmethod");


        if (OrderId.equalsIgnoreCase("null")) {

        } else {
            tvOrderId.setText(OrderId);
        }

        if (UserContactNo.equalsIgnoreCase("null")) {

        } else {
//            tv_phoneno.setText(UserContactNo);
        }

        if (productQuantity.equalsIgnoreCase("null")) {

        } else {
            tvQuantity.setText(productQuantity);
        }


        if (paymentmethod.equalsIgnoreCase("null")) {

        } else {
            tvPaymentMethod.setText(paymentmethod);
        }

        if (userfullname.equalsIgnoreCase("null")) {

        } else {
            tvUserName.setText(userfullname);
        }

        if (productdate.equalsIgnoreCase("null")) {

        } else {
            tvDate.setText(productdate);
        }

        if (getDeliveryAddress.equalsIgnoreCase("null")) {

        } else {
            tvAdress.setText(getDeliveryAddress);
        }


        if (distance.equalsIgnoreCase("null")) {
            tvDistance.setText("00 KM");
        } else {
            tvDistance.setText(distance);
        }


        if (OrderDeliveryTime.equalsIgnoreCase("null")) {
            tvDeliverTime.setText("00.00");
        } else {
            tvDeliverTime.setText(OrderDeliveryTime);
        }


        if (producttotamount.equalsIgnoreCase("null")) {

        } else {
            tvOrderTAmount.setText(producttotamount);
        }
        if (productImage.equalsIgnoreCase("null") || productImage == null) {

        } else {

            Glide.with(getApplicationContext())
                    .load(ApiClient.CATEGORY_PRODUCT_URL + productImage)
                    .placeholder(R.drawable.proimage)
                    .into(ivProductImage);
//            tvOrderTAmount.setText(producttotamount);
        }


        btn_accept.setOnClickListener(this);
        btn_reject.setOnClickListener(this);
        tvCallLayout.setOnClickListener(this);
        Log.e("productstatus", "productstatus openorderDetial:::" + productstatus);

        if (productstatus.equalsIgnoreCase("1")) {
            cardSpinner.setVisibility(View.VISIBLE);
            Latoutll.setVisibility(View.GONE);

//            cardSpinnertime.setVisibility(View.GONE);
//                            Toast.makeText(context, "Order Accepted Successfully", Toast.LENGTH_SHORT).show();
        } else if (productstatus.equalsIgnoreCase("2")) {
//                            Toast.makeText(context, "Order Complete Successfully", Toast.LENGTH_SHORT).show();
            cardSpinner.setVisibility(View.VISIBLE);

//            selectedItemstatus.se;
//            cardSpinnertime.setVisibility(View.GONE);
            Latoutll.setVisibility(View.GONE);

        } else if (productstatus.equalsIgnoreCase("3")) {
            //Toast.makeText(context, "Order Complete Successfully", Toast.LENGTH_SHORT).show();
            cardSpinner.setVisibility(View.VISIBLE);
            Latoutll.setVisibility(View.GONE);
//            cardSpinnertime.setVisibility(View.GONE);
        } else if (productstatus.equalsIgnoreCase("4")) {
            cardSpinner.setVisibility(View.VISIBLE);
            Latoutll.setVisibility(View.GONE);
//            cardSpinnertime.setVisibility(View.GONE);
            //Toast.makeText(context, "Order Complete Successfully", Toast.LENGTH_SHORT).show();
        } else if (productstatus.equalsIgnoreCase("0")) {
            //Toast.makeText(context, "Order Complete Successfully", Toast.LENGTH_SHORT).show();
            Latoutll.setVisibility(View.VISIBLE);
            cardSpinner.setVisibility(View.GONE);
        }


//        spinnerArrayAdapter = new ArrayAdapter(SellerProdcutDetailActivity.this, android.R.layout.simple_spinner_dropdown_item, Hours);
//        spinnerstatustime.setAdapter(spinnerArrayAdapter);


        /*spinnerstatustime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedItem = parent.getItemAtPosition(position).toString();
                if (selectedItem.equals("Select Time")) {
                    // do your stuff
//                    cardSpinnertime.setVisibility(View.INVISIBLE);
//                    btn_accept.setVisibility(View.GONE);
//                    btn_reject.setVisibility(View.GONE);

//                    btn_accept.setEnabled(false);
//                    btn_accept.setClickable(false);

                } else if (selectedItem.equals("10-15 Min")) {

                    spinnerArrayAdapter = new ArrayAdapter(SellerProdcutDetailActivity.this, android.R.layout.simple_spinner_dropdown_item, status);
                    spinnerstatus.setAdapter(spinnerArrayAdapter);


//                    selectedItemstatus="0"
//                    selectedItemstatus ="select status";
//                    Toast.makeText(getApplicationContext(), "selectedItemstatus ::" +selectedItemstatus, Toast.LENGTH_SHORT).show();
//position=0;
//                    status[0];
//                    status[0];
//                    cardSpinner.setVisibility(View.VISIBLE);
//                    selectedItemstatus="select status";
//                    Toast.makeText(getApplicationContext(), "Min", Toast.LENGTH_SHORT).show();
                    btn_accept.setEnabled(true);
                    btn_accept.setClickable(true);

//                if (selectedItem.equals("10-15 Min")) {
//                    // do your stuff
                } else if (selectedItem.equals("30-1 Hour")) {
                    spinnerArrayAdapter = new ArrayAdapter(SellerProdcutDetailActivity.this, android.R.layout.simple_spinner_dropdown_item, status);
                    spinnerstatus.setAdapter(spinnerArrayAdapter);

//                    selectedItemstatus="select status";

                    btn_accept.setEnabled(true);
                    btn_accept.setClickable(true);
//                    Toast.makeText(getApplicationContext(), "Hour", Toast.LENGTH_SHORT).show();

//                if (selectedItem.equals("10-15 Min")) {
//                    // do your stuff
                } else if (selectedItem.equals("3-4 Hours")) {
                    spinnerArrayAdapter = new ArrayAdapter(SellerProdcutDetailActivity.this, android.R.layout.simple_spinner_dropdown_item, status);
                    spinnerstatus.setAdapter(spinnerArrayAdapter);

//                    selectedItemstatus="select status";
                    btn_accept.setEnabled(true);
                    btn_accept.setClickable(true);
//                    Toast.makeText(getApplicationContext(), "Hours", Toast.LENGTH_SHORT).show();

//                if (selectedItem.equals("10-15 Min")) {
//                    // do your stuff
                }
//                if (selectedItem.equals("10-15 Min")) {
//                    // do your stuff
//                }
            }

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/


        spinnerArrayAdapter = new ArrayAdapter(SellerProdcutDetailActivity.this, android.R.layout.simple_spinner_dropdown_item, status);

        spinnerstatus.setAdapter(spinnerArrayAdapter);


        spinnerstatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedItemstatus = parent.getItemAtPosition(position).toString();
                if (position == 0) {
//                    updateOrderStatus(productSaleid, productuserid, "2");
                } else if (position == 1) {
                    Log.e("Ram", "ram::::" + position);
                    updateOrderStatus(productuserid, productSaleid, "2");

                    Latoutll.setVisibility(View.GONE);

                }/* else if (position == 2) {
                   Log.e("Ram", "ram::position:" + position);
                    updateOrderStatus(productuserid, productSaleid, "3");
                    Latoutll.setVisibility(View.GONE);
                    cardSpinner.setVisibility(View.GONE);

                }*/ else if (position == 2) {
                    Log.e("Ram", "position::position selectedItem:" + position);
                    otpScreen();
                }

            }

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.id_back:
                Intent intet = new Intent(SellerProdcutDetailActivity.this, SellerOpenOrderActivity.class);
                startActivity(intet);
                finish();
                break;
            case R.id.btn_accept:
//                Toast.makeText(this, "accept btn", Toast.LENGTH_SHORT).show();

                Log.e("Productid", "productSaleid::" + productSaleid);
                Log.e("Productid", "productuserid::" + productuserid);
//                Log.e("Productid", "productuserid:accept:" + selectedItem);

//                "" + selectedItem

//                updateOrderStatus(Userid, "" + saleid, "1", "" + selectedItem);
//                if (selectedItem.equals("10-15 Min") || selectedItem.equals("30-1 Hour") || selectedItem.equals("3-4 Hours")) {

                updateOrderStatus(productuserid, productSaleid, "1");
//                    cardSpinner.setVisibility(View.GONE);
                Latoutll.setVisibility(View.GONE);
//                    cardSpinnertime.setVisibility(View.GONE);

//                } else {
//                    Toast.makeText(this, "plz select time", Toast.LENGTH_SHORT).show();
//                }
                break;

            case R.id.btn_reject:

                log_out();

                break;
            case R.id.tvCallLayout:

                if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.CALL_PHONE},
                            REQUEST_CODE_ASK_PERMISSIONS);
                } else {
//                    System.out.println("phone no===" + tv_phoneno.getText().toString());
//                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + tv_phoneno.getText().toString()));
//                    startActivity(intent);

                    String number = UserContactNo;
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + number));
                    startActivity(callIntent);
                }
                break;


        }
    }


    private void updateOrderStatus(String user_id, String sale_id, String status) {
        CustomProgressbar.showProgressBar(SellerProdcutDetailActivity.this, false);

        Log.e("rahul", "user_id " + user_id);
        Log.e("rahul", "sale_id " + sale_id);
        Log.e("rahul", "status " + status);

//        Log.e("rahul", "order_delivery_time " + order_delivery_time);

        ApiClient.getClient().updateOrderStatus(user_id, sale_id, status).enqueue(new Callback<DeleteAddressModle>() {
            @Override
            public void onResponse(Call<DeleteAddressModle> call, Response<DeleteAddressModle> response) {

                Log.e("TAG", "onResponse: Update order " + new Gson().toJson(response.body()));
                CustomProgressbar.hideProgressBar();
//                pStatus.equals("");
                if (response.isSuccessful()) {

                    DeleteAddressModle addAddressResponseModle = response.body();
                    if (addAddressResponseModle.getStatus()) {
                        Toast.makeText(SellerProdcutDetailActivity.this, addAddressResponseModle.getMessage(), Toast.LENGTH_LONG).show();
                        //onBackPressed();
//                        getOPenOrders(constants.getUserID());

                    } else {
                        Toast.makeText(SellerProdcutDetailActivity.this, addAddressResponseModle.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    if (response.code() == 400) {
                        if (!false) {
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(response.errorBody().string());
                                CustomProgressbar.hideProgressBar();
                                String message = jsonObject.getString("message");
                                // Toast.makeText(SignupActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }

            }

            @Override
            public void onFailure(Call<DeleteAddressModle> call, Throwable t) {
                CustomProgressbar.hideProgressBar();
            }
        });


    }

    @Override
    public void onBackPressed() {
//        Toast.makeText(this, "now back", Toast.LENGTH_SHORT).show();

        Intent intet = new Intent(SellerProdcutDetailActivity.this, SellerOpenOrderActivity.class);
        startActivity(intet);
        finish();
        super.onBackPressed();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Call Permission Granted..Please dial again.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Call permission not granted", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void log_out() {
        final Dialog dialog = new Dialog(SellerProdcutDetailActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_cancelorder);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        ImageView ivlogoutClose = dialog.findViewById(R.id.ivlogoutCloseId);
        AppCompatImageView rllogout = dialog.findViewById(R.id.ivlogoutCloseId);
        CardView btn_logout = dialog.findViewById(R.id.btn_logout);

        ivlogoutClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateOrderStatus(productuserid, productSaleid, "3");
                Latoutll.setVisibility(View.GONE);
                dialog.dismiss();
            }
        });

        rllogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.cancel();
            }
        });

        dialog.show();
    }

    public void otpScreen() {
        final Dialog dialog = new Dialog(SellerProdcutDetailActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_otporder);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        ImageView ivlogoutClose = dialog.findViewById(R.id.ivlogoutCloseId);
        AppCompatImageView rllogout = dialog.findViewById(R.id.ivlogoutCloseId);
        otp_view_Id = dialog.findViewById(R.id.otp_view_Id);
        CardView btn_logout = dialog.findViewById(R.id.btn_logout);

        ivlogoutClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (otp_view_Id.getOTP().isEmpty() || otp_view_Id.getOTP().equals("") || otp_view_Id.getOTP().equals("null")) {
                    Toast.makeText(SellerProdcutDetailActivity.this, "Please Enter OTP ", Toast.LENGTH_SHORT).show();
                } else {
                    if (otp_view_Id.getOTP().equals("1234")) {
//                        Toast.makeText(SellerProdcutDetailActivity.this, " OTP ", Toast.LENGTH_SHORT).show();
//                        Intent intent = new Intent(SellerProdcutDetailActivity.this, FetchLocationActivity.class);
//                        startActivity(intent);

                        updateOrderStatus(productuserid, productSaleid, "4");
                        Latoutll.setVisibility(View.GONE);
                        cardSpinner.setVisibility(View.GONE);
                        dialog.dismiss();

                    }
                }


            }
        });

        rllogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.cancel();
            }
        });

        dialog.show();
    }


}