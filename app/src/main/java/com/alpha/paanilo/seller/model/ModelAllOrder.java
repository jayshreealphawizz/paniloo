package com.alpha.paanilo.seller.model;

public class ModelAllOrder {
    String uploadpicture ;

    public ModelAllOrder(String uploadpicture) {
        this.uploadpicture = uploadpicture;
    }

    public String getUploadpicture() {
        return uploadpicture;
    }

    public void setUploadpicture(String uploadpicture) {
        this.uploadpicture = uploadpicture;
    }
}
