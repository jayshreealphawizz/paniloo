package com.alpha.paanilo.seller;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alpha.paanilo.R;
import com.alpha.paanilo.SellerMyProdcutActiveModel;
import com.alpha.paanilo.SellerMyProdcutModel;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.seller.model.VerndorReportPojo;
import com.alpha.paanilo.utility.CheckNetwork;
import com.alpha.paanilo.utility.Constants;
import com.alpha.paanilo.utility.CustomProgressbar;
import com.alpha.paanilo.utility.CustomToast;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SellerMyProductActivity extends AppCompatActivity implements View.OnClickListener {
    AppCompatImageView iv_back, iv_notification;
    View id_toolbar;
    AppCompatTextView tv_title;
    ImageView ivProductImage;
    TextView tvProductName, tvMeasurement, tvProductPriceId, tvQuantity, tvDescription;
    Constants constants;
    CardView btn_editProduct ,btn_addproduct;
    String UserImage;
    List<String> imageProdcut;
    ArrayList<String> list = new ArrayList<>();
    SellerMyProdcutModel verndorReportPojo ;
    String parts ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller_my_product);
        init();
        id_toolbar = findViewById(R.id.id_toolbar);
        iv_back = id_toolbar.findViewById(R.id.id_back);
        iv_notification = id_toolbar.findViewById(R.id.id_notification);
        tv_title = id_toolbar.findViewById(R.id.tv_tittle);


        iv_notification.setVisibility(View.INVISIBLE);
//        iv_back.setOnClickListener(this);
        tv_title.setText("My Prodcuts");
        iv_back.setOnClickListener(this);
//        iv_notification.setOnClickListener(this);
    }

    public void init() {
        constants = new Constants(getApplicationContext());

        ivProductImage = findViewById(R.id.ivProductImage);
        tvProductName = findViewById(R.id.tvProductName);
        tvMeasurement = findViewById(R.id.tvMeasurement);
        tvProductPriceId = findViewById(R.id.tvProductPriceId);
        tvQuantity = findViewById(R.id.tvQuantity);
        tvDescription = findViewById(R.id.tvDescription);
        btn_editProduct = findViewById(R.id.btn_editProduct);
        btn_addproduct = findViewById(R.id.btn_addproduct);

        Log.e("prodcutimage ", "productimage" + ivProductImage);

//        constants.setProductMeasurement(vendorAddProdcutModel.getData().getUnit());
//        constants.setProductPrice(vendorAddProdcutModel.getData().getPrice());
//        constants.setProductQuantity(vendorAddProdcutModel.getData().getQuantity());
//        constants.setProductDest(vendorAddProdcutModel.getData().getProductDescription());
//        constants.setProductImage(vendorAddProdcutModel.getData().getProductImage());
        Log.e("UserImage", "data UserImage " + UserImage);
        Log.e("constants", "data UserImage " + constants.getUserID());
//        btn_editProduct.setVisibility(View.GONE);
        if (CheckNetwork.isNetAvailable(getApplicationContext())) {
            getMyProduct(constants.getUserID());
        } else {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.network), Toast.LENGTH_LONG).show();
        }


      /*  UserImage = constants.getProductImage();

        if (UserImage.equals("0")) {

        } else if (TextUtils.isEmpty(UserImage)) {

        } else {
            Glide.with(SellerMyProductActivity.this)
                    .load(ApiClient.USER_PROFILE_URL + UserImage)
                    .placeholder(R.drawable.ic_account_user)
                    .into(ivProductImage);
            Log.e("rahul::user Image: ", "rahul:dfsdf:: " + ApiClient.USER_PROFILE_URL + UserImage);
        }


        if (tvProductName != null) {
            tvProductName.setText(constants.getProductName());
        }

        if (tvMeasurement != null) {
            tvMeasurement.setText(constants.getProductMeasurement());
        }
        if (tvProductPriceId != null) {
            tvProductPriceId.setText(constants.getProductPrice());
        }

        if (tvQuantity != null) {
            tvQuantity.setText(constants.getProductQuantity());
        }
        if (tvDescription != null) {
            tvDescription.setText(constants.getProductDest());
        }*/
        btn_addproduct.setOnClickListener(this);
        btn_editProduct.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_editProduct:
                Intent intent = new Intent(SellerMyProductActivity.this, SellerProductUploadActivity.class);
                intent.putExtra("productid",  verndorReportPojo.getData().getProductId());
//                intent.putExtra("productname", verndorReportPojo.getData().getProductName());
//                intent.putExtra("productmes", verndorReportPojo.getData().getUnit());
                intent.putExtra("productprice", verndorReportPojo.getData().getPrice());
////                intent.putExtra("productqty", verndorReportPojo.getData().getQuantity());
                intent.putExtra("productdes",verndorReportPojo.getData().getProductDescription());
//                intent.putExtra("productimage",  parts);

//Log.e("proid" ,"proid" + verndorReportPojo.getData().getProductId());

//                Log.e("proid" ,"getPrice" + verndorReportPojo.getData().getPrice());
//                Log.e("proid" ,"getProductDescription" + verndorReportPojo.getData().getProductDescription());
                startActivity(intent);
                break;
            case R.id.id_back:
                Intent intet = new Intent(com.alpha.paanilo.seller.SellerMyProductActivity.this, SellerHomeActivity.class);
                startActivity(intet);
                finish();
                break;
            case R.id.btn_addproduct:
                Intent intenttt = new Intent(SellerMyProductActivity.this, SellerProductUploadActivity.class);
                startActivity(intenttt);
                finish();
                break;




        }
    }


    private void getMyProduct(String userId) {
        CustomProgressbar.showProgressBar(this, false);
        ApiClient.getClient().getMyProduct(userId).enqueue(new Callback<SellerMyProdcutModel>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<SellerMyProdcutModel> call, Response<SellerMyProdcutModel> response) {

                Log.e("TAG", "my Product onResponse: " + new Gson().toJson(response.body()));
                CustomProgressbar.hideProgressBar();

                 verndorReportPojo = response.body();

                Log.e("pro", "verndorReportPojo" + verndorReportPojo);
//                if(response.body()==null ){
//                    Log.e("pro", "verndorReportPojogggg");
//                    btn_editProduct.setVisibility(View.GONE);
//                }

                if (response.isSuccessful()) {
                    CustomProgressbar.hideProgressBar();

                    if (verndorReportPojo.getStatus() == true) {
                        Log.e("pro", "verndorReportPojo" + verndorReportPojo.getStatus());


//                        constants.setProductName(verndorReportPojo.getData().getProductName());

//                        tvProductName.setText(verndorReportPojo.getData().getProductName());
//                        tvMeasurement.setText(verndorReportPojo.getData().getUnit());
                        tvProductPriceId.setText(verndorReportPojo.getData().getPrice());
                       // tvQuantity.setText(verndorReportPojo.getData().getQuantity());
                        tvDescription.setText(verndorReportPojo.getData().getProductDescription());
                        Log.e("constants", "imgName" + verndorReportPojo.getData().getProductId());
                        Log.e("constants", "imgNameprice" + verndorReportPojo.getData().getPrice());
                        Log.e("constants", "imgNamedescription" + verndorReportPojo.getData().getProductDescription());


//                        btn_editProduct.setVisibility(View.VISIBLE);
//                       imageProdcut = verndorReportPojo.getData().getThumbnailsImage();
//                        Log.e("pro", "imgName" + verndorReportPojo.getData().getProductName());
                       if(verndorReportPojo.getData().getPrice().isEmpty() || verndorReportPojo.getData().getPrice().equals("") || verndorReportPojo.getData().getPrice().equals("null")) {
                           btn_addproduct.setVisibility(View.VISIBLE);
                           btn_editProduct.setVisibility(View.GONE);

                       }else{
                           btn_addproduct.setVisibility(View.GONE);
                           btn_editProduct.setVisibility(View.VISIBLE);
                       }
//                        if (verndorReportPojo.getData().getThumbnailsImage().isEmpty()) {
//                        } else {
//                            String imgName = (verndorReportPojo.getData().getThumbnailsImage()).substring((verndorReportPojo.getData().getThumbnailsImage().lastIndexOf(",") + 1));
//                            Log.e("pro", "imgName" + imgName);
//                            Glide.with(getApplicationContext())
//                                    .load(ApiClient.CATEGORY_PRODUCT_URL + imgName)
//                                    .into(ivProductImage);
////                                productimageID
//                        }


                       /* Log.e("pro", "pro" +verndorReportPojo.getData().getThumbnailsImage());
                        if (verndorReportPojo.getData().getThumbnailsImage().isEmpty()) {
                        } else {
//                            String imgName = verndorReportPojo.getData().getThumbnailsImage().substring((verndorReportPojo.getData().getThumbnailsImage().lastIndexOf(",") + 1));
                            List<String> string = verndorReportPojo.getData().getThumbnailsImage();
//
                            Log.e("pro", "pro" +string);

//                            Log.e("pro", "imgName" + imgName);
                             parts = string.set(0,string.get(0));
//                            String part1 = parts[0];
                            Log.e("pro", "parts:::" +parts);

                            Glide.with(getApplicationContext())
                                    .load(ApiClient.CATEGORY_PRODUCT_URL + parts)
                                    .placeholder(R.drawable.ic_account_user)
                                    .into(ivProductImage);
//                                productimageID
                        }*/

//                        if (verndorReportPojo.getData().getThumbnailsImage() == null) {
//                            Log.e("Null ", "Null::");
//                        } else {
//                            List<String> string = verndorReportPojo.getData().getThumbnailsImage();
//                            Log.e("rahul ", "string::" + string);
//                            String[] parts = string.split(",");
//                            String part1 = parts[0]; // 004
////                            String part2 = parts[1];
//                            Log.e("rahul ", "part1 Time::" + part1);
//                            list.add("" + part1);
////                            list.add("" + part2);
////                            list = new
//                            Log.e("rahullist ", "part1 list::" + list);
////                            mViewPagerAdapter = new AdapterHomeFirst(getContext(), list);
////                            vpHomeProduct.setAdapter(mViewPagerAdapter);
////                            tl_HomeProdcut.setupWithViewPager(vpHomeProduct);
////                            Log.e("rahul ", "part2 Time::" + part2);
//                        }

//                        Log.e("imageProdcut", "imageProdcut" + imageProdcut);
//                        List<Object> result1 = Arrays.stream(imageProdcut.spliterator(","))
//                                .collect(Collectors.toList());
//                        Log.e("imageProdcut", "result1" + result1);
//                        Glide.with(getApplicationContext())
//                                .load(ApiClient.CATEGORY_PRODUCT_URL + result1)
//                                .placeholder(R.drawable.ic_account_user)
//                                .into(ivProductImage);



//                        16176922281617686285773.jpg


//                        Log.e("bana::::", "bana");
//                        tvTOrder.setText("" + verndorReportPojo.getData().getTotalOrders());
//                        tvCOrder.setText("" + verndorReportPojo.getData().getCancelOrders());
//                        tvTEMoney.setText("Rs." + verndorReportPojo.getData().getTotalEarnMoney());


                    } else {
                        Toast.makeText(SellerMyProductActivity.this, "" + verndorReportPojo.getMessage(), Toast.LENGTH_SHORT).show();

                    }


                } else {
                    if (response.code() == 400) {
                        if (!false) {
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(response.errorBody().string());
                                CustomProgressbar.hideProgressBar();
                                String message = jsonObject.getString("message");
                                // Toast.makeText(SignupActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }

            }

            @Override
            public void onFailure(Call<SellerMyProdcutModel> call, Throwable t) {
                CustomProgressbar.hideProgressBar();
            }
        });

    }

    private void getProductactive(String product_id ,String userId,String status) {
        CustomProgressbar.showProgressBar(this, false);
        ApiClient.getClient().getProductactive(product_id,userId ,status).enqueue(new Callback<SellerMyProdcutActiveModel>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<SellerMyProdcutActiveModel> call, Response<SellerMyProdcutActiveModel> response) {

                Log.e("TAG", "my Product onResponse: " + new Gson().toJson(response.body()));
                CustomProgressbar.hideProgressBar();

                SellerMyProdcutActiveModel    verndorReportPojo = response.body();

                Log.e("pro", "verndorReportPojo" + verndorReportPojo);
//                if(response.body()==null ){
//                    Log.e("pro", "verndorReportPojogggg");
//                    btn_editProduct.setVisibility(View.GONE);
//                }

                if (response.isSuccessful()) {
                    CustomProgressbar.hideProgressBar();

                    if (verndorReportPojo.getStatus() == true) {
                        Log.e("pro", "verndorReportPojo" + verndorReportPojo.getStatus());


//                        constants.setProductName(verndorReportPojo.getData().getProductName());

//                        tvProductName.setText(verndorReportPojo.getData().getProductName());
//                        tvMeasurement.setText(verndorReportPojo.getData().getUnit());
//                        tvProductPriceId.setText(verndorReportPojo.getData().getPrice());
                        // tvQuantity.setText(verndorReportPojo.getData().getQuantity());
//                        tvDescription.setText(verndorReportPojo.getData().getProductDescription());


//                        imageProdcut = verndorReportPojo.getData().getThumbnailsImage();
//                        Log.e("pro", "imgName" + verndorReportPojo.getData().getProductName());
//                       if(verndorReportPojo.getData().getProductName().isEmpty() || verndorReportPojo.getData().getProductName().equals("") || verndorReportPojo.getData().getProductName().equals("null")) {
//                           btn_editProduct.setVisibility(View.GONE);
//                       }else{
//                           btn_editProduct.setVisibility(View.VISIBLE);
//                       }
//                        if (verndorReportPojo.getData().getThumbnailsImage().isEmpty()) {
//                        } else {
//                            String imgName = (verndorReportPojo.getData().getThumbnailsImage()).substring((verndorReportPojo.getData().getThumbnailsImage().lastIndexOf(",") + 1));
//                            Log.e("pro", "imgName" + imgName);
//                            Glide.with(getApplicationContext())
//                                    .load(ApiClient.CATEGORY_PRODUCT_URL + imgName)
//                                    .into(ivProductImage);
////                                productimageID
//                        }


                       /* Log.e("pro", "pro" +verndorReportPojo.getData().getThumbnailsImage());
                        if (verndorReportPojo.getData().getThumbnailsImage().isEmpty()) {
                        } else {
//                            String imgName = verndorReportPojo.getData().getThumbnailsImage().substring((verndorReportPojo.getData().getThumbnailsImage().lastIndexOf(",") + 1));
                            List<String> string = verndorReportPojo.getData().getThumbnailsImage();
//
                            Log.e("pro", "pro" +string);

//                            Log.e("pro", "imgName" + imgName);
                             parts = string.set(0,string.get(0));
//                            String part1 = parts[0];
                            Log.e("pro", "parts:::" +parts);

                            Glide.with(getApplicationContext())
                                    .load(ApiClient.CATEGORY_PRODUCT_URL + parts)
                                    .placeholder(R.drawable.ic_account_user)
                                    .into(ivProductImage);
//                                productimageID
                        }*/

//                        if (verndorReportPojo.getData().getThumbnailsImage() == null) {
//                            Log.e("Null ", "Null::");
//                        } else {
//                            List<String> string = verndorReportPojo.getData().getThumbnailsImage();
//                            Log.e("rahul ", "string::" + string);
//                            String[] parts = string.split(",");
//                            String part1 = parts[0]; // 004
////                            String part2 = parts[1];
//                            Log.e("rahul ", "part1 Time::" + part1);
//                            list.add("" + part1);
////                            list.add("" + part2);
////                            list = new
//                            Log.e("rahullist ", "part1 list::" + list);
////                            mViewPagerAdapter = new AdapterHomeFirst(getContext(), list);
////                            vpHomeProduct.setAdapter(mViewPagerAdapter);
////                            tl_HomeProdcut.setupWithViewPager(vpHomeProduct);
////                            Log.e("rahul ", "part2 Time::" + part2);
//                        }

//                        Log.e("imageProdcut", "imageProdcut" + imageProdcut);
//                        List<Object> result1 = Arrays.stream(imageProdcut.spliterator(","))
//                                .collect(Collectors.toList());
//                        Log.e("imageProdcut", "result1" + result1);
//                        Glide.with(getApplicationContext())
//                                .load(ApiClient.CATEGORY_PRODUCT_URL + result1)
//                                .placeholder(R.drawable.ic_account_user)
//                                .into(ivProductImage);



//                        16176922281617686285773.jpg


//                        Log.e("bana::::", "bana");
//                        tvTOrder.setText("" + verndorReportPojo.getData().getTotalOrders());
//                        tvCOrder.setText("" + verndorReportPojo.getData().getCancelOrders());
//                        tvTEMoney.setText("Rs." + verndorReportPojo.getData().getTotalEarnMoney());
                    } else {
                        Toast.makeText(SellerMyProductActivity.this, "" + verndorReportPojo.getMessage(), Toast.LENGTH_SHORT).show();

                    }


                } else {
                    if (response.code() == 400) {
                        if (!false) {
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(response.errorBody().string());
                                CustomProgressbar.hideProgressBar();
                                String message = jsonObject.getString("message");
                                // Toast.makeText(SignupActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }

            }

            @Override
            public void onFailure(Call<SellerMyProdcutActiveModel> call, Throwable t) {
                CustomProgressbar.hideProgressBar();
            }
        });

    }



}