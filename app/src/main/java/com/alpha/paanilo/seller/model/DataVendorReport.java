package com.alpha.paanilo.seller.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataVendorReport {
    @SerializedName("total_orders")
    @Expose
    private Integer totalOrders;
    @SerializedName("cancel_orders")
    @Expose
    private Integer cancelOrders;
    @SerializedName("total_earn_money")
    @Expose
    private String totalEarnMoney;

    public Integer getTotalOrders() {
        return totalOrders;
    }

    public void setTotalOrders(Integer totalOrders) {
        this.totalOrders = totalOrders;
    }

    public Integer getCancelOrders() {
        return cancelOrders;
    }

    public void setCancelOrders(Integer cancelOrders) {
        this.cancelOrders = cancelOrders;
    }

    public String getTotalEarnMoney() {
        return totalEarnMoney;
    }

    public void setTotalEarnMoney(String totalEarnMoney) {
        this.totalEarnMoney = totalEarnMoney;
    }

}
