package com.alpha.paanilo.seller;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.alpha.paanilo.HomeMainActivity;
import com.alpha.paanilo.MPremission.PermissionResultCallBack;
import com.alpha.paanilo.MPremission.PermissionUtils;
import com.alpha.paanilo.R;
import com.alpha.paanilo.utility.Constants;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class FetchLocationActivitySeller extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, PermissionResultCallBack, LocationListener {

    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    PendingResult<LocationSettingsResult> result;
    final static int REQUEST_LOCATION = 199;

    ProgressDialog pd;
    View view_toolbar;
    TextView iv_locationtext;
    String currentLocation;
    LocationManager locationManager;
    ImageView iv_back;

    PermissionUtils permissionUtils;
    public ArrayList<String> alForPermission;
    private static final int REQUEST_CODE = 200;
    Constants constants;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.location_fetch);
        iv_locationtext = findViewById(R.id.iv_locationtext);
        constants = new Constants(getApplicationContext());
        permissionUtils = new PermissionUtils(this);


        // for device model check===================
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            allPermission();
        } else {
            mGoogleApiClient = new GoogleApiClient.Builder(this).
                    addApi(LocationServices.API).
                    addConnectionCallbacks(this).
                    addOnConnectionFailedListener(this).build();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!checkPlayServices()) {
            Toast.makeText(FetchLocationActivitySeller.this, "You need to install Google Play Services to use the App properly", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        // stop location updates
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST);
            } else {
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("onActivityResult()", Integer.toString(resultCode));

        switch (requestCode) {
            case REQUEST_LOCATION:
                switch (resultCode) {
                    case Activity.RESULT_OK: {
                        System.out.println("ok callllllllllll");
                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        Location MobLoc = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                        try {

                            System.out.println(MobLoc.getLatitude() + "address=====" + MobLoc.getLongitude());
                            Geocoder geocoder;
                            List<Address> addresses;
                            geocoder = new Geocoder(this, Locale.getDefault());

                            try {
                                if (MobLoc != null) {
                                    addresses = geocoder.getFromLocation(MobLoc.getLatitude(), MobLoc.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                                    String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
//                                  String city = addresses.get(0).getLocality();
                                    String state = addresses.get(0).getAdminArea();
                                    String country = addresses.get(0).getCountryName();
                                    String postalCode = addresses.get(0).getPostalCode();
                                    String knownName = addresses.get(0).getFeatureName();


                                    String currentLocation = address;
                                    String city = addresses.get(0).getLocality() + "," + state;

                                    System.out.println(city + "address=====" + currentLocation);
                                    constants.setCurrentLocation(currentLocation);
                                    constants.setCity(city);
                                    constants.setLatitude(String.valueOf(addresses.get(0).getLatitude()));
                                    constants.setLongitude(String.valueOf(addresses.get(0).getLongitude()));

                                } else {
                                }

                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } catch (Exception e) {
                            allPermission();
                            try {
                                final Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        startActivity(new Intent(FetchLocationActivitySeller.this, SellerHomeActivity.class));
                                        finish();
                                    }
                                }, 2000);
                            } catch (Exception et) {
                                et.printStackTrace();
                            }
                            // Toast.makeText(this, "Location not fetch, Please click on try again!", Toast.LENGTH_SHORT).show();
                        }

                        // All required changes were successfully made
//                        Toast.makeText(FetchLocationActivity.this, "Location enabled by user!", Toast.LENGTH_LONG).show();
                        break;
                    }
                    case Activity.RESULT_CANCELED: {
                        System.out.println("cancel callllllllllll");

                        // The user was asked to change settings, but chose not to
                       // Toast.makeText(FetchLocationActivity.this, "Location not enabled, user cancelled.", Toast.LENGTH_LONG).show();
                        break;
                    }
                    default: {

                        System.out.println("defalut callllllllllll");
                        break;
                    }
                }
                break;
        }
    }

    public void callNextScreenIntent(String currentLocation, double latitude, double longitude) {
        System.out.println("====else call 1345======" + currentLocation);
        if (currentLocation.equals("")) {
            //   try_btn.setVisibility(View.VISIBLE);
        } else {
            System.out.println("====else call 1345======");
//            if (settingsMain.getUserLogin().equals("yes")) {
            startActivity(new Intent(FetchLocationActivitySeller.this, SellerHomeActivity.class));
            finish();
//            } else {
//                startActivity(new Intent(FetchLocationActivity.this, EnterMobile_Activity.class));
//                finish();
//            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(2 * 1000);
        mLocationRequest.setFastestInterval(4 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);
        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                //final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:

                        try {
                            if (ActivityCompat.checkSelfPermission(FetchLocationActivitySeller.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(FetchLocationActivitySeller.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling
                                //    ActivityCompat#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for ActivityCompat#requestPermissions for more details.
                                return;
                            }
                            Location MobLoc = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                            List<Address> addresses;
                            Geocoder geocoder;


                            geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                            if (MobLoc != null) {
                                addresses = geocoder.getFromLocation(MobLoc.getLatitude(), MobLoc.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
//                              String city = addresses.get(0).getLocality();
                                String state = addresses.get(0).getAdminArea();
                                String country = addresses.get(0).getCountryName();
                                String postalCode = addresses.get(0).getPostalCode();
                                String knownName = addresses.get(0).getFeatureName();
                                String currentLocation = address;
                                String city = addresses.get(0).getLocality() + "," + state;
                                constants.setCurrentLocation(currentLocation);
                                constants.setCity(city);
                                constants.setLatitude(String.valueOf(addresses.get(0).getLatitude()));
                                constants.setLongitude(String.valueOf(addresses.get(0).getLongitude()));

                                try {
                                    final Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            startActivity(new Intent(FetchLocationActivitySeller.this, SellerHomeActivity.class));
                                            finish();
                                        }
                                    }, 2000);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            else {

                                try {
                                    final Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            startActivity(new Intent(FetchLocationActivitySeller.this, SellerHomeActivity.class));
                                            finish();
                                        }
                                    }, 2000);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                        } catch (IOException e) {

                            e.printStackTrace();
                        }

                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        //...
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(
                                    FetchLocationActivitySeller.this,
                                    REQUEST_LOCATION);

                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        //...

                        break;
                }
            }
        });
    }

    public void allPermission() {
        alForPermission = new ArrayList<>();
        alForPermission.add(ACCESS_FINE_LOCATION);
        alForPermission.add(ACCESS_COARSE_LOCATION);
        permissionUtils.check_permission(alForPermission, getString(R.string.premission_des), REQUEST_CODE);
    }

    @Override
    public void PermissionGranted(int request_code) {
        mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(FetchLocationActivitySeller.this)
                .addOnConnectionFailedListener(FetchLocationActivitySeller.this).build();
        /// mGoogleApiClient.connect();
    }

    @Override
    public void PartialPermissionGranted(int request_code, ArrayList<String> granted_permissions) {
    }

    @Override
    public void PermissionDenied(int request_code) {
        allPermission();
    }

    @Override
    public void NeverAskAgain(int request_code) {
        allPermission();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        try {
            permissionUtils.onRequestPermissionsResult(requestCode, permissions, grantResults);
            if (requestCode == 200) {
                switch (grantResults[0]) {
                    // Toast.makeText(getApplicationContext(), "Permission required", Toast.LENGTH_LONG).show();
                    case Activity.RESULT_CANCELED: {
                        try {
                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    startActivity(new Intent(FetchLocationActivitySeller.this, SellerHomeActivity.class));
                                    finish();
                                }
                            }, 3000);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    }
                    default: {
                        allPermission();
                        break;
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onConnectionSuspended(int i) {
        ///try_btn.setVisibility(View.VISIBLE);

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        //  try_btn.setVisibility(View.VISIBLE);

    }

    @Override
    public void onLocationChanged(Location location) {

    }
}
