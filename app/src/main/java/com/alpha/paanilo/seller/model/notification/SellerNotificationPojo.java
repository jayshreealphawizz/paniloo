
package com.alpha.paanilo.seller.model.notification;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SellerNotificationPojo {

//    @SerializedName("status")
//    @Expose
//    private Boolean status;
//    @SerializedName("message")
//    @Expose
//    private String message;
//    @SerializedName("data")
//    @Expose
//    private List<SellerNotificationDatum> data = null;
//
//    public Boolean getStatus() {
//        return status;
//    }
//
//    public void setStatus(Boolean status) {
//        this.status = status;
//    }
//
//    public String getMessage() {
//        return message;
//    }
//
//    public void setMessage(String message) {
//        this.message = message;
//    }
//
//    public List<SellerNotificationDatum> getData() {
//        return data;
//    }
//
//    public void setData(List<SellerNotificationDatum> data) {
//        this.data = data;
//    }

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    @SerializedName("count")
    @Expose
    private String count;


    @SerializedName("data")
    @Expose
    private List<SellerNotificationDatum> data = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<SellerNotificationDatum> getData() {
        return data;
    }

    public void setData(List<SellerNotificationDatum> data) {
        this.data = data;
    }

}
