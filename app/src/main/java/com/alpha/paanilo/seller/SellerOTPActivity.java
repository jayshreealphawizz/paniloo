package com.alpha.paanilo.seller;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.cardview.widget.CardView;

import com.alpha.paanilo.R;
import com.alpha.paanilo.model.vendor_auth_pkg.OtpVerficationRes;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.seller.model.VerndorReportPojo;
import com.alpha.paanilo.seller.model.notification.SellerNotificationPojo;
import com.alpha.paanilo.utility.Constants;
import com.alpha.paanilo.utility.CustomProgressbar;
import com.google.gson.Gson;

import in.aabhasjindal.otptextview.OtpTextView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SellerOTPActivity extends AppCompatActivity implements View.OnClickListener {
    CardView btn_confirm;

    OtpTextView otp_view_Id;
    ProgressDialog pd;
    //    Constants constants;
    TextView tvEnterOtp;
    AppCompatImageView ivBackForgetId;
    private Constants constants;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.seller_activity_o_t_p);

        constants = new Constants(getApplicationContext());
        Log.e("otp get ", "otp::: " + constants.getOTP());

        Log.e("Mobile:::: ", "Mobile::: " + constants.getMobile());

        btn_confirm = findViewById(R.id.btn_confirm);
        otp_view_Id = findViewById(R.id.otp_view_Id);
        tvEnterOtp = findViewById(R.id.tvEnterOtp);
        ivBackForgetId = findViewById(R.id.ivBackForgetId);

//        tvEnterOtp.setText("Get Opt On this Number " + constants.getMobile());

//        otpVerficaton();
        btn_confirm.setOnClickListener(this);
        ivBackForgetId.setOnClickListener(this);

        pd = new ProgressDialog(com.alpha.paanilo.seller.SellerOTPActivity.this, R.style.AppCompatAlertDialogStyle);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_confirm:


                if (otp_view_Id.getOTP().isEmpty() || otp_view_Id.getOTP().equals("") || otp_view_Id.getOTP().equals("null")) {
                    Toast.makeText(this, "Please Enter OTP ", Toast.LENGTH_SHORT).show();
                } else {
                    AdminApprovalDialog();
                }

////                Log.e("Ram ", "Ram11111 ::::: " + otp_view_Id.getOTP());
//                if (otp_view_Id.getOTP().isEmpty() || otp_view_Id.getOTP().equals("") || otp_view_Id.getOTP().equals("null")) {
//                    Toast.makeText(this, "Please Enter OTP ", Toast.LENGTH_SHORT).show();
//                } else {
//                    if (otp_view_Id.getOTP().equals("1234")) {
//////                        Toast.makeText(this, " OTP ", Toast.LENGTH_SHORT).show();
//                        Intent intent = new Intent(SellerOTPActivity.this, FetchLocationActivity.class);
//                        startActivity(intent);
//                    }
//                }
////                    else if (!otp_view_Id.getOTP().equals(constants.getOTP())) {
////                        Toast.makeText(this, "Please Enter Valid OTP ", Toast.LENGTH_SHORT).show();
////                    } else {
////
////                        Toast.makeText(this, "Success otp", Toast.LENGTH_SHORT).show();
//////                        Intent intent = new Intent(OTPActivity.this, FetchLocationActivity.class);
//////                        startActivity(intent);
////                    }
////                }
                break;

            case R.id.ivBackForgetId:
                Intent intet = new Intent(com.alpha.paanilo.seller.SellerOTPActivity.this, Seller_SignUp_Activity.class);
                constants.setIsLogin("");
                startActivity(intet);
                finish();
                break;
        }
    }

    private void AdminApprovalDialog() {
        final Dialog dialog = new Dialog(com.alpha.paanilo.seller.SellerOTPActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.seller_dialog_admin_approval);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        ImageView iv_close = dialog.findViewById(R.id.iv_close);
//        AppCompatImageView rllogout = dialog.findViewById(R.id.ivlogoutCloseId);
//        Button btn_logout = dialog.findViewById(R.id.btn_logout);


//        final Dialog dialog = new Dialog(SellerOTPActivity.this, R.style.Theme_Dialog);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setCancelable(false);

//        dialog.setContentView(R.layout.seller_dialog_admin_approval);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        CardView cv_approval = dialog.findViewById(R.id.cv_approval);
//        ImageView iv_close = dialog.findViewById(R.id.iv_close);


        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                dialog.cancel();
                dialog.dismiss();
                Intent intent = new Intent(com.alpha.paanilo.seller.SellerOTPActivity.this, SellerLoginActivity.class);
                startActivity(intent);

            }
        });

        dialog.show();

    }


    public void otpVerficaton() {
        pd.setMessage("Loading");
        pd.show();
        (ApiClient.getClient().otpVerfication(constants.getOTP(), constants.getMobile())).enqueue(new Callback<OtpVerficationRes>() {
            @Override
            public void onResponse(Call<OtpVerficationRes> call, Response<OtpVerficationRes> response) {
                pd.dismiss();
                if (response.isSuccessful()) {
                    try {
                        Log.e("TAG", "OTp response : " + new Gson().toJson(response.body()));
                        OtpVerficationRes userInfo_pojo = response.body();
                        Boolean repmsg = userInfo_pojo.getStatus();
                        String message = userInfo_pojo.getMessage();
                        if (repmsg) {
                            Toast.makeText(com.alpha.paanilo.seller.SellerOTPActivity.this, message, Toast.LENGTH_SHORT).show();
                         /*   constants.setIsLogin("yes");
                            constants.setUserID(userInfo_pojo.getData().getUserId());
                            constants.setUsername(userInfo_pojo.getData().getUserFullname());
                            constants.setEmail(userInfo_pojo.getData().getUserEmail());
                            constants.setCity(userInfo_pojo.getData().getAddress());
                            constants.setImage(userInfo_pojo.getData().getUserImage());
//                          constants.setOTP(userInfo_pojo.getData().getOtp());
                            Log.e("Login :: ", "UserID :::: " + userInfo_pojo.getData().getUserId());
                            Log.e("Login :: ", "UserFull Name :::: " + userInfo_pojo.getData().getUserFullname());
                            Log.e("Login :: ", "UserEmail :::: " + userInfo_pojo.getData().getUserEmail());
                            Log.e("Login :: ", "Address :::: " + userInfo_pojo.getData().getAddress());
                            Log.e("Login :: ", "UserImage :::: " + userInfo_pojo.getData().getUserImage());
                            Log.e("Login :: ", "OOOO :::: " + userInfo_pojo.getData().getOtp());
*/
//                            startActivity(new Intent(OTPActivity.this, FetchLocationActivity.class)
//                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
//                            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
//                            finish();

                        } else {
                            Toast.makeText(com.alpha.paanilo.seller.SellerOTPActivity.this, message, Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                }


            }


            @Override
            public void onFailure(Call<OtpVerficationRes> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(com.alpha.paanilo.seller.SellerOTPActivity.this, getString(R.string.server_errors), Toast.LENGTH_SHORT).show();


            }
        });
    }


    private void otpVendorVarification(String otp) {
        CustomProgressbar.showProgressBar(this, false);
        ApiClient.getClient().otpVendorVarification(otp).enqueue(new Callback<VerndorReportPojo>() {
            @Override
            public void onResponse(Call<VerndorReportPojo> call, Response<VerndorReportPojo> response) {

                Log.e("TAG", "OTP onResponse: " + new Gson().toJson(response.body()));
                CustomProgressbar.hideProgressBar();
               /* if (response.isSuccessful()) {

                    OpenOrderPojo sellerNotificationPojo = response.body();
                    if (sellerNotificationPojo.getStatus()) {
                        arrayList = sellerNotificationPojo.getData();
                        adapterpicture.notifyDataSetChanged();
                        tv_notfound.setVisibility(View.GONE);
                        recyclerOpenOrder.setVisibility(View.VISIBLE);
                    } else {
                        //  Toast.makeText(getActivity(), addAddressResponseModle.getMessage(), Toast.LENGTH_LONG).show();
                        tv_notfound.setVisibility(View.VISIBLE);
                        recyclerOpenOrder.setVisibility(View.GONE);
                    }
                } else {

                    tv_notfound.setVisibility(View.VISIBLE);
                    recyclerOpenOrder.setVisibility(View.GONE);
                    if (response.code() == 400) {
                        if (!false) {
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(response.errorBody().string());
                                CustomProgressbar.hideProgressBar();
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }*/
            }

            @Override
            public void onFailure(Call<VerndorReportPojo> call, Throwable t) {
                CustomProgressbar.hideProgressBar();
            }
        });

    }


}

    /*private void otpVerifcation() {
        pd.setMessage("Loading");
        pd.show();
        (ApiClient.getClient().otpVerfication(constants.getOTP(), constants.getMobile()).enqueue(new Callback<LoginUserPojo>() {
            @Override
            public void onResponse(Call<LoginUserPojo> call, Response<LoginUserPojo> response) {
                pd.dismiss();
                if (response.isSuccessful()) {
                    try {
                        Log.e("TAG", "OTp response : " + new Gson().toJson(response.body()));
                        LoginUserPojo userInfo_pojo = response.body();
                        Boolean repmsg = userInfo_pojo.getStatus();
                        String message = userInfo_pojo.getMessage();

                        if (repmsg) {
                            Toast.makeText(OTPActivity.this, message, Toast.LENGTH_SHORT).show();

                            constants.setIsLogin("yes");
                            constants.setUserID(userInfo_pojo.getData().getUserId());
                            constants.setUsername(userInfo_pojo.getData().getUserFullname());
                            constants.setEmail(userInfo_pojo.getData().getUserEmail());

                            constants.setCity(userInfo_pojo.getData().getAddress());
                            constants.setImage(userInfo_pojo.getData().getUserImage());
//                               constants.setOTP(userInfo_pojo.getData().getOtp());

                            Log.e("Login :: ", "UserID :::: " + userInfo_pojo.getData().getUserId());
                            Log.e("Login :: ", "UserFull Name :::: " + userInfo_pojo.getData().getUserFullname());
                            Log.e("Login :: ", "UserEmail :::: " + userInfo_pojo.getData().getUserEmail());
                            Log.e("Login :: ", "Address :::: " + userInfo_pojo.getData().getAddress());

                            Log.e("Login :: ", "UserImage :::: " + userInfo_pojo.getData().getUserImage());
                            Log.e("Login :: ", "OOOO :::: " + userInfo_pojo.getData().getOtp());

                            startActivity(new Intent(OTPActivity.this, FetchLocationActivity.class)
                                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                            finish();

                        } else {
                            Toast.makeText(OTPActivity.this, message, Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<LoginUserPojo> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(OTPActivity.this, getString(R.string.server_errors), Toast.LENGTH_SHORT).show();
            }
        });
    }*/












