package com.alpha.paanilo.seller.model;

public class ModelSellerEarning {
    String uploadpicture ;

    public ModelSellerEarning(String uploadpicture) {
        this.uploadpicture = uploadpicture;
    }

    public String getUploadpicture() {
        return uploadpicture;
    }

    public void setUploadpicture(String uploadpicture) {
        this.uploadpicture = uploadpicture;
    }
}