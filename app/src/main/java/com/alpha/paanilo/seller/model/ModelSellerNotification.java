package com.alpha.paanilo.seller.model;

public class ModelSellerNotification {
    String uploadpicture ;

    public ModelSellerNotification(String uploadpicture) {
        this.uploadpicture = uploadpicture;
    }

    public String getUploadpicture() {
        return uploadpicture;
    }

    public void setUploadpicture(String uploadpicture) {
        this.uploadpicture = uploadpicture;
    }
}

