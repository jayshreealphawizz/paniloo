package com.alpha.paanilo.seller;

import com.alpha.paanilo.StallPkg.stallProfilePkg.DataVendor;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VendorLoginModel {
//    @SerializedName("status")
//    @Expose
//    private Boolean status;
//    @SerializedName("message")
//    @Expose
//    private String message;
//    @SerializedName("data")
//    @Expose
//    private DataVendorLogin data;
//
//    public Boolean getStatus() {
//        return status;
//    }
//
//    public void setStatus(Boolean status) {
//        this.status = status;
//    }
//
//    public String getMessage() {
//        return message;
//    }
//
//    public void setMessage(String message) {
//        this.message = message;
//    }
//
//    public DataVendorLogin getData() {
//        return data;
//    }
//
//    public void setData(DataVendorLogin data) {
//        this.data = data;
//    }


    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private DataVendorLogin data;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataVendorLogin getData() {
        return data;
    }

    public void setData(DataVendorLogin data) {
        this.data = data;
    }
}
