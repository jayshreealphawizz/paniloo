package com.alpha.paanilo.seller.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.alpha.paanilo.R;
import com.alpha.paanilo.retrofit.ApiClient;
import com.bumptech.glide.Glide;

import java.util.List;
import java.util.Objects;

public class AdapterHomeFirst extends PagerAdapter {
    Context context;
    List<String> images;
    LayoutInflater mLayoutInflater;

    public AdapterHomeFirst(Context context, List<String> images) {
        this.context = context;
        this.images = images;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {

        return images.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((LinearLayout) object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {

        View itemView = mLayoutInflater.inflate(R.layout.row_seller_acceptorder, container, false);


        ImageView imageView = (ImageView) itemView.findViewById(R.id.imageViewProdcut);

//        String imgName = datum.getProductImage().substring((datum.getProductImage().lastIndexOf(",") + 1));
//        Log.e("pro", "imgName" + imgName);


//        String imgName = images.substring((images.lastIndexOf(",") + 1));

//        Log.e("pro", "imgName" + imgName);

        Glide.with(context)
                .load(ApiClient.CATEGORY_PRODUCT_URL + images.get(position))
                .placeholder(R.drawable.placeholder)
                .into(imageView);

        Log.e("rahullist::::::::", "rahul::::: " + images.get(position));

//        imageView.setImageResource(Integer.parseInt(images.get(position)));

        Objects.requireNonNull(container).addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        container.removeView((LinearLayout) object);
    }
}
