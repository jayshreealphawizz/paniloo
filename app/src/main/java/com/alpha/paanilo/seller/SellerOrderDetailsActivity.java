package com.alpha.paanilo.seller;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.alpha.paanilo.AllOrderModel;
import com.alpha.paanilo.R;
import com.alpha.paanilo.model.banner_pkg.BannerInfo;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.seller.adapter.AdapterAllOrder;
import com.alpha.paanilo.seller.adapter.AdapterSellerOrderDetail;
import com.alpha.paanilo.utility.Constants;
import com.alpha.paanilo.utility.CustomProgressbar;
import com.bumptech.glide.Glide;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SellerOrderDetailsActivity extends AppCompatActivity implements View.OnClickListener {
    public ArrayList<BannerInfo> banner_first_list;
    ViewPager vpHomeBannerId;
    int[] images = {R.drawable.defaultimage, R.drawable.water, R.drawable.water};
    AdapterSellerOrderDetail mViewPagerAdapter;
    AppCompatImageView iv_back, iv_notification;
    View id_toolbar;
    AppCompatTextView tv_title;
    Constants constants;
    TextView tvQuantity, tvProductName, tvAmount, tvDescription, tvOrderUserName, tvOrderDate, tvOrderId, tvOrderTotal, tvPaymentStatus, tvShipingDescription;
    ImageView ivImageProdcut;
    String Status, UserFullName, name, nameQuantity, nameTotalamount, nameIMage, nameuserid, nameSaleid, nameDate, nameOrderid, namepaymentmethod, nameaddress;
    TextView tv_phoneno, tvstatusmode, tvUserName, tvAdress, tvDate, tvOrderTAmount, tvPaymentMethod;
    RecyclerView recycler_viewdetails;
    String UserPhone, productname, productimage, productuserid, productQuantity, producttotamount, productSaleid, productstatus, getDeliveryAddress, productdate, userfullname, OrderId, paymentmethod;
    ImageView ivProductImage;
    private TabLayout tlHomeBanner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller_order_details);

//    private void init() {
        init();

        id_toolbar = findViewById(R.id.id_toolbar);
        iv_back = id_toolbar.findViewById(R.id.id_back);
        iv_notification = id_toolbar.findViewById(R.id.id_notification);
        tv_title = id_toolbar.findViewById(R.id.tv_tittle);
//        tv_notfound = findViewById(R.id.tv_notfound);
        iv_notification.setVisibility(View.GONE);
        iv_back.setOnClickListener(this);
        tv_title.setText("All Order");
        iv_notification.setOnClickListener(this);
    }

    private void init() {
        tvQuantity = findViewById(R.id.tvQuantity);
        tv_phoneno = findViewById(R.id.tv_phoneno);

        constants = new Constants(SellerOrderDetailsActivity.this);

        tvOrderId = findViewById(R.id.tvOrderId);
        tvUserName = findViewById(R.id.tvUserName);
        tvAdress = findViewById(R.id.tvAdress);
        tvDate = findViewById(R.id.tvDate);
        tvOrderTAmount = findViewById(R.id.tvOrderTAmount);
        tvPaymentMethod = findViewById(R.id.tvPaymentMethod);
        tvstatusmode = findViewById(R.id.tvstatusmode);
//        cardSpinner = findViewById(R.id.cardSpinner);
        ivProductImage = findViewById(R.id.ivProductImage);


        Intent intent = getIntent();
        UserPhone = intent.getStringExtra("userphone");
        productimage = intent.getStringExtra("productimage");
        productname = intent.getStringExtra("prodcutname");
        productQuantity = intent.getStringExtra("productquantity");
        producttotamount = intent.getStringExtra("producttotamount");
        productuserid = intent.getStringExtra("productuserid");
        productSaleid = intent.getStringExtra("productSaleid");
        productstatus = intent.getStringExtra("productstatus");
        getDeliveryAddress = intent.getStringExtra("getDeliveryAddress");

        productdate = intent.getStringExtra("productdate");
        userfullname = intent.getStringExtra("userfullname");

        OrderId = intent.getStringExtra("OrderId");
        paymentmethod = intent.getStringExtra("paymentmethod");


        if (OrderId.equalsIgnoreCase("null")) {

        } else {
            tvOrderId.setText(OrderId);
        }


        if (UserPhone.equalsIgnoreCase("null")) {

        } else {
            tv_phoneno.setText(UserPhone);
        }


        if (productQuantity.equalsIgnoreCase("null")) {

        } else {
            tvQuantity.setText(productQuantity);
        }


        if (paymentmethod.equalsIgnoreCase("null")) {

        } else {
            tvPaymentMethod.setText(paymentmethod);
        }


        if (userfullname.equalsIgnoreCase("null")) {

        } else {
            tvUserName.setText(userfullname);
        }


        if (productdate.equalsIgnoreCase("null")) {

        } else {
            tvDate.setText(productdate);
        }


        if (getDeliveryAddress.equalsIgnoreCase("null")) {

        } else {
            tvAdress.setText(getDeliveryAddress);
        }


        if (producttotamount.equalsIgnoreCase("null")) {

        } else {
            tvOrderTAmount.setText(producttotamount);
        }

        Log.e("productstatus", "productstatus complete detials:::" + productstatus);

        if (productstatus.equalsIgnoreCase("null")) {

        } else {
            if (productstatus.equalsIgnoreCase("3")) {
                tvstatusmode.setText("Cancel");
                tvstatusmode.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.red));

            } else if (productstatus.equalsIgnoreCase("4")) {
                tvstatusmode.setText("Complete");
                tvstatusmode.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorgreen));

            }
        }

        if (productimage.equalsIgnoreCase("null") || productimage == null) {

        } else {
//
////            String string = openOrderPojo.getData().get(count - 1).getProductImage();
////            Log.e("rahul ", "string::" + string);
////            String[] parts = string.split(",");
////            String part1 = parts[0]; // 004
//////                            String part2 = parts[1];
////            Log.e("rahul ", "part1 Time::" + part1);
////            list.add("" + part1);
//
            Glide.with(getApplicationContext())
                    .load(ApiClient.CATEGORY_PRODUCT_URL + productimage)
                    .placeholder(R.drawable.proimage)
                    .into(ivProductImage);
//            tvOrderTAmount.setText(producttotamount);
        }
//        btn_accept.setOnClickListener(this);


//        if (CheckNetwork.isNetAvailable(SellerOrderDetailsActivity.this)) {
//
//            getCompleteOrderDetailsList(constants.getUserID(), productSaleid);
//
//        } else {
//            Toast.makeText(SellerOrderDetailsActivity.this, getResources().getString(R.string.network), Toast.LENGTH_LONG).show();
//        }


//        productstatus


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.id_back:
                Intent intet = new Intent(SellerOrderDetailsActivity.this, SellerCompleteOrderActivity.class);

                startActivity(intet);
//                finish();
                break;


        }
    }


}