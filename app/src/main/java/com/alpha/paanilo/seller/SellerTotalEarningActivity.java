package com.alpha.paanilo.seller;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alpha.paanilo.AllOrderModel;
import com.alpha.paanilo.DatumallOrder;
import com.alpha.paanilo.R;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.seller.adapter.AdapterSellerEarning;
import com.alpha.paanilo.utility.CheckNetwork;
import com.alpha.paanilo.utility.Constants;
import com.alpha.paanilo.utility.CustomProgressbar;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SellerTotalEarningActivity extends AppCompatActivity implements View.OnClickListener {
    List<DatumallOrder> arraylistpicture = new ArrayList<>();
    List<DatumallOrder> arraylistpicturenew = new ArrayList<>();


    AdapterSellerEarning adapterpicture;
    AppCompatImageView iv_back, iv_notification;
    View id_toolbar;
    AppCompatTextView tv_title;
    Constants constants;
    TextView tvTotalEaring;
    private RecyclerView recyclerAllOrder;
    private Object ModelSellerEarning;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller_total_earning);

        init();

        id_toolbar = findViewById(R.id.id_toolbar);
        iv_back = id_toolbar.findViewById(R.id.id_back);
        iv_notification = id_toolbar.findViewById(R.id.id_notification);
        tv_title = id_toolbar.findViewById(R.id.tv_tittle);

        iv_back.setOnClickListener(this);
        tv_title.setText("Total Earing");
        iv_notification.setOnClickListener(this);
    }

    private void init() {
        recyclerAllOrder = findViewById(R.id.recyclerAllOrder);
        tvTotalEaring = findViewById(R.id.tvTotalEaring);
        constants = new Constants(getApplicationContext());

        if (CheckNetwork.isNetAvailable(getApplicationContext())) {
            getAllOrderList(constants.getUserID(), "");
        } else {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.network), Toast.LENGTH_LONG).show();
        }
//        Collections.reverse(arraylistpicture);
//        LinearLayoutManager layoutMa = new LinearLayoutManager(com.alpha.paanilo.seller.SellerTotalEarningActivity.this, RecyclerView.VERTICAL, false);
//        layoutMa.setReverseLayout(true);
//        recyclerAllOrder.setLayoutManager(layoutMa);


        Collections.reverse(arraylistpicture);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        recyclerAllOrder.setHasFixedSize(true);
        recyclerAllOrder.setLayoutManager(layoutManager);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.id_back:
                Intent intet = new Intent(com.alpha.paanilo.seller.SellerTotalEarningActivity.this, SellerHomeActivity.class);
                startActivity(intet);
                finish();
                break;

            case R.id.id_notification:
                Intent noti = new Intent(com.alpha.paanilo.seller.SellerTotalEarningActivity.this, SellerNotificationActivity.class);
                startActivity(noti);
                finish();
                break;
        }

    }


    private void getAllOrderList(String userId, String date) {
        CustomProgressbar.showProgressBar(this, false);
        ApiClient.getClient().getAllOrderList(userId, date).enqueue(new Callback<AllOrderModel>() {
            @Override
            public void onResponse(Call<AllOrderModel> call, Response<AllOrderModel> response) {

                Log.e("TAG", "all order onResponse: " + new Gson().toJson(response.body()));
                CustomProgressbar.hideProgressBar();

                AllOrderModel allOrderModel = response.body();
//                if (allOrderModel.getTotal_earn_money().equalsIgnoreCase("null") || allOrderModel.getTotal_earn_money().equalsIgnoreCase("") || allOrderModel.getTotal_earn_money().isEmpty()) {
//                    tvTotalEaring.setText("Rs. " + 00);
//                } else {
//                    tvTotalEaring.setText("Rs. " + allOrderModel.getTotal_earn_money());
//                }
                if (response.isSuccessful()) {
                    try {
                        tvTotalEaring.setText("Rs. " + allOrderModel.getTotal_earn_money());
                        CustomProgressbar.hideProgressBar();
                        Log.e("TAG", "stall near by you response all stalllllll : " + new Gson().toJson(response.body()));

                        arraylistpicture = allOrderModel.getData();
                        for (int count = 0; count < arraylistpicture.size(); count++) {
                        }
//                        if (allOrderModel.getData().get(0).getStatus().equalsIgnoreCase("4")) {
                        adapterpicture = new AdapterSellerEarning(com.alpha.paanilo.seller.SellerTotalEarningActivity.this, arraylistpicture);
                        recyclerAllOrder.setAdapter(adapterpicture);
                        adapterpicture.notifyDataSetChanged();
//                            adapterpicture.notifyItemRemoved(0);
//                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        CustomProgressbar.hideProgressBar();

                    }
                } else {
                    Toast.makeText(SellerTotalEarningActivity.this, "Message " + allOrderModel.getMessage(), Toast.LENGTH_SHORT).show();
                    CustomProgressbar.hideProgressBar();

                }

            }

            @Override
            public void onFailure(Call<AllOrderModel> call, Throwable t) {
                CustomProgressbar.hideProgressBar();
            }
        });

    }
}