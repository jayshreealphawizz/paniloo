package com.alpha.paanilo.seller;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alpha.paanilo.R;
import com.alpha.paanilo.model.AllStallPkg.All_StallDataPojo;
import com.alpha.paanilo.nearby_stall_pkg.All_NearBy_StallActivity;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.seller.adapter.AdapterSellerTransection;
import com.alpha.paanilo.seller.adapter.AdapterSellerTransectionNew;
import com.alpha.paanilo.seller.model.ModelSellerTransection;
import com.alpha.paanilo.seller.model.VerndorReportPojo;
import com.alpha.paanilo.seller.model.openorder.OpenOrderPojo;
import com.alpha.paanilo.utility.CheckNetwork;
import com.alpha.paanilo.utility.Constants;
import com.alpha.paanilo.utility.CustomProgressbar;
import com.alpha.paanilo.utility.CustomToast;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SellerWalletActivity extends AppCompatActivity implements View.OnClickListener {

    List<DatumTranHis> arraylistpicture = new ArrayList<>();
    AdapterSellerTransectionNew adapterpicture;
    AppCompatImageView iv_back, iv_notification;
    View id_toolbar;
    AppCompatTextView tv_title;
    Constants constants;
    TextView tvTotalMoney;
    EditText tvCoupanAMount;
    CardView btn_send;
    private RecyclerView recyclerSellerTransaction;
    private Object ModelSellerTransection;
    private ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller_wallet);

        init();

        constants = new Constants(getApplicationContext());

        id_toolbar = findViewById(R.id.id_toolbar);
        iv_back = id_toolbar.findViewById(R.id.id_back);
        iv_notification = id_toolbar.findViewById(R.id.id_notification);
        tv_title = id_toolbar.findViewById(R.id.tv_tittle);

        pd = new ProgressDialog(SellerWalletActivity.this, R.style.AppCompatAlertDialogStyle);

        iv_back.setOnClickListener(this);
        tv_title.setText("Wallet");
        iv_notification.setOnClickListener(this);

        Log.e("rahul ", "rahul111111111  " + constants.getUserID());

        Log.e("rahul ", "rahul11222  " + constants.getresponseUserID());

//        walletHistory(constants.getUserID(), "40");
//        constants.getUserID()

        if (CheckNetwork.isNetAvailable(getApplicationContext())) {


            walletHistory(constants.getUserID());

        } else {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.network), Toast.LENGTH_LONG).show();
        }


//                if (CheckNetwork.isNetAvailable(getApplicationContext())) {
//walletHistory(constants.getresponseUserID(), ""+tvCoupanAMount.getText().toString().isEmpty());
//                } else {

//                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.network), Toast.LENGTH_LONG).show();
//                }

    }

    private void init() {
        tvCoupanAMount = findViewById(R.id.tvCoupanAMount);

        btn_send = findViewById(R.id.btn_send);
        tvTotalMoney = findViewById(R.id.tvTotalMoney);
        recyclerSellerTransaction = findViewById(R.id.recyclerSellerTransaction);


//        LinearLayoutManager layoutMa = new LinearLayoutManager(com.alpha.paanilo.seller.SellerWalletActivity.this, RecyclerView.VERTICAL, false);
//        layoutMa.setReverseLayout(true);
//        recyclerSellerTransaction.setLayoutManager(layoutMa);


        Collections.reverse(arraylistpicture);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        recyclerSellerTransaction.setHasFixedSize(true);
        recyclerSellerTransaction.setLayoutManager(layoutManager);



//        adapterpicture = new AdapterSellerTransectionNew(com.alpha.paanilo.seller.SellerWalletActivity.this, arraylistpicture);
//        recyclerSellerTransaction.setAdapter(adapterpicture);
//        getData();
        btn_send.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.id_back:
                Intent intet = new Intent(com.alpha.paanilo.seller.SellerWalletActivity.this, com.alpha.paanilo.seller.SellerHomeActivity.class);
                startActivity(intet);
                finish();
                break;

            case R.id.id_notification:
                Intent noti = new Intent(com.alpha.paanilo.seller.SellerWalletActivity.this, SellerNotificationActivity.class);
                startActivity(noti);
                finish();
                break;

            case R.id.btn_send:
//                Toast.makeText(this, "click withdraw btn", Toast.LENGTH_SHORT).show();
                if (CheckNetwork.isNetAvailable(SellerWalletActivity.this)) {
                    if (tvCoupanAMount.getText().toString().isEmpty()) {

                        new CustomToast().Show_Toast(getApplicationContext(), v, "Amount Can't Empty");
//                        tvCoupanAMount.getBackground().mutate().setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
//                        tvCoupanAMount.requestFocus();
                    } else {
//                        walletHistory(constants.getresponseUserID(), "" + tvCoupanAMount.getText().toString().isEmpty());

//                        if (CheckNetwork.isNetAvailable(getApplicationContext())) {

                        Log.e("TAG", "BBB "+constants.getUserID() );
                        Log.e("TAG", "BBBB 11111  "+constants.getresponseUserID() );


                        withdrawMoney(constants.getUserID(), tvCoupanAMount.getText().toString());
//                        } else {
//                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.network), Toast.LENGTH_LONG).show();
//                        }
//
//                        addAmount_Wallet();
//                        startRazorPayPayment(tvCoupanAMount.getText().toString());
                    }
                } else {

                    Toast.makeText(SellerWalletActivity.this, "Check Network Connection", Toast.LENGTH_LONG).show();
                }


                break;


        }

    }


    public void withdrawMoney(String userid, String amount) {
        pd.setMessage("Loading");
        pd.show();

        JsonObject jsonObject = new JsonObject();
        try {
//            jsonObject.addProperty("latitude", "-6.4901067");
//            jsonObject.addProperty("longitude", "106.8306951");
            jsonObject.addProperty("user_id", userid);
            jsonObject.addProperty("amount", amount);
            Log.e("TAG", "stall near by you request all stalllllll: " + jsonObject.toString());

        } catch (Exception e) {
            e.printStackTrace();
        }

        (ApiClient.getClient().withdrawMoney(jsonObject)).enqueue(new Callback<WithdrawMondeyModel>() {
            @Override
            public void onResponse(Call<WithdrawMondeyModel> call, Response<WithdrawMondeyModel> response) {
                pd.dismiss();
                Log.e("TAG", "response With Draw Money : " + new Gson().toJson(response.body()));

                WithdrawMondeyModel withdrawMondeyModel = response.body();
                if (response.isSuccessful()) {
                    pd.dismiss();

                    if (withdrawMondeyModel.getStatus() == 1) {
                        tvTotalMoney.setText("Rs." + withdrawMondeyModel.getData().getWalletAmount());
                    } else {
                        Toast.makeText(SellerWalletActivity.this, "" + withdrawMondeyModel.getMessage(), Toast.LENGTH_SHORT).show();

                    }

                } else {
                    if (response.code() == 400) {
                        if (!false) {
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(response.errorBody().string());
                                CustomProgressbar.hideProgressBar();
                                String message = jsonObject.getString("message");
                                // Toast.makeText(SignupActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }

               /* if (response.isSuccessful()) {
                    try {
                        Log.e("TAG", "stall near by you response all stalllllll : " + new Gson().toJson(response.body()));
                        All_StallDataPojo stallDataPojo = response.body();
                        String resMessage = stallDataPojo.getMessage();
                        if (stallDataPojo.getStatus()) {
                            tvNotFound.setVisibility(View.GONE);
                            stallInfo_list = stallDataPojo.getData();
                            all_nearby_stall_adapter.addStallList(stallInfo_list);
                        } else {
                            pd.dismiss();
                            tvNotFound.setVisibility(View.VISIBLE);
                            stallInfo_list.clear();
                            all_nearby_stall_adapter.addStallList(stallInfo_list);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        pd.dismiss();
                    }
                } else {
                    pd.dismiss();
                }*/
            }

            @Override
            public void onFailure(Call<WithdrawMondeyModel> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(SellerWalletActivity.this, getString(R.string.server_errors), Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void walletHistory(String userid) {
        pd.setMessage("Loading");
        pd.show();

        JsonObject jsonObject = new JsonObject();
        try {
//            jsonObject.addProperty("latitude", "-6.4901067");
//            jsonObject.addProperty("longitude", "106.8306951");
            jsonObject.addProperty("user_id", userid);
//            jsonObject.addProperty("amount", amount);
            Log.e("TAG", "stall near by you request all stalllllll: " + jsonObject.toString());

        } catch (Exception e) {
            e.printStackTrace();
        }

        (ApiClient.getClient().walletHistory(jsonObject)).enqueue(new Callback<WithdrawPozo>() {
            @Override
            public void onResponse(Call<WithdrawPozo> call, Response<WithdrawPozo> response) {
                pd.dismiss();
                Log.e("TAG", "response Wall History : " + new Gson().toJson(response.body()));
                WithdrawPozo withdrawPozo = response.body();

                if (response.isSuccessful()) {
                    try {
                        pd.dismiss();
                        Log.e("TAG", "stall near by you response all stalllllll : " + new Gson().toJson(response.body()));

//                        if (withdrawPozo.getStatus().equals("1")) {


                        arraylistpicture = withdrawPozo.getData();

                        for (int i = 0; i < arraylistpicture.size(); i++) {

                        }
                        Log.e("rahul", "rahul::::: " + arraylistpicture.size());
//                        adapterpicture = new AdapterSellerTransection(SellerWalletActivity.this, arraylistpicture);
//                        recyclerSellerTransaction.setAdapter(adapterpicture);

//                        if(arraylistpicture>0)
                        adapterpicture = new AdapterSellerTransectionNew(com.alpha.paanilo.seller.SellerWalletActivity.this, arraylistpicture);
                        recyclerSellerTransaction.setAdapter(adapterpicture);
                        adapterpicture.notifyDataSetChanged();


//                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                        pd.dismiss();
                    }
                } else {
                    Toast.makeText(SellerWalletActivity.this, "" + withdrawPozo.getMessage(), Toast.LENGTH_SHORT).show();

                    pd.dismiss();
                }
            }

            @Override
            public void onFailure(Call<WithdrawPozo> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(SellerWalletActivity.this, getString(R.string.server_errors), Toast.LENGTH_SHORT).show();
            }
        });
    }


}