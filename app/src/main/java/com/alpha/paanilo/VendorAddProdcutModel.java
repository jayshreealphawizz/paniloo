package com.alpha.paanilo;

import com.alpha.paanilo.seller.DataWithDrawMoney;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VendorAddProdcutModel {

//    @SerializedName("status")
//    @Expose
//    private Boolean status;
//    @SerializedName("message")
//    @Expose
//    private String message;
//
//    @SerializedName("data")
//    @Expose
//    private Dataaddproduct data;
//
//
//
//    public Boolean getStatus() {
//        return status;
//    }
//
//    public void setStatus(Boolean status) {
//        this.status = status;
//    }
//
//    public String getMessage() {
//        return message;
//    }
//
//    public void setMessage(String message) {
//        this.message = message;
//    }
//
//
//    public Dataaddproduct getData() {
//        return data;
//    }
//
//    public void setData(Dataaddproduct data) {
//        this.data = data;
//    }
@SerializedName("status")
@Expose
private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Dataaddproduct data;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Dataaddproduct getData() {
        return data;
    }

    public void setData(Dataaddproduct data) {
        this.data = data;
    }


}
