package com.alpha.paanilo.home_pkg;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.alpha.paanilo.R;
import com.alpha.paanilo.model.stallPkg.Stall_Info;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.utility.Constants;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.List;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.MyViewHolder> implements Filterable {
    private final Constants constants;
    public List<Stall_Info> searchList;
    public List<Stall_Info> searchList1;
    Context context;
    private LayoutInflater mInflater;
    private HomeStallOnClickListener stallOnClickListener;

    public HomeAdapter(Context activity) {
        context = activity;
        constants = new Constants(activity);
    }

    public HomeAdapter(Context activity, HomeStallOnClickListener stall) {
        context = activity;
        constants = new Constants(activity);
        this.stallOnClickListener = stall;
    }

    public void addStallList(ArrayList<Stall_Info> searchList) {
        this.searchList = searchList;
        this.searchList1 = searchList;
        notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    searchList = searchList1;
                } else {
                    List<Stall_Info> filteredList = new ArrayList<>();
                    for (Stall_Info row : searchList1) {
                        if (row.getUserFullname().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }
                    searchList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = searchList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                searchList = (ArrayList<Stall_Info>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_fragment_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        final Stall_Info detail = searchList.get(position);

        holder.tv_stall_name.setText(detail.getStore_name());
        holder.tv_stall_des.setText(detail.getCurrentLocation());

        if(!detail.getDistance().equalsIgnoreCase("")) {
            String valuuu = String.format("%.2f", Double.valueOf(detail.getDistance()));
            System.out.println("==========" + valuuu);
            Log.e("rahulraj ", "rahulra:::" + valuuu);
        holder.tv_stall_distance.setText("(" + valuuu + " Kms)");

        }
//        constants.setRating("" + productReviews.get(0).getRating());


//        holder.Rating.setRating(Float.valueOf(detail.getRating()).floatValue());

        if (detail.getRating().equals("")) {
            holder.tvRatingPoint.setText("0.0");
            holder.Rating.setRating(Float.parseFloat("0"));
        } else {
            holder.tvRatingPoint.setText(detail.getRating());
            holder.Rating.setRating(Float.parseFloat(detail.getRating()));
        }

//        if (detail.getRating().equals("")) {
//
//        }else {
//            holder.Rating.setRating(Float.parseFloat(detail.getRating()));
//        }
//        String rat = constants.getRating();

//        if (rat.equals("")) {
//            holder.Rating.setRating(Float.parseFloat("" + rat));
//        } else {
//            holder.Rating.setRating(Float.parseFloat("" + rat));
//        }

//        holder.Rating.setRating(Float.parseFloat(""+productReviews.get(position).getWaterQuality()));

        if (detail.getLogin_status().equals("Online")) {
            holder.tvStatus.setTextColor(Color.parseColor("#13BA08"));

            holder.tvStatus.setText("Online");

        }else if (detail.getLogin_status().equals("Offline")) {
            holder.tvStatus.setTextColor(Color.parseColor("#DA0100"));
            holder.tvStatus.setText("Offline");


        }


        holder.tvReviews.setText("(" + detail.getTotalReview() + " Reviews)");


        if (detail.getUserImage().equals("") || detail.getUserImage() == null || detail.getUserImage().equals("null")) {

            Log.e("fdfsd", "dimage::if cond:: " + detail.getUserImage());

            holder.id_stallpic.setImageResource(R.drawable.placeholder);


        } else {

            Log.e("fdfsd", "dimage:::: " + detail.getUserImage());
            Glide.with(context)
                    .load(ApiClient.VENDOR_PROFILE_URL + detail.getUserImage())
                    .placeholder(R.drawable.placeholder)
                    .into(holder.id_stallpic);
//            Log.e("fdfsd", "dimage::111:: " + ApiClient.VENDOR_PROFILE_URL + detail.getUserImage());
        }


        constants.setHomeVendrorID("");

//        holder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                constants.setVendorClick("clicked");
//                constants.setHomeVendrorID(searchList.get(position).getUserId());
//                constants.setVendor(searchList.get(position).getUserId());
//
//                Bundle bundle=new Bundle();
//                bundle.putString("url",searchList.get(position).getUserImage());
//
//                StallProfileActivity fragobj=new StallProfileActivity();
//                fragobj.setArguments(bundle);
//                ((HomeMainActivity)(context)).getSupportFragmentManager().beginTransaction()
//                        .replace(R.id.container, fragobj)
//                        .addToBackStack(null)
//                        .commit();
//            }
//        });


    }

    @Override
    public int getItemCount() {
        return searchList == null ? 0 : searchList.size();
    }

    public interface HomeStallOnClickListener {
        void onClick(View view, int position, Stall_Info stall_info);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvStatus ,tv_stall_name, tv_stall_des, tv_stall_distance, tvReviews, tvRatingPoint;
        AppCompatImageView id_stallpic;
        LinearLayout ll_wholebox;
        RatingBar Rating;

        public MyViewHolder(View view) {
            super(view);
            tv_stall_name = view.findViewById(R.id.tv_stall_name);
            tv_stall_des = view.findViewById(R.id.tv_stall_des);
            id_stallpic = view.findViewById(R.id.id_stallpic);
            tv_stall_distance = view.findViewById(R.id.tv_stall_distance);
            ll_wholebox = view.findViewById(R.id.ll_wholebox);

            tvReviews = view.findViewById(R.id.tvReviews);

            tvRatingPoint = view.findViewById(R.id.tvRatingPoint);
            Rating = view.findViewById(R.id.Rating);
            tvStatus = view.findViewById(R.id.tvStatus);
            ll_wholebox.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            stallOnClickListener.onClick(v, getAdapterPosition(), searchList.get(getAdapterPosition()));
        }
    }
}