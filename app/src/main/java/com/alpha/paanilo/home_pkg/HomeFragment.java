package com.alpha.paanilo.home_pkg;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alpha.paanilo.HomeMainActivity;
import com.alpha.paanilo.Notification_Activity;
import com.alpha.paanilo.R;
import com.alpha.paanilo.Roomdatabase.DatabaseClient;
import com.alpha.paanilo.Roomdatabase.ProductDataTask;
import com.alpha.paanilo.StallPkg.StallProfileActivity;
import com.alpha.paanilo.model.notificationPkg.NotificationClearPozo;
import com.alpha.paanilo.model.notificationPkg.NotificationCountModel;
import com.alpha.paanilo.model.notificationPkg.NotificationPojo;
import com.alpha.paanilo.model.stallPkg.StallDataPojo;
import com.alpha.paanilo.model.stallPkg.Stall_Info;
import com.alpha.paanilo.product_detail_pkg.Product_details_Activity;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.seller.SellerHomeActivity;
import com.alpha.paanilo.seller.model.notification.SellerNotificationPojo;
import com.alpha.paanilo.utility.AppSession;
import com.alpha.paanilo.utility.Constants;
import com.alpha.paanilo.utility.CustomProgressbar;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.Place.Field;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.facebook.FacebookSdk.getApplicationContext;


public class HomeFragment extends Fragment implements OnMapReadyCallback, HomeAdapter.HomeStallOnClickListener {
    private final List<Marker> markerList = new ArrayList<>();
    public ArrayList<Stall_Info> stallInfo_list;
    View v;
    RecyclerView rv_home_item;
    ProgressDialog pd;
    Constants constants;
    HomeAdapter searchAdapter;
    View toolbar;
    AppCompatTextView tv_location;
    Double vendor_lat, vendor_long;
    int AUTOCOMPLETE_REQUEST_CODE = 1;
    int taskList_size;
    String user_id;
    String latitude, longi;
    String UseridUser;
    TextView tvCountNoti;
    private GoogleMap mMap;
    private RelativeLayout id_notification;
    private AppCompatEditText etSearchStall;
    private StringBuilder vendorId;
    private String strVendorId;
    private AppCompatImageView id_back;
    private AppCompatTextView tvSallNear;
    private List<Address> addresses;
    private Geocoder geocoder;
    private LinearLayout linearLayout, llcountlayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.home_fragment_rv, container, false);
        vendorId = new StringBuilder();
        geocoder = new Geocoder(getActivity(), Locale.getDefault());
        Places.initialize(getActivity(), getString(R.string.google_maps_key));
        init(v);
        return v;
    }

    public void init(View v) {
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        toolbar = v.findViewById(R.id.toolbar);
        id_notification = toolbar.findViewById(R.id.id_notification);
        llcountlayout = toolbar.findViewById(R.id.llcountlayout);
        tvCountNoti = toolbar.findViewById(R.id.tvCountNoti);


        tv_location = toolbar.findViewById(R.id.tv_location);
        id_back = toolbar.findViewById(R.id.id_back);
        etSearchStall = toolbar.findViewById(R.id.etSearchStallId);

        rv_home_item = v.findViewById(R.id.id_rv);
        tvSallNear = v.findViewById(R.id.tvSallNearid);
        linearLayout = v.findViewById(R.id.llLodingId);
        stallInfo_list = new ArrayList<>();
        pd = new ProgressDialog(getActivity(), R.style.AppCompatAlertDialogStyle);
//        rv_home_item.setLayoutManager(new LinearLayoutManager(getContext()));

        Collections.reverse(stallInfo_list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        rv_home_item.setHasFixedSize(true);
        rv_home_item.setLayoutManager(layoutManager);


        constants = new Constants(getActivity());

//        getStallNearBy("" + latitude, "" + longi);
//        vendor_lat = String.valueOf(constants.getLatitude());
//        vendor_long = constants.getLongitude();

        latitude = constants.getLatitude();
        longi = constants.getLongitude();
        UseridUser = constants.getUserID();

        Log.e("latitude Id:::::", "Locatino  " + latitude);
        Log.e("longi Id:::::", "Locatino Address " + longi);


        Log.e("Location Id:::::", "Locatino  " + constants.getCity());
        Log.e("Location Id:::::", "Locatino Address " + constants.getAddress());
        Log.e("Location Id:::::", "Locatino currentlocation " + constants.getCurrentLocation());

//        Log.e("Location Id:::::","Locatino currentlocation " + constants.get()) ;

        tv_location.setText(constants.getCity());
        searchAdapter = new HomeAdapter(getContext(), this);
        rv_home_item.setAdapter(searchAdapter);

        getCartDataTasks();

//        Toast.makeText(getContext(), "nnnnnnnnnnnnn ", Toast.LENGTH_SHORT).show();
        getStallNearBy("" + latitude, "" + longi);

        id_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Field> fields = Arrays.asList(Field.ADDRESS, Field.NAME, Field.LAT_LNG);
                Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields).build(getActivity());
                startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
            }
        });


        id_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                startActivity(new Intent(getActivity(), Notification_Activity.class));
                getSellerNotificationListdata(constants.getUserID());
            }
        });


        etSearchStall.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                searchAdapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {
                searchAdapter.getFilter().filter(s);
            }
        });
        getNotification();


    }


    public void getStallNearBy(final String latitude, final String longitute) {
        Log.e("TAG", "stall near by you request : " + latitude + "," + longitute);
        JsonObject jsonObject = new JsonObject();
        try {
            jsonObject.addProperty("latitude", latitude);
            jsonObject.addProperty("longitude", longitute);
        } catch (Exception e) {
            e.printStackTrace();
        }
//        stallInfo_list.clear();
        (ApiClient.getClient().getstallnearbyme(jsonObject)).enqueue(new Callback<StallDataPojo>() {
            @Override
            public void onResponse(Call<StallDataPojo> call, Response<StallDataPojo> response) {
                Log.e("TAG", "near bt : " + new Gson().toJson(response.body()));
//                stallInfo_list.clear();
                if (response.isSuccessful()) {
                    // pd.dismiss();
                    // pd.dismiss();
                    try {
                        Log.e("TAG", "stall near by you response : " + new Gson().toJson(response.body()));
                        StallDataPojo stallDataPojo = response.body();
                        String resMessage = stallDataPojo.getMessage();
                        if (stallDataPojo.getStatus()) {
                            tvSallNear.setVisibility(View.GONE);
                            stallInfo_list = stallDataPojo.getData();
                            for (int i = 0; i < stallInfo_list.size(); i++) {
                                vendorId.append(stallInfo_list.get(i).getUserId() + ",");
                            }

                            strVendorId = vendorId.toString();
                            strVendorId = strVendorId.substring(0, strVendorId.length() - 1);
                            searchAdapter.addStallList(stallInfo_list);
                            searchAdapter.notifyDataSetChanged();

                            // pd.dismiss();
                        } else {
                            pd.dismiss();
                            tvSallNear.setVisibility(View.VISIBLE);
//                            Toast.makeText(getActivity(), resMessage, Toast.LENGTH_SHORT).show();
                            stallInfo_list.clear();
                            searchAdapter.addStallList(stallInfo_list);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<StallDataPojo> call, Throwable t) {
                try {
                    Toast.makeText(getActivity(), getString(R.string.server_errors), Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        try {
            mMap = googleMap;
            Log.e("latitude::::", "latitude::::" + latitude);
            Log.e("getLongitude::::", "getLongitude::::" + longi);

            final LatLng TutorialsPoint = new LatLng(Double.valueOf(latitude), Double.valueOf(longi));
            mMap.addMarker(new MarkerOptions().position(TutorialsPoint));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(TutorialsPoint, 16));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void getCartDataTasks() {
        class GetTasks extends AsyncTask<Void, Void, List<ProductDataTask>> {
            @Override
            protected List<ProductDataTask> doInBackground(Void... voids) {
                List<ProductDataTask> taskList = DatabaseClient
                        .getInstance(getContext())
                        .getAppDatabase()
                        .taskDao()
                        .getAll();
                return taskList;
            }

            @Override
            protected void onPostExecute(List<ProductDataTask> tasks) {
                super.onPostExecute(tasks);
                System.out.println("======task list size=====" + tasks.size());
                taskList_size = tasks.size();
            }
        }

        GetTasks gt = new GetTasks();
        gt.execute();
    }

    //    ===================
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                vendor_lat = place.getLatLng().latitude;
                vendor_long = place.getLatLng().longitude;
                List<Address> addresses;
                try {
                    addresses = geocoder.getFromLocation(vendor_lat, vendor_long, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                    String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    String state = addresses.get(0).getAdminArea();
                    String currentLocation = address;
                    String city = addresses.get(0).getLocality() != null ? addresses.get(0).getLocality() + "," + state : addresses.get(0).getSubAdminArea();
                    constants.setCurrentLocation(currentLocation);
                    constants.setCity(city);
                    constants.setLatitude(String.valueOf(vendor_lat));
                    constants.setLongitude(String.valueOf(vendor_long));

                    mMap.getUiSettings().setMyLocationButtonEnabled(true);

                    LatLng sydney = new LatLng(vendor_lat, vendor_long);
                    mMap.addMarker(new MarkerOptions().position(sydney));

                    mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

                    mMap.animateCamera(CameraUpdateFactory.zoomTo(16), 1500, null);

                    tv_location.setText(city);
//                    Toast.makeText(getContext(), "ssssssss", Toast.LENGTH_SHORT).show();
//                    getStallNearBy("" + latitude, "" + longi);
                    getStallNearBy("" + vendor_lat, "" + vendor_long);

                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
            } else if (resultCode == RESULT_CANCELED) {
            }
        }
    }

    @Override
    public void onClick(View view, int position, Stall_Info detail) {
        switch (view.getId()) {
            case R.id.ll_wholebox:

//                Intent intent = new Intent(getContext(), Product_details_Activity.class);
//                intent.putExtra("userid", detail.getUserId());
//                intent.putExtra("distance", detail.getDistance());
//                startActivity(intent);
//                break;
                clearCart();
                String FINALVENDORID = AppSession.getStringPreferences(getContext(), "final_vendorID");
                System.out.println(detail.getUserId() + "==all near stall activity==vendorID id====" + FINALVENDORID);

                Log.e(taskList_size + "FINALVENDORID", "FINALVENDORID:::::: " + FINALVENDORID);
                Log.e(taskList_size + "detail.getUserId()", "detail.getUserId():::::: " + detail.getUserId());

                if (FINALVENDORID != null) {
                    if (FINALVENDORID.equals(detail.getUserId())) {
                        AppSession.setStringPreferences(getContext(), "final_vendorID", detail.getUserId());
                        constants.setVendor(detail.getUserId());
                        Bundle bundle = new Bundle();
                        bundle.putString("url", detail.getUserImage());

                        Intent intent = new Intent(getContext(), Product_details_Activity.class);
                        intent.putExtra("userid", detail.getUserId());
                        intent.putExtra("distance", detail.getDistance());
                        startActivity(intent);

                    } else {
                        if (FINALVENDORID.isEmpty()) {
                            AppSession.setStringPreferences(getContext(), "final_vendorID", detail.getUserId());
                            constants.setVendor(detail.getUserId());
                            Bundle bundle = new Bundle();
                            bundle.putString("url", detail.getUserImage());

                            Intent intent = new Intent(getContext(), Product_details_Activity.class);
                            intent.putExtra("userid", detail.getUserId());
                            intent.putExtra("distance", detail.getDistance());
                            startActivity(intent);
                        } else {
                            if (taskList_size == 0) {
                                AppSession.setStringPreferences(getContext(), "final_vendorID", detail.getUserId());
                                constants.setVendor(detail.getUserId());
                                Bundle bundle = new Bundle();
                                bundle.putString("url", detail.getUserImage());

                                Intent intent = new Intent(getContext(), Product_details_Activity.class);
                                intent.putExtra("userid", detail.getUserId());
                                intent.putExtra("distance", detail.getDistance());
                                startActivity(intent);
                            } else {
                                cartdialoge();
                            }
                        }
                    }
                } else {
                    AppSession.setStringPreferences(getContext(), "final_vendorID", detail.getUserId());
                    constants.setVendor(detail.getUserId());
                    Bundle bundle = new Bundle();
                    bundle.putString("url", detail.getUserImage());
//                    StallProfileActivity fragobj = new StallProfileActivity();
                    Intent intent = new Intent(getContext(), Product_details_Activity.class);
                    intent.putExtra("userid", detail.getUserId());
                    intent.putExtra("distance", detail.getDistance());
                    startActivity(intent);
                }
        }
    }

    private void cartdialoge() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.clear_cart_dialoge);
        AppCompatButton no = dialog.findViewById(R.id.mbtnNoId);
        AppCompatButton mbtnYes = dialog.findViewById(R.id.mbtnYesId);

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        mbtnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearCart();
                dialog.dismiss();
                AppSession.setStringPreferences(getContext(), "final_vendorID", "");
            }
        });
        dialog.show();
    }


    private void clearCart() {
        class GetTasks extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... voids) {
                DatabaseClient.getInstance(getContext())
                        .getAppDatabase()
                        .taskDao()
                        .delete();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
            }
        }
        GetTasks gt = new GetTasks();
        gt.execute();
    }

//    @Override
//    public void onResume() {
//        super.onResume();
//
//        if (searchAdapter != null) {
//            getStallNearBy("" + vendor_lat, "" + vendor_long);
//
//        }
//    }

    public void getNotification() {
        pd.setMessage("Loading");
        pd.show();
        JsonObject jsonObject = new JsonObject();
        try {
            jsonObject.addProperty("user_id", constants.getUserID());
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e("TAG", "noitifictaion  request : " + new Gson().toJson(jsonObject));
        (ApiClient.getClient().getNotification(jsonObject)).enqueue(new Callback<NotificationPojo>() {
            @Override
            public void onResponse(Call<NotificationPojo> call, Response<NotificationPojo> response) {
                pd.dismiss();
                if (response.isSuccessful()) {
                    try {
                        Log.e("TAG", "noitifictaion  response buyer : " + new Gson().toJson(response.body()));
                        NotificationPojo notificationPojo = response.body();
                        if (notificationPojo.getStatus()) {

//                            searchAdapter.notifyDataSetChanged();
                            llcountlayout.setVisibility(View.VISIBLE);
                            tvCountNoti.setText("" + notificationPojo.getCount());


                        } else {
                            llcountlayout.setVisibility(View.GONE);
                            Toast.makeText(getApplicationContext(), notificationPojo.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<NotificationPojo> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(getApplicationContext(), getString(R.string.server_errors), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void getSellerNotificationListdata(String userId) {
//        CustomProgressbar.showProgressBar(getApplicationContext(), false);
        ApiClient.getClient().clearNotification(userId).enqueue(new Callback<NotificationCountModel>() {
            @Override
            public void onResponse(Call<NotificationCountModel> call, Response<NotificationCountModel> response) {

                Log.e("TAG", "getSellerNotificationList onResponse: " + new Gson().toJson(response.body()));
                CustomProgressbar.hideProgressBar();

                NotificationCountModel sellerNotificationPojo = response.body();

                if (response.isSuccessful()) {
                    CustomProgressbar.hideProgressBar();
                    if (sellerNotificationPojo.getStatus()) {
//                            searchAdapter.notifyDataSetChanged();
                        llcountlayout.setVisibility(View.VISIBLE);
                        tvCountNoti.setText("" + sellerNotificationPojo.getCount());

                    } else {
                        llcountlayout.setVisibility(View.GONE);
                        Toast.makeText(getApplicationContext(), sellerNotificationPojo.getCount(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    llcountlayout.setVisibility(View.GONE);
                    if (response.code() == 400) {
                        if (!false) {
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(response.errorBody().string());
                                CustomProgressbar.hideProgressBar();
                                String message = jsonObject.getString("message");
                                // Toast.makeText(SignupActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }


            }

            @Override
            public void onFailure(Call<NotificationCountModel> call, Throwable t) {
                CustomProgressbar.hideProgressBar();
            }
        });


    }


}

