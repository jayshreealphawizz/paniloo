package com.alpha.paanilo.editprofile_pkg;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.DatePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.alpha.paanilo.HomeMainActivity;
import com.alpha.paanilo.R;
import com.alpha.paanilo.authenticationModule.SignUp_Activity;
import com.alpha.paanilo.model.profilePkg.EditProfilePozo;
import com.alpha.paanilo.product_detail_pkg.Product_details_Activity;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.utility.CheckNetwork;
import com.alpha.paanilo.utility.Constants;
import com.alpha.paanilo.utility.CustomToast;
import com.alpha.paanilo.utility.ImagePicker;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfile_Activity extends AppCompatActivity implements View.OnClickListener {

    private static final int PERMISSION_READ_EXTERNAL_STORAGE = 100;
    private static final int SELECT_PICTURE = 101;
    private static Animation shakeAnimation;
    String profilImgPath, userid;
    SimpleDateFormat dateFormatter;
    Constants constants;
    private String imagePath;
    private File fileForImage;
    private AppCompatTextView tv_dob, tv_title;
    private TextInputEditText tv_user_name, tv_email_address, tv_mobile_number, tv_city;
    private CardView btn_save;
    private AppCompatImageView iv_back, ivCameraImgEdit;
    private View id_toolbar;
    private ProgressDialog pd;
    //    private String imagePath;
    private CircleImageView civProfil;
AppCompatImageView ivDateIMage ;
    String loginstatus ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editprofile);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        init();
    }

    private void init() {
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

        ivCameraImgEdit = findViewById(R.id.ivCameraImgEditId);
        tv_city = findViewById(R.id.tv_city);
        tv_user_name = findViewById(R.id.tv_user_name);
        tv_dob = findViewById(R.id.tv_dob);

        ivDateIMage = findViewById(R.id.ivDateIMage);
        tv_email_address = findViewById(R.id.tv_email_address);
        tv_mobile_number = findViewById(R.id.tv_mobile_number);

        id_toolbar = findViewById(R.id.id_toolbar);
        iv_back = id_toolbar.findViewById(R.id.id_back);
        civProfil = findViewById(R.id.civProfilId);
        tv_title = id_toolbar.findViewById(R.id.tv_tittle);

        civProfil.setOnClickListener(this);
        iv_back.setOnClickListener(this);
        ivCameraImgEdit.setOnClickListener(this);
        tv_title.setText(getString(R.string.editprofile));

        btn_save = findViewById(R.id.btn_save);
        btn_save.setOnClickListener(this);

        constants = new Constants(getApplicationContext());
        pd = new ProgressDialog(EditProfile_Activity.this, R.style.AppCompatAlertDialogStyle);
        shakeAnimation = AnimationUtils.loadAnimation(this, R.anim.shake);

        userid = constants.getUserID();
        tv_user_name.setText(constants.getUsername());
        tv_email_address.setText(constants.getEmail());

        Log.e("rahul:::" , "rahul::: " + constants.getAddress());

        Log.e("rahul:::" , "rahul::: " + constants.getCity());

        Log.e("rahul:::" , "rahul::: " + constants.getCurrentLocation());

//        Log.e("rahul:::" , "rahul::: " + constants.getStatusCheck());
//        Log.e("rahul:::" , "rahul::: " + constants.getIsLogin());

         loginstatus  =      constants.getIsLoginStatus("");

        Log.e("login statusnow ", "status::::  " + loginstatus);

        if (loginstatus != null) {
            if (loginstatus.equals("1")) {
                tv_city.setText(constants.getCity());
            }else if (loginstatus.equals("2")) {
                tv_city.setText(constants.getCity());
            }else if (loginstatus.equals("0")) {
                tv_city.setText(constants.getAddress());
            }
        }else {
            tv_city.setText(constants.getAddress());
        }


        String userImage = constants.getImage();


//        if (userImage.isEmpty()) {
//        } else {
//            Log.e("rahul::Banfdfdsfsdfa: ", "rahul:dfsdf:: " + userImage.toString());
//            Glide.with(getContext())
//                    .load(ApiClient.CATEGORY_PRODUCT_URL + constants.getImage())
//                    .placeholder(R.drawable.ic_account_user)
//                    .into(ivuserProfile);
////                                productimageID
//        }

//        Log.e("hm :: ", "hm:::" + userImage);


        if (userImage.equals("0")) {

        } else if (TextUtils.isEmpty(userImage)) {

        } else {
//            Log.e("hmfdfdf:: ", "hmfdfdfdf::::" + userImage);
            Glide.with(EditProfile_Activity.this)
                    .load(ApiClient.USER_PROFILE_URL + userImage)
                    .placeholder(R.drawable.userimage)
                    .into(civProfil);

        }

        if (constants.getMobile().equals("0")) {

        } else {
            tv_mobile_number.setText(constants.getMobile());

            if (loginstatus != null) {
                if (loginstatus.equals("1")) {
                    tv_city.setText(constants.getCity());
                }else if (loginstatus.equals("2")) {
                    tv_city.setText(constants.getCity());
                }else if (loginstatus.equals("0")) {
                    tv_city.setText(constants.getAddress());
                }
            }else {
                tv_city.setText(constants.getAddress());
            }

//            tv_city.setText(constants.getAddress());
            tv_dob.setText(constants.getdob());
        }

        if (constants.getdob().equals("0")) {

        } else {

            tv_dob.setText(constants.getdob());
        }


        tv_dob.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance(Locale.getDefault());
                DatePickerDialog datePickerDialog;
                datePickerDialog = new DatePickerDialog(EditProfile_Activity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
                                //todo
                                Calendar newDate = Calendar.getInstance();
                                newDate.set(year, month, dayOfMonth);
                                tv_dob.setText(dateFormatter.format(newDate.getTime()));
                            }
                        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();
                datePickerDialog.getDatePicker().setMaxDate(setMaxDate());
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_save:
                validation(v);
                break;
            case R.id.id_back:
                finish();
                break;
            case R.id.ivCameraImgEditId:

                askStoragePermission();

                break;
        }
    }

    private void validation(View v) {
        if (tv_user_name.getText().toString().isEmpty()) {
            // new CustomToast().Show_Toast(this, v, getString(R.string.username_error));
            tv_user_name.startAnimation(shakeAnimation);
            tv_user_name.getBackground().mutate().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            tv_user_name.requestFocus();
            tv_user_name.setError(getString(R.string.username_error));

        } else if (!tv_user_name.getText().toString().matches("^[a-zA-Z\\s]+")) {
            // new CustomToast().Show_Toast(this, v, "Invalid user name");
            tv_user_name.startAnimation(shakeAnimation);
            tv_user_name.getBackground().mutate().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            tv_user_name.requestFocus();
            tv_user_name.setError(getString(R.string.invalid_user_name));
        } else if (tv_email_address.getText().toString().isEmpty()) {
            // new CustomToast().Show_Toast(this, v, getString(R.string.email_error));
            tv_email_address.startAnimation(shakeAnimation);
            tv_email_address.getBackground().mutate().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            tv_email_address.requestFocus();
            tv_email_address.setError(getString(R.string.email_error));

        } else if (!Constants.isValidEmail(tv_email_address.getText().toString())) {
            // new CustomToast().Show_Toast(this, v, getString(R.string.invalid_email_error));
            tv_email_address.startAnimation(shakeAnimation);
            tv_email_address.getBackground().mutate().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            tv_email_address.requestFocus();
            tv_email_address.setError(getString(R.string.invalid_email_error));

        } else if (tv_mobile_number.getText().toString().isEmpty()) {
            //new CustomToast().Show_Toast(this, v, getString(R.string.mobile_error));
            tv_mobile_number.startAnimation(shakeAnimation);
            tv_mobile_number.getBackground().mutate().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            tv_mobile_number.requestFocus();
            tv_mobile_number.setError(getString(R.string.mobile_error));

        } else if (tv_mobile_number.getText().toString().length() <= 9) {
            // new CustomToast().Show_Toast(this, v, getString(R.string.valid_mobile_error));
            tv_mobile_number.startAnimation(shakeAnimation);
            tv_mobile_number.getBackground().mutate().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            tv_mobile_number.requestFocus();
            tv_mobile_number.setError(getString(R.string.valid_mobile_error));

        } else if (tv_dob.getText().toString().isEmpty()) {
            new CustomToast().Show_Toast(this, v, getString(R.string.dob_error));
            tv_dob.startAnimation(shakeAnimation);
            tv_dob.getBackground().mutate().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            tv_dob.requestFocus();
            // tv_dob.setError(getString(R.string.dob_error));
        }/* else if (tv_city.getText().toString().isEmpty()) {
            // new CustomToast().Show_Toast(this, v, getString(R.string.city_error));
            tv_city.startAnimation(shakeAnimation);
            tv_city.getBackground().mutate().setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            tv_city.requestFocus();
            tv_city.setError(getString(R.string.city_error));
        } */else {
            if (CheckNetwork.isNetAvailable(getApplicationContext())) {

                updateProfile();

            } else {
                new CustomToast().Show_Toast(this, v, getResources().getString(R.string.plz_check_your_intrenet_text));
            }
        }
    }


    private void updateProfile() {
        pd.setMessage("Loading");
        pd.show();
        MultipartBody.Part imgFileStation = null;

        Log.e("profilImgPath::: ", "pppp " + profilImgPath);


        if (profilImgPath == null) {
        } else {
            fileForImage = new File(profilImgPath);

//            File fileForImage = file;
            System.out.println(fileForImage + "====file name=" + fileForImage.getName());
            RequestBody requestFileOne = RequestBody.create(MediaType.parse("multipart/form-data"), fileForImage);
            imgFileStation = MultipartBody.Part.createFormData("user_image", fileForImage.getName(), requestFileOne);
            Log.e("profilImgPath::: ", "pppp " + profilImgPath);
            Log.e("profilImgPath::: ", "fileForImage " + fileForImage);
            Log.e("profilImgPath::: ", "imgFileStation " + imgFileStation);
        }

        String tv_userName = tv_user_name.getText().toString();
        String tv_emailAddress = tv_email_address.getText().toString();
        final String tv_mobileNumber = tv_mobile_number.getText().toString();
        String tvDob = tv_dob.getText().toString();
        final String city = tv_city.getText().toString();

        MultipartBody.Part user_id = MultipartBody.Part.createFormData("user_id", userid);
        MultipartBody.Part user_fname = MultipartBody.Part.createFormData("user_fullname", tv_userName);
        MultipartBody.Part user_email = MultipartBody.Part.createFormData("user_email", tv_emailAddress);
        MultipartBody.Part user_mobile = MultipartBody.Part.createFormData("user_phone", tv_mobileNumber);
        MultipartBody.Part user_Bday = MultipartBody.Part.createFormData("user_bdate", tvDob);
        final MultipartBody.Part user_city = MultipartBody.Part.createFormData("user_city", city);

        System.out.println("id===" + userid + "==user_fullname===" + tv_userName + "==tv_emailAddress==" + tv_emailAddress + "==tv_mobileNumber==" + tv_mobileNumber
                + tvDob + "==city=" + city + "===image==" + imgFileStation);

        (ApiClient.getClient().editProfile(user_id, user_fname, user_email, user_mobile,
                user_Bday, user_city, imgFileStation)).enqueue(new Callback<EditProfilePozo>() {
            @Override
            public void onResponse(Call<EditProfilePozo> call, Response<EditProfilePozo> response) {
                pd.dismiss();
                if (response.isSuccessful()) {
                    try {
                        pd.dismiss();
                        Log.e("TAG", "edit profile  response :111111111111 " + new Gson().toJson(response.body()));
                        EditProfilePozo userInfoPojo = response.body();
                        Boolean repmsg = userInfoPojo.getStatus();
                        String message = userInfoPojo.getMessage();
                        if (repmsg) {

                            constants.setUsername(userInfoPojo.getData().getUserFullname());
                            constants.setEmail(userInfoPojo.getData().getUserEmail());
                            // constants.setCity(userInfoPojo.getData().getUserCity());
                            constants.setMobile(userInfoPojo.getData().getUserPhone());
                            constants.setImage(userInfoPojo.getData().getUserImage());
                            constants.setdob(userInfoPojo.getData().getUserBdate());

                            Toast.makeText(EditProfile_Activity.this, message, Toast.LENGTH_SHORT).show();

                            startActivity(new Intent(EditProfile_Activity.this, HomeMainActivity.class));
                            finish();
                        } else {
                            Toast.makeText(EditProfile_Activity.this, message, Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    System.out.println("error==========");
                }
            }

            @Override
            public void onFailure(Call<EditProfilePozo> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(EditProfile_Activity.this, getString(R.string.server_errors), Toast.LENGTH_SHORT).show();
            }
        });

    }


    //    =================== image upload ======
//    private void askStoragePermission() {
//        if (ActivityCompat.checkSelfPermission(EditProfile_Activity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
//            {
//                ActivityCompat.requestPermissions(EditProfile_Activity.this,
//                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
//                        PERMISSION_READ_EXTERNAL_STORAGE);
//            }
//        } else {
//            chooseFromGallery();
//        }
//    }


    private void askStoragePermission() {
        if (ActivityCompat.checkSelfPermission(EditProfile_Activity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            {
                ActivityCompat.requestPermissions(EditProfile_Activity.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        PERMISSION_READ_EXTERNAL_STORAGE);
            }
        } else {
            chooseFromGallery();
        }
    }


//    private void chooseFromGallery() {
//        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//        intent.setType("image/*");
//        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
//    }

    private void chooseFromGallery() {
        CropImage.activity().start(EditProfile_Activity.this);
    }

    private void resultCrop(Uri resultUri) {
        profilImgPath = compressImage(String.valueOf(resultUri));

        Log.e("profilImgPath:::: ", "rahul:::: " + profilImgPath);

//        tv_image_path.setText(imagePath);
    }


    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
//                                           @NonNull int[] grantResults) {
//        switch (requestCode) {
//            case PERMISSION_READ_EXTERNAL_STORAGE:
//                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    chooseFromGallery();
//                } else {
//                    Toast.makeText(getApplicationContext(), "Permission Denied", Toast.LENGTH_LONG).show();
//                }
//                break;
//
//            default:
//                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//                break;
//        }
//    }

//    @RequiresApi(api = Build.VERSION_CODES.P)
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == SELECT_PICTURE) {
//            if (resultCode == RESULT_OK) {
//                try {
//                    Uri imageUri = data.getData();
//                    Bitmap bitmap = ImagePicker.getImageFromResult(getApplicationContext(), resultCode, data);
//                    civProfil.setImageBitmap(bitmap);
//                    profilImgPath = getRealPathFromURI(getApplicationContext(), imageUri);
//
//                    System.out.println("profile path=1111111111==" + profilImgPath);
//
//                } catch (NullPointerException e) {
//                    e.printStackTrace();
//                }
//
//            }
//        }
//    }

    //    ===============================

    private long setMaxDate() {
        long oldMillis;
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int updateyear = year - 14;
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        formatter.setLenient(false);
        Date curDate = new Date();
        long curMillis = curDate.getTime();
        String curTime = formatter.format(curDate);
        String oldTime = currentdateOfTheYear();
        Date oldDate = null;
        try {
            oldDate = formatter.parse(oldTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        oldMillis = oldDate.getTime();
        return oldMillis;
    }

    private String currentdateOfTheYear() {
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String formattedDate = simpleDateFormat.format(c);
        return formattedDate;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    chooseFromGallery();
                } else {
                    Toast.makeText(EditProfile_Activity.this, "Permission Denied", Toast.LENGTH_LONG).show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    public String compressImage(String imageUri) {

        String filePath = getRealPathFromURI(Uri.parse(imageUri));

        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

        //      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
        //      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);


        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

        //      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 900.0f;
        float maxWidth = 670.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

        //      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

        //      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

        //      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

        //      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[8 * 512];

        try {
            //          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);

            //            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            //            bmp.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            //            byte[] imageInByte = stream.toByteArray();
            //            long lengthbmp = imageInByte.length;
            //            System.out.println(lengthbmp);

        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

        //      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);

            //          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        civProfil.setImageBitmap(scaledBitmap);
        //return scaledBitmap;

        Log.e(scaledBitmap + "civProfile ", "civprofile " + civProfil);

        return filename;

    }

    public String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), getString(R.string.app_name) + "/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }
        return inSampleSize;
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getApplicationContext().getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        System.out.println("Path is file get real path se====" + result);
        return result;
    }

//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
//        startActivity(intent);
//        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
//        finish();
//    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                Uri resultUri = result.getUri();
                resultCrop(resultUri);
                Log.e("rahul ", " res " + resultUri);

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Toast.makeText(this, "Cropping failed: " + error, Toast.LENGTH_LONG).show();
            }
        }

    }

}
