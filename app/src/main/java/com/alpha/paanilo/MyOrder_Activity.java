package com.alpha.paanilo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.alpha.paanilo.invoicePkg.Invoice_Activity;
import com.alpha.paanilo.myOrderPkg.OrderHistoryAdapter;
import com.alpha.paanilo.myOrderPkg.orderHistoryPkg.CancelOrderPozo;
import com.alpha.paanilo.myOrderPkg.orderHistoryPkg.MyOrder;
import com.alpha.paanilo.myOrderPkg.orderHistoryPkg.OrderHistoryPozo;
import com.alpha.paanilo.retrofit.ApiClient;
import com.alpha.paanilo.utility.AppSession;
import com.alpha.paanilo.utility.CheckNetwork;
import com.alpha.paanilo.utility.Constants;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyOrder_Activity extends AppCompatActivity implements View.OnClickListener, OrderHistoryAdapter.OrderOnClick {
    AppCompatImageView iv_back;
    RecyclerView rv_city;
    List<MyOrder> historyList;
    View id_toolbar;
    AppCompatTextView tv_title;
    TextView tv_nodata;
    RelativeLayout rl_notification;
    ProgressDialog pd;
    OrderHistoryAdapter orderHistoryAdapter;
    private Constants constants;
    private SwipeRefreshLayout srlMyOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_rv);
        init();
        if (CheckNetwork.isNetAvailable(getApplicationContext())) {
            getMyOrder();
        } else {
            Toast.makeText(getApplicationContext(), "Check Network Connection", Toast.LENGTH_LONG).show();
        }

        srlMyOrder.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (CheckNetwork.isNetAvailable(getApplicationContext())) {
                    getMyOrder();
                } else {
                    Toast.makeText(getApplicationContext(), "Check Network Connection", Toast.LENGTH_LONG).show();
                }
                srlMyOrder.setRefreshing(false);
            }
        });


    }


    private void init() {
        id_toolbar = findViewById(R.id.id_toolbar);
        iv_back = id_toolbar.findViewById(R.id.id_back);
        tv_title = id_toolbar.findViewById(R.id.tv_tittle);

        srlMyOrder = findViewById(R.id.srlMyOrderId);
        rv_city = findViewById(R.id.id_rv);
        tv_nodata = findViewById(R.id.tv_nodata);
        tv_title.setText(getString(R.string.myorder));

        iv_back.setOnClickListener(this);

        pd = new ProgressDialog(MyOrder_Activity.this, R.style.AppCompatAlertDialogStyle);

        historyList = new ArrayList<>();
//        Collections.reverse(historyList);
//        rv_city.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

//        rv_city.setrev(true);

        LinearLayoutManager layoutMa = new LinearLayoutManager(MyOrder_Activity.this, RecyclerView.VERTICAL, false);
//        layoutMa.setReverseLayout(true);
        rv_city.setLayoutManager(layoutMa);

        constants = new Constants(getApplicationContext());


        orderHistoryAdapter = new OrderHistoryAdapter(getApplicationContext(), this);
        rv_city.setAdapter(orderHistoryAdapter);

    }

    public void cancelOrder(String orderId) {
        pd.setMessage("Loading");
        pd.show();
        JsonObject jsonObject = new JsonObject();
        try {
            jsonObject.addProperty("user_id", constants.getUserID());
            jsonObject.addProperty("order_id", orderId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.e("TAG", "my order  request : " + new Gson().toJson(jsonObject));
        (ApiClient.getClient().cancelOrder(jsonObject)).enqueue(new Callback<CancelOrderPozo>() {
            @Override
            public void onResponse(Call<CancelOrderPozo> call, Response<CancelOrderPozo> response) {
                pd.dismiss();
                if (response.isSuccessful()) {
                    try {
                        Log.e("TAG", "my order  response : " + new Gson().toJson(response.body()));
                        CancelOrderPozo cancelOrderPozo = response.body();
                        if (cancelOrderPozo.getStatus()) {
                            Toast.makeText(getApplicationContext(), cancelOrderPozo.getMessage(), Toast.LENGTH_SHORT).show();
                            if (CheckNetwork.isNetAvailable(getApplicationContext())) {
                                getMyOrder();
                            } else {
                                Toast.makeText(getApplicationContext(), "Check Network Connection", Toast.LENGTH_LONG).show();
                            }
                        } else {
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<CancelOrderPozo> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(getApplicationContext(), getString(R.string.server_errors), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getMyOrder() {
        pd.setMessage("Loading");
        pd.show();
        JsonObject jsonObject = new JsonObject();

        Log.e("TAG", "my order  iiiii : " + constants.getUserID());

        try {
            jsonObject.addProperty("user_id", constants.getUserID());
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e("TAG", "my order  request : " + new Gson().toJson(jsonObject));
        (ApiClient.getClient().getMyOrder(jsonObject)).enqueue(new Callback<OrderHistoryPozo>() {
            @Override
            public void onResponse(Call<OrderHistoryPozo> call, Response<OrderHistoryPozo> response) {
                pd.dismiss();
                if (response.isSuccessful()) {
                    try {
                        Log.e("TAG", "my order  response : " + new Gson().toJson(response.body()));
                        OrderHistoryPozo orderHistoryPozo = response.body();
                        if (orderHistoryPozo.getStatus()) {
                            historyList = orderHistoryPozo.getMyOrder();
                            orderHistoryAdapter.addOrdeHistory(historyList);
                            tv_nodata.setVisibility(View.GONE);
                        } else {
                            tv_nodata.setVisibility(View.VISIBLE);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<OrderHistoryPozo> call, Throwable t) {
                pd.dismiss();
                tv_nodata.setVisibility(View.VISIBLE);
                Toast.makeText(getApplicationContext(), getString(R.string.server_errors), Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void orderOnClick(View view, int position, MyOrder myOrder) {
        switch (view.getId()) {
            case R.id.tvViewMyOrderId:
                // Toast.makeText(this, "now click " + historyList.get(position).getOrderId(), Toast.LENGTH_SHORT).show();
                AppSession.setStringPreferences(getApplicationContext(), "orderFlag", "MyOrder");
                Intent intent = new Intent(getApplicationContext(), Invoice_Activity.class);
                intent.putExtra("saleId", historyList.get(position).getOrderId());
                intent.putExtra("status", historyList.get(position).getStatus());
//                Log.e("banasa ", "banasaoooo0101010:: " + historyList.get(position).getOrderId());
//                Log.e("banasa ", "banasaSSSS:: " + historyList.get(position).getStatus());
                startActivity(intent);

                break;
            case R.id.cartclick:
                AppSession.setStringPreferences(getApplicationContext(), "orderFlag", "MyOrder");
                Intent intent1 = new Intent(getApplicationContext(), Invoice_Activity.class);
                intent1.putExtra("saleId", historyList.get(position).getOrderId());
                intent1.putExtra("status", historyList.get(position).getStatus());
//                Log.e("banasa ", "banasaoooo1111 " + historyList.get(position).getOrderId());
//                Log.e("banasa ", "banasaSSS1S111 " + historyList.get(position).getStatus());

                startActivity(intent1);
                // Toast.makeText(this,
                break;
            case R.id.tvOrderStatusId:
//                Toast.makeText(this, "now click1111 ", Toast.LENGTH_SHORT).show();
              /*  if (myOrder.getStatus().equalsIgnoreCase("0")) {
                    cancelOrder(myOrder.getOrderId());
                } else if (myOrder.getStatus().equalsIgnoreCase("1")) {
                    cancelOrder(myOrder.getOrderId());
                }*/
                break;
            //tvOrderStatusId
        }
//        if (myOrder.getStatus().equalsIgnoreCase("0")) {
//            cancelOrder(myOrder.getOrderId());
//        } else if (myOrder.getStatus().equalsIgnoreCase("1")) {
//            cancelOrder(myOrder.getOrderId());
//        }


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.id_back:
                finish();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                break;
        }
    }
}
